import React , { useState, useEffect} from 'react'
import { getEdgeRecord } from "./../services/VisualService";


function EdgeSelected({dialogData}) {

    const [data, setData] = useState([]);
    useEffect( () => {
        getEdgeRecord(dialogData.source, dialogData.target).then( data => {
            setData(data);
            // console.log(data)
        }, [])
    })



    let dialog = (
        <div >
            {dialogData.source} {dialogData.target}
            <table  style={{border: "1px solid black"}}>
                <thead>
                    <tr>
                        <td  >No</td>
                        <td colSpan="2" style={ {textAlign: "center"} } >From Link</td>
                        <td colSpan="2" style={ {textAlign: "center"} } >To Link</td>
                        <td  >Status</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td  style={ {textAlign: "center"} }>Port No.</td>
                        <td  style={ {textAlign: "center"} }>Description</td>
                        <td  style={ {textAlign: "center"} }>Port No.</td>
                        <td  style={ {textAlign: "center"} }>Description</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody >
                {
                data.map( (data,index) => 
                <tr>
                    <td>{data.no}</td>
                    <td>{data.port_idx}</td>
                    <td>{data.desc_s}</td>
                    <td>{data.port_id_interconnect}</td>
                    <td>{data.desc_t}</td>
                    <td>{data.status}</td>
                </tr>
                )
            }
                </tbody>
            </table>
            
        </div>
    )


    return (
        <div>
            {dialog}
        </div>
    )
    
}

export default EdgeSelected
