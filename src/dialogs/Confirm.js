import React from 'react'
import Dialog from "@material-ui/core/Dialog";

const Confirm = (props) => {
    const { title, children, open, setOpen, onConfirm } = props;

    return (
        <Dialog open={open} onClose={ setOpen }>
            <h1>{title}</h1>
            <h5>{children}</h5>
            {/* <span>Do you want to delete { msg.map( system => <span> {system.name}, </span> )}?</span> */}
            <button onClick={ setOpen }>Cancel</button>
            <button onClick={ onConfirm }>Confirm</button>
            
        </Dialog>
       
    )
}

export default Confirm
