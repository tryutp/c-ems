import {environment} from "../Environment";


export function edit_port(id, description, reserved, token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });

    const params = {
        description: description,
        reserved: reserved,
    };
    return fetch( environment.api +`api/unit/port/update/${id}`, {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function edit_port_list(id,display_no, description, token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });

    const params = {
        display_no: display_no,
        description: description,
        
    };
    return fetch( environment.api +`api/unit/port/update/${id}`, {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function reserve_port(port_id,  token) {
    // console.log(view_type,start_no,port_qty)
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
   

    return fetch(environment.api + `api/unit/port/reserve/${port_id}`, {
        method: 'PUT',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function unreserve_port(port_id,  token) {
    // console.log(view_type,start_no,port_qty)
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
   

    return fetch(environment.api + `api/unit/port/unreserve/${port_id}`, {
        method: 'DELETE',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
