import {
    environment
} from "../Environment";



// export function user_login(username, password, remote_name, ip) {
//     const headers = new Headers({
//         "Content-Type": "application/json",
//         "Cache-Control": "no-cache",
//         Pragma: "no-cache",
//         Expires: "Sat, 01 Jan 2000 00:00:00 GMT"
//     });
//     const params = {
//         "username": username,
//         "password": password,
//         "remote_name": remote_name,
//         "ip_address": ip
//     };
//     return fetch(environment.api + "user/login", {
//         headers: headers,
//         params: params
//     }).then(res => res.json()).catch(error => {
//         const response_err = {
//             ret: -1,
//             msg: 'Error ' + error.status + ': ' + error.statusText
//         }
//         return response_err
//     })
// }

// export function user_login_test(username, password, ip) {

//     const headers = new Headers({
//         "Content-Type": "application/json",
//         "Cache-Control": "no-cache",
//         Pragma: "no-cache",
//         Expires: "Sat, 01 Jan 2000 00:00:00 GMT",
//         token: ""
//     });
//     const params = {
//         username: username,
//         password: password,
//         ip: "192.168.10.100"
//     };

//     // return  axios.post(Environment.api+'auth/login/', {
//     //     headers: headers,
//     //     params: params
//     // }).then( response => console.log(response))

//     console.log('yut')
//     fetch('http://192.168.67.100/api/user/login.php', {
//             method: 'POST',
//             headers: {
//                 Accept: 'application/json',
//                 'Content-Type': 'application/json',
//             },
//             body: JSON.stringify(params),
//         })
//         .then(function (response) {
//             // console.log('yut')
//             return response.json();
//         })
//         .then(myJson => {
//             console.log(myJson)
//             // console.log('yut')
//             // console.log(this.state.data)
//             // this.renderCytoscapeElement()

//         });


//     // fetch('http://192.168.67.90/service-clustering/API/intercon/get_visual_data.php', {
//     //         headers: {
//     //             'Content-Type': 'application/json',
//     //             'Accept': 'application/json'
//     //         },
//     //         params: params
//     //     })
//     //     .then(function (response) {
//     //         return response.json();
//     //     })
//     //     .then(myJson => {
//     //         this.setState({
//     //             data: myJson
//     //         })
//     //         // console.log(this.state.data)
//     //         // this.renderCytoscapeElement()

//     //     });
// }

// export function login(username, password) {
//     const params = { "username": username, "password": password };
//     // const ret_obj = await fetch('http://192.168.60.226/api/user/login.php', {
//     const ret_obj = fetch(Environment.api+'auth/login/', {
//         method: 'POST',
//         headers: {
//             Accept: 'application/json',
//             'Content-Type': 'application/json',
//         },
//         body: JSON.stringify(params),
//     })
//         .then((response) => response.json())
//         .then((responseJson) => {
//             // console.log(responseJson)
//             return responseJson;
//         })
//         .catch((error) => {
//             console.error(error);
//             let ret_obj = {
//                 ret: -1,
//                 msg: error
//             }
//             return ret_obj;
//         });
//     return ret_obj;
// }



export function login(username, password, ip_address, remote_server_id) {
    const headers = new Headers({
        "Content-Type": "application/json",
    });

    const params = {
        username: username,
        password: password,
        ip_address: ip_address,
        remote_server_id: remote_server_id,
    };
    return fetch(environment.api + "api/auth/login", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
    .catch( error =>{
        console.error('There was an error!', error);
    })
}

export function logout(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
    });

    const params = {
        token: token
    };
    return fetch(environment.api + "api/auth/logout", {
        method: 'DELETE',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    ).catch( error =>{
        console.error('There was an error!', error);
    })
}

export function verify_token(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/system/info", {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    ).catch( error =>{
        console.error('There was an error!', error);
    })
}

export function get_remote(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/auth/remote/get", {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function create_remote(data, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token,
    });

    const params = data;
    return fetch(environment.api + "api/auth/remote/create", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
}

export function edit_remote(id, data, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token,
    });

    const params = data;
    return fetch(environment.api + `api/auth/remote/update/${id}`, {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
}
export function remove_remote(id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token,
    });

    return fetch(environment.api + `api/auth/remote/remove/${id}`, {
        method: 'DELETE',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
}

export function get_remote_type(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/auth/remote/type/get", {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_remote_login(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/auth/remote/list", {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function login_remote(username, password, ip_address, remote_server_id) {
    const headers = new Headers({
        "Content-Type": "application/json",
    });

    const params = {
        username: username,
        password: password,
        ip_address: ip_address,
        remote_server_id: remote_server_id,
    };
    return fetch(environment.api + "api/auth/remotelogin", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
}

export function get_user_account(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/user/get/", {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function get_auth_level(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/auth/list", {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function edit_user_account(id, data, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token,
    });

    const params = data;
    return fetch(environment.api + `api/user/update/${id}`, {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
}
export function register_user_account(username, password, firstname,lastname, email, auth_id ,token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    const params = {
        "username": username,
        "password": password,
        "firstname": firstname,
        "lastname": lastname,
        "email": email,
        "auth_id": auth_id,
    };
    return fetch(environment.api + "api/user/register", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
}

export function remove_user_account(id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token,
    });

    return fetch(environment.api + `api/user/remove/${id}`, {
        method: 'DELETE',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
}