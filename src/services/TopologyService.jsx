import { environment } from "../Environment";


export function get_containerList(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/topology/container/list", {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function get_containerTree(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/topology/container/tree", {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function edit_container(id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/topology/container/get/" + id, {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}


export function create_container(formData, token) {
    const headers = new Headers({
        // "Content-Type": "application/json",
        "token": token
    });


    return fetch(environment.api + "api/topology/container/create", {
        method: 'POST',
        headers: headers,
        body: formData
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
}
export function update_container(formData, id, token) {
    const headers = new Headers({
        // "Content-Type": "application/json",
        // 'Accept': 'application/json',
        "token": token
    });

    return fetch(environment.api + "api/topology/container/update/" + id, {
        method: 'POST',
        headers: headers,
        body: formData
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
}
export function delete_container(id, token) {
    const headers = new Headers({
        // "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/topology/container/remove/" + id, {
        method: 'DELETE',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
}



export function get_elementList(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/topology/element/list", {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}


export function drop_regsiteredUnit(draggedBox_id, dropBox_id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    const params = {
        "unit_id": draggedBox_id,
    };
    return fetch(environment.api + "api/topology/mapping/element/set/" + dropBox_id, {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function remove_regsiteredUnit(draggedBox_id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    return fetch(environment.api + "api/topology/mapping/element/remove/" + draggedBox_id, {
        method: 'DELETE',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function open_next_container(container_id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    return fetch(environment.api + "api/topology/mapping/element/get/" + container_id, {
        method: 'GET',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function open_next_global_container(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    return fetch(environment.api + "api/topology/mapping/element/get", {
        method: 'GET',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}



// export function add_auto_interconnection(source, target, qty, case_percent, token) {
//     const headers = new Headers({
//         "Content-Type": "application/json",
//         "token": token
//     });
//     const params = {
//         "unit_id1": source,
//         "unit_id2": target,
//         "qty": qty,
//         "case_percent": case_percent,
//     };
//     console.log(params)
//     return fetch(environment.api + "api/topology/mapping/intercon/auto/create", {
//         method: 'POST',
//         headers: headers,
//         body: JSON.stringify(params)
//     }).then(res => res.json()).then(data => {return data;} )
// }
export function add_auto_interconnection(data,token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    const params = data;
    // console.log(params)
    return fetch(environment.api + "api/topology/mapping/intercon/auto/create", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(data => {return data;} )
}
// export function add_auto_interconnection(source, target, qty, case_percent, token) {
//     const headers = new Headers({
//         "Content-Type": "application/json",
//         "token": token
//     });
//     const params = {
//         "unit_id1": source,
//         "unit_id2": target,
//         "qty": qty,
//         "case_percent": case_percent,
//     };
//     console.log(params)
//     return fetch(environment.api + "api/topology/mapping/intercon/auto/create", {
//         method: 'POST',
//         headers: headers,
//         body: JSON.stringify(params)
//     }).then(res => res.json()).then(
//         data => {
//             console.log(data.status)
//             return data;
//         }
//     )
// }
export function add_manual_interconnection(source, target, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    const params = {
        "port_fiber1": source,
        "port_fiber2": target,
    };
    // console.log(params)
    return fetch(environment.api + "api/topology/mapping/intercon/manual/create", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
}


export function get_addedInterconnection(unit_id1, unit_id2, token) {
    // console.log(unit_id1, unit_id2)
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    return fetch(environment.api + "api/topology/mapping/intercon/get/" + unit_id1 + "/" + unit_id2, {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_interconnection(ref_id1, ref_id2, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    const params = {
        "ref_id1": ref_id1,
        "ref_id2": ref_id2,
    };
    return fetch(environment.api + "api/topology/intercon/get", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_interconnection_visual(node_id1, node_id2, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    const params = {
        "node_id1": node_id1,
        "node_id2": node_id2,
    };
    return fetch(environment.api + "api/visual/intercon/get", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_registeredUnit_list(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    return fetch(environment.api + "api/unit/get", {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function add_global_topology(unit_id, cont_id, x_axist, y_axist, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    const params = {
        "cont_id": cont_id,
        "unit_id": unit_id,
        "x_axist": x_axist,
        "y_axist": y_axist
    };
    return fetch(environment.api + "api/topology/mapping/element/add", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function add_local_topology(containerPage_id, unit_id, cont_id, x_axist, y_axist, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    const params = {
        "cont_id": cont_id,
        "unit_id": unit_id,
        "x_axist": x_axist,
        "y_axist": y_axist
    };
    return fetch(environment.api + "api/topology/mapping/element/add/" + containerPage_id, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function add_container_topology(container_id, unit_id, cont_id, x_axist, y_axist, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    const params = {
        "cont_id": cont_id,
        "unit_id": unit_id,
        "x_axist": x_axist,
        "y_axist": y_axist
    };
    return fetch(environment.api + "api/topology/mapping/element/add/" + container_id, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function edit_global(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    return fetch(environment.api + "api/topology/mapping/edit", {
        method: 'POST',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function save_position_unit(position_array, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    const params = position_array;
    // console.log("params",position_array)
    return fetch(environment.api + "api/topology/mapping/element/move", {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            
            return data;
        }
    )
}
export function remove_unit(id, token) {
    // console.log(id,token)
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/topology/mapping/element/remove/" + id, {
        method: 'DELETE',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
}

export function remove_interconnection(id, token) {
    // console.log(id, token)
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + `api/topology/mapping/intercon/remove/${id}`, {
        method: 'DELETE',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
}
export function apply_topology(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    return fetch(environment.api + "api/topology/mapping/apply", {
        method: 'POST',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_available_registered(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    return fetch(environment.api + "api/topology/mapping/unit/get", {
        method: 'GET',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}


export function cancel_topology(token) {
    const headers = new Headers({
        // "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/topology/mapping/cancel", {
        method: 'DELETE',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
}

export function check_to_add_into_container(cont_id, elm_id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    const params = {
        "elm_id": elm_id,
    };
    return fetch(environment.api + `api/topology/container/check/${cont_id}`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}



export function reset_topology(token) {
    const headers = new Headers({
        // "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "/api/topology/mapping/clear", {
        method: 'DELETE',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
}