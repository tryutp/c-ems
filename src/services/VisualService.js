// import axios from "axios";
// import {
//     CLusterEnvi
// } from "../Environment";
import {environment} from "../Environment";
// export function getEdgeRecord(robot_id1, robot_id2) {
//     const sp_getIntercon = "call cluster.get_edge_interconnect(" + robot_id1 + ',' + robot_id2 + ");";
//     const params = {
//         query_str: sp_getIntercon,
//     };

//     return axios.get(CLusterEnvi.api + "robot/exec_get_sp.php", {
//         headers: {
//             'Content-Type': 'application/json',
//             'Accept': 'application/json'
//         },
//         params: params
//     }).then(
//         response => {
//             return response.data
//         }
//     )    
// }


// export function get_global_topology(token){

//     const headers = new Headers({
//         "Content-Type": "application/json",
//         "token" : token
//     });
//     console.log(environment.api +"api/visual/element/get/")
//     return fetch( environment.api +"api/visual/element/get/", {
//         method: 'GET',
//         headers: headers
//     }).then(res => res.json()).then(
//         data => {
//             // console.log(data)
//             return data;
//         }
//     )
// }
export function get_global_topology(cont_id, port_idx, token){

    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    // console.log(environment.api +"api/visual/element/get/")
    const params = {
        "port_idx": port_idx
    };
    if(cont_id){
        return fetch( environment.api +`api/visual/element/get/${cont_id}`, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(params)
        }).then(res => res.json()).then(
            data => {
                // console.log(data)
                return data;
            }
        )
    }
    if(!cont_id){
        return fetch( environment.api +`api/visual/element/get/`, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(params)
        }).then(res => res.json()).then(
            data => {
                // console.log(data)
                return data;
            }
        )
    }
    
}
// export function get_local_topology(id, token){

//     const headers = new Headers({
//         "Content-Type": "application/json",
//         "token" : token
//     });
//     const params = {
//         "port_idx": id
//     };
//     return fetch( environment.api +`api/visual/element/get/${id}`, {
//         method: 'POST',
//         headers: headers,
//         body: JSON.stringify(params)
//     }).then(res => res.json()).then(
//         data => {
//             // console.log(data)
//             return data;
//         }
//     )
// }


