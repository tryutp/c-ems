import { environment } from "../Environment";


export function get_route_connectivity_old(option, port_no1, port_no2, qty, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    const params = {
        "option": option,
        "port_no1": port_no1,
        "port_no2": port_no2,
        "qty": qty
    };
    return fetch(environment.api + "api/task/connectivity/connection/route/get", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function get_route_connectivity(option, port_no1, port_no2, qty, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    const params = {
        "option": option,
        "port_fiber1": port_no1,
        "port_fiber2": port_no2,

    };
    return fetch(environment.api + "api/task/connectivity/connection/route/get", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function get_fiber_option(port_no1, port_no2, token) {
    // console.log(port_no1, port_no2)
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    const params = {
        "port_no1": port_no1,
        "port_no2": port_no2,
    };
    return fetch(environment.api + "api/task/connectivity/connection/option/get", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_tasks(token) {

    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    return fetch(environment.api + "api/task/get", {
        method: 'GET',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_connected(token) {

    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    return fetch(environment.api + "api/task/connectivity/connection/get", {
        method: 'GET',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function create_connectivity(start_date,remark,port_no1, port_no2, detail, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    const params = {
        "start_date": start_date,
        "remark": remark,
        "port_fiber1": port_no1,
        "port_fiber2": port_no2,
        "connection_data": detail,
    };
    // console.log(params)
    return fetch(environment.api + "api/task/connectivity/connection/create", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
}

export function disconnect_connectivity(id, start_date, remark, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    const params = {
        "start_date": start_date,
        "remark": remark
    };
    // console.log(params)
    return fetch(environment.api + `api/task/connectivity/disconnection/create/${id}`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
}

export function connection_detail(id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + `api/task/connectivity/connection/detail/${id}`, {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data.status)
            return data;
        }
    )
}

export function remove_task(task_id,token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    return fetch( environment.api +`api/task/connectivity/remove/${task_id}`, {
        method: 'DELETE',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function rollback_task(task_id,remark, token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    const params = {
        "remark": remark
    };
    return fetch( environment.api +`api/task/connectivity/rollback/${task_id}`, {
        method: 'DELETE',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_task_log(data, token) {

    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    const params = data;
    return fetch(environment.api + "api/log/task", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_task_log_count(search, timeoffset, token) {

    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    const params = {
        "search": search,
        "timeoffset": timeoffset
    };
    return fetch(environment.api + "api/log/task/count", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function switch_polarity(port_idx1, port_idx2, start_date, remark, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    const params = {
        "port_idx1": port_idx1,
        "port_idx2": port_idx2,
        "start_date": start_date,
        "remark": remark,
    };
    return fetch(environment.api + "api/task/connectivity/connection/polarity/switch", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}