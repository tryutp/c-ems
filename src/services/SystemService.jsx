
import {environment} from "../Environment";



export function register_system(name, mac_address, ip_address, model_id,password, token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });

    const params = {
        name: name,
        mac_address: mac_address,
        ip_address: ip_address,
        model_id: model_id,
        password: password,
    };
    return fetch( environment.api +"api/unit/register", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function update_system(id, name, mac_address, ip_address, password , token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });

    const params = {
        name: name,
        mac_address: mac_address,
        ip_address: ip_address,
        password: password,
    };
    return fetch( environment.api +`api/unit/update/${id}`, {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function disable_unit(unit_id,token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    return fetch( environment.api +`api/unit/disable/${unit_id}`, {
        method: 'DELETE',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function enable_unit(unit_id, token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });

    return fetch( environment.api +`api/unit/enable/${unit_id}`, {
        method: 'POST',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_unit(token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });

    return fetch( environment.api +"api/unit/get", {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}


export function get_port_filtering(unit_id, smu_type, token){
    const headers = new Headers({
        // "Content-Type": "application/json",
        "token" : token
    });

    // const params = {
    //     "unit_id": unit_id,
    //     "smu_type": smu_type,
    // };
    return fetch( environment.api +`api/unit/${unit_id}/port/type/${smu_type}`, {
        method: 'GET',
        headers: headers,
        // body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_system_list(token){

    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    return fetch( environment.api +"api/unit/get", {
        method: 'GET',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_model_list(token){

    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    return fetch( environment.api +"api/unit/model/list", {
        method: 'GET',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function remove_system(system_id,token){

    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    return fetch( environment.api +"api/unit/remove/" + system_id, {
        method: 'DELETE',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_datetime_setting(token){

    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    return fetch( environment.api +"api/system/setting/datetime/get", {
        method: 'GET',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function get_network_setting(token){

    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    return fetch( environment.api +"api/system/setting/network/get", {
        method: 'GET',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function get_timezone_list(token){

    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    return fetch( environment.api +"api/system/setting/datetime/timezone/list", {
        method: 'GET',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function save_network_static(staticSetting, token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });

    const params = staticSetting;
    return fetch( environment.api +"api/system/setting/network/ip/static", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function save_network_dhcp( token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });

    return fetch( environment.api +`api/system/setting/network/ip/dhcp`, {
        method: 'PUT',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function save_datetime_manual(datetime, timezone_id, token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    const params = {
        "time": datetime,
        "timezone_id": timezone_id
    };
    return fetch( environment.api +`api/system/setting/datetime/manual`, {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function save_datetime_ntp(timeserver1,timeserver2,timeserver3, timezone_id, token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    const params = {
        "timeserver1": timeserver1,
        "timeserver2": timeserver2,
        "timeserver3": timeserver3,
        "timezone_id": timezone_id

    };
    return fetch( environment.api +`api/system/setting/datetime/auto`, {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function save_timezone(timezone_id, token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
   
    return fetch( environment.api +`api/system/setting/datetime/timezone/set/${timezone_id}`, {
        method: 'PUT',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function save_timeOffset(time_offset, token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    const params = {
        "time_offset": time_offset
    };
    return fetch( environment.api +`api/system/setting/datetime/offset/set`, {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function reboot_system(token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    // const params = {
    //     "duration": duration
    // }
    return fetch( environment.api +"api/system/reboot", {
        method: 'POST',
        headers: headers,
        // body: JSON.stringify(params)

    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}


export function shutdown_system( duration, token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    const params = {
        "duration": duration
    }

    // console.log(duration, token)
    return fetch( environment.api +"api/system/shutdown", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)

    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function cancel_shutdown_system( token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });

    return fetch( environment.api +"api/system/shutdown/cancel", {
        method: 'DELETE',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function acknowledge_single_notification(noti_id,token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    return fetch( environment.api +`api/notification/acknowledge/single/${noti_id}`, {
        method: 'DELETE',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function acknowledge_group_notification(severity_id,token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    return fetch( environment.api +`api/notification/acknowledge/multiple/${severity_id}`, {
        method: 'DELETE',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function acknowledge_all_group_notification(token){
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    return fetch( environment.api +`api/notification/acknowledge/multiple/`, {
        method: 'DELETE',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function upload_firmware( fileData, fileType, fileSize, file, token){
    const headers = new Headers({
        "token" : token
    });
    return fetch(environment.api +"api/system/upload", {
      method: 'POST',
      // 👇 Set headers manually for single file upload
      headers: headers,
    //   fileData
      body: fileData
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function execute_firmware( action, token){
    // const headers = new Headers({
    //     "Content-Type": fileType,
    //     'Content-Length': `${fileSize}`,
    //     "token" : token
    // });
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });

    const params = {
        "action": action
    }

    return fetch(environment.api +"api/system/execute", {
      method: 'POST',
      // 👇 Set headers manually for single file upload
      headers: headers,
    //   fileData
      body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

// public uploadFirmwareFile(data) {
//     let uploadURL = this._api + 'APIv1/upgrade_firmware/upload.php';
//     // return this.httpClient.post<any>(uploadURL, data);

//     return this.httpClient.post<any>(uploadURL, data, {
//       reportProgress: true,
//       observe: 'events'
//     }).pipe(map((event) => {

//       switch (event.type) {

//         case HttpEventType.UploadProgress:
//           const progress = Math.round(100 * event.loaded / event.total);
//           return { status: 'progress', message: progress, ret: 1 };

//         case HttpEventType.Response:
//           return event.body;
//         // default:
//         // return `Unhandled event: ${event.type}`;
//       }
//     }))
//     .catch((error: any) => {
//       const response_object = [{ ret: -1, msg: 'Internal server error' }]
//       return response_object;
//     })

//   }