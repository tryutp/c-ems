import { environment } from "../Environment";

export function register_customer(c_username, c_firstname, c_lastname, c_email, c_password, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    const params = {
        "username": c_username,
        "password": c_password,
        "firstname": c_firstname,
        "lastname": c_lastname,
        "email": c_email,
    }
    return fetch(environment.api + "api/customer/create", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_port_assign_panel(customer_id,view_type,start_no,port_qty, unit_id, smu_type, search,token) {
    // console.log(view_type,start_no,port_qty)
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    const params = {
        "cus_id": customer_id,
        "view_type": view_type,
        "start_no": start_no,
        "port_qty": port_qty,
        "unit_id": unit_id,
        "smu_type": smu_type,
        "search": search,
    }

    return fetch(environment.api + `api/unit/port/assign/panel`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params),
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}


export function remove_customer_account(cus_id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    return fetch(environment.api + `api/customer/remove/${cus_id}`, {
        method: 'DELETE',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function remove_customer_port(port_id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    return fetch(environment.api + `api/unit/port/unassign/${port_id}`, {
        method: 'DELETE',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function assign_port_to_customer(cus_id, port_idx, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    const params = {
        "cus_id": cus_id,
        "port_idx": port_idx,
    }
    return fetch(environment.api + "api/unit/port/assign", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_customer_list(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/customer/list", {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_customer_count(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/customer/summary", {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function update_customer_info(customer_id, c_firstname, c_lastname, c_email, c_password, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    const params = {
        "password": c_password,
        "firstname": c_firstname,
        "lastname": c_lastname,
        "email": c_email,
    };
    return fetch(environment.api + `api/customer/update/${customer_id}`, {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_customer(customer_id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + `api/customer/get/${customer_id}`, {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_customer_port(customer_id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + `api/customer/${customer_id}/port`, {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}