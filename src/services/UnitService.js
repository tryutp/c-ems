import {environment} from "../Environment";



export function get_port(unit_id,token){
    // console.log(unit_id)
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    return fetch( environment.api +`api/topology/mapping/unit/${unit_id}/port/type/0`, {
        method: 'GET',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function get_port_by_type(unit_id,type,token){
    // console.log(unit_id)
    const headers = new Headers({
        "Content-Type": "application/json",
        "token" : token
    });
    return fetch( environment.api +`api/topology/mapping/unit/${unit_id}/port/type/${type}`, {
        method: 'GET',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_unit_port_panel(unit_id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + `api/unit/${unit_id}/details/port/panel`, {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    ).catch( error => {
        return {
            "status": 401
        }
        // console.log(error)
    })
}
export function get_unit_status(unit_id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + `api/unit/${unit_id}/details/status`, {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function get_unit_port_summary(unit_id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + `api/unit/${unit_id}/details/port/summary`, {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_unit_state(unit_id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + `api/unit/${unit_id}/details/state`, {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_unit_info(unit_id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + `api/unit/${unit_id}/details/info`, {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_unit_notification(unit_id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + `api/unit/${unit_id}/details/notification`, {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}