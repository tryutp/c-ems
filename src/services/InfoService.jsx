import { environment } from "../Environment";

export function get_current_time() {

    const headers = new Headers({
        "Content-Type": "application/json",
        'Cache-Control': 'no-cache, no-store, must-revalidate, post-check=0, pre-check=0',
        Pragma: "no-cache",
        Expires: '0'
        // Expires: "Sat, 01 Jan 2000 00:00:00 GMT"
    });
    const params = {};
    return fetch(environment.api + "device/current_timestamp", {
        headers: headers,
        params: params
    }).then(res => res.json()).then(
        data => {
            return data;
        }
    )
}


export function get_port_summary(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/unit/port/summary", {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function get_unit_summary(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/unit/summary", {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function get_unit_overview_status(unitPerPage, page, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    const params = {
        "unit_per_page": unitPerPage,
        "page": page,
    };
    return fetch(environment.api + "api/unit/status", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_unit_overview_all(unitPerPage, page, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    const params = {
        "unit_per_page": unitPerPage,
        "page": page,
    };
    return fetch(environment.api + "api/unit/info", {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params)
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}



export function get_system_info(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/system/info", {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function get_system_status(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    return fetch(environment.api + "api/system/status", {
        method: 'GET',
        headers: headers
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_unit_by_state(state, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/unit/state/" + state, {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function get_unit_by_all( token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/unit/state/", {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}




// export function get_notifications(token) {
//     const headers = new Headers({
//         "Content-Type": "application/json",
//         "token": token
//     });

//     return fetch(environment.api + "api/notification/get", {
//         method: 'GET',
//         headers: headers,
//     }).then(res => res.json()).then(
//         data => {
//             // console.log(data)
//             return data;
//         }
//     )
// }
export function get_unitNotification(id, severity_id, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + `api/unit/${id}/notification/severity/${severity_id}`, {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_intercon_summary( token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + `api/visual/intercon/summary`, {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}


export function get_total_port_count(unit_id, smu_type, search, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    const params = {
        "unit_id": unit_id,
        "smu_type": smu_type,
        "search": search,
    }
    return fetch(environment.api + `api/unit/port/panel/count`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params),
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function get_total_port_edit_count(unit_id, smu_type, search, token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    const params = {
        "unit_id": unit_id,
        "smu_type": smu_type,
        "search": search,
    }
    return fetch(environment.api + `api/unit/port/list/count`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params),
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function get_all_notifications(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    const params = {
        "unit_id": null,
        "severity_id": null
    }

    return fetch(environment.api + `api/notification/get`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params),
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}
export function get_notifications_by_severity(unit_id, severity_id,token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    const params = {
        "unit_id": unit_id,
        "severity_id": severity_id
    }

    return fetch(environment.api + `api/notification/get`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params),
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_notifications_amount(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    const params = {
        "unit_id": null,
        "severity_id": null
    }

    return fetch(environment.api + `api/notification/count`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params),
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_port_panel(view_type, start_no, port_qty, unit_id, smu_type, search, token) {
    // console.log(view_type,start_no,port_qty)
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    const params = {
        "view_type": view_type,
        "start_no": start_no,
        "port_qty": port_qty,
        "unit_id": unit_id,
        "smu_type": smu_type,
        "search": search,
    }

    return fetch(environment.api + `api/unit/port/panel`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params),
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_port_panel_edit( start_no, port_qty, unit_id, smu_type, search, token) {
    // console.log(view_type,start_no,port_qty)
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });
    const params = {
        "start_no": start_no,
        "port_qty": port_qty,
        "unit_id": unit_id,
        "smu_type": smu_type,
        "search": search,
    }

    return fetch(environment.api + `api/unit/port/list`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params),
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}



// export function get_unit_info(unitPerPage, page, token) {
//     const headers = new Headers({
//         "Content-Type": "application/json",
//         "token": token
//     });


//     return fetch(environment.api + "api/unit/status", {
//         method: 'POST',
//         headers: headers,
//         body: JSON.stringify(params)
//     }).then(res => res.json()).then(
//         data => {
//             // console.log(data)
//             return data;
//         }
//     )
// }

export function get_severities(token) {
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + "api/notification/severity/list", {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}

export function get_port_single( port_idx, token) {
    // console.log(view_type,start_no,port_qty)
    const headers = new Headers({
        "Content-Type": "application/json",
        "token": token
    });

    return fetch(environment.api + `api/unit/port/${port_idx}`, {
        method: 'GET',
        headers: headers,
    }).then(res => res.json()).then(
        data => {
            // console.log(data)
            return data;
        }
    )
}