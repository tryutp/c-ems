

import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import './App.css';
import Home from "./components/Home";
import * as UserService from "./services/UserService";
import { Loading } from "./shared/Loading";
// import Login from "./components/Login";
import Login from "./components/Login/Login";

// import PrivateRoute from './components/PrivateRoute'
// import { AuthContext } from ".//components/Auth";

class App extends React.Component {
  // const [tokenInStorage, setTokenInStorage] = useState("");
  // const [userObj, setUserObj] = useState( JSON.parse(localStorage.getItem("userData")) || "")
  // const setTokens = (data) => {
  //   localStorage.setItem("tokens", JSON.stringify(data));
  //   setUserObj(data);
  //   console.log(data)
  // }
  constructor(props) {
    super(props)
    this.state = {
      authenticated: false,
      isVerified: false,
      loading: false,
    };
  }

  componentDidMount() {
    const userObj = JSON.parse(localStorage.getItem("userData"));
    if (userObj) {
      // console.log(userObj);
      this.verifyToken(userObj.token);
      // this.setState({authenticated: true, isVerified: true})
    } else {
      // console.log('not found');
      this.setState({ authenticated: false, isVerified: true })
    }
  }
  verifyToken(token) {
    // console.log('yut')
    UserService.verify_token(token).then(result => {
      // console.log(result)
      if (result.status === 1) {
        this.setState({ authenticated: true, isVerified: true })
      } 
      else if (result.status === -1) {
        if (localStorage.getItem('userData')) {
          localStorage.removeItem('userData');
          this.setState({ authenticated: false, isVerified: true })
        }

      }
    })
  }
  render() {
    // console.log(process.env.REACT_APP_ENVI)
    if (this.state.isVerified) {
      if (this.state.authenticated) {
        return <ProtectedRoute />
      } else {
        return <RouteRender />
      }
    } else {
      return <Loading open={this.state.loading} />
    }
  }
}
export default App;


const RouteRender = () => {
  return (
    // <AuthContext.Provider value={{ userObj, setUserObj: setTokens }}>
    <Router>
      <Switch>
        {/* <PrivateRoute exact path='/' component={Home} /> */}
        {/* { this.state.authenticated && 
          <Route path='/' component={Home} />
        } */}
        <Route path='/' component={Login} />
      </Switch>
    </Router>
    // </AuthContext.Provider>
  );
}

const ProtectedRoute = () => {
  return (
    // <AuthContext.Provider value={{ userObj, setUserObj: setTokens }}>
    <Router>
      <Switch>
        {/* <PrivateRoute exact path='/' component={Home} /> */}

        {/* { this.state.authenticated && 
          <Route path='/' component={Home} />
        } */}
        <Route path='/' component={Home} />
        {/* <Route exact path='/overview' component={Home} />
        <Route exact path='/dragdrop' component={Home} />
        <Route exact path='/cytoscape' component={Home} />
        <Route exact path='/template-mapping' component={Home} />
        <Route exact path='/login' component={Home} /> */}
      </Switch>
    </Router>
    // </AuthContext.Provider>
  );
}