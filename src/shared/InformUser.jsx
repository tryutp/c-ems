


import React from "react";
import './InformUser.scss'

import { createModal } from "react-modal-promise";
import Modal from "react-modal";
const SuccessModal = ({ open, close, text, title }) => {
  const submit = () => close("confirm");
  const cancel = () => close("dismiss");
  const closeIn = (timeout) => {
    setTimeout(cancel, timeout)
  }

  return (
    <Modal
      isOpen={open}
      toggle={cancel}
      className="inform-modal"
      overlayClassName="default-overlay"
      onRequestClose={cancel}
      // closeTimeoutMS={500}
      onAfterOpen={() => { closeIn(2000) }}
    >
      <div className="success inform-container">
      <button onClick={cancel} id="page-close-btn" style={{opacity:"0"}}  autoFocus className="close"></button>
      <i class="fas fa-check-circle green"></i>
        {
          title && <header><h1 toggle={cancel}>{title}</h1></header>
        }
        <span>{text}</span>
        
      </div>
      
    </Modal>
  );
};

const UnsuccessModal = ({ open, close, text, title }) => {
  const submit = () => close("confirm");
  const cancel = () => close("dismiss");
  const closeIn = (timeout) => {
    setTimeout(cancel, timeout)
  }

  if (title == "Unauthorized") {
    window.location.reload(false);
  }
  return (
    <Modal
      isOpen={open}
      toggle={cancel}
      className="inform-modal"
      overlayClassName="default-overlay"
      onRequestClose={cancel}
      // closeTimeoutMS={500}
      onAfterOpen={() => { closeIn(4000) }}
    >
       <div className="unsuccess inform-container">
       <button onClick={cancel} id="page-close-btn" style={{opacity:"0"}} autoFocus className="close"></button>
       <i class="fas fa-times-circle red"></i>
        {
          title && <header><h1 toggle={cancel}>{title}</h1></header>
        }
        
        <span>{text}</span>
      </div>

    </Modal>
  );
};

export const unsuccess = createModal(UnsuccessModal);
export const success = createModal(SuccessModal);
