import React, { useState, useEffect  } from "react";
import './Confirm.scss'
import * as InformUser from "./../shared/InformUser";
import { createModal } from "react-modal-promise";
import Modal from "react-modal";
const ComfirmationModal = ({ open, close, text, title , actionLeft, actionRight }) => {
  
  const submit = (event) => {
    event.preventDefault();
    close("confirm")
  };
  const cancel = () => close(null);

  return (
    <Modal
      isOpen={open}
      toggle={cancel}
      className="confirmation-modal"
      overlayClassName="confirmation-overlay"
      onRequestClose={cancel}
    // closeTimeoutMS={500}
    >
      <div className="confirmStyle">

        <header className="h2">
          <h2 toggle={cancel}>{title}</h2>
        </header>
        <hr />
        <div className="paragraph">
          <span>{text}</span>
        </div>
    <form onSubmit={submit}>
        <div className="btn-row">
          <button id="confirm-submit-btn" type="submit" autoFocus style={{marginRight:"0.5vw"}}  className="submit btn">
            {
              actionLeft && <span>{actionLeft}</span>
            }
            {
              !actionLeft && <span>Confirm</span>
            }
            
          </button>
          <button id="confirm-cancel-btn" className="cancel btn" onClick={cancel}>
          {
              actionRight && <span>{actionRight}</span>
            }
            {
              !actionRight && <span>Cancel</span>
            }
          </button>


        </div>
        </form>
      </div>



    </Modal>
  );
};

export const openConfirmation = createModal(ComfirmationModal);




const ComfirmRemarkModal = ({ open, close, text, title }) => {
  const submit = () => {
    if(remark){
      if( !validateNoQoute(remark) ){
        close({ "confirm": 1, "remark": remark, })
      }else{
        InformUser.unsuccess({ text: "Single quote and Double quote are not allowed."})
      }
    }
    if(!remark){
      close({ "confirm": 1, "remark": remark, })

    }
    
  };

  function validateNoQoute(input){
        
    if(!input.match(/^[^'"]*$/)){
        return true
    }
  }
  const cancel = () => close(null);
  const [remark, setRemark] = useState(null);

  var textareaHandle = (event) => {
    setRemark(event.target.value)
  }

  // console.log('yut')
  return (
    <Modal
      isOpen={open}
      toggle={cancel}
      className="confirmationWithRemark-modal"
      overlayClassName="confirmation-overlay"
      onRequestClose={cancel}
    // closeTimeoutMS={500}
    >
      <div className="confirmWithRemark">

        {/* <header className="h1"> */}
          <h2 toggle={cancel}>{title}</h2>
        {/* </header> */}
        <hr />
        <div className="paragraph">
          <span>{text}</span>
          <textarea name="remark" id="remark-textarea" rows="2"   maxlength="255"  style={{resize:'none'}}  onChange={textareaHandle} placeholder="remark"></textarea>
        </div>

        <div className="btn-row">
          <button id="confirm-submit-btn"  style={{marginRight:"0.5vw"}} autoFocus className="submit btn" onClick={submit}>
            <span>Confirm</span>
          </button>
          <button id="confirm-cancel-btn" className="cancel btn" onClick={cancel}>
            <span>Cancel</span>
          </button>


        </div>
      </div>



    </Modal>
  );
};

export const openConfirmationRemark = createModal(ComfirmRemarkModal);


let selected = "sdfsfs";
const ComfirmLoginModal = ({ open, close, text, title, options }) => {

  const submit = () => close();
  const cancel = () => close(null);

  const [serverOption, setServerOption] = useState(0);
  
 
  useEffect(() => {
    selected = options[0];
  }, []);
  
  function selectServer (option, index)  {
    setServerOption(index);
    selected = option;
  }

  function confirmHandler(a){
    close(a)
  }

  return (
    <Modal
      isOpen={open}
      toggle={cancel}
      className="confirmation-login-modal"
      overlayClassName="confirmation-overlay"
      onRequestClose={cancel}
    // closeTimeoutMS={500}
    >
      <div className="confirmLoginStyle">

        <header className="h1">
          <h2 toggle={cancel}>{title}</h2>
        </header>
        <hr />
        <div className="paragraph">
          <span>{text}</span>
          <div className="user-replace-options">
          {
            options && options.map( (option, index) =>
              
              <button key={`server-${index+1}`} id={`server-${index+1}`} onClick={ ()=>{ selectServer(option,index) }} className="btn option"> <input type="radio" id="serverOption" name="serverOption" onChange={ ()=>{ selectServer(option,index) }} value={index} checked={ serverOption === index }  /> <span>{option.login_type}, {option.online_time} actives</span> </button>
              )
          }
          </div>
          
        </div>

        <div className="btn-row">
          <button id="continue-anyway-btn" style={{marginRight:"0.5vw"}} className="submit btn" onClick={()=>{confirmHandler(selected)}}>
            <span>Continue Anyway</span>
          </button>
          <button id="cancel-btn" className="cancel btn" onClick={cancel}>
            <span>Cancel</span>
          </button>


        </div>
      </div>



    </Modal>
  );
};

export const openLoginConfirmation = createModal(ComfirmLoginModal);