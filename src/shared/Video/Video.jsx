import React, { Component } from 'react'

export class Video extends Component {

    constructor(props){
        super(props)
        this.state = {
            
        }
    }

    componentDidMount(){
        document.getElementById('tutorial').addEventListener('ended', ()=>{ this.closeHandler()  },false);
    }
    componentWillUnmount(){
        document.getElementById('tutorial').removeEventListener('ended', ()=>{ this.closeHandler()  },false);
    }

    closeHandler() {
        // console.log(this.props)
        this.props.close()
    }



    render() {
        return (
            <div>
                <header><h2>{this.props.videoTitle}</h2><hr /></header>
                
                <video id="tutorial" style={{width:"40vw"}} autoPlay controls>
                    <source src={this.props.videoSrc} type="video/mp4" />
                </video>
            </div>
        )
    }
}

export default Video
