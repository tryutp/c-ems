import React, { Component } from 'react'
import Modal from "react-modal";
import "./Loading.scss"
import ClipLoader from "react-spinners/ClipLoader";

export class Loading extends Component {


    constructor(props) {
        super(props);
        this.state = {

        }
    }



    render() {

        // console.log(this.props)


        return (
            <Modal
                isOpen={this.props.open}
                className="loading-modal"
                overlayClassName="loading-overlay"
            >
                <div className="loading-center">
                <ClipLoader loading={true}  css={{"animation": "animation-s8tf20-no-shrink 0.75s 0s infinite linear"}}  color="#FFFFFF" size={200} />
                </div>
                {/* <div className="loading-screen">
                <p>Loading ...</p> 
            </div> */}
            </Modal>
            // <div> <p>{this.props.open}</p> </div>
        )
    }
}

export default Loading
