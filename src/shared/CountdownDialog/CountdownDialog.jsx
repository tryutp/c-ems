import React, { useState, useEffect } from "react";
import "./CountdownDialog.scss";
import { createModal } from "react-modal-promise";
import Modal from "react-modal";

var timeLeft;

const CountdownDialog = ({ open, close, text, title, countdownTime, cantClose }) => {
    const submit = () => close("confirm");
    const cancel = () => close(null);

    const [timeLeft, setTimeLeft] = useState(countdownTime);

    useEffect(() => {
        const interval = setInterval(() => {
          setTimeLeft( timeLeft - 1 )
        }, 1000);
        if(timeLeft === 0){
            close("confirm")
        }

        return () => clearInterval(interval);
      });

 

    return (
        <Modal
            isOpen={open}
            toggle={cancel}
            className="confirmation-modal"
            overlayClassName="confirmation-overlay"
            // onRequestClose={cancel}
            // onAfterOpen={() => {
            //     countingDown();
            // }}

            // closeTimeoutMS={500}
        >
            <div className="countdown-dialog-container">
                <header className="h2">
                    <h2 toggle={cancel}>{title}</h2>
                </header>
                <hr />
                <div className="paragraph">
                    <span>
                        {text} {timeLeft} seconds.
                    </span>
                </div>

                <div className="btn-row center">
                    {
                        !cantClose && <button className="cancel btn" onClick={cancel}>
                        <span>Cancel</span>
                    </button>
                    }
                    
                </div>
            </div>
        </Modal>
    );
};

export const openCountdownDialog = createModal(CountdownDialog);
