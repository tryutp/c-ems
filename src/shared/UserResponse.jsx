import React from "react";
import './Confirm.scss'

import { createModal } from "react-modal-promise";
import Modal from "react-modal";
const ConfirmUserResponseModal = ({ open, close, text, title }) => {
  const submit = () => close("confirm");
  const cancel = () => close(null);
  const closeIn = (timeout) => {
    setTimeout(cancel, timeout)
  }
  return (
    <Modal
      isOpen={open}
      toggle={cancel}
      className="confirmation-modal"
      overlayClassName="confirmation-overlay"
      onRequestClose={cancel}
      onAfterOpen={() => { closeIn(3000) }}
      // closeTimeoutMS={500}
    >
      <div className="success inform-container" style={{padding:'2vw 1.5vw'}}>
      <i class="fas fa-check-circle green"></i>
        {
          title && <header><h1 toggle={cancel}>{title}</h1></header>
        }
        
        <span>{text}</span>
      </div>



    </Modal>
  );
};

export const onUserResponse = createModal(ConfirmUserResponseModal);
