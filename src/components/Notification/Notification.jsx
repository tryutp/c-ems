import React, { Component } from "react";
import { openConfirmation } from "../../shared/Confirmation";
import * as InfoService from "./../../services/InfoService";
import * as SystemService from "./../../services/SystemService";
import * as InformUser from "./../../shared/InformUser";
// import * as InfoService from "./../../services/InfoService";
import { Loading } from "./../../shared/Loading";
import "./Notification.scss";
import ReactTooltip from "react-tooltip";
import moment from "moment";

export class Notification extends Component {
	token = JSON.parse(localStorage.getItem("userData")).token;
	time_offset = JSON.parse(localStorage.getItem("userData")).time_offset;
	interval;

	constructor(props) {
		super(props);
		this.state = {
			notiData: "",
			severities: "",
			select_severity: -1,
			loading: false,
		};
	}

	componentDidMount() {
		this.setState({ loading: true }, async () => {
			await this.getAllNotification();
			await this.getSeverities();
			this.setState({ loading: false });
		});
		this.interval = setInterval(() => {
			this.getAllNotification();
		}, 3000);
	}
	componentWillUnmount() {
		if (this.interval) {
			clearInterval(this.interval);
		}
	}

	async getSeverities() {
		await InfoService.get_severities(this.token).then((result) => {
			// console.log("severities", result);
			if (result.status === 1) {
				this.setState({ severities: result.data });
			}
		});
	}
	async getAllNotification() {
		// console.log(this.state.select_severity);
		if (this.state.select_severity === -1) {
			await InfoService.get_all_notifications(this.token).then(
				async (result) => {
					console.log(result);
					if (result.status === 1) {
						var notification = result.data;
						var timeCaledLogs = await this.processAddDateOfTime(
							notification
						);
						// console.log(timeCaledLogs)
						this.setState({ notiData: timeCaledLogs });
					}
				}
			);
		} else if (
			this.state.select_severity !== -1 &&
			this.state.select_severity >= 0 &&
			this.state.select_severity <= 7
		) {
			await InfoService.get_notifications_by_severity(
				null,
				this.state.select_severity,
				this.token
			).then(async (result) => {
				// console.log(result);
				if (result.status === 1) {
					var notification = result.data;
					var timeCaledLogs = await this.processAddDateOfTime(
						notification
					);
					// console.log(timeCaledLogs)
					this.setState({ notiData: timeCaledLogs });
				}
			});
		}
	}

	async processAddDateOfTime(logData) {
		// var  logs = JSON.parse(JSON.stringify(logData));
		var logs = logData;

		for (var i = 0; i < logs.length; i++) {
			const newAddDate = await this.calculateTime(logs[i]["noti_date"]);
			logs[i]["noti_date"] = newAddDate;
		}
		// await logs.map( async  (log,index) => {
		//     const newAddDate = await this.calculateTime(log.add_date)
		//     const newStartDate = await this.calculateTime(log.start_date)
		//     const newFinishDate = await this.calculateTime(log.finish_date)

		//     logs[index]["start_date"] = newStartDate;
		//     logs[index]["finish_date"] = newFinishDate;

		//     // console.log(newTime)
		//     // await temp.push(newTime)
		// } )
		// console.log(logs)
		// console.log(logs)
		return logs;

		// return temp;
	}

	async calculateTime(serverTime) {
		var rawServerTime = await moment(serverTime);
		// console.log(rawServerTime)
		// console.log(timeOffset)
		var offset = moment.duration(this.time_offset);
		// // var temp = moment(server_time).add(1, 'm')
		// console.log(offset)
		var currentTime = await rawServerTime
			.add(offset)
			.format("YYYY-MM-DD HH:mm:ss");
		console.log(currentTime);
		return currentTime;
		// console.log(currentTime)
	}
	async submitAcknowledge() {
		var severity_id = this.state.select_severity;
		var severity = "";
		this.state.severities.map((level) => {
			if (severity_id === level.id) {
				severity = level;
			}
		});

		if (!this.state.notiData.length || this.state.notiData.length == 0) {
			InformUser.unsuccess({ text: "No notification to acknowledge" });
		} else {
			var message = "";
			if (!severity) {
				message = `Are you sure you want to acknowledge all notifications?`;
			}
			if (severity) {
				message = `Are you sure you want to acknowledge all ${severity.severity} notifications?`;
			}

			// console.log(severity_id);

			const dialog = await openConfirmation({
				title: "Notification Acknowledge",
				text: message,
			});
			if (dialog) {
				if (!severity) {
					this.setState({ loading: true }, async () => {
						await SystemService.acknowledge_all_group_notification(
							this.token
						).then(async (result) => {
							if (result.status === 1) {
								InformUser.success({ text: result.msg });
								this.getAllNotification();
							}
							if (result.status !== 1) {
								InformUser.unsuccess({ text: result.msg });
							}
							this.setState({ loading: false });
						});
					});
				}
				if (severity) {
					this.setState({ loading: true }, async () => {
						await SystemService.acknowledge_group_notification(
							severity.id,
							this.token
						).then((result) => {
							if (result.status === 1) {
								InformUser.success({ text: result.msg });
							}
							if (result.status !== 1) {
								InformUser.unsuccess({ text: result.msg });
							}
							this.setState({ select_severity: -1 }, async () => {
								await this.getAllNotification();
								this.setState({ loading: false });
							});
						});
					});
				}
			}
		}
	}

	async acknowledgeThis(notification) {
		// console.log(notification)
		const dialog = await openConfirmation({
			title: "Notification Acknowledge",
			text: `${notification.location}'s ${notification.severity} regarding of ${notification.error_name}. Are you sure you want to acknowledge this notification?`,
		});
		if (dialog) {
			this.setState({ loading: true }, async () => {
				await SystemService.acknowledge_single_notification(
					notification.id,
					this.token
				).then(async (result) => {
					if (result.status === 1) {
						InformUser.success({ text: result.msg });
						await this.getAllNotification();
						// this.removeThisNotification(notification);
					}
					if (result.status !== 1) {
						InformUser.unsuccess({ text: result.msg });
					}
					this.setState({ loading: false });
				});
			});
		}
	}
	removeThisNotification(notification) {
		var removeId = notification.id;
		var notifications = this.state.notiData;
		notifications.map((noti, index) => {
			// console.log(noti)
			if (noti.id === removeId) {
				notifications.splice(index, 1);
			}
		});
		// console.log(notifications)
		this.setState({ notiData: notifications });
	}

	async handleInteger(event) {
		const severity_id = parseInt(event.target.value);
		this.setState({ [event.target.name]: severity_id }, () => {
			this.getAllNotification();
		});
	}

	noTask = () => {
		switch (this.state.select_severity) {
			case -1:
				return <span className="camera-offline">NO NOTIFICATION</span>;
				break;
			case 0:
				return <span className="camera-offline">NO EMERGENCY</span>;
				break;
			case 1:
				return <span className="camera-offline">NO ALERT</span>;
				break;
			case 2:
				return <span className="camera-offline">NO CRITICAL</span>;
				break;
			case 3:
				return <span className="camera-offline">NO ERROR</span>;
				break;
			case 4:
				return <span className="camera-offline">NO WARNING</span>;
				break;
			case 5:
				return <span className="camera-offline">NO NOTICE</span>;
				break;
			case 6:
				return <span className="camera-offline">NO INFORMATION</span>;
				break;
			case 7:
				return <span className="camera-offline">NO DEBUG</span>;
				break;
			default:
				break;
		}
	};

	render() {
		// console.log(this.props);
		return (
			<div className="notification-container">
				<Loading open={this.state.loading} />
				<button
					onClick={() => {
						this.props.close();
					}}
					className="close fit-btn"
				>
					<i class="fas fa-times"></i>
				</button>
				<header className="h2">
					<h2>Notifications</h2>
				</header>
				<div className="table-container">
					<table id="notification-table">
						<tr className="tr-head">
							<th>
								<span>Unit</span>
							</th>
							<th>
								<span>Error Code</span>
							</th>
							<th>
								<span>Description</span>
							</th>
							<th>
								<span>Date & Time</span>
							</th>
							<th>
								<span>Severity</span>
							</th>
							<th style={{ width: "5%" }}>
								{/* <span>Action</span> */}
							</th>
						</tr>

						{this.state.notiData &&
							this.state.notiData.map((noti, index) => (
								<tr
									id={`notification-${index + 1}`}
									className="row-table"
									onClick={() => {
										this.acknowledgeThis(noti);
									}}
								>
									<td
										id={`unit-${index + 1}`}
										className="text-center"
									>
										<span>{noti.noti_from}</span>
									</td>
									<td
										id={`error-code-${index + 1}`}
										className="text-center"
									>
										<span>{noti.error_code}</span>
									</td>
									<td
										id={`description-${index + 1}`}
										className="text-center"
									>
										<span>{noti.error_name}</span>
									</td>
									<td
										id={`date-time-${index + 1}`}
										className="text-center"
									>
										<span>{noti.noti_date}</span>
									</td>
									<td
										id={`severity-${index + 1}`}
										className="text-center"
									>
										<span>{noti.severity}</span>
									</td>
									<td
										id={`tooltip-${index + 1}`}
										className="text-center font-hide"
									>
										<ReactTooltip
											id="informTooltip"
											multiline={true}
											place="left"
											type="dark"
											effect="solid"
										/>
										<i
											data-for="informTooltip"
											data-tip="Click to acknowledge"
											class="fas fa-check-circle"
										></i>
										{/* <button className="fit-btn submit">
                                            <span onClick={ ()=>{ this.acknowledgeThis(noti)}}>Acknowledge</span>
                                        </button> */}
									</td>
								</tr>
							))}
					</table>
					{this.state.notiData.length === 0 && (
						<div
							id="no-notification-data"
							className="no-remaining-task"
						>
							{this.noTask()}
						</div>
					)}
				</div>
				<div className="btn-row border-top">
					<select
						style={{ marginRight: "1vw" }}
						name="select_severity"
						id="severity-select"
						value={this.state.select_severity}
						onChange={(event) => {
							this.handleInteger(event);
						}}
					>
						<option value={-1}>All serverities</option>
						{this.state.severities &&
							this.state.severities.map((level, index) => (
								<option
									id={`severity-option-${index + 1}`}
									value={level.id}
								>
									{level.severity}
								</option>
							))}
					</select>

					<button
						id="acknowledge-btn"
						className="fit-btn submit"
						onClick={() => {
							this.submitAcknowledge();
						}}
					>
						<span>Acknowledge All</span>
					</button>
				</div>
			</div>
		);
	}
}

export default Notification;
