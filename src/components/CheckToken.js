import React, {useState } from 'react';
import {  Route  } from "react-router-dom";
import Home from "./Home";
import Login from "./Login";

export default function CheckToken() {

    const [userData, setUserData] = useState(
        localStorage.getItem('userData') || ''
    );
    // console.log(userData);
    if(userData){
        <Route exact path='/' component={Home} />
    }else{
        return <Route exact path='/Login' component={Login} />
    }


}
