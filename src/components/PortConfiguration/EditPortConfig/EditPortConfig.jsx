import React, { Component } from 'react'
import './EditPortConfig.scss';
import * as PortService from "./../../../services/PortService";
import * as InformUser from "./../../../shared/InformUser";
import { onUserResponse } from "./../../../shared/UserResponse";
export class EditPortConfig extends Component {

    token = JSON.parse(localStorage.getItem('userData')).token;
    nameIsSaved = false;
    reservedIsSaved = false;

    constructor(props) {
        super(props);
        this.state = {
            display_no: this.props.data.display_no,
            description: this.props.data.description,
            reserved: this.props.data.reserved,
            invalided:false,
        }
    }

    componentDidMount() {
        // if(this.props.data.reserved === 1){
        //     // console.log('true')
        //     this.setState({ reserved: false })
        // }else{
        //     this.setState({ reserved: true })
        // }
        // console.log(this.props.data)

    }


    handleInput = (event) => {
        this.nameIsChanged = true;
        this.setState({ [event.target.name]: event.target.value })
    }
    handleCheck = (event) => {
        this.reservedIsChanged = true;
        this.setState({ [event.target.name]: event.target.checked })
    }
    handleSelect = (event) => {
        this.reservedIsChanged = true;
        this.setState({ [event.target.name]: event.target.value })
    }
    inputValidation(event){
        event.preventDefault()
        const { data, close } = this.props;
        const originalPortAlias = this.props.data.display_no;
        const originalDescription = this.props.data.description;
        const originalReserved = this.props.data.reserved;

        this.setState({
            invalided : true
        }, ( )=>{
            // console.log( this.state.invalided )
        })
        if(this.state.display_no === ""){
            InformUser.unsuccess({ text: "Port Alias is required." })
        }
        else if(this.state.description === ""){
            InformUser.unsuccess({ text: "Port Description is required." })
        }
        else if(this.validateNoQoute(this.state.description)){
            InformUser.unsuccess({ text: "Singel quote and Double quote are not allowed in port description." })
        }
        else if( this.state.display_no < 0 || this.state.display_no > 99999 ){
            InformUser.unsuccess({ text: "Port alias contains maximum of 5 digits." })
        }
        else {
            this.submitUpdate()
        }


        
    }
    validateInput(input){

        if(this.state.invalided){

            if( input === "ALIAS_INPUT" ){
                if( this.state.display_no === "" || this.state.display_no < 0 || this.state.display_no > 99999 ) {
                    return " invalid-input "
                }
            }

            if( input === "TEXT_INPUT" ){
                if( !this.state.description || this.validateNoQoute(this.state.description) ) {
                    return " invalid-input "
                }
            }

        }
        
        
            
        
    }
    validateName(input){
        if(!input.match( /^[\w-.]{0,40}$/)){
            return true
        }
    }
    validateIpAddr(input){
        if(!input.match(/^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/gm)){
            return true
        }
    }
    validateMacAddr(input){
        if(!input.match(/[\s\S]{3,40}/)){
            return true
        }
    }
    validateNoQoute(input){
        if(!input.match(/^[^'"]*$/)){
            return true
        }
    }

    submitUpdate() {

        const { data, close } = this.props;
        const originalPortAlias = this.props.data.display_no;
        const originalDescription = this.props.data.description;
        const originalReserved = this.props.data.reserved;

        
        // var reserved = this.state.reserved;
        // if(reserved){
        //     reserved = 0;
        // }else{
        //     reserved = 1;
        // }


        if ((this.state.description !== originalDescription) || (this.state.display_no !== originalPortAlias)) {
            this.setState({ invalided:false })
            PortService.edit_port_list(data.idx, this.state.display_no, this.state.description, this.token).then(async result => {
                // console.log(result)
                if (result.status === 1) {
                    // const dialog = await onUserResponse({ text: result.msg })
                    InformUser.success({ text: result.msg })
                    this.props.refreshData()
                    this.nameIsSaved = true;
                    // this.props.close()
                } else {
                    InformUser.unsuccess({ text: result.msg })
                    this.nameIsSaved = true;
                }
                this.closeCondition()
            })
        } else {
            this.nameIsSaved = true;
        }
        // console.log(this.state.reserved)

        if (this.state.reserved !== originalReserved) {
            if (parseInt(this.state.reserved) === 1) {
                // console.log('in')
                PortService.reserve_port(data.idx, this.token).then(async result => {
                    // console.log("reserve", result)
                    if (result.status === 1) {
                        // const dialog = await onUserResponse({ text: result.msg })
                        InformUser.success({ text: result.msg })
                        this.props.refreshData()
                        this.reservedIsSaved = true;
                    } else {
                        InformUser.unsuccess({ text: result.msg })
                        this.reservedIsSaved = true;
                    }
                    this.closeCondition()
                })
            } else {
                PortService.unreserve_port(data.idx, this.token).then(async result => {
                    // console.log("unreserve", result)
                    if (result.status === 1) {
                        // const dialog = await onUserResponse({ text: result.msg })
                        InformUser.success({ text: result.msg })
                        this.props.refreshData()
                        this.reservedIsSaved = true;
                    } else {
                        InformUser.unsuccess({ text: result.msg })
                        this.reservedIsSaved = true;
                    }
                    this.closeCondition()
                })
            }
        } else {
            this.reservedIsSaved = true;
        }
        this.closeCondition()
    }
    closeCondition() {
        // console.log(this.nameIsSaved, this.reservedIsSaved)
        if (this.nameIsSaved && this.reservedIsSaved) {
            this.props.close()
        }
    }

    render() {

        const { data, close } = this.props
        // console.log(data)

        return (
            <div className="edit-port-config-container">
                <header><h2>Edit Port-{data.display_no}</h2></header>
                <form onSubmit={(event) => { this.inputValidation(event) }}>
                <div className="content">
                    <div className="input-group">
                        <div className="left">
                            <span>Port Alias :</span>
                        </div>
                        <div className="right">
                            <input id="display_no" name="display_no" value={this.state.display_no}  className={this.validateInput("ALIAS_INPUT")}  type="number" min={0} max={99999} placeholder="" onChange={this.handleInput} />
                        </div>
                    </div>
                    <div className="input-group">
                        <div className="left">
                            <span>Description :</span>
                        </div>
                        <div className="right">
                            <input id="description" maxLength="255" name="description" value={this.state.description}  className={this.validateInput("TEXT_INPUT")}  type="text" placeholder="" onChange={this.handleInput} />
                        </div>
                    </div>
                    <div className="input-group">
                        <div className="left">
                            <span>Status :</span>
                        </div>
                        <div className="right">
                            {/* <input style={{ width: "fit-content" }} id="reserved" name="reserved" value={this.state.reserved} type="checkbox" checked={this.state.reserved} onChange={this.handleCheck} /> */}
                            <select style={{ width: "10vw" }} name="reserved" id="status-select" value={this.state.reserved} onChange={this.handleSelect}>
                                <option id="status-1" value={0}>Enable</option>
                                <option id="status-2" value={1}>Reserved/Disable</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div className="btn-row">
                    <button className="btn submit" type="submit"  style={{marginRight:"0.5vw"}}> <span>Confirm</span></button>
                    <button className="btn cancel" type="button" onClick={close}><span>Cancel</span></button>
                </div>
                </form>
            </div>
        )
    }
}

export default EditPortConfig
