import React, { Component } from 'react'
import './PortConfiguration.scss'
import * as InfoService from "./../../services/InfoService";
import * as SystemService from "./../../services/SystemService";
import ReactPaginate from 'react-paginate';
import ReactTooltip from 'react-tooltip';
import { Loading } from "./../../shared/Loading";
import ClipLoader from "react-spinners/ClipLoader";
import Modal from 'react-modal';
import * as TaskService from "./../../services/TaskService";
import * as InformUser from "./../../shared/InformUser";
import { openConfirmation } from "./../../shared/Confirmation";
import { EditPortConfig } from "./EditPortConfig/EditPortConfig";
import { Option } from "./../Interconnection/Manual/Option/Option";
import Switch from "react-switch";
import * as PortService from "./../../services/PortService";


export class PortConfiguration extends Component {
    token = JSON.parse(localStorage.getItem('userData')).token;
    interval;
    editingPort;
    filterSetting;
    constructor(props) {
        super(props);
        this.state = {
            // origin: this.props.origin,
            // target: this.props.target,

            totalPort: '',
            portPerPage: 200,
            view_type: 2,

            tableData: '',
            sourceData: '',
            targetData: '',
            currentPageSource: 0,
            currentPageTarget: 0,

            taskList: '',

            totalPage: '',
            pageIndex: '',
            // sourceIndex: '',
            // targetIndex: '',

            selectedSource: '',
            selectedTarget: '',


            filterSourceUnit: null,
            filterSourceSmu: null,
            filterSourceSearch: null,
            ActualfilterSourceSearch: null,

            pageForwardIndex: 1,
            loading: false,

            loadingSource: false,
            loadingTarget: false,
            // routeSearchModal: false,

            editPortConfig: false,
            filterSourceUnitResult: '',
            filterSourceSmuResult: '',

            filterOptionModal: false,
            showFrontPanelNumber: false,

            tableView: true,
            autoMode: true,

            editedList: [],
            totalEdited: 0,

        }

    }

    componentWillMount() {
        // console.log(this.props.source,this.props.target)
    }
    componentDidMount() {

        this.setState({ loading: true }, async () => {
            // await this.getTotalPort();
            await this.getUnitList();
            await this.getPortData(true);
            // await this.getPortData(false);

            this.setState({ loading: false });
        })


    }
    componentWillUnmount() {
        if (this.interval) {
            clearInterval(this.interval)
        }
    }

    chunk = (arr, size) => arr.reduce((acc, e, i) => (i % size ? acc[acc.length - 1].push(e) : acc.push([e]), acc), []);
    async getTask() {
        await TaskService.get_tasks(this.token).then(result => {
            // console.log(result);
            if (result.status == 1) {
                this.setState({ taskList: result.data });
            } else {
                InformUser.unsuccess({ text: result.msg });
            }
        })
    }

    async getUnitList() {
        await SystemService.get_unit(this.token).then(result => {
            // console.log("yut", result)
            if (result.status === 1) {
                this.setState({ unitList: result.data })
            }
        })
    }
    // getTotalPort() {
    //     InfoService.get_total_port_count(this.token).then(result => {
    //         // console.log(result);
    //         if (result.status === 1) {

    //             const port_qty = this.state.portPerPage;
    //             const totalPage = Math.floor(result.data.port_count / port_qty)

    //             var pageIndex = [...Array(totalPage).keys()]

    //             this.setState({
    //                 totalPort: result.data.port_count,
    //                 totalPage: totalPage,
    //                 pageIndex: pageIndex,
    //             })
    //         }
    //     })
    // }
    getTotalPort(unit_id, smu_type, search, isSource) {
        // console.log(unit_id, smu_type, search,)

        InfoService.get_total_port_edit_count(unit_id, smu_type, search, this.token).then(result => {
            // console.log("count", result);
            if (result.status === 1) {

                const port_qty = this.state.portPerPage;
                var totalPage = Math.ceil(result.data.port_count / port_qty);
                if(totalPage === 0){
                    totalPage = 1;
                }

                var pageIndex = [...Array(totalPage).keys()]

                this.setState({
                    totalPort: result.data.port_count,
                    totalPage: totalPage,
                    pageIndex: pageIndex,
                })


            }
        })
    }





    async getPortData(isSource) {
        const view_type = this.state.view_type;
        const port_qty = this.state.portPerPage;

        var start_no = ((this.state.currentPageSource) * port_qty) + 1;;
        var unit_id = this.state.filterSourceUnit;
        var smu_type = this.state.filterSourceSmu;
        var search = this.state.ActualfilterSourceSearch;
        // console.log("notice", this.state.filterSourceSearch, typeof (this.state.filterSourceSearch))



        // console.log("notice", unit_id, typeof (unit_id))
        const rowAmount = 8;
        const portPerRow = port_qty / rowAmount;


        // console.log(view_type, start_no, port_qty, unit_id, smu_type, search,)
        var filterResult = this.filterResultDescription(unit_id, smu_type, isSource)

        await this.getTotalPort(unit_id, smu_type, search, isSource)



        await InfoService.get_port_panel_edit(start_no, port_qty, unit_id, smu_type, search, this.token).then(async result => {
            // console.log("filter", result);
            if (result.status === 1) {
                if (result.data) {
                    var array = await this.chunk(result.data, portPerRow);

                    // console.log(array)
                    this.setState({
                        tableData: result.data,
                        sourceData: array,
                        filterSourceResult: filterResult,
                    });

                } else { }

            }
        });
    }

    searchPortDescription = (event) => {
        event.preventDefault();
        // console.log(this.state.filterTargetSearch)
        
        this.setState({ 
            loadingSource: true, 
            ActualfilterSourceSearch: this.state.filterSourceSearch,
            currentPageSource: 0, 
        }, async ()=>{
            this.setState({sourceData: []});
            await this.getPortData(true)
            this.setState({ loadingSource: false})
        })

    }

    filterResultDescription(unit_id, smu_type, isSource) {
        // console.log("unit_id", unit_id, smu_type)
        var unit_id_text;
        var smu_type_text;

        if (!unit_id) {
            unit_id_text = "All units"
        } else {
            this.state.unitList.map(unit => {
                // console.log(unit)
                if (unit.id === parseInt(unit_id)) {
                    unit_id_text = `${unit.name}`
                }
            })
        }



        if (smu_type === 0) {
            smu_type_text = "East"
        } else if (smu_type === 1) {
            smu_type_text = "West"
        } else {
            smu_type_text = "East and West"
        }

        if (isSource) {
            this.setState({
                filterSourceUnitResult: unit_id_text,
                filterSourceSmuResult: smu_type_text
            })
        } else {
            this.setState({
                filterTargetUnitResult: unit_id_text,
                filterTargetSmuResult: smu_type_text
            })
        }


    }



    handlePage = (event, isSource) => {
        let selected = parseInt(event.target.value);
        this.setState({ [event.target.name]: selected })

        if (isSource) {
            this.setState({ currentPageSource: selected, loadingSource: true }, async () => {
                await this.getPortData(true);
                this.setState({ loadingSource: false }
                    // , () => { console.log(this.state.currentPageSource, this.state.totalPage) }
                );
            });
        } else {
            this.setState({ currentPageTarget: selected, loadingTarget: true }, async () => {
                await this.getPortData(false);
                this.setState({ loadingTarget: false });
            });
        }
    }
    forwardPage(isSource) {
        // console.log(this.state.totalPage)

        if (isSource) {
            const page = parseInt(this.state.currentPageSource)
            if (page < (this.state.totalPage - 1)) {
                var pageNext = page + 1;
                // console.log(pageNext)
                this.setState({ currentPageSource: pageNext, loadingSource: true }, async () => {
                    await this.getPortData(true);
                    this.setState({ loadingSource: false })
                });
            }

        } else {
            const page = parseInt(this.state.currentPageTarget)
            if (page < (this.state.totalPage - 1)) {
                var pageNext = page + 1;
                this.setState({ currentPageTarget: pageNext, loadingTarget: true }, async () => {
                    await this.getPortData(false);
                    this.setState({ loadingTarget: false })
                });
            }

        }
    }
    backwardPage(isSource) {
        // console.log('yut')

        if (isSource) {
            const page = parseInt(this.state.currentPageSource)
            if (page > 0) {
                var pageNext = page - 1;
                this.setState({ currentPageSource: pageNext, loadingSource: true }, async () => {
                    await this.getPortData(true);
                    this.setState({ loadingSource: false })
                });
            }

        } else {
            const page = parseInt(this.state.currentPageTarget)
            if (page > 1) {
                var pageNext = page - 1;
                this.setState({ currentPageTarget: pageNext, loadingTarget: true }, async () => {
                    await this.getPortData(false);
                    this.setState({ loadingTarget: false })
                });
            }

        }
    }
  
    editThisPort(port) {
        this.editingPort = port;
        this.setState({ editPortConfig: true })
    }

    lightStyleClasses(port, isSource) {

        var defaultStyle = " port ";
        var operation = port.port_operation_id;
        // console.log(port)
        if (isSource) {
            if (port.idx === this.state.selectedSource.idx) {
                defaultStyle = " selected port ";
            }
            if (port.smu_type === this.state.selectedTarget.smu_type) {
                defaultStyle = defaultStyle + " same-type "
            }
            if (port.reserved === 1) {
                defaultStyle += " disabled "
            }
            switch (operation) {
                case -1:
                    return defaultStyle += " connected "
                    break;
                case 0:
                    return defaultStyle + " disabled "
                    break;
                case 1:
                    return defaultStyle + " available "
                    break;
                case 2:
                    return defaultStyle + " connected "
                    // return defaultStyle + " connecting "
                    break;
                case 3:
                    return defaultStyle + " available "
                    // return defaultStyle + " disconnecting "
                    break;
                case 4:
                    return defaultStyle + " connected "
                    // return defaultStyle + " recovering "
                    break;
                case 5:
                    return defaultStyle + " locking "
                    break;
                default:
                    return defaultStyle
            }
        }
        // else {
        //     if (port.idx === this.state.selectedTarget.idx) {
        //         defaultStyle = " selected port ";
        //     }
        //     if (port.smu_type === this.state.selectedSource.smu_type) {
        //         defaultStyle = defaultStyle + " same-type "
        //     }
        //     if(port.enabled){
        //         defaultStyle += " reserved "
        //     }
        //     switch (operation) {
        //         case -1:
        //             return defaultStyle += " connected "
        //             break;
        //         case 0:
        //             return defaultStyle + " disabled "
        //             break;
        //         case 1:
        //             return defaultStyle + " available "
        //             break;
        //         case 2:
        //             return defaultStyle + " connecting "
        //             break;
        //         case 3:
        //             return defaultStyle + " disconnecting "
        //             break;
        //         case 4:
        //             return defaultStyle + " recovering "
        //             break;
        //         case 5:
        //             return defaultStyle + " locking "
        //             break;
        //         default:
        //             return defaultStyle
        //     }
        // }





    }
    openFilterOption(isSource) {

        this.filterSetting = {
            "unit": this.state.filterSourceUnit,
            "smu_type": this.state.filterSourceSmu,
            // "search": this.state.filterSourceSearch,
        }
        this.setState({ filterOptionModal: true })


    }
    closeEditPortConfig = () => {
        this.setState({ editPortConfig: false }, () => {
            this.getPortData(true)
        })
    }
    closeFilterOption = (isSource, setting) => {
        // console.log(setting)

        var type_temp;
        if (setting.smu_type) {
            type_temp = parseInt(setting.smu_type);
        } else {
            type_temp = setting.smu_type;
        }
        this.setState({
            filterSourceUnit: parseInt(setting.unit),
            filterSourceSmu: type_temp,
            currentPageSource: 0,
            filterOptionModal: false,
        }, () => {
            this.setState({ loadingSource:true } , async ()=>{ 
                await this.getPortData(true);
                this.setState({ loadingSource:false })
             })
        })

    }
    refreshPortData = () => {
        this.getPortData(true)
    }
    handleSwitch = (event) => {
        this.setState({ showFrontPanelNumber: event });
    }
    handleSwitchHidden = (event) => {
        // console.log(event)
        this.setState({ showFrontPanelNumber: event.target.checked });
    }
    handleValueInput = (event) => {

        const searchText = event.target.value;
        const searchName = event.target.name;

        this.setState({ [event.target.name]: searchText });

        if( searchText === "" && searchName === "filterSourceSearch" ){
            this.setState({ ActualfilterSourceSearch: "" },()=>{
                this.getPortData(true)
            })
        }
    }
    countEdited() {
        let editedList = [...this.state.editedList];
        var total = 0;
        editedList.map(port => {
            if (port) {
                total += 1;
            }
        })
        return total;
    }
    storeEditedList(event, index) {
        let editedList = [...this.state.editedList];
        let tableData = [...this.state.tableData];

        var input_name = event.target.name;

        tableData[index][`${input_name}`] = event.target.value;
        editedList[index] = tableData[index];
        // console.log(editedList)

        this.setState({ editedList }, () => {
            var totalEdited = this.countEdited();
            this.setState({ totalEdited: totalEdited })
        });
        // console.log(tableData[index], editedList)
    }
    clearEdited() {
        this.setState({ editedList: [] },()=>{
            this.setState({ loading: true }, async ()=>{
                await this.getPortData(true)
                this.setState({ loading:false })
            })
            
        })
    }
    async inputValidation(){
        let editedList = this.state.editedList;
        var foundInvalid = false;
        // console.log(editedList)
        await editedList.map(async port => {
            // console.log(port)
            if(port){
                if(port.display_no === "" || this.validateNoQoute(port.description) || port.display_no < 0 || port.display_no > 99999  ){
                    foundInvalid = true;
                }
            }
            
        })

        if( foundInvalid ){
            InformUser.unsuccess({ text: "Incorrect update information" })
        }
        if( !foundInvalid ){
            this.saveEdited()
        }
    }

    async saveEdited() {
        let editedList = this.state.editedList;
        let errorOccur = 0;

        var required = true;
        editedList.map(port => {
            if (port && (!port['display_no'] || !port['description'])) {
                required = false;
            }
        })
        if (!required) {
            InformUser.unsuccess({ text: `Both port alias and description is required. Please try again.` })
        } else {

            const dialog = await openConfirmation({
                title: "Save Port Configuration",
                text: `You have initiated port configuration of ${this.state.totalEdited} port(s). Are you sure you want to save the new configuation?`
            })
            if (dialog) {
                this.setState({ loading: true }, async () => {
                    // console.log(editedList)
                    await editedList.map(async port => {
                        // console.log(port)
                        if (port) {
                            await PortService.edit_port_list(port.idx, port.display_no, port.description, this.token).then(result => {
                                // console.log(result)
                                if (result.status === 1) {
                                    // const dialog = await onUserResponse({ text: result.msg })
                                    // InformUser.success({ text: result.msg })
                                    // this.props.refreshData()
                                    // this.nameIsSaved = true;
                                    // this.props.close()
                                } else {
                                    // InformUser.unsuccess({ text: result.msg })
                                    // this.nameIsSaved = true;
                                    errorOccur += 1;
                                }
                            })
                        }

                    })
                    await this.getPortData(true)
                    await this.setState({ editedList: [] })
                    if (errorOccur > 0) {
                        InformUser.unsuccess({ text: `${errorOccur} configuration(s) was failed. Port alias already exists` })
                    } else {
                        InformUser.success({ text: `Successfully saved ${this.state.totalEdited} configuration(s).` })
                    }




                    this.setState({ loading: false }, () => { 
                        // console.log('data') 
                    })
                })
            }


        }
    }
    editedStyles(port, input) {
        // console.log(port)
        var index = port.idx;
        var classes = "";
        let editedList = [...this.state.editedList];
        editedList.map(edited => {
            if (edited && (edited.idx === index)) {
                classes = classes + " input-edited ";
            }
        })
        if( input === "ALIAS_INPUT" ){
            if( port.display_no === "" || port.display_no < 0 || port.display_no > 99999 ){
                classes = classes + " blank-input ";
            }
        }
        if( input === "TEXT_INPUT" ){
            if( !port.description || this.validateNoQoute(port.description) ){
                classes = classes + " blank-input ";
            }
        }
        

        return classes;

    }

    validateNoQoute(input){
        
        if(!input.match(/^[^'"\\]*$/)){
            return true
        }
    }


    toggleMode(isAuto) {
        if ((isAuto && !this.state.tableView) || (!isAuto && this.state.tableView)) {
            if( !this.state.tableView && (this.state.editedList.length > 0) ){
                this.clearEdited();
            }
            this.setState({ 
                tableView: !this.state.tableView,
                showFrontPanelNumber:  false,
            })
        }
    }

    tooltipDescription(port) {

        var description = `Description:  ${port.description}`;
        
        description = description + `<br/>Unit:  ${port.unit_name} ( ${port.smu_type_desc} )`;
        description = description + `<br/>Connected Count:  ${port.connected_count}`;

        if (port.reserved === 1) {
            description = description + `<hr/>Reserved by ${port.reserved_change_by}`;
        }

        if (port.port_operation_id === -1 && (port.connected_task_desc)) {
            if (port.connected_task_desc) {
                description = description + `<hr/>Connected to ${port.connected_name}<br/>Task: ${port.connected_task_desc}`;

            } else {
                description = description + `<br/>Connected to ${port.connected_name}`;

            }
        }
        return `<span>${description}</span>`;
    }

    render() {
        // console.log(this.state.editedList)
        const { sourceData, targetData } = this.state
        return (
            <div className="port-config-container">
                <Loading open={this.state.loading} />
                <Modal
                    isOpen={this.state.editPortConfig}
                    className="edit-port-config-body"
                    overlayClassName="default-overlay"
                    onRequestClose={this.closeEditPortConfig}
                // closeTimeoutMS={500}
                >
                    <EditPortConfig data={this.editingPort} close={this.closeEditPortConfig} refreshData={this.refreshPortData} />

                </Modal>
                <Modal
                    isOpen={this.state.filterOptionModal}
                    className="filter-option-body"
                    overlayClassName="filter-option-overlay"
                    onRequestClose={() => { this.closeFilterOption(true, this.filterSetting) }}
                // closeTimeoutMS={500}
                >
                    <Option isSource={true} header={"Port"} filterSetting={this.filterSetting} close={this.closeFilterOption} />

                </Modal>
                <header className="h1"><h1>MANAGEMENT: PORT CONFIGURATION</h1></header>
                {/* <button onClick={this.props.close} className="close fit-btn"><i class="fas fa-window-close"></i></button> */}
                <div className="content-container">
                    <div className="left-content">
                        <div className="section-container">
                            <div className="lr-container">
                                <div className="left">
                                    {/* {
                                        !this.state.tableView && <button className="btn btn-shade submit view" onClick={() => { this.setState({ tableView: true, showFrontPanelNumber: false }) }}> <span>Port Table</span></button>
                                    }
                                    {
                                        this.state.tableView && <button className="btn btn-shade submit view" onClick={() => { this.setState({ tableView: false, showFrontPanelNumber: false }) }}> <span>Port List</span></button>
                                    } */}


                                    <div className="toggle-btn">
                                        <button id="port-panel-btn" className={this.state.tableView ? "auto selected-mode" : "auto"} onClick={() => { this.toggleMode(true) }} ><h2 >Port Panel</h2></button>
                                        <button id="port-table-btn" className={!this.state.tableView ? "manual selected-mode" : "manual"} onClick={() => { this.toggleMode(false) }} ><h2>Port Table</h2></button>
                                    </div>




                                    <span>Page:</span>
                                    <button id="previous-btn" onClick={() => { this.backwardPage(true) }} className={(this.state.currentPageSource === 0 ? " btn-disable " : " ") + "fit-btn submit next"}>
                                        <span>Previous</span>
                                    </button>

                                    <select id="selection-page" name="currentPageSource" id="page" value={this.state.currentPageSource} onChange={(event) => { this.handlePage(event, true) }}>
                                        {
                                            this.state.pageIndex && this.state.pageIndex.map((page, index) =>
                                                <option id={`page-${index+1}`} className={page === this.state.currentPageSource ? "selected-page" : ""} value={page}>{page + 1}</option>
                                            )
                                        }
                                    </select>
                                    <button id="next-btn" onClick={() => { this.forwardPage(true) }} className={(this.state.currentPageSource === (this.state.totalPage - 1) ? " btn-disable " : " ") + "fit-btn submit next"}>
                                        <span>Next</span>
                                        {/* <input type="number" id="page-forward-index" name="pageForwardIndex" value={this.state.pageForwardIndex} onChange={this.handlePageChange}/> */}
                                    </button>
                                    <ClipLoader color="#B3B3B3" loading={this.state.loadingSource} size={20} />
                                </div>
                                <div className="right">
                                    {
                                        this.state.tableView && <div className="row-center"> <span style={{ marginRight: "0.5vw" }}>Front Panel View: </span> <Switch
                                        id="normal-switch"
                                            onChange={this.handleSwitch}
                                            uncheckedIcon={false}
                                            checkedIcon={false}
                                            // height={20}
                                            // width={32}
                                            className={(this.state.showFrontPanelNumber ? " checked " : " uncheck ") + " toggle-switch "}
                                            checked={this.state.showFrontPanelNumber} />
                                            <input type="checkbox" name="showFrontPanelNumber" id="hidden-switch" style={{position:"absolute",opacity:"0"}} checked={ this.state.showFrontPanelNumber } onChange={this.handleSwitchHidden} />
                                             </div>

                                    }
                                    {
                                        !this.state.tableView && <div className="row-center active"> <span style={{ marginRight: "0.5vw" }}>Enable Multiple Editing: </span> <Switch
                                            onChange={this.handleSwitch}
                                            uncheckedIcon={false}
                                            checkedIcon={false}
                                            className={(this.state.showFrontPanelNumber ? " checked " : " uncheck ") + " toggle-switch "}
                                            checked={this.state.showFrontPanelNumber} />
                                            <input type="checkbox" name="showFrontPanelNumber" id="hidden-switch" style={{position:"absolute",opacity:"0"}} checked={ this.state.showFrontPanelNumber } onChange={this.handleSwitchHidden} /> </div>

                                    }
                                    <button id="filter-btn" className="sm-btn submit filter" onClick={() => { this.openFilterOption(true) }}> <span> Filter: <span id="filter-unit" className="result">{this.state.filterSourceUnitResult}</span> <span id="filter-smu" className="result">{this.state.filterSourceSmuResult}</span> </span> </button>

                                    <form onSubmit={(event) => { this.searchPortDescription(event, true) }} style={{ marginRight: "1vw" }}>
                                        <ReactTooltip id='searchSource' place="top" className="noteClass defaultTooltip" arrowColor="#404040"  effect="solid"  />
                                        <input 
                                            id="search-input" 
                                            data-for="searchSource" 
                                            data-tip="Port index, port description, and port alias" type="text" 
                                            maxLength={20} 
                                            className="search-input" 
                                            placeholder="Search" 
                                            name="filterSourceSearch" 
                                            value={this.state.filterSourceSearch} 
                                            onChange={this.handleValueInput} />
                                        <button id="search-btn" className="fit-btn submit search-btn" type="submit"><i class="fas fa-search"></i></button>
                                    </form>
                                </div>
                            </div>
                            <div className="box">
                                {
                                    (this.state.tableView && sourceData) && sourceData.map((row, index )=>
                                        <div key={`row-${index+1}`}  id={`row-${index+1}`} className="row">
                                            <ReactTooltip id='portTooltip'  place="top" html={true} className="portClass defaultTooltip" arrowColor="#404040" effect="solid" />
                                            {
                                                row.map((port, index) =>
                                                    <div
                                                        // data-html="true"
                                                        // data-tip={`Port-${port.idx}  ${port.unit_name} - ${port.smu_type_desc}`}
                                                        id={`port-no-${index+1}`}
                                                        key={`port-no-${index+1}`}
                                                        data-for='portTooltip'
                                                        data-tip={this.tooltipDescription(port)}
                                                        onClick={() => { this.editThisPort(port) }}
                                                        className={this.lightStyleClasses(port, true)}
                                                    // className={this.state.selectedSource.idx === port.idx ? "selected port" : "port"}
                                                    >
                                                        {
                                                            !this.state.showFrontPanelNumber && <span>{port.display_no}</span>
                                                        }
                                                        {
                                                            this.state.showFrontPanelNumber && <span>{port.front_no}</span>
                                                        }
                                                        <div className="light-status"></div>

                                                    </div>

                                                )
                                            }
                                        </div>
                                    )
                                }
                                {
                                    (!this.state.tableView) && <div className="table-size">
                                        <table>
                                            {/* <thead> */}
                                            <tr>
                                                <th style={{ width: "10%" }}><span>Cluster Port Index</span></th>
                                                <th style={{ width: "10%" }}><span>Cluster Port No.</span></th>
                                                <th style={{ width: "10%" }}><span>Interconnection Index Pair</span></th>
                                                <th style={{ width: "15%" }}><span>Unit Name</span></th>
                                                <th style={{ width: "10%" }}><span>Unit Port No.</span></th>
                                                <th style={{ width: "10%" }}><span>Connected Count</span></th>
                                                <th style={{ width: "10%" }}><span>Status</span></th>
                                                <th style={{ width: "10%" }}><span>Port Alias</span></th>
                                                <th style={{ width: "15%" }}><span>Port Description</span></th>
                                            </tr>
                                            {/* </thead> */}
                                            {/* <tbody> */}
                                            {
                                                this.state.tableData && this.state.tableData.map((port, index) =>
                                                    <tr
                                                        id={`port-list-${index+1}`}
                                                        key={`port-list-${index+1}`}
                                                    >
                                                        <td id={`port-cluster-index-${index+1}`} className="text-center"><span>{port.idx}</span></td>
                                                        <td id={`port-cluster-no-${index+1}`}  className="text-center">
                                                            {
                                                                port.cluster_port_no && <span>{port.cluster_port_no}</span>
                                                            }
                                                            {
                                                                !port.cluster_port_no && <span>-</span>
                                                            }
                                                        </td>
                                                        <td id={`port-intercon-pair-${index+1}`}  className="text-center">
                                                            {
                                                                port.interconnect_fiber && <span> {port.interconnect_fiber} </span>
                                                            }
                                                            {
                                                                !port.interconnect_fiber && <span>-</span>
                                                            }
                                                        </td>
                                                        <td id={`unit-name-${index+1}`}  className="text-center"><span>{port.unit_name}</span></td>
                                                        <td id={`port-unit-index-${index+1}`}  className="text-center"><span>{port.front_no}</span></td>
                                                        <td id={`port-connection-count-${index+1}`} className="text-center"><span>{port.connected_count}</span></td>
                                                        <td id={`port-status-${index+1}`} className="text-center"><span>{port.status_name}</span></td>
                                                        {
                                                            !this.state.showFrontPanelNumber && <td id={`port-alias-${index+1}`} className="text-center"><span>{port.display_no}</span></td>
                                                        }
                                                        {
                                                            this.state.showFrontPanelNumber && <td id={`port-alias-${index+1}`} className="text-center" >
                                                                <input type="number" required
                                                                    className={(this.editedStyles(port, "ALIAS_INPUT") + " port-alias ")} required
                                                                    id={`edited-alias-${index+1}`} name={`display_no`} value={port.display_no} placeholder={port.display_no} min={0} max={99999} onChange={(event) => { this.storeEditedList(event, index) }} />
                                                            </td>
                                                        }

                                                        {
                                                            !this.state.showFrontPanelNumber && <td id={`description-${index+1}`}  className="text-center"><span>{port.description}</span></td>
                                                        }
                                                        {
                                                            this.state.showFrontPanelNumber && <td id={`description-${index+1}`} className="text-center" >
                                                                <input type="text" required
                                                                    className={(this.editedStyles(port, "TEXT_INPUT") + " port-desc ")} maxLength="255" 
                                                                    id={`edited-description-${index+1}`} name={`description`} value={port.description ? port.description : ""} placeholder={port.description} onChange={(event) => { this.storeEditedList(event, index) }} />
                                                            </td>
                                                        }

                                                    </tr>
                                                )
                                            }
                                            {/* </tbody> */}
                                        </table>
                                    </div>


                                }
                                {
                                    (sourceData.length === 0 ) && <div id="no-data-task" className="no-remaining-task" style={{height:"10vw"}}><span className="camera-offline">NO UNIT AVAILABLE</span></div>
                                }
                                {
                                    (this.state.editedList.length > 0) && <div className="btn-row">

                                        <button id="submit-btn" 
                                        className="btn submit" 
                                        style={{marginRight:"0.5vw"}} 
                                        onClick={() => { this.inputValidation() }} >
                                            <span>Save ({this.state.totalEdited})</span>
                                        </button>
                                        <button id="cancel-btn" className="btn cancel" onClick={() => { this.clearEdited() }}><span>Cancel</span></button>
                                    </div>
                                }
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        )
    }
}

export default PortConfiguration
