import React, { Component } from 'react'
import * as TaskService from "./../../services/TaskService";
import * as InformUser from "../../shared/InformUser";
import { openConfirmation, openConfirmationRemark } from "./../../shared/Confirmation";
import './Disconnection.scss'
import Button from "../layouts/Button";
import Modal from 'react-modal';
import { ConnectionTable } from "./ConnectionTable";
import { ConfirmDisconnection } from "./ConfirmDisconection/ConfirmDisconnection";
import moment from 'moment';
import ReactTooltip from "react-tooltip";


export class Disconnection extends Component {

    token = JSON.parse(localStorage.getItem('userData')).token;
    time_offset = JSON.parse(localStorage.getItem('userData')).time_offset;
    interval;

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            taskList: '',
            deletingTasks: [],
            connectionTableDialog: false,
            connectionDetail: '',
            disconnectTask: '',

            searchDescriptionInput: '',
            searchAddByInput: '',
            searchRemarkInput: '',
            isFoundConnection: true,
            currConnectionCount: ''
        }
    }

    componentDidMount() {
        this.setState({ loading: true }, async () => {
            await this.getConnected();
            this.interval = setInterval(() => {
                this.getConnected();
            }, 2000);
            this.setState({ loading: false })
        })
    }

    componentWillUnmount(){
        if( this.interval ){ clearInterval(this.interval) }
    }

    async getConnected() {
        await TaskService.get_connected(this.token).then(async result => {
            // console.log(result);
            if (result.status == 1) {

                var task = result.data;
                if(this.state.currConnectionCount != task.length) {
                    var foundConnection = task.length > 0;
                    this.setState({isFoundConnection: foundConnection, currConnectionCount: task.length});
                }
                var timeCaledTask = await this.processColumnOfTime(task)
                this.setState({ taskList: timeCaledTask });
            } else {
                InformUser.unsuccess({ text: result.msg });
            }
        })
    }

    async processColumnOfTime(logData){
        var  logs = JSON.parse(JSON.stringify(logData));
        // var  logs = logData;
        for( var i=0; i< logs.length; i++){
            // const newAddDate = await this.calculateTime(logs[i]["add_date"])
            const newStartDate = await this.calculateTime(logs[i]["start_date"])
            const newFinishDate = await this.calculateTime(logs[i]["finish_date"])
            // logs[i]["add_date"] = newAddDate;
            logs[i]["start_date"] = newStartDate;
            logs[i]["finish_date"] = newFinishDate;
        }
        return logs;
    }
    async calculateTime(serverTime) {

        var rawServerTime = await moment(serverTime);
        // console.log(rawServerTime)
        // console.log(timeOffset)
        var offset = moment.duration(this.time_offset)
        // // var temp = moment(server_time).add(1, 'm')
        // console.log(offset)
        var currentTime =  await rawServerTime.add(offset).format("ddd YYYY-MM-DD HH:mm:ss")
        // console.log(currentTime)
        return currentTime;
        // console.log(currentTime)
    }

    // clickRowHandle(task, index) {
    //     var row = document.getElementById("task-row-" + index)
    //     var checkbox = document.getElementById("checkbox-" + index);
    //     console.log(checkbox.checked)
    //     if (!checkbox.checked) {
    //         checkbox.checked = true;
    //         row.classList.add("highlight")

    //         const deletingTasks = this.state.deletingTasks;
    //         deletingTasks.push(task);
    //         this.setState({ deletingTasks:deletingTasks })
    //     } else {
    //         checkbox.checked = false;
    //         row.classList.remove("highlight")
    //         this.removeDeletingTask(task.id);

    //     }
    //     console.log(this.state.deletingTasks)
    // }
    // removeDeletingTask(id) {
    //     this.deletingTasks.forEach((task, index) => {
    //         if (task.id === id) {
    //             this.deletingTasks.splice(index, 1);
    //         }
    //     })
    // }

    // disconnectAll(){
    //     console.log('data')
    //     const dialog = openConfirmationRemark({
    //         title: "Disconnection Confirmation?",
    //         text: "Are you sure you want to dis connect... PLease create disconnection all API"
    //     })
    // }

    openDetailTask(task) {

        // , async ()=> {
        //     TaskService.connection_detail(task.id, this.token).then( result => {
        //         console.log(task)
        //         if(result.status === 1){
        //             this.setState({ 
        //                 connectionDetail: result.data,
        //                 connectionTableDialog: true,
        //             })
        //         }else{
        //             InformUser.unsuccess({ text : "There is no connection information."})
        //         }
        //     })
        // }

    }
    async disconnectTask(task) {
        // console.log(task)

        // await this.openDetailTask(task);
        this.setState({
            disconnectingTask: task,
            connectionTableDialog: true,
            // loading:true ,
        })




        // const id = task.id;
        // var start_date = null;
        // var remark = null;
        // const dialog = await openConfirmationRemark({
        //     title: "Disconnection Confirmation?",
        //     text: `Are you sure you want to disconnect ${task.description}?`
        // })
        // // console.log(dialog.remark)
        // if (dialog) {
        //     TaskService.disconnect_connectivity(id, null, null, this.token).then(result => {
        //         console.log(result)
        //         if (result.status == 1) {
        //             InformUser.success({ text: result.msg })
        //             this.getConnected();
        //         } else {
        //             InformUser.unsuccess({ text: result.msg })
        //         }
        //     });
        // }

    }

    closeConnectionTable = async () => {
        await this.getConnected();
        this.setState({ connectionTableDialog: false },()=>{
            this.searchOperator("searchDescriptionInput", 1)
        })
    }

    onSearchInput =(event,tdNumber)=> {
        this.setState({ [event.target.name]:event.target.value },()=>{
            this.searchOperator(event.target.name, tdNumber)
        })
        
    }
    searchOperator(inputName, tdNumber) {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById(inputName);
        filter = input.value.toUpperCase();
        table = document.getElementById("sourceTable");
        tr = table.getElementsByTagName("tr");
        let checkFound = false;
        for (i = 2; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[tdNumber];
          if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
              checkFound = true;
            } else {
              tr[i].style.display = "none";
            }
          }       
        }
        this.setState({isFoundConnection: checkFound})
      }

    render() {
        return (
            <div className="disconnection-page">
                {/* <Modal
                    isOpen={this.state.connectionTableDialog}
                    className="disconnection-table-body"
                    overlayClassName="default-overlay"
                    onRequestClose={this.closeConnectionTable}
                    // closeTimeoutMS={500}
                    >
                    <ConnectionTable data={this.state.connectionDetail} task={this.state.disconnectingTask} close={this.closeConnectionTable} />

                </Modal> */}
                <Modal
                    isOpen={this.state.connectionTableDialog}
                    className="confirm-con-dis-body"
                    overlayClassName="default-overlay"
                    onRequestClose={this.closeConnectionTable}
                // closeTimeoutMS={500}
                >
                    {/* data={this.state.connectionDetail} */}
                    <ConfirmDisconnection task={this.state.disconnectingTask} close={this.closeConnectionTable} />

                </Modal>
                <header className="h1">
                    <h1>CONNECTIVITY: DISCONNECTION</h1>
                </header>

                <div className="content">
                    <div className="table-container">

                    
                    <div className="table-size">
                        {/* <div className="btn-row">
                            {
                                (this.state.deletingTasks.length > 0) && <button style={{marginRight: "15px"}}>Remove selected</button>
                            }
                            <button className="btn submit" onClick={this.disconnectAll.bind(this)}>Disconnect All</button>


                        </div> */}
                        
                        <table id="sourceTable">
                            <tr className="tr-head">
                                {/* <th rowSpan="2" > <span></span> </th> */}
                                <th style={{width:"10%"}}> <span>Connection No.</span> </th>
                                {/* <th colSpan="2" > <span>Source</span> </th> */}
                                {/* <th colSpan="2" > <span>Target</span> </th> */}
                                {/* <th rowSpan="2" > <span>Operation</span> </th> */}
                                <th style={{width:"20%"}}> <span>Description</span> </th>
                                <th style={{width:"10%"}}><span>Add By</span></th>
                                <th style={{width:"15%"}}> <span>Start Date</span> </th>
                                <th style={{width:"15%"}}> <span>Finish Date</span> </th>
                                <th style={{width:"20%"}}> <span>Remark</span> </th>
                                <th style={{width:"10%"}}> <span>Action</span> </th>
                            </tr>
                            {/* <tr className="tr-head">
                                <th className="second-row" > <span>Port</span> </th>
                                <th className="second-row"> <span>Description</span> </th>
                                <th className="second-row"> <span>Port</span> </th>
                                <th className="second-row"> <span>Description</span> </th>
                            </tr> */}
                            
                            <tr className="no-hovering">
                                       
                                        <td  className="text-center" > </td>
                                        <td style={{width:"20%"}} className="text-center" >
                                            <input id="searchDescriptionInput" type="text" 
                                            maxLength={20} placeholder="Search" name="searchDescriptionInput" value={this.state.searchDescriptionInput} onChange={(event)=>{this.onSearchInput(event,1)}}  /></td>
                                        <td style={{width:"10%"}} className="text-center" >
                                            <input id="searchAddByInput" type="text" maxLength={20}  placeholder="Search" name="searchAddByInput" value={this.state.searchAddByInput} onChange={(event)=>{this.onSearchInput(event,2)}}  /> </td>
                                        <td className="text-center" ></td>
                                        <td className="text-center" ></td>
                                        <td style={{width:"20%"}} className="text-center" >
                                            <input id="searchRemarkInput" type="text" maxLength={20}  placeholder="Search" name="searchRemarkInput" value={this.state.searchRemarkInput} onChange={(event)=>{this.onSearchInput(event,5)}}  /> </td>
                                        <td className="text-center action-row"></td>
                                    </tr>
                                    {
                                        (this.state.taskList.length > 0) && <ReactTooltip id={`remark-tooltip`} html={true} className="remarkClass defaultTooltip" place="top" arrowColor="#404040"  effect="solid"
                                        />
                                    }
                            {
                                this.state.taskList && this.state.taskList.map((task, index) =>
                                    <tr
                                    key={`connection-${index+1}`}
                                    id={`connection-${index+1}`}
                                    // onClick={() => { this.selectPort(port, true) }}
                                    // className={this.state.sourcePort.port_no == port.port_no ? "selected" : (this.state.targetPort.port_no == port.port_no ? "selected" : "")}
                                    // id={"task-row-" + index}
                                    // onClick={() => { this.clickRowHandle(task, index) }}
                                    >
                                        {/* <td className="text-center checkbox-td">
                                            <input id={"checkbox-" + index} type="checkbox" onChange={ (e)=> {pre}} />
                                        </td> */}
                                        <td id={`no-${index+1}`} style={{width:"10%"}}  className="text-center" ><span>{task.no}</span> </td>
                                        <td id={`description-${index+1}`}  style={{width:"20%"}}  className="text-center" ><span>{task.description}</span> </td>
                                        <td id={`add-by-${index+1}`}  style={{width:"10%"}} className="text-center" ><span>{task.add_by}</span> </td>
                                        {/* <td className="text-center" ><span>{task.port1_description}</span> </td> */}
                                        {/* <td className="text-center" ><span>{task.port_no2}</span> </td> */}
                                        {/* <td className="text-center" ><span>{task.port2_description}</span> </td> */}
                                        {/* <td className="text-center" ><span>12</span> </td> */}
                                        <td id={`start-date-${index+1}`}  style={{width:"15%"}}  className="text-center" ><span>{task.start_date}</span> </td>
                                        <td id={`finish-date-${index+1}`}  className="text-center" ><span>{task.finish_date}</span> </td>
                                        {/* <td className="text-center" ><span>{port.smu_type_desc}</span> </td> */}
                                        <td id={`remark-${index+1}`}  style={{width:"20%"}} className="text-center" >
                                            <span 
                                                style={{width:"13vw"}} 
                                                className="ellipsis-text"
                                                data-for="remark-tooltip"
                                                data-tip={ task.remark.length > 36 ? `<span>${task.remark}</span>` : ""} 
                                            >{task.remark}</span> 
                                        </td>
                                        <td id={`action-${index+1}`}   className="text-center action-row">
                                            <button  id={`disconnect-${index+1}`}  onClick={() => { this.disconnectTask(task) }} className="sm-btn submit"><span>Disconnect</span></button>
                                        </td>
                                    </tr>
                                )
                            }

                        </table>
                        {
                            !this.state.isFoundConnection &&
                            <div className='no-connection'>
                                <span>NO CONNECTION FOUND</span>
                            </div>
                        }
                        
                        {/* {
                                        (this.state.taskList.length === 0) && <div id="no-data-task" className="no-remaining-task" style={{height:"7vw"}}><span className="camera-offline">NO CONNECTION</span></div>
                                    } */}
                    </div>

                    </div>
                </div>

            </div>
        )
    }
}

export default Disconnection
