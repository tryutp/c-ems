import React, { Component } from 'react'
import './ConnectionTable.scss'
import * as TaskService from "./../../services/TaskService";
import * as InformUser from "../../shared/InformUser";
import { openConfirmation, openConfirmationRemark } from "./../../shared/Confirmation";


export class ConnectionTable extends Component {
    token = JSON.parse(localStorage.getItem('userData')).token;

    constructor(props) {
        super(props);
        this.state = {

        }
    }

    async disconnectAll() {

        const { task } = this.props;
        // console.log(task)

        const id = task.id;
        var start_date = null;
        var remark = null;
        const dialog = await openConfirmationRemark({
            title: "Disconnection Confirmation?",
            text: `Are you sure you want to disconnect ${task.description}?`
        })
        // console.log(dialog.remark)
        if (dialog) {
            TaskService.disconnect_connectivity(id, null, dialog.remark, this.token).then(result => {
                // console.log(result)
                if (result.status == 1) {
                    InformUser.success({ text: result.msg })
                    this.props.close()
                } else {
                    InformUser.unsuccess({ text: result.msg })
                }
            });
        }


    }


    render() {
        // console.log(this.props.task)
        const { data } = this.props
        return (
            <div className="connection-table-container">
                <button onClick={this.props.close} style={{ fontSize: "1vw" }} className="close fit-btn"><i class="fas fa-window-close"></i></button>
                <header className="shift-down"><h2>Connection Details</h2></header>
                <div className="content">
                    <div className="table-size">
                        <table>
                            <tr className="tr-head">
                                <th><span>Connection No.</span></th>
                                <th><span>Source Port</span></th>
                                <th><span>Source Fiber</span></th>
                                <th><span>Target Port</span></th>
                                <th><span>Target Fiber</span></th>
                                <th><span>Operation Date</span></th>
                            </tr>
                            {
                                data && data.map(connection =>
                                    <tr>
                                        <td className="text-center"><span>{connection.no}</span></td>
                                        <td className="text-center"><span>{connection.port_no1}</span></td>
                                        <td className="text-center"><span>{connection.fiber_no1}</span></td>
                                        <td className="text-center"><span>{connection.port_no2}</span></td>
                                        <td className="text-center"><span>{connection.fiber_no2}</span></td>
                                        <td className="text-center"><span>{connection.connected_date}</span></td>
                                    </tr>





                                )
                            }
                        </table>
                    </div>
                    <header className="fit-parent">
                        <div className="btn-row" style={{marginTop:"1vw"}}>
                            <button onClick={() => { this.disconnectAll() }} className="submit btn">Disconnect All</button>
                        </div>
                    </header>
                </div>
            </div>
        )
    }
}

export default ConnectionTable
