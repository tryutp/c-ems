import React, { Component } from "react";
import "./Connectivity.scss";
import cytoscape from "cytoscape";
import Modal from "react-modal";

import { Loading } from "../../../shared/Loading";

import * as SystemService from "../../../services/SystemService";
import * as InformUser from "../../../shared/InformUser";
import { openConfirmation } from "../../../shared/Confirmation";
import * as TaskService from "../../../services/TaskService";
// import { Connectivity } from "./Connectivity";
import { ElementVisual } from "./../../ElementVisual/ElementVisual";

export class ConfirmDisconnection extends Component {
	token = JSON.parse(localStorage.getItem("userData")).token;
	cytoRoute;
	cy;
	graphStyle = cytoscape
		.stylesheet()
		.selector("edge")
		.css({
			width: "1",
			// 'curve-style': 'bezier',
			// 'curve-style': 'segments',
			// 'curve-style': 'unbundled-bezier',
			// 'control-point-step-size': '100px',
			// 'line-fill': 'linear-gradient',
			// 'line-gradient-stop-colors': 'data(colorCode)',
			// 'line-gradient-stop-positions': ['0%', '50%', '50%', '100%'],
			// "loop-direction": "data(direction)",
			// "loop-sweep": "70deg",
			// 'line-color': 'data(colorCod)',
			// 'name': 'data(name)',
			"line-color": "#282828",
			// 'font-size': '12px',
			// 'font-family': 'helvetica, arial, verdana, sans-serif',
			// 'font-weight': 300,
			// 'text-outline-width': 2,
			// 'text-outline-color': 'black',
			// 'control-point-distance': 100,
			// 'target-arrow-shape': 'data(arrow_target)',
			// 'source-arrow-shape': 'data(arrow_source)',
			// 'target-arrow-shape': 'circle',
			// 'source-arrow-shape': 'circle',
			// 'source-arrow-color': 'data(sourceColor)',
			// 'target-arrow-color': 'data(targetColor)',
		})

		.selector("node")
		.css({
			shape: "circle",
			width: "40",
			height: "40",
			"background-color": "#282828",

			// 'background-clip': 'none',
			// 'background-fit': 'contain',
			// 'background-opacity': '0',

			content: "data(name)",
			"text-valign": "center",
			"text-outline-width": 1,
			"text-outline-color": "#282828",
			color: "#B3B3B3",
			"font-size": "12",
		})
		.selector(":parent")
		.style({
			content: "",
			"text-valign": "top",
			"text-halign": "center",
			border: "2px solid",
			"border-color": "#282828",
			shape: "roundrectangle",
			"text-outline-width": 1.5,
			"text-outline-color": "#282828",
			color: "#B3B3B3",
			// 'background-color': 'red',
			"background-opacity": "0",
		})
		.selector(".hover")
		.style({
			"text-outline-width": 2.5,
			"text-outline-color": "#121212",
			color: "#B3B3B3",
			// 'background-color': '#1d2b36',
			// 'background-image': 'url("assets/Robot_img_50.png")',
			// 'object-fit': 'cover',
			// 'background-repeat': 'no-repeat',
			// 'background-position': 'center center',

			// 'font-size': 14,
			"font-weight": "600",
			// 'border-width': '0',
			"border-color": "#121212",
		});

	constructor(props) {
		super(props);
		this.state = {
			taskList: "",
			isSelectingSourcePort: true,
			sourceSystemId: 1,
			targetSystemId: 2,
			smuTypeSource: "0",
			smuTypeTarget: "1",

			tableSourceData: "",
			tableTargetData: "",
			sourceRow: "",
			targetRow: "",

			systemList: "",

			sourcePort: "",
			targetPort: "",

			remarkTextarea: "",

			routeData: "",
			horizontalLayout: "",
			loading: false,

			isSelectingFiber: false,
			fiberSourceList: "",
			fiberTargetList: "",

			displayingRoute: "",
			sourceFilterStatus: "",
			targetFilterStatus: "",
			panelView: false,
			showRouteOption: false,
			routeSearchModal: false,
		};
		// this.submitButton = React.createRef();
	}

	componentWillMount() {
		Modal.setAppElement("body");
		this.setState({ loading: true }, async () => {
			// await this.searchRoute()

			this.setState({ loading: false });
		});
	}
	componentDidMount() {
		// this.setState({ loading: true }, async () => {
		//   // await this.getSystemList();
		//   // await this.getTask();
		//   // await this.sourceFilterPort();
		//   // await this.targetFilterPort();
		//   // document.getElementById('myInput').focus()
		//   this.setState({ loading: false, panelView: true })
		// });
		// console.log(document.getElementById("submit-disconnection"))
		// this.submitButton.current.focus();
		const node = document.getElementById("disconnect-btn");
		const yut = document.getElementById("remark-textarea");
		// console.log(yut)
		yut.focus();
		// document.getElementById('remark-textarea').focus();
		node.addEventListener("keydown", ({ key }) => {
			if (key === "Enter") {
				// console.log('yut')
				this.disconnectAll();
			}
		});
	}
	componentWillUnmount() {
		// const node = document.getElementById("submit-disconnection");
	}

	async getTask() {
		await TaskService.get_tasks(this.token).then((result) => {
			// console.log(result);
			if (result.status == 1) {
				this.setState({ taskList: result.data });
			} else {
				InformUser.unsuccess({ text: result.msg });
			}
		});
	}

	async getSystemList() {
		await SystemService.get_unit(this.token).then((result) => {
			// console.log(result);
			if (result.status == 1) {
				this.setState({ systemList: result.data }, () => {
					// console.log(this.state.systemList);
				});
			} else {
				InformUser.unsuccess({ text: result.msg });
				// console.log("getSystemList", result.data);
			}
		});
	}

	searchSourceTable() {
		var input, filter, table, tr, td, cell, i, j;
		input = document.getElementById("myInput");
		filter = input.value.toUpperCase();
		// if (this.state.isSelectingSourcePort) {
		table = document.getElementById("sourceTable");
		// } else {
		// table = document.getElementById("targetTable");
		// }

		tr = table.getElementsByTagName("tr");
		for (i = 1; i < tr.length; i++) {
			// Hide the row initially.
			tr[i].style.display = "none";

			td = tr[i].getElementsByTagName("td");
			for (var j = 0; j < td.length; j++) {
				cell = tr[i].getElementsByTagName("td")[j];
				if (cell) {
					if (cell.innerHTML.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
						break;
					}
				}
			}
		}
	}
	searchTargetTable() {
		var input, filter, table, tr, td, cell, i, j;
		input = document.getElementById("myInput");
		filter = input.value.toUpperCase();
		// if (this.state.isSelectingSourcePort) {
		// table = document.getElementById("sourceTable");
		// } else {
		table = document.getElementById("targetTable");
		// }

		tr = table.getElementsByTagName("tr");
		for (i = 1; i < tr.length; i++) {
			// Hide the row initially.
			tr[i].style.display = "none";

			td = tr[i].getElementsByTagName("td");
			for (var j = 0; j < td.length; j++) {
				cell = tr[i].getElementsByTagName("td")[j];
				if (cell) {
					if (cell.innerHTML.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
						break;
					}
				}
			}
		}
	}
	selectChange = (event, isFromSource) => {
		this.setState({ [event.target.name]: event.target.value }, () => {
			if (isFromSource) {
				this.sourceFilterPort();
			} else {
				this.targetFilterPort();
			}
		});
	};

	async sourceFilterPort() {
		// console.log(this.state.sourceSystemId, this.state.smuType);
		await SystemService.get_port_filtering(
			this.state.sourceSystemId,
			this.state.smuTypeSource,
			this.token
		).then((result) => {
			// console.log(result);
			if (result.status == 1) {
				this.setState(
					{
						tableSourceData: result.data,
						sourceFilterStatus: this.filteringStatus(
							this.state.sourceSystemId,
							this.state.smuTypeSource
						),
					},
					() => {}
				);
			} else {
				InformUser.unsuccess({ text: result.msg });
			}
		});
	}

	filteringStatus() {
		if (this.state.sourceSystemId) {
		}
	}

	async targetFilterPort() {
		// console.log(this.state.targetSystemId, this.state.smuType);
		await SystemService.get_port_filtering(
			this.state.targetSystemId,
			this.state.smuTypeTarget,
			this.token
		).then((result) => {
			// console.log(result);
			if (result.status == 1) {
				this.setState({ tableTargetData: result.data }, () => {});
			} else {
				InformUser.unsuccess({ text: result.msg });
			}
		});
	}

	selectPort(port, isFromSource) {
		// console.log(port);
		const source = this.state.sourcePort;
		const target = this.state.targetPort;

		if (isFromSource) {
			if (source.port_no !== port.port_no) {
				this.setState({ sourcePort: port });
			} else {
				this.removeSelectedPort(true);
			}
		} else {
			if (target.port_no !== port.port_no) {
				this.setState({ targetPort: port });
			} else {
				this.removeSelectedPort(false);
			}
		}

		// if (port.port_no == target.port_no) {
		//   this.removeSelectedPort(false)
		// } else {
		//   if (!source) {
		//     this.setState({ sourcePort: port });
		//   } else {
		//     if (port.port_no == source.port_no) {
		//       this.removeSelectedPort(true);
		//     } else {
		//       this.setState({ targetPort: port });
		//     }
		//     if (port.port_no == target.port_no) {
		//       this.removeSelectedPort(false);
		//     }

		//   }
		// }

		// if (port.port_no != this.state.targetPort.port_no) {
		//   if (port.port_no != this.state.sourcePort.port_no) {
		//     this.setState({ sourcePort: port });
		//   } else {
		//     this.removeSelectedPort(true);
		//   }

		// } else {
		//   InformUser.unsuccess({
		//     text: "The port you are choosing is already selected to be target port. Please choose another port to create connectivity."
		//   })
		// }

		// if (port.port_no != this.state.sourcePort.port_no) {
		//   if (port.port_no != this.state.targetPort.port_no) {
		//     this.setState({ targetPort: port });
		//   } else {
		//     this.removeSelectedPort(false)
		//   }

		// } else {
		//   InformUser.unsuccess({
		//     text: "The port you are choosing is already selected to be target port. Please choose another port to create connectivity."
		//   })
		// }
	}
	removeSelectedPort(isSource) {
		if (isSource) {
			this.setState({ sourcePort: "" });
		} else {
			this.setState({ targetPort: "" });
		}
	}

	searchRoute_new = (data1, data2) => {
		// console.log(data1, data2);
		this.setState(
			{
				sourcePort: data1,
				targetPort: data2,
			},
			() => {
				this.searchRoute();
			}
		);
	};

	async searchRoute() {
		// const sourceFiber = this.state.sourcePort.port_fiber;
		// const targetFiber = this.state.targetPort.port_fiber;

		const sourceFiber = this.props.source.port_fiber;
		const targetFiber = this.props.target.port_fiber;

		// console.log(sourceFiber, targetFiber);

		await TaskService.get_route_connectivity(
			1,
			sourceFiber,
			targetFiber,
			2,
			this.token
		).then((result) => {
			// console.log("Search Route:", result);
			if (result.status == 1) {
				if (result.data !== null) {
					var data = {
						nodes: result.data.nodes,
						edges: result.data.edges,
					};
					this.setState(
						{
							routeData: data,
							horizontalLayout: result.data.horizontal,
							displayingRoute:
								"Port-" +
								this.props.source.port_no +
								" and Port-" +
								this.props.target.port_no,
							// isSelectingFiber: true,
						},
						async () => {
							await this.renderCytoscapeElement();
						}
					);
				} else {
					InformUser.unsuccess({ text: "No data received" });
				}
			} else {
				InformUser.unsuccess({ text: result.msg });
				this.props.close(false);
			}

			// this.setState({ routeData: data}, ()=> {
			//   this.renderCytoscapeElement()
			// })
		});
		// searchRoute() {

		//   const port_no1 = this.state.sourcePort.port_no;
		//   const port_no2 = this.state.targetPort.port_no;

		//   TaskService.get_route_connectivity(1, port_no1, port_no2, 2, this.token).then(result => {
		//     console.log("Search Route:", result)
		//     if (result.status == 1) {
		//       if (result.data !== null) {
		//         var data = {
		//           nodes: result.data.nodes,
		//           edges: result.data.edges,
		//         }
		//         this.setState({
		//           routeData: data,
		//           horizontalLayout: result.data.horizontal,
		//           displayingRoute: 'Route: [ Port-' + this.state.sourcePort.port_no + " to Port-" + this.state.targetPort.port_no + " ]",
		//           // isSelectingFiber: true,
		//         }, () => {
		//           this.renderCytoscapeElement()
		//         });
		//       } else {
		//         InformUser.unsuccess({ text: "No data received" })
		//       }

		//     } else {
		//       InformUser.unsuccess({ text: result.msg })
		//     }

		//     // this.setState({ routeData: data}, ()=> {
		//     //   this.renderCytoscapeElement()
		//     // })
		//   })

		// var data = {
		//   nodes: cytoRoute.nodes,
		//   edges: cytoRoute.edges,
		// }
		// this.setState({ routeData: data }, () => {
		//   console.log(this.state.routeData)
		//   this.renderCytoscapeElement()
		// })
	}

	matchFiber() {
		const port_no1 = this.state.sourcePort.port_no;
		const port_no2 = this.state.targetPort.port_no;

		TaskService.get_fiber_option(port_no1, port_no2, this.token).then(
			(result) => {
				// console.log("Fetch Fibers:", result);
				if (result.status == 1) {
					if (result.data !== null) {
						this.setState(
							{
								// routeData: data,
								// horizontalLayout: result.data.horizontal,
								// displayingRoute: 'Route: [ Port-' +this.state.sourcePort.port_no +" to Port-" +this.state.targetPort.port_no +" ]",
								fiberSourceList: result.data.port_fiber1,
								fiberTargetList: result.data.port_fiber2,
								isSelectingFiber: true,
							},
							() => {
								// this.renderCytoscapeElement()
								this.initialMatchFiber();
							}
						);
					} else {
						InformUser.unsuccess({ text: "No data received" });
					}
				} else {
					InformUser.unsuccess({ text: result.msg });
				}

				// this.setState({ routeData: data}, ()=> {
				//   this.renderCytoscapeElement()
				// })
			}
		);
	}
	initialMatchFiber() {
		// const myLine = new LeaderLine(
		//   document.getElementById('start'),
		//   document.getElementById('end')
		// );
		// console.log('yut')
		const fiber1 = this.state.fiberSourceList;
		const fiber2 = this.state.fiberTargetList;

		var fiber1Length = fiber1.filter((list) => list.status !== 0);
		var fiber2Length = fiber2.filter((list) => list.status !== 0);
		// console.log(fiber1Length, fiber2Length)
		var matchedArray = [];
		if (fiber1Length.length <= fiber2Length.length) {
			fiber1.map((fiber) => {
				if (fiber.status === 1) {
					// console.log(fiber.fiber_no);

					fiber2.map((matchedFiber) => {
						if (
							matchedFiber.port_fiber_no === fiber.port_fiber_no
						) {
							// console.log(fiber.fiber_no, matchedFiber.fiber_no);
							matchedArray.push({ fiber, matchedFiber });
						}
					});
				}
			});
			// console.log(matchedArray);
		} else {
		}
	}

	createHorizontalLayout() {
		const data = [
			["1", "3", "5"],
			["2", "4", "6"],
		];
		const horizontalLayout = this.state.horizontalLayout;
		var horizonFormat = [];

		// horizonData.push({ node: this.cy.$id(node) })
		horizontalLayout.forEach((route) => {
			var h_route = [];
			route.forEach((node) => {
				h_route.push({ node: this.cy.$id(node) });
			});
			horizonFormat.push(h_route);
		});

		// console.log("Creating H", horizonFormat);
		return horizonFormat;
	}

	renderCytoscapeElement() {
		// const data = {
		//   nodes: [
		//     {
		//       data: {
		//         id: "1-3-5",
		//         name: "Shortest Path : 1-3-5",
		//         // size: 30
		//       }
		//     },
		//     {
		//       data: {
		//         id: "1",
		//         name: "n1",
		//         parent: "1-3-5",
		//         // size: 30
		//       }
		//     },

		//     {
		//       data: {
		//         id: "3",
		//         name: "n3",
		//         parent: "1-3-5",
		//         // size: 30
		//       }
		//     },

		//     {
		//       data: {
		//         id: "5",
		//         name: "n5",
		//         parent: "1-3-5",
		//         // size: 30
		//       }
		//     },

		//     {
		//       data: {
		//         id: "2-4-6",
		//         name: "Less Unit Consumption : 2-4-6",
		//         // size: 30
		//       }
		//     },
		//     {
		//       data: {
		//         id: "2",
		//         name: "n2",
		//         parent: "2-4-6",
		//         // size: 30
		//       }
		//     },

		//     {
		//       data: {
		//         id: "4",
		//         name: "n4",
		//         parent: "2-4-6",
		//         // size: 30
		//       }
		//     },

		//     {
		//       data: {
		//         id: "6",
		//         name: "n6",
		//         parent: "2-4-6",
		//         // size: 30
		//       }
		//     },

		//   ],
		//   edges: [

		//     {
		//       data: {
		//         source: "1",
		//         target: "3"
		//       }
		//     },

		//     {
		//       data: {
		//         source: "3",
		//         target: "5"
		//       }
		//     },

		//   ]

		this.cy = cytoscape({
			container: document.getElementById("cy"),
			style: this.graphStyle,
			zoom: 2,
			minZoom: 0.1,
			maxZoom: 2,
			// wheelSensitivity: 0.2,
			elements: this.state.routeData,
			// zoomingEnabled: false,
			userZoomingEnabled: false,
			// panningEnabled: false,
			userPanningEnabled: false,
			boxSelectionEnabled: false,
			autoungrabify: true,
			autounselectify: true,
			layout: {
				name: "cola",
				convergenceThreshold: 100, // end layout sooner, may be a bit lower quality
				// animate: false
			},
		});

		//After Cytoscape instance was declared, Call nodes's ID to create layout
		var horizontalData = this.createHorizontalLayout();
		// console.log("H Result",horizontalData)
		this.cy
			.layout({
				name: "cola",
				alignment: {
					horizontal: horizontalData,
				},
			})
			.run();

		this.cy.on("tap", ":parent", async (event) => {
			const connection_data = event.target.data().connection_data;
			// console.log(connection_data);
			const dialog = await openConfirmation({
				title: "Create Connection",
				text: "Are you sure you want choose route ...",
			});
			if (dialog) {
				// console.log('yutyutyut')
				this.choosePath(connection_data);
			}
		});
		this.cy.on("mouseover", ":parent", (event) => {
			// console.log(event.target.data())
			const target = event.target;
			target.addClass("hover");
		});
		this.cy.on("mouseout", ":parent", (event) => {
			const target = event.target;
			target.removeClass("hover");
		});
	}

	async choosePath_from_edge(connection_data) {
		// console.log("yut before");
		const port_no1 = this.props.source.port_fiber;
		const port_no2 = this.props.target.port_fiber;

		// console.log(port_no1, port_no2, connection_data);
		await TaskService.create_connectivity(
			port_no1,
			port_no2,
			connection_data,
			this.token
		).then(async (result) => {
			// console.log("yut", result);
			if (result.status == 1) {
				await InformUser.success({ text: result.msg });
				// this.getTask();
				this.props.close(true);
			} else {
				InformUser.unsuccess({ text: result.msg });
			}
		});
	}

	async disconnectAll() {
		// event.preventDefault();
		const { task } = this.props;
		// console.log("submitted", task);

		const id = task.id;
		var start_date = null;
		var remark = this.state.remarkTextarea;
		// const dialog = await openConfirmationRemark({
		//     title: "Disconnection Confirmation?",
		//     text: `Are you sure you want to disconnect ${task.description}?`
		// })
		// console.log(dialog.remark)
		// if (dialog) {

		if(!remark.match(/^[^'"]*$/)){
			InformUser.unsuccess({ text: "Qoutes are not allowed in remark." })
		}
		else {
			TaskService.disconnect_connectivity(id, null, remark, this.token).then(
				(result) => {
					// console.log(result);
					if (result.status == 1) {
						InformUser.success({ text: result.msg });
						this.props.close();
					} else {
						InformUser.unsuccess({ text: result.msg });
					}
				}
			);
		}
		
		// }
	}
	openPanelView() {
		const source = this.state.tableSourceData;

		const target = this.state.tableTargetData;

		var sourceRow = this.chunkPort(source);
		var targetRow = this.chunkPort(source);
		// console.log(sourceRow);

		// var targetRow = chunk(target, targetSize);

		// const dividedSourceSize = sourceSize

		// console.log(fourDividedSource);
		this.setState({
			sourceRow: sourceRow,
			targetRow: targetRow,
			panelView: true,
		});
	}

	chunkPort(portArray) {
		const chunk = (arr, size) =>
			arr.reduce(
				(acc, e, i) => (
					i % size ? acc[acc.length - 1].push(e) : acc.push([e]), acc
				),
				[]
			);
		var portSize = portArray.length / 3;
		var portRow = chunk(portArray, portSize);

		var fourDividedPort = [];
		portRow.map((row) => {
			var minorArray = chunk(row, 4);
			fourDividedPort.push(minorArray);
		});

		return fourDividedPort;
	}

	getPanelSelection() {
		this.setState({ panelView: false });
	}

	textareaHandle = (event) => {
		this.setState({ [event.target.name]: event.target.value });
	};

	toggleShowRouteOption() {
		// if (this.state.showRouteOption) {
		//   this.setState({ showRouteOption: false })
		// } else {
		//   this.setState({ showRouteOption: true })
		// }

		this.setState({
			routeSearchModal: true,
		});
	}

	disconnectDesc() {
		if (this.props.task.remark && this.props.task.remark !== "-") {
			return (
				"Are you sure you want to disconnect connectivity between " +
				this.props.task.description +
				" (" +
				this.props.task.remark +
				") ?"
			);
		}
		if (!this.props.task.remark || this.props.task.remark === "-") {
			return (
				"Are you sure you want to disconnect connectivity between " +
				this.props.task.description +
				"?"
			);
		}
	}
	optionDisplayNone() {
		if (this.state.showRouteOption) {
			return { display: "none" };
		} else {
			// return { opacity: "0.5" }
		}
	}
	choosePort() {
		// this.props.choosePort(this.state.selectedSource, this.state.selectedTarget);
		// this.props.close()
		this.setState({ routeSearchModal: true });
	}
	closeRouteSearch = (shouldClearSelected) => {
		this.setState({ routeSearchModal: false });
		// if (shouldClearSelected) {
		//   this.setState({
		//     selectedSource: "",
		//     selectedTarget: "",
		//     routeSearchModal: false,
		//   })

		// } else {
		//   this.setState({ routeSearchModal: false })

		// }
	};

	render() {
		// console.log(this.props);

		return (
			<div className="connectivity" id="connectivity-container">
				<Loading open={this.state.loading} />
				<Modal
					isOpen={this.state.routeSearchModal}
					className="element-visual-body"
					overlayClassName="no-overlay"
					onRequestClose={() => {
						this.closeRouteSearch(false);
					}}
					// closeTimeoutMS={500}
				>
					<ElementVisual
						element={this.props.task.first_port_idx}
						close={this.closeRouteSearch}
					/>
				</Modal>

				<h1>Create Disconnection</h1>

				{/* {
            !this.state.displayingRoute && <header><h2 className="shift-down">Route Solution</h2></header>
          }
          {
            this.state.displayingRoute && <header><h2 className="shift-down">{this.state.displayingRoute}</h2></header>
          } */}

				<hr />
				{/* <div  className="path-section">
        
      </div> */}

				<div className="paragraph">
					<span>{this.disconnectDesc()}</span>
					<textarea
						name="remarkTextarea"
						id="remark-textarea"
						cols="1"
						rows="3"
						maxlength="255"
						style={{ resize: "none" }}
						onChange={this.textareaHandle}
						placeholder="remark"
					></textarea>
				</div>
				<div className="lr-container">
					<div className="left">
						<span
							id="show-mor-detail-link"
							className="show-text"
							onClick={() => {
								this.toggleShowRouteOption();
							}}
						>
							Show more details
						</span>
					</div>

					<button
						id="disconnect-btn"
						className="btn submit"
						type="button"
						autoFocus
						style={{ marginRight: "0.5vw" }}
						// ref={this.submitButton}
						onClick={() => {
							this.disconnectAll();
						}}
					>
						<span>Submit</span>
					</button>
					<button
						id="cancel-btn"
						type="button"
						className="btn cancel"
                        
						onClick={() => {
							this.props.close();
						}}
					>
						<span>Cancel</span>
					</button>
				</div>
			</div>
		);
	}
}

export default ConfirmDisconnection;
