import React from 'react'
import './ContextMenu.scss'

function ContextMenu({style,clickSameOrigin, selectedOrigin, viewDetails, chooseOrigin, chooseTarget}) {
    return (
        <div className="contextMenu" style={style}>
            <div onClick={viewDetails} className="contextMenu--option">View Details</div>
            { !selectedOrigin && <div onClick={chooseOrigin} className="contextMenu--option">Select Origin</div>  }
            { clickSameOrigin && <div onClick={chooseOrigin} className="contextMenu--option">Unselect Origin</div> }
            { !clickSameOrigin && <div onClick={chooseOrigin} className="contextMenu--option">Replace Origin</div>  }
            {/* <div  onClick={chooseOrigin} className="contextMenu--option">Select Origin</div> */}
            {  selectedOrigin && !clickSameOrigin && <div onClick={chooseTarget} className="contextMenu--option">Select Destination</div> }
        </div>
    )
}

export default ContextMenu
