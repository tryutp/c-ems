import React, { Component } from 'react'
import { useDrag } from "react-dnd";
import './Dragbox.scss'

import { environment } from "./../../Environment";

export const DragBox = ({system, selected, moveDragbox}) => {

    // console.log(system.name ,system.model_id)
    const [{isDragging}, drag] = useDrag({
        item: {
            name: system.name,
            id: system.id,
            type: system.model_id,
        },
        end: (item, monitor) => {
            const dropResult = monitor.getDropResult();
            if(dropResult){
                // console.log(item.id +", from " +item.type +" to " +dropResult.name)
                moveDragbox(system.id,dropResult.id)
            }else{
                // console.log('its gonna be removed',system.id)
                moveDragbox(system.unit_id,null)
            }
            
            // passedData.push({id:4 , name: name })
            // if(dropResult && dropResult.name === 'Column 1'){
            //     setIsFirstColumn(true)
            // } else {
            //     setIsFirstColumn(false);
            // }
        },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
        }),
    });

    const opacity = isDragging ? 0.4 : 1;

    return (
        <div  className="fill-container">
            <div id={"dragbox-"+system.id}  ref={drag} className="dragbox" style={{opacity}}>
        <img src={ environment.api + system.img_path} alt=""/>
        <p className="dragbox-content">{system.name} {system.model_id}</p>
        
    </div>
    </div>
        
    )
}

export default DragBox

// className={ selected.name == draggedBox.name ? "dragbox highlight" : "dragbox"}