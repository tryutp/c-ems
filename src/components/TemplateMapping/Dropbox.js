import React from 'react'
import { useDrop } from 'react-dnd';
import './Dropbox.scss'
import { environment } from "../../Environment";

export const Dropbox = ({children, onCtxClickHandle, onClickHandle ,style, template, containerStyle}) => {
  
    const [{canDrop, isOver},drop] = useDrop({
        
        accept: template.model_id,
        drop: ()=> ({name: template.name, id: template.id}),
        collect: (monitor) => ({
            isOver: monitor.isOver(),
            canDrop: monitor.canDrop(),
        })
        
    });
    // console.log('options ', {canDrop, isOver});
    // console.log('options ', {accept});
    
    if(template.model_id == -1){
        // console.log(environment.api + containerStyle['background-image'])
        return (
            <div 
            id={"dragbox-"+template.id}
            ref={drop} 
            onContextMenu={onCtxClickHandle} 
            onClick={onClickHandle}
            className={ template.model_id == -1 ? "dropbox is-container" : "dropbox"}  
            style={style}>
                {/* <span>{template.name} Container</span> */}
                <img className="container-pic" src={ environment.api + containerStyle['background-image'] } alt="" srcset=""/>

            </div>
        )
    }else{
        return (
            <div 
            ref={drop} 
            onContextMenu={onCtxClickHandle}
            className={ template.model_id != -1 ? "dropbox can-drop" : "dropbox"}  
            style={style}>
                {/* <span>Drop system with {template.model_id}</span> */}
                {children}
            </div>
        )
    }
    
    
}


