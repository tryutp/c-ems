import React from 'react';
import { useDrop } from 'react-dnd';

export const Dustbin = ({children, className, title, style}) => {

    const [, drop] = useDrop({
        accept: 'Our first type',
        drop: () => ({name: title}),
    });
    return (
        <div ref={drop} style={style} className={className}>
            {title}
            {children}
        </div>
    )
}