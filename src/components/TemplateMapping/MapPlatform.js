import React, { Component, ReactDOM } from 'react'
import './MapPlatform.scss'
// import newMap from './../../assets/map.jpg'
import model1 from "../../assets/model1.png";
import model2 from "../../assets/model2.png";
import modelOther from "../../assets/model1Copy.png";
import { environment } from "./../../Environment";
import Modal from 'react-modal';
import ModeContainer from "../Interconnection/ModeContainer";
import * as TopologyService from "../../services/TopologyService";
import * as SystemService from "./../../services/SystemService";
import ContextMenu from "./Menu/ContextMenu";
import { Dropbox } from "./Dropbox";
import { DragBox } from "./Dragbox";

import TreeView from '@material-ui/lab/TreeView';
import TreeItem from '@material-ui/lab/TreeItem';
import { MinusSquare, PlusSquare, CloseSquare } from "../../shared/TreeViewIcon";
import { Loading } from "../../shared/Loading";
import * as UnitService from "../../services/UnitService";
import * as InformUser from "../../shared/InformUser";

export class MapPlatform extends Component {

    token = JSON.parse( localStorage.getItem('userData') ).token
    templateStyle = [];
    backgroundStyle = '';
    contextMenuPosition = '';
    templateHeight = '700';

    clickedTemp = '';
    
    deletedRegistered = [];
    
    

    constructor(props){
        super(props);
        this.state = {
            containerList: [
                // {name: 'Rack' },
                // {name: 'Room' },
                // {name: 'Building' },
                // {name: 'Building' },
                // {name: 'Building' },
                // {name: 'Building' },
                // {name: 'Building' },
                // {name: 'Building' },
                // {name: 'Building' },
                // {name: 'Building' },
                // {name: 'Building' },
                // {name: 'Building' },
                // {name: 'Building' },
                // {name: 'Building' },
                // {name: 'Building' },
                // {name: 'Building' },
                // {name: 'Building' },
                // {name: 'Building' },
                // {name: 'Building' },
            ],
            registeredSystems: [
                // {name: '228s-01' ,model_id: '1', picture: 'https://i2.wp.com/www.iosappszone.com/wp-content/uploads/2017/08/Emoji-ios.png?fit=256%2C256&ssl=1' ,curr_location: 'systemBox'},
            
                // {name: '1150q-01' ,model_id: '3', picture: 'https://i2.wp.com/www.iosappszone.com/wp-content/uploads/2017/08/Emoji-ios.png?fit=256%2C256&ssl=1',curr_location: 'systemBox'},
                // {name: '228s-03' ,model_id: '1', picture: 'https://i2.wp.com/www.iosappszone.com/wp-content/uploads/2017/08/Emoji-ios.png?fit=256%2C256&ssl=1',curr_location: 'systemBox'},
                // {name: '228s-03' ,model_id: '1', picture: 'https://i2.wp.com/www.iosappszone.com/wp-content/uploads/2017/08/Emoji-ios.png?fit=256%2C256&ssl=1',curr_location: 'systemBox'},
                // {name: '228s-03' ,model_id: '1', picture: 'https://i2.wp.com/www.iosappszone.com/wp-content/uploads/2017/08/Emoji-ios.png?fit=256%2C256&ssl=1',curr_location: 'systemBox'},
                // {name: '228s-03' ,model_id: '1', picture: 'https://i2.wp.com/www.iosappszone.com/wp-content/uploads/2017/08/Emoji-ios.png?fit=256%2C256&ssl=1',curr_location: 'systemBox'},
                // {name: '228s-03' ,model_id: '1', picture: 'https://i2.wp.com/www.iosappszone.com/wp-content/uploads/2017/08/Emoji-ios.png?fit=256%2C256&ssl=1',curr_location: 'systemBox'},
                // {name: '228s-03' ,model_id: '1', picture: 'https://i2.wp.com/www.iosappszone.com/wp-content/uploads/2017/08/Emoji-ios.png?fit=256%2C256&ssl=1',curr_location: 'systemBox'},
                // {name: '228s-03' ,model_id: '1', picture: 'https://i2.wp.com/www.iosappszone.com/wp-content/uploads/2017/08/Emoji-ios.png?fit=256%2C256&ssl=1',curr_location: 'systemBox'},
                // {name: '228s-03' ,model_id: '1', picture: 'https://i2.wp.com/www.iosappszone.com/wp-content/uploads/2017/08/Emoji-ios.png?fit=256%2C256&ssl=1',curr_location: 'systemBox'},
            ],
            graphData: '',
            mapPicture: {
                url: '',
                height: '',
                width: ''
                
            },
            selectedTemplate: '',
            menuPopup: false,
            clickSameOrigin: false,
            originIsSelected: false,
            selectedOrigin: false,
            originData: '',
            targetData: '',
            selectedTarget: '',
            openInterconnection: false,
            loading: false,
            containerHistory: [],
            containerData: '',

        }
    }
    
    componentDidMount(){
        window.addEventListener('resize', this.responsiveDropBoxLayout)
        window.addEventListener('click', this.menuClose);
        window.addEventListener('scroll', this.menuClose);
        document.getElementById("templateBox").addEventListener('contextmenu', function(event){ event.preventDefault(); })

        this.getData()
    }
    componentWillUnmount(){
        window.removeEventListener('resize', this.responsiveDropBoxLayout)
        window.removeEventListener('click', this.menuClose);
        window.removeEventListener('scroll', this.menuClose);
        document.getElementById("templateBox").removeEventListener('contextmenu', function(event){ event.preventDefault(); })
    }

    getData = () => {
        // SystemService.get_system(this.token).then( result => {
        //     console.log(result)
        //     if(result.status == 1){
        //         this.setState( {  } )
        //     }else{
        //         console.log(result.msg)
        //     }
        // })
        this.setState({ loading: true }, async () => {
            await this.getTemplates();
            await this.getRegisteredSystem();
            await this.getContainerTree();
            this.setState({ loading: false})
        })
        
        
    }
    async getContainerTree(){
        await TopologyService.get_containerTree(this.token).then( result => {
            
            if(result.status == 1){
                this.setState( { containerData : result.data } )
            }else{
                // console.log(result)
            }
        })
    }
    async getTemplates(){
        await TopologyService.get_containerList(this.token).then( result => {
            // console.log("get templates: ",result)
            if(result.status == 1){
                this.setState( { containerList:result.data } )
            }else{
                // console.log(result.msg)
            }
        })
    }
    async getRegisteredSystem(){
        await SystemService.get_unit(this.token).then( result => {
            
            if(result.status == 1){
                this.setState( { registeredSystems: '' } , ()=>{
                    this.setState( { registeredSystems: result.data } )
                })
            }else{
                // console.log(result.msg)
            }
        })
    }

    responsiveDropBoxLayout =() => {
        var mapWidth = document.getElementById('backgroundImage').clientWidth;
        if(mapWidth <  this.state.mapPicture.width){
            
            this.generateStyle(this.state.graphData)
        }
        
    }

    generateStyle(data){
        this.templateStyle = [];
        // var you = document.getElementById('backgroundImage').getBoundingClientRect()
        var currentHeight = document.getElementById('backgroundImage').clientHeight
        var currentWidth = document.getElementById('backgroundImage').clientWidth
        // this.setState({ test: document.getElementById('backgroundImage').clientWidth})
        // console.log(you.width, you.height)
        // console.log( document.getElementById('backgroundImage').width, document.getElementById('backgroundImage').height)
        // console.log(currentWidth, currentHeight)
        // console.log("Width, Height : " +this.state.mapPicture.width +" " +this.state.mapPicture.height)

        data.nodes.map( template => {
            

            var zoomAdjust = this.state.mapPicture.width / currentWidth;

            var aHeight = template.style.height / zoomAdjust;
            
            var aWidth = template.style.width  / zoomAdjust;
            

            var improvedTop = (template.position.y / zoomAdjust) - ( aHeight / 2 ) ;
            var improvedLeft = (template.position.x / zoomAdjust) - ( aWidth / 2 ) ;
            var aHeight = aHeight + "px"
            var aWidth = aWidth + "px"

            // console.log(zoomAdjust, improvedTop,improvedLeft, aWidth, aHeight)
            // console.log(template)
            if(template.data.model_id == "1"){
                this.templateStyle.push({
                    height:aHeight, 
                    width:aWidth, 
                    top: improvedTop,
                    left: improvedLeft,
                    backgroundImage: "url(" + model1 + ")",
                })
            }else if(template.data.model_id == "2"){
                this.templateStyle.push({
                    height:aHeight, 
                    width:aWidth, 
                    top: improvedTop,
                    left: improvedLeft,
                    backgroundImage: "url(" + model2 + ")",
                })
            }else if(template.data.model_id == "-1"){
                this.templateStyle.push({
                    height:aHeight, 
                    width:aWidth, 
                    top: improvedTop,
                    left: improvedLeft,
                    backgroundImage: "none",
                })
            }else{
                this.templateStyle.push({
                    height:aHeight, 
                    width:aWidth, 
                    top: improvedTop,
                    left: improvedLeft,
                    backgroundImage: "url(" + modelOther + ")",
                })
            }
            
            // console.log(template.position)
        })

        //the grapgData needs to be complately cleaned out before changing to the new template
        //or else there gonna have bug in the accept in dropbox 
        this.setState({ graphData : ''}, ()=>{
            // this.createContainSystemFormat(data)
            this.setState({ graphData : data}, ()=>{
                // console.log(this.state.graphData)
            })
        })
        
    }
 
    
    menuOpen (event, template) {
        // console.log(event)
        // console.log(template)
        if(template.contain_system){
            // console.log('yess')
            this.clickedTemp = template.contain_system;
        }else{
            // console.log('nah')
        }
        // this.clickedTemp = template.data.contain_system;

        if(this.clickedTemp.name == this.state.selectedOrigin.name){
            this.setState({ clickSameOrigin: true})
        }else{
            this.setState({ clickSameOrigin: false})
        }
        if(this.state.selectedOrigin){
            this.setState({ originIsSelected: true})
        }
        // console.log(this.state.selectedOrigin)
        
        this.contextMenuPosition = '';
        const clickX = event.clientX + "px";
        const clickY = event.clientY + "px";
        this.contextMenuPosition = {
            top: clickY,
            left: clickX,
            position: 'absolute',
            zIndex: '4',

        }
        if(!this.state.menuPopup){
            this.setState({ menuPopup: true} , ()=> {
            })
        }else{
            this.setState({ menuPopup: false})
        }
        
        

        // })
    }
    viewDetails = () => {
        if(this.clickedTemp){
            // console.log('details', this.clickedTemp)
        }else{
            // console.log('view');
        }

    }
    chooseOrigin = () => {
        if(this.clickedTemp){  //on cxtClick Dropbox, the data is stored in the box
            // console.log('select origin', this.clickedTemp)
            if( !this.state.selectedOrigin ){  //Initail case: there still no selected 
                this.setState({ selectedOrigin: this.clickedTemp, originIsSelected: true }) 
            }
            if( this.clickedTemp.name != this.state.selectedOrigin.name ){
                this.setState({ selectedOrigin: this.clickedTemp, originIsSelected: true }) //Replace
            }if( this.state.clickSameOrigin){
                this.setState({ selectedOrigin: '', originIsSelected: false }) //Unselect
            }
        }else{
            // console.log('no system inside container to select');
        }
        
        // if(this.clickedTemp != ''){
        //     this.setState({ selectedOrigin: this.clickedTemp })
        // }else{
        //     alert(" This container has no system in it.")
        // }

    }
    chooseTarget = () => {
        if(this.clickedTemp){
            // console.log('select target', this.clickedTemp)
            this.setState({ selectedTarget: this.clickedTemp}, ()=> { 
                // this.fetchInterconnectionData()
                this.setState({ openInterconnection:true})
            })


        }else{
            // console.log('no system inside container to select');
        }
    }

    fetchInterconnectionData(){

      
        this.setState({ loading:true }, async ()=> {

            await UnitService.get_port(this.state.selectedOrigin.unit_id,this.token).then(  result => {
                // console.log(result)
                if(result.status == 1){
                    this.setState({ originData: result.data })
                }else{
                    InformUser.unsuccess({ text: result.msg})
                }
            })
            await UnitService.get_port(this.state.selectedTarget.unit_id, this.token).then( result => {
                // console.log(result)
                if(result.status == 1){
                    this.setState({ targetData: result.data })
                }else{
                    InformUser.unsuccess({ text: result.msg})
                }
            })
            this.setState({ loading:false, openInterconnection:true })
        });
        

        // this.setState({ openInterconnection: true})
    }

    menuClose =() =>{
        if(this.state.menuPopup){
            this.setState({ menuPopup: false })
        }
    }
   
    
    goBackContainerHistory(){
        var history = this.state.containerHistory;
        history.pop();
        
        if(history.length <= 1){  //the first data is in correct format
            this.setState( { containerHistory: history } , ()=> {
                var choosingContainer = history[history.length - 1]
                // console.log(choosingContainer)
                var formattedContainer = {
                    id: "",
                    img_path: "there's no img_path from openNextContainer function",
                    name: choosingContainer.name,
                }
                this.getNextContainer(formattedContainer);
            } )
        }else{ //need to be re-formatted
            this.setState( { containerHistory: history } , ()=> {
                var choosingContainer = history[history.length - 1]
                // console.log(choosingContainer)
    
                var formattedContainer = {
                    id: choosingContainer.ref_id,
                    img_path: "there's no img_path from openNextContainer function",
                    name: choosingContainer.name,
                }
                this.getNextContainer(formattedContainer);
            } )
        }
        

        
    }
    pushContainerHistory(container,refreshContainer,event){
        // console.log( container)
        
        if(refreshContainer){
            event.preventDefault()
            var treeFormateToContainer = {
                id: container.cont_id,
                name: container.name,
                img_path: "there's no img_path from openNextContainer function",
            }
            this.setState({ containerHistory: [treeFormateToContainer] }, () => {
                // console.log(this.state.containerHistory)
                this.selectEditingContainer(treeFormateToContainer)
            })
        }else{
            var history = this.state.containerHistory;
            history.push(container);
            this.setState({ containerHistory: history }, ()=> {
                // console.log("History",this.state.containerHistory)
                var formattedContainer = {
                    id: container.ref_id,
                    img_path: "there's no img_path from openNextContainer function",
                    name: container.name,
                }
                this.getNextContainer(formattedContainer);
            } ) 
        }
        
    }
    // onTreeClicked(event,node){
    //     event.preventDefault()
    //     console.log(event,node)
    //     if(node.cont_id){
    //         var container = { id:node.cont_id , name:node.name}
    //         this.chooseTemplate(container)
    //     }else{
    //         console.log('there is no cont_id for fetching data in ChooseTemplate') 
    //     }
        
    // }
    selectEditingContainer(container){
        // console.log(container)
        this.state.selectedTemplate = container.name;
        this.getRegisteredSystem();
        this.setState({ loading:true }, async ()=> {
            await TopologyService.edit_container(container.id, this.token).then( result => {
                // console.log("choose result ",result)
                if( result.status == 1){
                    
                    var bg = {
                        url: environment.api + result.data.bg_path,
                        height: result.data.bg_height,
                        width: result.data.bg_width
                    }
                    var plotData = {
                        nodes: result.data.element
                    }
                    this.setState( { 
                        // graphData: plotData,
                        mapPicture: bg
                    }, ()=> {
                       
                        let background = new Image();
                        background.onload = () => {
                            this.generateStyle(plotData)
                        }
                        background.src = this.state.mapPicture.url;
                    })
                }else{
                    // console.log("Status -1 ",result.msg)
                }
                this.setState({ loading:false });
            })
        })
        
    }
    getNextContainer(container){
        // console.log(container)
        this.state.selectedTemplate = container.name;
        this.setState({ loading:true }, async()=> {

            await TopologyService.open_next_container(container.id, this.token).then( result => {
                // console.log("choose result ",result)
                if( result.status == 1){
                    
                    var bg = {
                        url: environment.api + result.data.bg_path,
                        height: result.data.bg_height,
                        width: result.data.bg_width
                    }
                    var plotData = {
                        nodes: result.data.element
                    }
                    this.setState( { 
                        // graphData: plotData,
                        mapPicture: bg
                    }, ()=> {
                       
                        let background = new Image();
                        background.onload = () => {
                            this.generateStyle(plotData)
                        }
                        background.src = this.state.mapPicture.url;
                    })
                }else{
                    // console.log("Status -1 ",result.msg)
                }
            })
            this.setState({ loading:false })
        })
        
    }

    // createContainSystemFormat(data){  // in this.setState for graphDaat happens only in generateStyle, moveDragbox
    //     data.nodes.map( container => {
    //         if(container.data.model_id == -1){
    //             console.log(container.data.name);
    //         }
    //     })
    // }

    moveDragbox(draggedBox_id,  dropBox_id){
        // console.log('Dragged '+draggedBox_id +' to '+dropBox_id)
        if(dropBox_id){
            TopologyService.drop_regsiteredUnit(draggedBox_id,dropBox_id, this.token).then( result => {
                if(result.status == 1){
    
                    //update new data
                    // console.log(this.state.graphData.nodes)  
                    // console.log("Drop", result)
                    var plotData = {
                        nodes: result.data.element
                    } 
                    // this.createContainSystemFormat(plotData)       
                    this.setState( { graphData: plotData } , ()=> {
                        // console.log("yut",result);
                        this.spliceUnitArr(draggedBox_id);  
                    })
                    //splice the dragbox in left panel
                }else{
                    // console.log(result.msg)
                }
            });
        }else{
            // console.log('gonna drop this')
            //draggedBox_id indicates unit_id
            

            var unit_id  = draggedBox_id;
            this.state.graphData.nodes.map( node => {
                if(node.data.contain_system && node.data.contain_system.unit_id == unit_id){

                    //at this point, there are ID, UNIT_ID, REF_ID, ELM_ID. just choose one
                    // console.log(node.data.contain_system.unit_id)
                    var restoreRegisteredID = node.data.contain_system.unit_id
                    TopologyService.remove_regsiteredUnit(node.data.id, this.token).then( result => {
                        // console.log("Remove", result)
                        if(result.status == 1){
    
                            //update new data
                            // console.log(this.state.graphData.nodes)  
                            // console.log(result)
                            var plotData = {
                                nodes: result.data.element
                            } 
                            // this.createContainSystemFormat(plotData)       
                            this.setState( { graphData: plotData } , ()=> {
                               this.restoreUnitArr(restoreRegisteredID);
                            })
                            //restore the dragbox in left panel
                        }else{
                            // console.log(result.msg)
                        }
                    })
                }
            })
            
        }
        
    }
    spliceUnitArr(id){
        var newState = this.state.registeredSystems
        // console.log("splice regis arr",newState)
        newState.forEach( (node,index) => {
            if( node.id == id){
                // console.log( index)
                this.deletedRegistered.push(node);
                newState.splice(index, 1)
            }
        });
        this.setState({registeredSystems: ""}, ()=>{
            this.setState({registeredSystems: newState})
        })
    }
    restoreUnitArr(id){
        var newState = this.state.registeredSystems
        // console.log("restore regis arr",newState)
        this.deletedRegistered.forEach( (unit,index) => {
            if( unit.id == id){
                // console.log( index)
                newState.push(unit);
            }
        });
        this.setState({registeredSystems: ""}, ()=>{
            this.setState({registeredSystems: newState})
        })
    }

    closeDialog () {
        this.setState({ openInterconnection: false, selectedOrigin: '', selectedTarget: ''})

         this.refreshDatawithSVGLine()
    }
    refreshDatawithSVGLine(){

    }
    

   
   

    render() {
        const renderTree = (nodes) => (
            <TreeItem id={nodes.id} key={nodes.id} nodeId={nodes.id} label={nodes.name} onLabelClick={ (event)=> { this.pushContainerHistory(nodes,true,event) }}>
                {Array.isArray(nodes.children) ? nodes.children.map((node) => renderTree(node)) : null}
            </TreeItem>
        );
        return (
            <div>
                <Loading open={this.state.loading} />
                <Modal 
                isOpen={this.state.openInterconnection} 
                className="modal-body" 
                overlayClassName="modal-overlay" 
                onRequestClose={  this.closeDialog.bind(this)} 
                closeTimeoutMS={500}
                >
                    <ModeContainer  
                    origin={this.state.selectedOrigin}  
                    target={this.state.selectedTarget}
                    originData={this.state.originData}  
                    targetData={this.state.targetData}
                      />
                    
                </Modal>
                {/* <Modal 
                    isOpen={this.state.isAlert} 
                    className="alert-modal-body" 
                    overlayClassName="alert-modal-overlay" 
                    onRequestClose={  ()=> this.setState({ isAlert: false }) } 
                    // closeTimeoutMS={500}
                    >
                    <Unsuccess >{this.state.alertMsg}</Unsuccess>
                </Modal> */}
                {
                    this.state.menuPopup && 
                    <ContextMenu 
                        clickSameOrigin={this.state.clickSameOrigin} 
                        selectedOrigin={ this.state.selectedOrigin} 
                        viewDetails={this.viewDetails} 
                        chooseOrigin={ this.chooseOrigin} 
                        chooseTarget={this.chooseTarget} 
                        className="context-menu" 
                        style={this.contextMenuPosition} 
                    />
                }
                <h1>Mapping Platform</h1>
                {/* <div className="circle no1">yut</div> */}
                
                <div className="map-platform-box">
                    <div className="left-box">

                        <div className="container-box">
                            <h2>Containers</h2>
                            <div className="template-list">
                            <TreeView
                                // className={classes.root}
                                // defaultCollapseIcon={<ExpandMoreIcon />}
                                defaultCollapseIcon={<MinusSquare />}
                                defaultExpandIcon={<PlusSquare />}
                                defaultEndIcon={<CloseSquare />}
                                defaultExpanded={['root']}
                                onNodeToggle={this.nodeToggle}
                                // defaultExpandIcon={<ChevronRightIcon />}
                                >
                                    {renderTree(this.state.containerData)}
                                </TreeView>
                            {
                                // this.state.containerList.map( container => 
                                // <p className={this.state.selectedTemplate == container.name ? "selected" : ""} onClick={ ()=> { this.pushContainerHistory(container,true)}}>{container.name}</p>
                                // )
                            }
                            </div>
                        </div>
                        <div className="system-box">
                            <h2>Unit Systems</h2>
                            <div className="system-list">
                            {
                                 this.state.registeredSystems &&this.state.registeredSystems.map( system => 
                                    <div className="dragbox-container">
                                        <DragBox 
                                        className="dragbox" 
                                        // selected={this.state.selectedOrigin} 
                                        system={system}
                                        moveDragbox={this.moveDragbox.bind(this)}
                                        />
                                       
                                    </div>
                                    
                                    // <p  >{system.name}</p>
                                    // console.log(system.name)
                                )
                            }
                            </div>
                            
                        </div>
                        <div className="selected-box">
                            <header><h2>Selected Origin System</h2></header>
                            <div className="selected-info">
                                <img src={this.state.selectedOrigin.picture} className="image-box" placeholder="image"></img>
                                <p className="selected-content">Name : <strong id="source-name">{this.state.selectedOrigin.name}</strong> </p>
                                <p className="selected-content">Location : <strong id="source-location" >{this.state.selectedOrigin.curr_location}</strong> </p>
                                <p  className="selected-content">Unit Model : <strong id="source-model">{this.state.selectedOrigin.model_id}</strong> </p>
                            </div>
                        </div>
                    </div>
                    <div className="right-box">
                        {/* <header>{this.state.selectedTemplate}</header> */}
                        <div className="container-history">
                        {
                            
                            this.state.containerHistory && this.state.containerHistory.map( map =>
                                    <span id={"history-"+map.name}>
                                        {map.name}
                                    </span>
                            )
                            

                          
                        }
                        <button 
                        className={ this.state.containerHistory.length >= 2 ? "show-back-btn" : "hide" }
                        onClick={this.goBackContainerHistory.bind(this)}
                        >Back</button>
                          </div>
                        <div id="templateBox"  className={ this.state.graphData ? 'template-box' : 'template-box transplarent'}>
                            
                            <img   id="backgroundImage" className="background-map" src={this.state.mapPicture.url} alt=""/>
                            {
                                this.state.graphData && this.state.graphData.nodes.map((template,index) => 
                                // <div className="dropbox" style={this.templateStyle[index]} ></div>
                                

                                    <div>

                                            <Dropbox  
                                            style={this.templateStyle[index]}  
                                            onCtxClickHandle={ (event)=> { this.menuOpen(event,template.data)  }}  
                                            onClickHandle={ ()=> { this.pushContainerHistory(template.data,false)  } }
                                            className="dropbox" 
                                            // systemData={template.data}
                                            template={template.data}
                                            containerStyle={template.style}
                                            >
                                                {
                                                    template.data.contain_system && <DragBox 
                                                    // selected={this.state.selectedOrigin}  
                                                    system={template.data.contain_system} 
                                                    moveDragbox={this.moveDragbox.bind(this)}></DragBox>
                                                }
                                                    
                                                        
                                                    
                                            </Dropbox>

                                    </div>
                                    
                                )
                            }
                            
                            
                        </div>
                        
                    </div>
                </div>
                
            </div>
        )
    }
}

export default MapPlatform
