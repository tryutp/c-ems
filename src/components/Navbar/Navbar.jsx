import React, { Component } from "react";
import { Link, useHistory } from "react-router-dom";
import { withRouter } from "react-router";
import PropTypes from "prop-types";
import "./Navbar.scss";
import logo from "./../../assets/original_logo.png";
import * as UserService from "../../services/UserService";
import { Redirect, Route } from "react-router-dom";
import * as InfoService from "./../../services/InfoService";
import Modal from "react-modal";
import { Notification } from "./../Notification/Notification";
import { Loading } from "./../../shared/Loading";
import * as TopologyService from "./../../services/TopologyService";

import { openConfirmation } from "../../shared/Confirmation";
import { onUserResponse } from "../../shared/UserResponse";
import * as InformUser from "../../shared/InformUser";
import { PortConfiguration } from "./../PortConfiguration/PortConfiguration";
import ReactTooltip from "react-tooltip";

class Navbar extends Component {
	token = JSON.parse(localStorage.getItem("userData")).token;
	userObj = JSON.parse(localStorage.getItem("userData"));

	// interval;
	static propTypes = {
		match: PropTypes.object.isRequired,
		location: PropTypes.object.isRequired,
		history: PropTypes.object.isRequired,
	};

	constructor(props) {
		super(props);
		this.state = {
			currentLocation: "",
			redirect: false,
			notificationModal: false,
			notificationAmount: "",
			backToLogin: false,

			loading: false,
		};
	}
	componentWillMount() {
		Modal.setAppElement("body");
	}
	componentDidMount() {
		this.getTotalNotification();

		// this.pageLocation()
		// this.interval
		window.interval = setInterval(() => {
			this.getTotalNotification();
		}, 2000);
	}
	componentWillUnmount() {
		if (window.interval) {
			clearInterval(window.interval);
		}
	}

	pageLocation() {
		const pathname = this.props.location.pathname;
		if (this.state.currentLocation !== pathname) {
			this.setState({ currentLocation: pathname });
		}
		// console.log(pathname)
	}
	async getTotalNotification() {
		InfoService.get_notifications_amount(this.token).then((result) => {
			// console.log(result)
			if (result.status === 1) {
				this.setState({ notificationAmount: result.data });
			}
			else if ( result.status === -1){
				// console.log("-1")
				localStorage.removeItem("userData");
				window.location.reload()
				
			}
			// else{
			//     InformUser.unsuccess({ text: result.msg })
			// }
		});
	}

	async logout() {
		let location = this.props.location.pathname;
		if( ['/map-management','/port-assignment'].includes(location)){
			if (location === "/port-assignment") {
				const dialog = await this.leavePortAllocation()
				if (dialog && dialog === "confirm") {
					await this.props.history.push('/overview');
					this.logout2()
				}
			}
			if (location === "/map-management") {
				const dialog = await this.leaveMapManagement()
				if (dialog && dialog === "confirm") {
					await this.cancelEdittingTopology('/overview');
					this.logout2()
				}
			}
		}
		else{
			this.logout2()
		}

	}

	async logout2(){
		const dialog = await openConfirmation({
			title: "Logout Confirmation",
			text: "Are you sure you want to logout of the system?",
		});
		if (dialog) {
			UserService.logout(this.token).then((result) => {
				// console.log(result)
				if (result.status == 1) {
					localStorage.removeItem("userData");
					this.setState({ redirect: true }, () => {
						window.location.reload(false);
					});
				}
			});
		}
	}

	async leavePortAllocation(){
		const dialog = await openConfirmation({
			title: "Port Allocation",
			text: "You are leaving the Port Allocation without submitting changes or cancelling back to Customer Management. Are you sure you want to cancel the allocation?",
			actionLeft: "Leave anyway",
			actionRight: "Stay",
		});
		return dialog
	}

	async leaveMapManagement(){
		const dialog = await openConfirmation({
			title: "Topology Edit",
			text: "You are leaving the Edit Topology without applying the topology. Are you sure you want to cancel the topology?",
			actionLeft: "Leave anyway",
			actionRight: "Stay",
		});
		return dialog
	}


	async navigatesTo(path) {
		const location = this.props.location.pathname;
		if (location === "/map-management") {
			const dialog = await this.leaveMapManagement()
			if (dialog && dialog === "confirm") {
				await this.cancelEdittingTopology(path);
			}
		}
		if (location === "/port-assignment") {
			const dialog = await this.leavePortAllocation()
			if (dialog && dialog === "confirm") {
				await this.props.history.push(path);
			}
		}
		if ((location !== "/map-management") && (location !== "/port-assignment") ) {
			await this.props.history.push(path);
		}
	}

	cancelEdittingTopology(path) {
		this.setState({ loading: true }, async () => {
			await TopologyService.cancel_topology(this.token).then(
				async (result) => {
					// console.log(result)
					if (result.status == 1) {
						// const dialog = await onUserResponse({ text: result.msg })
						this.setState(
							{
								loading: false,
								// properLeavingPage: true,
							},()=>{
								this.props.history.push(path);
							}
						);
					} else {
						this.setState(
							{
								loading: false,
								// properLeavingPage: true,
							}
						);
						InformUser.unsuccess({ text: result.msg });
					}
				}
			);
		});
	}

	openNotificationModal() {
		this.setState({ notificationModal: true });
	}

	closeNotificationModal = () => {
		this.setState({ notificationModal: false });
	};

	render() {
		// if (this.state.backToLogin) {
		//     return <Redirect push to={{
		//         pathname: "/",
		//     }} />;
		// }
		// console.log(this.props.history.location)
		console.log(this.props.location.pathname)

		const { pathname } = this.props.location;
		return (
			<div className="navbar">
				<Loading open={this.state.loading} />
				<Modal
					isOpen={this.state.notificationModal}
					className="notification-body"
					overlayClassName="alert-modal-overlay"
					onRequestClose={this.closeNotificationModal}
					// closeTimeoutMS={500}
				>
					<Notification close={this.closeNotificationModal} />
				</Modal>
				<div className="left">
					<div className="logo">
						<a
							id="xenoptics-link"
							href="https://xenoptics.com/"
							target="_blank"
						>
							<img src={logo}></img>
						</a>
					</div>
				</div>
				<div className="navigation-link">
					<ul>
						<li
							id="overview-link"
							onClick={() => {
								this.navigatesTo("/overview");
							}}
							className={
								pathname === "/" ||
								pathname === "/overview" ||
								pathname === "/xen/web/"
									? " current-page "
									: ""
							}
						>
							<h1>Home</h1>
						</li>

						<div id="connectivity-link" className="dropdown">
							<li
								id="connectivity-link-1"
								onClick={() => {
									this.navigatesTo("/connectivity");
								}}
								className={
									pathname === "/connectivity" ||
									pathname === "/disconnection" ||
									pathname === "/remaining-task"
										? " current-page "
										: ""
								}
							>
								<h1>Connectivity</h1>
							</li>

							<div className="dropdown-content">
								<ul>
									{/* <Link to="/connectivity"> */}
									<li
										onClick={() => {
											this.navigatesTo("/connectivity");
										}}
										className={
											pathname === "/connectivity"
												? " current-page "
												: ""
										}
										id="connectivity-sublink"
									>
										<h2>Connection</h2>
									</li>
									<li
										onClick={() => {
											this.navigatesTo("/disconnection");
										}}
										className={
											pathname === "/disconnection"
												? " current-page "
												: ""
										}
										id="disconnect-sublink"
									>
										<h2>Disconnection</h2>
									</li>

									<li
										onClick={() => {
											this.navigatesTo("/remaining-task");
										}}
										className={
											pathname === "/remaining-task"
												? " current-page "
												: ""
										}
										id="remaining-task-sublink"
									>
										<h2>Remaining Task</h2>
									</li>
								</ul>
							</div>
						</div>
						<div className="dropdown">
							<li
								onClick={() => {
									this.navigatesTo("/topology");
								}}
								className={
									pathname === "/topology" ||
									pathname === "/map-management"
										? " current-page "
										: ""
								}
								id="topology-link"
								className="dropbtn"
							>
								<h1>Topology</h1>
							</li>
							{/* <div class="dropdown-content">
                  <ul id="visualization-dd">
                    <Link id="topology" to="/topology">
                      <li id="topology-li">
                        <h2>Visualization</h2>
                      </li>
                    </Link>
                    <Link id="map-management" to='/topology'>
                                        <li id="map-managemen-li">Map Management</li>
                                    </Link>
                    <Link id="container" to='/container'>
                                            <li id="container"><h2>Template Setting</h2></li>
                                        </Link>
                  </ul>
                </div> */}
						</div>
						<div className="dropdown">
							<li
								onClick={() => {
									this.navigatesTo("/system-registration");
								}}
								className={
									(pathname === "/system-registration" ||
									pathname === "/port-configuration" ||
									pathname === "/account-management" ||
									pathname === "/customer-management" ||
									pathname === "/port-assignment" ||
									pathname === "/remote-authentication"
										? " current-page "
										: "") + " dropbtn "
								}
								id="management-link"
							>
								<h1>Management</h1>
							</li>
							<div className="dropdown-content">
								<ul>
									<li
										onClick={() => {
											this.navigatesTo(
												"/system-registration"
											);
										}}
										className={
											pathname === "/system-registration"
												? " current-page "
												: ""
										}
										id="unit-management-sublink"
									>
										<h2>Unit Management</h2>
									</li>

									<li
										onClick={() => {
											this.navigatesTo(
												"/port-configuration"
											);
										}}
										className={
											pathname === "/port-configuration"
												? " current-page "
												: ""
										}
										id="port-configuration-sublink"
									>
										<h2>Port Configuration</h2>
									</li>

									<li
										onClick={() => {
											this.navigatesTo(
												"/account-management"
											);
										}}
										className={
											pathname === "/account-management"
												? " current-page "
												: ""
										}
										id="account-management-sublink"
									>
										<h2>User Account</h2>
									</li>

									<li
										onClick={() => {
											this.navigatesTo(
												"/customer-management"
											);
										}}
										className={
											pathname === "/customer-management"
												? " current-page "
												: ""
										}
										id="customer-management-sublink"
									>
										<h2>Customer Account</h2>
									</li>

									<li
										onClick={() => {
											this.navigatesTo(
												"/remote-authentication"
											);
										}}
										className={
											pathname ===
											"/remote-authentication"
												? " current-page "
												: ""
										}
										id="remote-authentication-sublink"
									>
										<h2>Remote Authentication</h2>
									</li>
								</ul>
							</div>
						</div>
						{/* <div class="dropdown">
                            <Link to='/customer-management'>
                                <li id="sysytem-registration" class="dropbtn"><h2>Customer</h2></li>
                            </Link>

                        </div> */}

						<li
							onClick={() => {
								this.navigatesTo("/task-log");
							}}
							className={
								pathname === "/task-log" ? " current-page " : ""
							}
							id="log-link"
						>
							<h1>Log</h1>
						</li>

						<div className="dropdown">
							<li
								onClick={() => {
									this.navigatesTo("/general-setting");
								}}
								className={
									pathname === "/general-setting"
										? " current-page "
										: ""
								}
								id="network-setting-link"
								className="dropbtn"
							>
								<h1>Settings</h1>
							</li>
						</div>
					</ul>
				</div>

				<div className="notification">
				
							<ReactTooltip
								id="remoteTooltip"
								place="left"
								// type="warning"
								effect="solid"
								className="remoteClass defaultTooltip"
								offset={{right: 7}}
							/>
						

					<button id="user-btn" className={ (this.userObj.remote_server ? "" : " local ") + " btn user-btn "} data-for="remoteTooltip"
								data-tip={( this.userObj.remote_server ? `Remote: ${this.userObj.remote_server}` : `Server: Local`)} >
						
						{this.userObj.remote_server && (
							<span className="remote-btn" >
								<i class="fas fa-desktop"></i>
							</span>
						)}
						{!this.userObj.remote_server && (
							<span className="remote-btn" >
								<i class="fas fa-user"></i>
							</span>
						)}
						<span className="ellipsis">{`${this.userObj.username}`}</span>
						<span className="role-text">{`(${this.userObj.auth_name})`}</span>
					</button>
					<button
						id="notification-panel-btn"
						className="fit-btn submit noti-icon"
						onClick={this.openNotificationModal.bind(this)}
					>
						<i class="fas fa-bell"></i>
						<span id="total-notification">
							{this.state.notificationAmount.noti_count}
						</span>
					</button>

					<span
						id="logout-btn"
						className="logout-btn sm-btn submit"
						id="logout-btn"
						onClick={this.logout.bind(this)}
					>
						<span>Logout</span>
					</span>
				</div>
				{this.state.redirect && <Redirect to="" />}
			</div>
		);
	}
}

Navbar.defaultProps = {
	title: "Cluster EMS",
};

// export default Navbar;
export default withRouter(Navbar);
