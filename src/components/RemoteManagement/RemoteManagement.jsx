import React, { Component } from "react";
import "./RemoteManagement.scss";
import * as UserService from "./../../services/UserService";
import { openConfirmation } from "../../shared/Confirmation";
import { onUserResponse } from "../../shared/UserResponse";
import * as InformUser from "../../shared/InformUser";
import Modal from "react-modal";
import { RemoteRegistration } from "./RemoteRegistration";
import { RemoteEdit } from "./RemoteEdit";
import ReactTooltip from "react-tooltip";

import moment from "moment";
export class RemoteManagement extends Component {
	token = JSON.parse(localStorage.getItem("userData")).token;
	time_offset = JSON.parse(localStorage.getItem("userData")).time_offset;

	constructor(props) {
		super(props);
		this.state = {
			remoteList: "",
			serverType: "1",
			serverName: "",
			ipAddress: "",
			port: "",
			secretKey: "",
			protocol_version: 2,
			authen_container: "",
			group_naming: "",

			registrationIsOpen: false,
			edittingIsOpen: false,
			editingData: "",
			invalided: false,
		};
	}

	componentDidMount() {
		this.getRemote();
		this.getRemoteType();
	}

	handleInput = (event) => {
		this.setState({
			[event.target.name]: event.target.value,
		});
	};

	async submitRegistration(event) {
		event.preventDefault()
		this.setState({ invalided: true });

		var dataIsNotOkay = await this.verifyData(
			this.state.ipAddress,this.state.port
		);
		if (dataIsNotOkay.found) {
			InformUser.unsuccess({ text: `${dataIsNotOkay.msg}` });
		}

		if (!dataIsNotOkay.found) {
			this.setState({ invalided: false });

			var data = "";
			if (this.state.serverType === "3") {
				data = {
					server_name: this.state.serverName,
					remote_type_id: this.state.serverType,
					ip_address: this.state.ipAddress,
					port: this.state.port,
					secret_key: this.state.secretKey,
					protocol_ver: this.state.protocol_version,
					auth_container: this.state.authen_container,
					naming_attribute: this.state.group_naming,
				};
			}
			if (this.state.serverType !== "3") {
				data = {
					server_name: this.state.serverName,
					remote_type_id: this.state.serverType,
					ip_address: this.state.ipAddress,
					port: this.state.port,
					secret_key: this.state.secretKey,
					protocol_ver: null,
					auth_container: null,
					naming_attribute: null,
				};
			}

			// console.log(data);
			const dialog = await openConfirmation({
				title: "Create Remote Authentication",
				text: `Are you sure you want to create new remote authentication?`,
			});
			if (dialog) {
				await UserService.create_remote(data, this.token).then(
					(result) => {
						// console.log(result);
						if (result.status === 1) {
							InformUser.success({ text: result.msg });
							this.getRemote();
							this.resetInput()
							// this.props.close(true);
						} else {
							InformUser.unsuccess({ text: result.msg });
						}
					}
				);
			}
		}
	}

	async verifyData( ip_ddress, remote_port ) {
        var found = false;
        var msg = "";
        
        const {
			serverType,
			serverName,
			ipAddress,
			port,
			secretKey,
			protocol_version,
			authen_container,
			group_naming,
		} = this.state;
        

        
		if( !remote_port.match(/^[0-9]{0,8}$/)){
			found = true;
            msg = "Port is incorrect.";
		}
        
		if(!ipAddress.match(/^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/gm)){
            found = true;
            msg = "IP address is incorrect.";
        }


        // if(!systemName.match( /^[\w-]{3,40}$/)){
        //     found = true;
        //     msg = "Unit Name only allow _(Underscore) and -(Dash) as special charecter with maximum of 40 characters.";
        // }
        if (!serverName.trim() || !ipAddress || !port || !secretKey  ) {
            found = true;
            msg = "Remote server information is incomplete.";
			
        }
		if(serverType === "3" && (!protocol_version || !group_naming) ){
			found = true;
			msg = "Remote server information is incomplete.";
		}
		if( this.validateNoQoute(serverName) ){
			found = true;
			msg = "Single quote and Double quote are not allowed.";
		}

		if( this.validateName(serverName) ){
			found = true;
			msg = "Server name only allow _ . and - as special character in the range of 3 to 20 characters.";
		}


		if( serverType === "3" &&
		(this.validateNoQoute(secretKey) || 
		this.validateNoQoute(authen_container) ||
		this.validateNoQoute(group_naming)) ){
			found = true;
			msg = "Single quote and Double quote are not allowed.";
		}
        return {
            found: found,
            msg: msg,
        };
    }

	validateInput(input){

		const {
			serverType,
			serverName,
			ipAddress,
			port,
			secretKey,
			protocol_version,
			authen_container,
			group_naming,
		} = this.state;

        if(this.state.invalided){

            if( input === "NAME_INPUT" ){
                if( !serverName.trim() || this.validateNoQoute(serverName) || this.validateName(serverName) ) {
                    return " invalid-input "
                }
            }

            if( input === "IP_INPUT" ){
                if( !ipAddress || this.validateIpAddr(this.state.ipAddress) ) {
                    return " invalid-input "
                }
            }

            if( input === "PORT_INPUT" ){
                if( !port || this.validatePort(port) ) {
                    return " invalid-input "
                }
            }

            if( input === "KEY_INPUT" ){
                if( !secretKey || this.validateNoQoute(secretKey) ) {
                    return " invalid-input "
                }
            }

			if( input === "CONTAINER_INPUT" ){
                if(  this.validateNoQoute(authen_container) ) {
                    return " invalid-input "
                }
            }

			if( input === "NAMING_INPUT" ){
                if( !group_naming || this.validateNoQoute(group_naming) ) {
                    return " invalid-input "
                }
            }
			
        } 
    }

	validateNoQoute(input){
        
        if(!input.match(/^[^'"]*$/)){
            return true
        }
    }

	validateIpAddr(input){
        if(!input.match(/^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/gm)){
            return true
        }
    }

	validatePort(input){
        if(!input.match(/^[0-9]{0,8}$/)){
            return true
        }
    }

	validateName(input) {
		if (!input.match(/^[\w-.]{0,20}$/)) {
			return true;
		}
	}

	getRemoteType() {
		UserService.get_remote_type(this.token).then((result) => {
			// console.log("type", result);
			if (result.status === 1) {
				this.setState({ server_type: result.data });
			} else {
				InformUser.unsuccess({ text: result.msg });
			}
		});
	}

	getRemote() {
		UserService.get_remote(this.token).then(async (result) => {
			// console.log("data ", result);
			if (result.status === 1) {
				if (result.data) {
					var remotes = result.data;
					var timeCaledRemotes = await this.processColumnOfTime(
						remotes
					);

					this.setState({ remoteList: timeCaledRemotes });
				}
			} else {
				InformUser.unsuccess({ text: result.msg });
			}
		});
	}
	async processColumnOfTime(logData) {
		var logs = JSON.parse(JSON.stringify(logData));
		// var  logs = logData;
		for (var i = 0; i < logs.length; i++) {
			const newAddDate = await this.calculateTime(logs[i]["add_date"]);
			// const newStartDate = await this.calculateTime(logs[i]["act_start_date"])
			// const newFinishDate = await this.calculateTime(logs[i]["finish_date"])
			logs[i]["add_date"] = newAddDate;
			// logs[i]["act_start_date"] = newStartDate;
			// logs[i]["finish_date"] = newFinishDate;
		}
		return logs;
	}
	async calculateTime(serverTime) {
		var rawServerTime = await moment(serverTime);
		// console.log(rawServerTime)
		// console.log(timeOffset)
		var offset = moment.duration(this.time_offset);
		// // var temp = moment(server_time).add(1, 'm')
		// console.log(offset)
		var currentTime = await rawServerTime
			.add(offset)
			.format("YYYY-MM-DD HH:mm:ss");
		// console.log(currentTime)
		return currentTime;
		// console.log(currentTime)
	}
	// edit(remote){

	// }

	async editRemote(remote) {
		this.setState({
			editingData: remote,
			edittingIsOpen: true,
		});
	}

	closeEditingModal = (isRefresh) => {
		if (isRefresh) {
			this.getRemote();
		}
		this.setState({ edittingIsOpen: false });
	};

	async removeRemote(remote) {
		// console.log(remote);
		const dialog = await openConfirmation({
			title: "Remove Confirmation",
			text: `Are you sure you want to remove ${remote.server_name}?`,
		});
		if (dialog) {
			UserService.remove_remote(remote.id, this.token).then((result) => {
				// console.log(result);
				if (result.status === 1) {
					InformUser.success({ text: result.msg });
					this.getRemote();
				} else {
					InformUser.unsuccess({ text: result.msg });
				}
			});
		}
	}
	openRegisterModal() {
		this.setState({ registrationIsOpen: true });
	}
	closeRegisterModal = (isRefresh) => {
		if (isRefresh) {
			this.getRemote();
			// console.log("yut");
		}
		this.setState({
			registrationIsOpen: false,
			edittingIsOpen: false,
		});
	};

	resetInput() {
		this.setState({
			serverType: "1",
			serverName: "",
			ipAddress: "",
			port: "",
			secretKey: "",
			protocol_version: 2,
			authen_container: "",
			group_naming: "",
		});
	}

	render() {
		return (
			<div className="remote-authentication-container">
				{/* <Modal
					isOpen={this.state.registrationIsOpen}
					className="remote-registration-body"
					overlayClassName="default-overlay"
					onRequestClose={() => {
						this.closeRegisterModal(false);
					}}
					// closeTimeoutMS={500}
				>
					<RemoteRegistration
						server_type={this.state.server_type}
						close={this.closeRegisterModal}
					/>
				</Modal> */}
				<Modal
					isOpen={this.state.edittingIsOpen}
					className="remote-registration-body"
					overlayClassName="default-overlay"
					onRequestClose={() => {
						this.closeRegisterModal(false);
					}}
					// closeTimeoutMS={500}
				>
					<RemoteEdit
						server_type={this.state.server_type}
						editingData={this.state.editingData}
						close={this.closeEditingModal}
					/>
				</Modal>

				<header className="h1">
					<h1>MANAGEMENT: REMOTE AUTHENTICATION SERVER</h1>
				</header>
				<div className="system-management-layout">
					{/* <div className="lr-container handle-tab">
                        <div className="left">
                            <button id="new-remote-btn" onClick={this.openRegisterModal.bind(this)} className="sm-btn submit"><span>New Remote</span></button>
                        </div>
                        <div className="right">
                        </div>
                    </div> */}

					<div className="registration">
						<header className="h2">
							<h2>Registration</h2>
						</header>
						<form
							onSubmit={this.submitRegistration.bind(this)}
							autoComplete="off"
						>
							<div className="regis-form">
								<div className="input-group">
									<div className="left">
										<span>Server type :</span>
									</div>
									<div className="right">
										<select
											name="serverType"
											id="server-type-input"
											value={this.state.serverType}
											onChange={this.handleInput}
										>
											{this.state.server_type &&
												this.state.server_type.map(
													(type) => (
														<option value={type.id}>
															{type.name}
														</option>
													)
												)}
										</select>
									</div>
								</div>
								<div className="input-group">
									<div className="left">
										<span>Server name :</span>
									</div>
									<div className="right">
										<input
											type="text"
											placeholder="Server name"
											id="server-name-input"
											name="serverName"
											maxLength="40"
											className={this.validateInput("NAME_INPUT")}
											value={this.state.serverName}
											onChange={this.handleInput}
										/>
									</div>
								</div>

								<div className="input-group">
									<div className="left">
										<span>IP address :</span>
									</div>
									<div className="right">
										<input
											type="text"
											placeholder="192.168.0.1"
											id="ip-address-input"
											name="ipAddress"
											maxLength="40"
											className={this.validateInput("IP_INPUT")}
											value={this.state.ipAddress}
											onChange={this.handleInput}
										/>
									</div>
								</div>

								<div className="input-group">
									<div className="left">
										<span>Port :</span>
									</div>
									<div className="right">
										<input
											type="number"
											placeholder="22"
											id="server-port-input"
											name="port"
											min="1"
											max="65535"
											className={this.validateInput("PORT_INPUT")}
											value={this.state.port}
											onChange={this.handleInput}
										/>
									</div>
								</div>

								<div className="input-group">
									<div className="left">
										{this.state.serverType !== "3" && (
											<span>Secret key :</span>
										)}
										{this.state.serverType === "3" && (
											<span>Base DN :</span>
										)}
									</div>
									<div className="right">
										<input
											type="text"
											placeholder="dc=example,dc=com"
											id="secret-key-input"
											name="secretKey"
											maxLength="140"
											className={this.validateInput("KEY_INPUT")}
											value={this.state.secretKey}
											onChange={this.handleInput}
										/>
									</div>
								</div>
								{this.state.serverType === "3" && <hr />}
								{this.state.serverType === "3" && (
									<div className="input-group">
										<div className="left">
											<span>Protocol version :</span>
										</div>
										<div className="right">
											<select
												name="protocol_version"
												id="protocol-version-input"
												value={
													this.state.protocol_version
												}
												onChange={this.handleInput}
											>
												<option
													id="version-2"
													value="2"
												>
													2
												</option>
												<option
													id="version-3"
													value="3"
												>
													3
												</option>
											</select>
										</div>
									</div>
								)}
								{this.state.serverType === "3" && (
									<div className="input-group ">
										<div className="left">
											<span>
												Authentication container :
											</span>
										</div>
										<div className="right ">
											<input
												type="text"
												// required
												placeholder="OU=software,DC=example,DC=com"
												id="authen-container-input"
												name="authen_container"
												maxLength="1000"
												className={this.validateInput("CONTAINER_INPUT")}
												value={
													this.state.authen_container
												}
												onChange={this.handleInput}
											/>
											<ReactTooltip
												id="infoTooltip"
												html={true}
												place="right" className="noteClass text-left defaultTooltip" arrowColor="#404040"  effect="solid"
											/>

											<button
												id="note-btn"
												type="button"
												data-for="infoTooltip"
												data-tip="<span>Note: Semi-colon separated.<br/>The full container path must be specified containing a dc=component.<br/>Example: CN=users1,DC=example,DC=com</span>"
												className="inform fit-btn"
												
											>
												<i class="fas fa-question-circle"></i>
											</button>
											{/* 
                                    <span><strong>Note: Semi-colon separated.</strong> The full</span>
                                     <span>container path must be specified containing </span>
                                     <span>a dc=component. Example: CN=users1</span>
                                     <span>,DC=example,DC=com;CN=users2</span>
                                     <span>,DC=example,DC=com</span> */}
										</div>
									</div>
								)}
								{this.state.serverType === "3" && (
									<div className="input-group">
										<div className="left">
											<span>
												Group naming attribute :
											</span>
										</div>
										<div className="right">
											<input
												type="text"
												placeholder="cn"
												id="group-naming-input"
												name="group_naming"
												maxLength="40"
												value={this.state.group_naming}
												className={this.validateInput("NAMING_INPUT")}
												onChange={this.handleInput}
											/>
										</div>
									</div>
								)}
							</div>
							<div className="btn-row">
								<button
									id="confirm-btn"
									type="submit"
									className="btn submit"
									style={{ marginRight: "0.5vw" }}
								>
									<span>Confirm</span>
								</button>
								<button
									id="cancel-btn"
									type="button"
									onClick={() => {
										this.resetInput();
									}}
									className="btn cancel"
								>
									<span>Cancel</span>
								</button>
							</div>
						</form>
						{/* <div className="btn-row submit-regis">
                            <button
                                id="submit-btn"
                                style={{marginRight:"0.5vw"}}
                                className="btn submit"
                                onClick={this.submitRegistration.bind(this)}
                            >
                                <span>Submit</span>
                            </button>
                            <button
                                id="cancel-btn"
                                className="btn cancel"
                                onClick={this.resetInput.bind(this)}
                            >
                                <span>Clear</span>
                            </button>
                        </div> */}
					</div>

					<div className="management-container">
						<div className="editting-container">
							<header className="h2">
								<h2>Remote Authentication Server</h2>
							</header>
							<div className="table-size">
								<table id="remote-server-table">
									<tr>
										<th>
											<span>No</span>
										</th>
										<th>
											<span>Server Type</span>
										</th>
										<th>
											<span>Server Name</span>
										</th>
										<th>
											<span>IP Address</span>
										</th>
										<th>
											<span>Port</span>
										</th>
										<th>
											<span>Add Date</span>
										</th>
										<th>
											<span>Actions</span>
										</th>
									</tr>
									{this.state.remoteList &&
										this.state.remoteList.map(
											(remote, index) => (
												<tr
													key={`remote-${index + 1}`}
													id={`remote-${index + 1}`}
												>
													<td
														id={`no-${index + 1}`}
														className="text-center"
														style={{width:"10%"}}
													>
														<span>{remote.no}</span>
													</td>
													<td
														id={`remote-type-${
															index + 1
														}`}
														className="text-center"
														style={{width:"10%"}}
													>
														<span>
															{
																remote.remote_type_name
															}
														</span>
													</td>
													<td
														id={`server-name-${
															index + 1
														}`}
														className="text-center"
														style={{width:"25%"}}
													>
														<span>
															{remote.server_name}
														</span>
													</td>
													<td
														id={`ip-address-${
															index + 1
														}`}
														className="text-center"
														style={{width:"10%"}}
													>
														<span>
															{remote.ip_address}
														</span>
													</td>
													<td
														id={`server-port-${
															index + 1
														}`}
														className="text-center"
														style={{width:"10%"}}
													>
														<span>
															{remote.port}
														</span>
													</td>
													<td
														id={`add-date-${
															index + 1
														}`}
														className="text-center"
														style={{width:"15%"}}
													>
														<span>
															{remote.add_date}
														</span>
													</td>
													<td
														id={`action-${
															index + 1
														}`}
														className="text-center"
														style={{width:"20%"}}
													>
														<div className="action-row">
															<button
																id={`edit-btn-${
																	index + 1
																}`}
																onClick={() => {
																	this.editRemote(
																		remote
																	);
																}}
																className="sm-btn submit shift-right"
															>
																<span>
																	Edit
																</span>
															</button>
															<button
																id={`remove-btn-${
																	index + 1
																}`}
																onClick={() => {
																	this.removeRemote(
																		remote
																	);
																}}
																className="sm-btn submit"
															>
																<span>
																	Remove
																</span>
															</button>
														</div>
													</td>
												</tr>
											)
										)}
								</table>
								{
                                    (this.state.remoteList.length === 0) && <div id="no-data-task" className="no-remaining-task" style={{height:"7vw"}}><span className="camera-offline">NO REMOTE SERVER</span></div>
                                }
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default RemoteManagement;
