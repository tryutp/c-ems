import React, { Component } from "react";
import "./RemoteManagement.scss";
import * as UserService from "./../../services/UserService";
import { openConfirmation } from "../../shared/Confirmation";
import { onUserResponse } from "../../shared/UserResponse";
import * as InformUser from "../../shared/InformUser";
import ReactTooltip from 'react-tooltip';

export class RemoteRegistration extends Component {
    token = JSON.parse(localStorage.getItem("userData")).token;

    constructor(props) {
        super(props);
        this.state = {
            serverType: 1,
            serverName: "",
            ipAddress: "",
            port: "",
            secretKey: "",
            protocol_version: 2,
            authen_container: "",
            group_naming: "",
        };
    }

    handleInput = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
        });
    };

    async submitRegistration() {
        var data = '';
        if(this.state.serverType === "3"){
            data = {
                server_name: this.state.serverName,
                remote_type_id: this.state.serverType,
                ip_address: this.state.ipAddress,
                port: this.state.port,
                secret_key: this.state.secretKey,
                protocol_ver: this.state.protocol_version,
                auth_container: this.state.authen_container,
                naming_attribute: this.state.group_naming,
            };
        }
        if(this.state.serverType !== "3"){
            data = {
                server_name: this.state.serverName,
                remote_type_id: this.state.serverType,
                ip_address: this.state.ipAddress,
                port: this.state.port,
                secret_key: this.state.secretKey,
                protocol_ver: null,
                auth_container: null,
                naming_attribute: null,
            };
        }
        
        // console.log(data);
        const dialog = await openConfirmation({
            title: "Create Remote Authentication",
            text: `Are you sure you want to create new remote authentication?`,
        });
        if (dialog) {
            await UserService.create_remote(data, this.token).then(
                async (result) => {
                    // console.log(result);
                    if (result.status === 1) {
                        await InformUser.success({ text: result.msg });

                        this.props.close(true);
                    } else {
                        InformUser.unsuccess({ text: result.msg });
                    }
                }
            );
        }
    }

    render() {
        const { server_type, close } = this.props;
        return (
            <div className="remote-registration-container">
                <header>
                    <h1>Remote Registration</h1>
                </header>
                <div className="content">
                    <div className="input-form">
                        <div className="input-group">
                            <div className="left">
                                <span>Server Type :</span>
                            </div>
                            <div className="right">
                                <select
                                    name="serverType"
                                    id="server-type"
                                    value={this.state.serverType}
                                    onChange={this.handleInput}
                                >
                                    {server_type &&
                                        server_type.map((type) => (
                                            <option value={type.id}>
                                                {type.name}
                                            </option>
                                        ))}
                                </select>
                            </div>
                        </div>
                        <div className="input-group">
                            <div className="left">
                                <span>Server Name :</span>
                            </div>
                            <div className="right">
                                <input
                                    type="text"
                                    placeholder="LDAP"
                                    id="server-name"
                                    name="serverName"
                                    value={this.state.serverName}
                                    onChange={this.handleInput}
                                />
                            </div>
                        </div>

                        <div className="input-group">
                            <div className="left">
                                <span>IP Address :</span>
                            </div>
                            <div className="right">
                                <input
                                    type="text"
                                    placeholder="192.168.67.102"
                                    id="ip-address"
                                    name="ipAddress"
                                    value={this.state.ipAddress}
                                    onChange={this.handleInput}
                                />
                            </div>
                        </div>

                        <div className="input-group">
                            <div className="left">
                                <span>Port :</span>
                            </div>
                            <div className="right">
                                <input
                                    type="text"
                                    placeholder="3000"
                                    id="port"
                                    name="port"
                                    value={this.state.port}
                                    onChange={this.handleInput}
                                />
                            </div>
                        </div>

                        <div className="input-group">
                            <div className="left">
                                {this.state.serverType !== "3" && (
                                    <span>Secret Key :</span>
                                )}
                                {this.state.serverType === "3" && (
                                    <span>Base DN :</span>
                                )}
                            </div>
                            <div className="right">
                                <input
                                    type="text"
                                    placeholder=""
                                    id="secret-key"
                                    name="secretKey"
                                    value={this.state.secretKey}
                                    onChange={this.handleInput}
                                />
                            </div>
                        </div>
                        {this.state.serverType === "3" && (
                            <div className="input-group">
                                <div className="left">
                                    <span>Protocol version :</span>
                                </div>
                                <div className="right">
                                    
                                    <select name="protocol_version" id="protocol_version" value={this.state.protocol_version}
                                        onChange={this.handleInput} >
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>
                        )}
                        {this.state.serverType === "3" && (
                            <div className="input-group ">
                                <div className="left">
                                    <span>Authentication container :</span>
                                </div>
                                <div className="right ">
                                    <input
                                        type="text"
                                        required
                                        placeholder=""
                                        id="authen_container"
                                        name="authen_container"
                                        value={this.state.authen_container}
                                        onChange={this.handleInput}
                                    />
                                <ReactTooltip id='infoTooltip' multiline={true} place="left" type="dark" effect="solid" />

                                        <button 
                                        data-for='infoTooltip'
                                        data-tip="Note: Semi-colon separated. The full container path must be specified containing a dc=component.<br/> Example: CN=users1,DC=example,DC=com"
                                        className="inform fit-btn"><i class="fas fa-question-circle"></i></button>
{/* 
                                    <span><strong>Note: Semi-colon separated.</strong> The full</span>
                                     <span>container path must be specified containing </span>
                                     <span>a dc=component. Example: CN=users1</span>
                                     <span>,DC=example,DC=com;CN=users2</span>
                                     <span>,DC=example,DC=com</span> */}
                                </div>
                            </div>
                        )}
                        {this.state.serverType === "3" && (
                            <div className="input-group">
                                <div className="left">
                                    <span>Group naming attribute :</span>
                                </div>
                                <div className="right">
                                    <input
                                        type="text"
                                        placeholder=""
                                        id="group_naming"
                                        name="group_naming"
                                        value={this.state.group_naming}
                                        onChange={this.handleInput}
                                    />
                                </div>
                            </div>
                        )}
                    </div>
                    <div className="btn-row">
                        <button
                            onClick={this.submitRegistration.bind(this)}
                            className="btn submit"
                        >
                            <span>Confirm</span>
                        </button>
                        <button
                            onClick={() => {
                                this.props.close();
                            }}
                            className="btn cancel"
                        >
                            <span>Cancel</span>
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default RemoteRegistration;
