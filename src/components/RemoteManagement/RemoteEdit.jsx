import React, { Component } from "react";
import "./RemoteManagement.scss";
import * as UserService from "./../../services/UserService";
import { openConfirmation } from "../../shared/Confirmation";
import { onUserResponse } from "../../shared/UserResponse";
import * as InformUser from "../../shared/InformUser";
import ReactTooltip from "react-tooltip";

export class RemoteEdit extends Component {
	token = JSON.parse(localStorage.getItem("userData")).token;

	constructor(props) {
		super(props);
		this.state = {
			serverType: "",
			serverName: "",
			ipAddress: "",
			port: "",
			secretKey: "",
			protocol_version: "",
			authen_container: "",
			group_naming: "",
		};
	}

	componentDidMount() {
		var protocol_ver = this.props.editingData.protocol_ver;
		var remote_type_id = this.props.editingData.remote_type_id;
		if (protocol_ver) {
			protocol_ver = this.props.editingData.protocol_ver.toString();
		}
		if (remote_type_id) {
			remote_type_id = this.props.editingData.remote_type_id.toString();
		}
		this.setState({
			serverType: remote_type_id,
			serverName: this.props.editingData.server_name,
			ipAddress: this.props.editingData.ip_address,
			port: this.props.editingData.port,
			secretKey: this.props.editingData.secret_key,
			protocol_version: protocol_ver,
			authen_container: this.props.editingData.auth_container,
			group_naming: this.props.editingData.naming_attribute,
		});
	}
	handleInput = (event) => {
		this.setState({
			[event.target.name]: event.target.value,
		});
	};

	async submitEdit(event) {
		event.preventDefault();
		this.setState({ invalided: true });

		var dataIsNotOkay = await this.verifyData(
			this.state.ipAddress,
			this.state.port
		);
		if (dataIsNotOkay.found) {
			InformUser.unsuccess({ text: `${dataIsNotOkay.msg}` });
		}

		if (!dataIsNotOkay.found) {
			this.setState({ invalided: false });

			var data = "";
			if (this.state.serverType === "3") {
				data = {
					server_name: this.state.serverName,
					remote_type_id: this.state.serverType,
					ip_address: this.state.ipAddress,
					port: this.state.port,
					secret_key: this.state.secretKey,
					protocol_ver: this.state.protocol_version,
					auth_container: this.state.authen_container,
					naming_attribute: this.state.group_naming,
				};
			}
			if (this.state.serverType !== "3") {
				data = {
					server_name: this.state.serverName,
					remote_type_id: this.state.serverType,
					ip_address: this.state.ipAddress,
					port: this.state.port,
					secret_key: this.state.secretKey,
					protocol_ver: null,
					auth_container: null,
					naming_attribute: null,
				};
			}

			// console.log(data);
			const dialog = await openConfirmation({
				title: "Update Remote Authentication",
				text: `Are you sure you want to update remote authentication?`,
			});
			if (dialog) {
				await UserService.edit_remote(
					this.props.editingData.id,
					data,
					this.token
				).then(async (result) => {
					// console.log(result);
					if (result.status === 1) {
						await InformUser.success({ text: result.msg });

						this.props.close(true);
					} else {
						InformUser.unsuccess({ text: result.msg });
					}
				});
			}
		}
	}

    async verifyData( ip_ddress, remote_port ) {
        var found = false;
        var msg = "";
        
        const {
			serverType,
			serverName,
			ipAddress,
			port,
			secretKey,
			protocol_version,
			authen_container,
			group_naming,
		} = this.state;
        
    
        
		if( !remote_port.match(/^[0-9]{0,8}$/)){
			found = true;
            msg = "Port is incorrect.";
		}
        
		if(!ipAddress.match(/^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/gm)){
            found = true;
            msg = "IP address is incorrect.";
        }


        // if(!systemName.match( /^[\w-]{3,40}$/)){
        //     found = true;
        //     msg = "Unit Name only allow _(Underscore) and -(Dash) as special charecter with maximum of 40 characters.";
        // }
        if (!serverName.trim() || !ipAddress || !port || !secretKey  ) {
            found = true;
            msg = "Remote server information is incomplete.";
			
        }
		if(serverType === "3" && (!protocol_version || !group_naming) ){
			found = true;
			msg = "Remote server information is incomplete.";
		}
		if( this.validateNoQoute(serverName) ){
			found = true;
			msg = "Sigle quote and Double quote are not allowed.";
		}

		if( this.validateName(serverName) ){
			found = true;
			msg = "Server name only allow _ . and - as special character in the range of 3 to 20 characters.";
		}


		if( serverType === "3" &&
		(this.validateNoQoute(secretKey) || 
		this.validateNoQoute(authen_container) ||
		this.validateNoQoute(group_naming)) ){
			found = true;
			msg = "Sigle quote and Double quote are not allowed.";
		}

        return {
            found: found,
            msg: msg,
        };
    }

	validateInput(input){

		const {
			serverType,
			serverName,
			ipAddress,
			port,
			secretKey,
			protocol_version,
			authen_container,
			group_naming,
		} = this.state;

        if(this.state.invalided){

            if( input === "NAME_INPUT" ){
                if( !serverName.trim()  || this.validateNoQoute(serverName) || this.validateName(serverName) ) {
                    return " invalid-input "
                }
            }

            if( input === "IP_INPUT" ){
                if( !ipAddress || this.validateIpAddr(this.state.ipAddress) ) {
                    return " invalid-input "
                }
            }

            if( input === "PORT_INPUT" ){
                if( !port || this.validatePort(port) ) {
                    return " invalid-input "
                }
            }

            if( input === "KEY_INPUT" ){
                if( !secretKey || this.validateNoQoute(secretKey) ) {
                    return " invalid-input "
                }
            }

			if( input === "CONTAINER_INPUT" ){
                
				if( !authen_container || this.validateNoQoute(authen_container) ){
                    return " invalid-input "

				}
            }

			if( input === "NAMING_INPUT" ){
                if( !group_naming || this.validateNoQoute(group_naming) ) {
                    return " invalid-input "
                }
            }
			
        } 
    }

	validateName(input) {
		if (!input.match(/^[\w-.]{0,20}$/)) {
			return true;
		}
	}

	validateNoQoute(input){
        
        if(!input.match(/^[^'"]*$/)){
            return true
        }
    }

	validateIpAddr(input){
        if(!input.match(/^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/gm)){
            return true
        }
    }

	validatePort(input){
        if(!input.match(/^[0-9]{0,8}$/)){
            return true
        }
    }

	render() {
		// console.log(this.props);
		const { server_type, close } = this.props;
		return (
			<div className="remote-editing-container">
				<header>
					<h1>Remote Edit</h1>
				</header>
				<div className="content">
					<form
						onSubmit={this.submitEdit.bind(this)}
						autoComplete="off"
					>
						<div className="input-form">
							<div className="input-group">
								<div className="left">
									<span>Server Type :</span>
								</div>
								<div className="right">
									<select
										disabled
										name="serverType"
										id="server-type-input1"
										value={this.state.serverType}
										onChange={this.handleInput}
									>
										{server_type &&
											server_type.map((type, index) => (
												<option
													key={`server-type-${
														index + 1
													}`}
													id={`server-type-${
														index + 1
													}`}
													value={type.id}
												>
													{type.name}
												</option>
											))}
									</select>
								</div>
							</div>
							<div className="input-group">
								<div className="left">
									<span>Server Name :</span>
								</div>
								<div className="right">
									<input
										type="text"
										placeholder="Server name"
										id="server-name-input1"
										name="serverName"
                                        maxLength="40"
                                        className={this.validateInput("NAME_INPUT")}
										value={this.state.serverName}
										onChange={this.handleInput}
									/>
								</div>
							</div>

							<div className="input-group">
								<div className="left">
									<span>IP Address :</span>
								</div>
								<div className="right">
									<input
										type="text"
										placeholder="192.168.0.1"
										id="ip-address-input1"
										name="ipAddress"
                                        maxLength="40"
                                        className={this.validateInput("IP_INPUT")}
										value={this.state.ipAddress}
										onChange={this.handleInput}
									/>
								</div>
							</div>

							<div className="input-group">
								<div className="left">
									<span>Port :</span>
								</div>
								<div className="right">
									<input
										type="number"
										placeholder="22"
										id="server-port-input1"
										name="port"
                                        min="1"
										max="65535"
                                        className={this.validateInput("PORT_INPUT")}
										value={this.state.port}
										onChange={this.handleInput}
									/>
								</div>
							</div>
							<div className="input-group">
								<div className="left">
									{this.state.serverType !== "3" && (
										<span>Secret Key :</span>
									)}
									{this.state.serverType === "3" && (
										<span>Base DN :</span>
									)}
								</div>
								<div className="right">
									<input
										type="text"
										placeholder="dc=example, dc=com"
										id="secret-key-input1"
										name="secretKey"
                                        maxLength="140"
                                        className={this.validateInput("KEY_INPUT")}
										value={this.state.secretKey}
										onChange={this.handleInput}
									/>
								</div>
							</div>

							{this.state.serverType === "3" && <hr />}
							{this.state.serverType === "3" && (
								<div className="input-group">
									<div className="left">
										<span>Protocol version :</span>
									</div>
									<div className="right">
										<select
											name="protocol_version"
											id="protocol-version-input1"
											value={this.state.protocol_version}
											onChange={this.handleInput}
										>
											<option id="version-2" value="2">
												2
											</option>
											<option id="version-3" value="3">
												3
											</option>
										</select>
									</div>
								</div>
							)}
							{this.state.serverType === "3" && (
								<div className="input-group with-note">
									<div className="left">
										<span>Authentication container :</span>
									</div>
									<div className="right">
										<input
											type="text"
											placeholder="dc=example, dc=com"
											id="authen-container-input1"
											name="authen_container"
                                            maxLength="1000"
                                            className={this.validateInput("CONTAINER_INPUT")}
											value={this.state.authen_container}
											onChange={this.handleInput}
										/>
										<ReactTooltip
											id="infoTooltip"
											html={true}
											place="left" className="noteClass text-left defaultTooltip" arrowColor="#404040"  effect="solid"
										/>

										<button
											id="note-btn"
                                            type="button"
											data-for="infoTooltip"
											data-tip="<span>Note: Semi-colon separated. The full container path must be specified containing a dc=component.<br/> Example: CN=users1,DC=example,DC=com</span>"
											className="inform fit-btn"
										>
											<i class="fas fa-question-circle"></i>
										</button>
										{/* <span>
                                        <strong>
                                            Note: Semi-colon separated.
                                        </strong>{" "}
                                        The full
                                    </span>
                                    <span>
                                        container path must be specified
                                        containing{" "}
                                    </span>
                                    <span>
                                        a dc=component. Example: CN=users1
                                    </span>
                                    <span>,DC=example,DC=com;CN=users2</span>
                                    <span>,DC=example,DC=com</span> */}
									</div>
								</div>
							)}
							{this.state.serverType === "3" && (
								<div className="input-group">
									<div className="left">
										<span>Group naming attribute :</span>
									</div>
									<div className="right">
										<input
											type="text"
											placeholder="cn"
											id="group-naming-input1"
											name="group_naming"
                                            maxLength="40"
                                            className={this.validateInput("NAMING_INPUT")}
											value={this.state.group_naming}
											onChange={this.handleInput}
										/>
									</div>
								</div>
							)}
						</div>
						<div className="btn-row">
							<button
								id="confirm-btn1"
								type="submit"
								className="btn submit"
								style={{ marginRight: "0.5vw" }}
							>
								<span>Confirm</span>
							</button>
							<button
								id="cancel-btn1"
								type="button"
								onClick={() => {
									this.props.close();
								}}
								className="btn submit"
							>
								<span>Cancel</span>
							</button>
						</div>
					</form>
				</div>
			</div>
		);
	}
}

export default RemoteEdit;
