import React from 'react'

function Button (props) {
    const { children, height, width, style } = props;

    var buttonStyle = {
        paddingTop: `${height}px`,
        paddingBottom: `${height}px`,
        paddingLeft: `${width}px`,
        paddingRight: `${width}px`,
    }
    return (
        <button style={ buttonStyle, style} className="btn submit">{children}</button>
    )
}

export default Button
