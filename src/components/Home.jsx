import { createBrowserHistory } from "history";


import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Navbar from "./Navbar/Navbar";
// import Connectivity from './Connectivity/Connectivity';
import Header from './Header';
import Overview from './Overview/Overview';
import Error from './Error';
import Footer from './Footer/Footer';
import { DragDrop } from "./DragAndDrop/DragDrop";
// import Cytoscape from './Cytoscape/Cytoscape'
import { MapPlatform } from "./TemplateMapping/MapPlatform";
import SystemRegistration from "./SystemRegistration/SystemRegistration";
import './Home.scss'
import { Visualization } from "./Visualization/Visualization";
import { TaskLog } from "./TaskLog/TaskLog";
import MapManagement from "./MapManagement/MapManagement";
import Container from "./Container/Container";
import { Disconnection } from "./Disconnection/Disconnection";
import { RemoteManagement } from "./RemoteManagement/RemoteManagement";
import App from "./../App";
import { FrontPanel } from "./Interconnection/Manual/FrontPanel";
import PortConfiguration from "./PortConfiguration/PortConfiguration";
import { CustomerManagement } from "./CustomerManagement/CustomerManagement";
import { PortAssignment } from "./PortAssignment/PortAssignment";
import { AccountManagement } from "./AccountManagement/AccountManagement";
import { NetworkSetting } from "./NetworkSetting/NetworkSetting";
import { TimeSetting } from "./TimeSetting/TimeSetting";
import cytoscape from "cytoscape";
import cyCanvas from 'cytoscape-canvas';
import contextMenus from 'cytoscape-context-menus';
import automove from 'cytoscape-automove';
import cola from 'cytoscape-cola';
import { RemainingTask } from "./../components/RemainingTask/RemainingTask";



import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";

import ModalContainer from 'react-modal-promise'

cytoscape.use(cyCanvas)
cytoscape.use(contextMenus);
cytoscape.use(automove);
cytoscape.use(cola);





const portProps = [{
  name: '1',
  description: 'No. 1'
},
{
  name: '2',
  description: 'No. 2'
},
{
  name: '3',
  description: 'No. 3'
}
]

const Home = () => {


  // console.log('here too')

  


  return (

    <Router history={history}>
      <div className='home' >
        <Navbar className="navbar"  />
        <div className='main-content' >
          <DndProvider backend={HTML5Backend} >
            <Switch>
              <Route exact path='/login' component={App} />
              <Route exact path='/' component={Overview} />
              <Route path='/xen/web' component={Overview} />
              <Route exact path='/overview' component={Overview} />
              <Route exact path='/connectivity' component={FrontPanel} />
              <Route exact path='/topology' component={Visualization} />
              <Route exact path='/disconnection' component={Disconnection} />
              <Route exact path='/remote-authentication' component={RemoteManagement} />
              <Route exact path='/port-configuration' component={PortConfiguration} />
              <Route exact path='/customer-management' component={CustomerManagement} />
              <Route exact path='/port-assignment' component={PortAssignment} />
              <Route exact path='/dragdrop' component={DragDrop} />
              <Route exact path='/container' component={Container} />
              <Route exact path='/map-management' component={ MapManagement } />
              <Route exact path='/template-mapping' component={MapPlatform} />
              <Route exact path='/system-registration' component={SystemRegistration} />
              <Route exact path='/account-management' component={AccountManagement} />
              <Route exact path='/general-setting' component={NetworkSetting} />
              <Route exact path='/time-setting' component={TimeSetting} />
              <Route exact path='/task-log' component={TaskLog} />
              <Route exact path='/remaining-task' component={RemainingTask} />
              <Route path='' component={Error} />

            </Switch>
          </DndProvider>
        </div> <Footer />
      </div>
      < ModalContainer />
    </Router>

    //     <Router>
    //   <div className='App'>
    //     <Header />
    //     <Navbar />
    //     <div className='main-content'>
    //       <Switch>
    //         <Route exact path='/' component={Home} />
    //         <Route exact path='/connectivity' render={(props) =>
    //         <Fragment>
    //           <Connectivity prods={portProps}/>
    //         </Fragment>
    //       } />
    //       <Route path='' component={Error} />
    //       </Switch>
    //     </div>
    //     <Footer />
    //   </div>
    // </Router>
  );
}

export default Home;



export const history = createBrowserHistory()