import React from 'react';
import { useDrop } from 'react-dnd';
import { ItemTypes } from './ItemTypes';
import { Box } from "./Box";
import './Dustbin.scss'



export const Dustbin = ({children, className, title}) => {
    const [{isOver,canDrop}, drop] = useDrop({
        accept: 'Our first type',
        drop: () => ({name: title}),
        collect: (monitor) => ({
            isOver: monitor.isOver(),
            canDrop: monitor.canDrop(),
        })
    });
    // console.log('options ', canDrop, isOver);
    return (
        <div ref={drop} className={className}>
            {title}
            {children}
        </div>
    )
}