import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import DragPlatform from "./DragPlatform";

export class DragDrop extends Component {
    render() {
        return (
            <div>
                <DndProvider backend={HTML5Backend}>
                    <DragPlatform />
                </DndProvider>
            </div>
        )
    }
}

export default DragDrop
