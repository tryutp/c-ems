import React, { memo, useState } from 'react'
import { Dustbin } from "./Dustbin";
import { Box } from "./Box";
import { SystemBox } from "./SystemBox";
import './Dnd.scss'

const droplist = [
	{id: 1,name: "228S"},
	{id: 2,name: "576D"},
	{id: 3,name: "1152Q"}
]
const dragElements = [
	{id: 1,name: "228S"},
	{id: 2,name: "576D"},
	{id: 3,name: "1152Q"}
]

export const Container = () => {

	const [dropElements, setDropElements] = useState([
		{id: 1,name: "box1"},
		{id: 2,name: "box2"}
	])


	function appendData(data) {
		// var temp = dropElements
		let yut = []
		for ( var i=0 ; i< dropElements.length ; i++){
			yut.push(dropElements[i])
		}
		yut.push(data)
		// console.log(yut)
		setDropElements(yut)
	


	}
	function onDelete() {
		// console.log('gonna delete')
	}

    return (
        <div className="container-template">
            <div className="column-tab">
				<Dustbin title='XSOS System' className='column first-column'>
                    {/* {isFirstColumn && Item} */}
					{
						dragElements.map( element => 
							<div key={element.id}>
								<Box name={element.name} passedData={dropElements} passedFunction={appendData} onDelete={onDelete}></Box>
							</div> 
						)
					}
                </Dustbin>
			</div>
			<div className="drop-space">
				<Dustbin title='Empty Container' className='column second-column'>
                    {/* {!isFirstColumn && Item} */}

					
                </Dustbin>
			</div>
                

        </div>
    );
}