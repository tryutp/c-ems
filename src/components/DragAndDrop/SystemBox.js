import React from 'react';
import { useDrop } from 'react-dnd';
import { ItemTypes } from './ItemTypes';
import { Box } from "./Box";
import { Dustbin } from "./Dustbin";
const style = {
    height: '40rem',
    width: '10rem',
    margin: '1.5rem',
    // marginRight: '1.5rem',
    // marginBottom: '1.5rem',
    color: 'blue',
    padding: '1rem',
    textAlign: 'center',
    fontSize: '1rem',
    lineHeight: 'normal',
    float: 'left',
    backgroundColor: 'LightSalmon'
};

const dragElements = [
	{name: "XSOS-228S"},
	{name: "XSOS-576D"},
	{name: "XSOS-1152Q"}
]
export const SystemBox = ({name}) => {
    const [{ canDrop, isOver }, drop] = useDrop({
        accept: ItemTypes.BOX,
        drop: ( mornitor) => ({ name: name }),
        collect: (monitor) => ({
            isOver: monitor.isOver(),
            canDrop: monitor.canDrop(),
        }),
    });
    const isActive = canDrop && isOver;
    
    // if (isActive) {
    //     backgroundColor = 'darkgreen';
    // }
    // else if (canDrop) {
    //     backgroundColor = 'darkkhaki';
    // }
    return (
    <div ref={drop} style={{ ...style}}>
			{isActive ? 'Release to drop' : 'System XSOS'}
            {
                dragElements.map( element => 
                    <Box name={element.name} />
                )
            }
           

	</div>);
};
