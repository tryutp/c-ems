import React, { Component } from 'react'
import { ItemTypes } from "./ItemTypes";
import { useDrag } from "react-dnd";
import './Box.scss'


export const Box = ({name, passedData , passedFunction, onDelete}) => {
    const [{isDragging}, drag] = useDrag({
        item: {name: name, type: 'Our first type'},
        end: (item, monitor) => {
            const dropResult = monitor.getDropResult();
            // console.log(item.name +" to " +dropResult.name)
            // passedData.push({id:4 , name: name })
            passedFunction({id:4 , name: name })
            // if(dropResult && dropResult.name === 'Column 1'){
            //     setIsFirstColumn(true)
            // } else {
            //     setIsFirstColumn(false);
            // }
        },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
        }),
    });

    const opacity = isDragging ? 0.4 : 1;

    return (
        <div ref={drag} className='movable-item' style={{opacity}}>
            {name}
            <button> X </button>
            {/* onClick={onDelete({})} */}
        </div>
    )
}



