import React, { Component } from 'react'
import { useState, useEffect, useRef } from 'react';
import { Redirect, Route } from 'react-router-dom'
import logo from './../../assets/original_logo.png'
import Home from '../Home'
import './Login.scss'
import { Environment } from "../../Environment";
import * as UserService from "../../services/UserService";
import * as InfoService from "../../services/InfoService";
import ModalContainer from 'react-modal-promise'
import { Loading } from "./../../shared/Loading";
import { openLoginConfirmation } from "./../../shared/Confirmation";
import Modal from "react-modal";
import ReactTooltip from 'react-tooltip';

// import * as UserService from "./../../services/UserService";
// import { openConfirmation } from "../../shared/Confirmation";
// import { onUserResponse } from "../../shared/UserResponse";
import * as InformUser from "./../../shared/InformUser";

export class Login extends Component {

    local_ip = localStorage.getItem("ip");


    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            startRedirect: false,
            goToCustomer: false,
            remoteServer: '',
            currentTime: '',
            selectedRemoteServer: '',
            remoteServer: 'local',
            remoteList: '',
            loading: false,
        }
    }

    // componentWillMount(){
    //     UserService.get_remote().then( async data => {
    //         console.log(data)
    //         await this.setState( { remoteServer: data}) ;
    //     });
    //     console.log(this.state.remoteServer)
    //     InfoService.get_current_time().then( async data => {
    //         console.log(data[0])
    //     })
    // }
    componentWillMount() {
        Modal.setAppElement('body');
        // this.setState({ loading: true }, async () => {
        //   // await this.searchRoute()

        //   this.setState({ loading: false })
        // })

    }
    componentDidMount() {
        // UserService.get_remote().then( data => {
        //     console.log(data, typeof(data))
        //     this.setState( { remoteServer: data }) ;
        // });
        // console.log('here')
        this.getRemoteLogin();
    }
    getRemoteLogin() {
        UserService.get_remote_login().then(result => {
            // console.log(result)
            if (result.status === 1) {
                this.setState({ remoteList: result.data });
            }
        })
    }

    validateNoQoute(input){
        
        if(!input.match(/^[^'"]*$/)){
            return true
        }
    }

    async verifyData(username, password) {
		var msg = null;

		// if (!username.match(/^[\w-.]{3,40}$/)) {
		// 	return "Username only allow _ . and - as special character in the range of 3 to 40 characters.";
		// }

        // if (username.length > 40) {
		// 	return "Username only allow _ . and - as special character in the range of 3 to 40 characters.";
		// }

		if (this.validateNoQoute(password) || this.validateNoQoute(username)) {
			return "Single quote and Double quote are not allowed.";
		}

        if(password.length > 100) {
            return "Password is allowed character in the range up to 100 characters."
        }

		return msg;
	}

    handleSubmit = async (event) => {
        var validateMsg = await this.verifyData(this.state.username, this.state.password);
        if(!this.state.username || !this.state.password) {
            InformUser.unsuccess({ text: 'Please input username and password' });
        } else if(validateMsg) {
            InformUser.unsuccess({ text: validateMsg });
        } else {
            event.preventDefault();
            // alert("You are submitting " + this.state.password);
            // console.log(this.state.remoteServer)
            if (this.local_ip == null || this.local_ip == '') {
                this.local_ip = '-';
            }

            this.setState({ loading: true }, async () => {
                // if (this.state.remoteServer === "Local") {
                let remote_server_id = this.state.remoteServer === "local" ? null : this.state.remoteServer;
                await UserService.login(this.state.username, this.state.password, this.local_ip, remote_server_id).then(async result => {
                    // console.log("data1", result.status)
                    if (result.status === 1) {
                        const user = JSON.stringify(result.data)
                        await localStorage.setItem('userData', user)
                        window.location.reload(false);
                        // this.setState({ startRedirect: true }, () => {  });
                    } else if (result.status === 2) {
                        // console.log('status = 2')
                        this.setState({ loading: false }, async () => {
                            const dialog = await openLoginConfirmation({
                                title: "Login Confirmation",
                                text: result.msg,
                                options: result.data
                            })
                            // console.log("yut",dialog)
                            if (dialog) {
                                const tokenTemp = dialog.token;
                                this.setState({ loading: true }, () => {
                                    UserService.logout(tokenTemp).then(async (LogoutResult) => {
                                        // console.log(LogoutResult)
                                        await localStorage.removeItem('userData');
                                        await UserService.login(this.state.username, this.state.password, this.local_ip, remote_server_id).then(async loginResult => {
                                            // console.log(loginResult)
                                            if (loginResult.status === 1) {
                                                const user = JSON.stringify(loginResult.data)
                                                // localStorage.setItem('userData', user)
                                                await localStorage.setItem('userData', user)
                                                window.location.reload(false);
                                            } else {
                                                InformUser.unsuccess({ text: result.msg });
                                                this.setState({ loading: false });
                                            }
                                        })
                                    })
                                })

                            }
                        });

                    } else {
                        InformUser.unsuccess({ text: result.msg });
                        this.setState({ loading: false });
                    }

                })

            })

        } 
        // else {
        //     InformUser.unsuccess({ text: 'Please input username and password' });
        // }
        // if( username === "tryut" && password === "1234"){
        //     localStorage.setItem('userData', JSON.stringify({
        //         username: username,
        //         password: password,
        //         token: "123456789"
        //     }));
        //     await this.setState({ startRedirect : true });
        //     console.log(this.state.startRedirect)
        // }

    }

    inputHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({ [nam]: val });
    }
    // inputSelect=(event)=>{
    //     console.log( Object.entries (this.state.remoteServer[event.target.value] ))
    //     const storeState = Object.entries (this.state.remoteServer[event.target.value] )
    //     this.setState({selectedRemoteServer: storeState });
    // }
    inputSelect = (event) => {
        // console.log(this.state.remoteServer[event.target.value])

        // var tesss = [ this.state.testFormat[event.target.value] ]
        // console.log(tesss)
        // if(this.state.testFormat[event.target.value]){
        this.setState({ selectedRemoteServer: this.state.remoteServer[event.target.value] });
        // }

    }

    forwarding(){
        this.setState({ goToCustomer: true },()=>{
            window.location.reload(false);
        })
    }



    render() {

        if( this.state.goToCustomer ){
            return <Redirect to="/private" />;
        }
        return (

            <div className='container'>
                < ModalContainer />
                {/* < ModalContainer /> */}
                <Loading open={this.state.loading} />
                <div className='container-bg'>
                    <div id="logo-container" className='logo'>
                        <a href="https://xenoptics.com/" target="_blank">
                            <img src={logo} alt='Logo'></img>
                        </a>

                    </div>
                    {/* <button className="btn"> */}
                    {/* <div className="private-btn"> */}
                        <span className="distinct">ELEMENT MANAGEMENT SYSTEM</span>
                        {/* <i class="fas fa-arrow-alt-circle-right"></i>
                    </div> */}
                    
                    {/* </button> */}
                    <div className='form'>
                        <div className='input-group'>
                            {/* <label id="username-label" htmlFor='username'><span>Username</span></label> */}
                            <input id="username-id" type='text' name='username' onChange={this.inputHandler} placeholder='Username' maxlength="40" onKeyPress={event => { if(event.key == 'Enter') {this.handleSubmit(event);} }}></input>
                        </div>
                        <div className='input-group'>
                            {/* <label id="password-label" htmlFor='password'><span>Password</span></label> */}
                            <input id="password-id" type='password' name='password' onChange={this.inputHandler} placeholder='Password' maxlength="100" onKeyPress={event => { if(event.key == 'Enter') {this.handleSubmit(event);} }}></input>
                        </div>
                        <div className="input-group">
                            <select name="remoteServer" id="remote-server" onChange={this.inputHandler}>
                                <option value="local">Local</option>
                                {
                                    this.state.remoteList && this.state.remoteList.map(remote =>
                                        <option value={remote.id}>{remote.server_name}</option>
                                    )
                                }
                            </select>
                        </div>

                        <hr style={{width:"100%"}} />
                        <button id="login-btn" type='submit' className='btn submit login' onClick={this.handleSubmit}><span>Login</span></button>

                        <ReactTooltip id='forwardTooltip' multiline={true} place="left" type="dark" effect="solid" />
                        <span onClick={ ()=>{ this.forwarding() }} data-for="forwardTooltip" data-tip="Customer Private Patching" className="forward-btn"><i class="fas fa-arrow-alt-circle-right"></i></span>





                    </div>
                </div>
                {/* {
                this.state.remoteServer[0].remote_name

            } */}
                {/* {this.state.remoteServer } */}
                { this.state.startRedirect && <Redirect to='' />}


            </div>
        )
    }
}

export default Login
