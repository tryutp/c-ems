import React, { Component } from "react";
import Chart from "chart.js";
import "./Overview.scss";
import { Redirect } from "react-router-dom";
import * as InfoService from "./../../services/InfoService";
import * as CustomerService from "./../../services/CustomerService";
import { openConfirmation } from "../../shared/Confirmation";
import { onUserResponse } from "../../shared/UserResponse";
import * as InformUser from "../../shared/InformUser";
import LinearProgress from "@material-ui/core/LinearProgress";
import { LiveView } from "./../LiveView/LiveView";
import Modal from "react-modal";
import ClipLoader from "react-spinners/ClipLoader";
import ReactTooltip from "react-tooltip";
import { UnitDashboard } from "./UnitDashboard/UnitDashboard";
import InterconSummary from "./InterconSummary/InterconSummary";
import { environment } from "../../Environment";
import { Notification } from "./../Notification/Notification";
import * as TaskService from "./../../services/TaskService";
import moment from "moment";
import { Link } from "react-router-dom";

export class Overview extends Component {
	token = JSON.parse(localStorage.getItem("userData")).token;
	time_offset = JSON.parse(localStorage.getItem("userData")).time_offset;

	portCanvas;

	interval;
	intervalSystemInfo;
	rerenderCondition = {
		available_port: null,
		available_inter_port: null,
		disable_port: null,
	};

	openingUnit;

	constructor(props) {
		super(props);
		this.portChart = null;
		this.state = {
			portSummary: "",

			ConnectionTypePort: "",
			connectionTypePercentage: "",

			availablePort: "",
			availablePercentage: "",

			availableInterconnectPort: "",
			availableInterconnectPercentage: "",
			availableInterconnectPercentage2: "",
			customerCount: "",
			assignedPort: "",

			totalUnitOnline: "",
			totalUnitOffline: "",
			totalUnitAlarm: "",

			operatingUnitList: "",
			unitStatusList: "",
			systemInfo: "",
			systemStatus: "",
			totalPort: "",

			interconThesholdPct: "",
			interconBelowTheshold: "",

			loading: false,
			liveView: false,

			unitDashboardOpen: false,
			interconSummary: false,

			toCustomerManagement: false,
			toConnectivity: false,
			taskList: "",
			notificationModal: false,
			goToElementVisual: false,
			navigatesToTask: false,
		};
	}

	componentDidMount() {
		this.resizeChart();
		window.addEventListener("resize", this.resizeChart);

		this.setState({ loading: true }, async () => {
			await this.getCustomerCount();
			await this.getPortSummary();
			await this.getSystemInfo();
			await this.getSystemStatus();
			await this.getUnitSummary();
			await this.getUnitOverviewStatus();
			await this.getUnitState();
			await this.getTask();

			this.setState({ loading: false }, () => {
				// this.renderPortCanvas();
				this.setupInterval();
			});
		});
	}
	setupInterval() {
		this.interval = setInterval(async () => {
			await this.getCustomerCount();
			await this.getPortSummary();
			await this.getUnitSummary();
			await this.getUnitOverviewStatus();
			await this.getUnitState();
			await this.getSystemInfo();
			await this.getSystemStatus();
			await this.getTask();
		}, 2000);
	}
	clearIntervals() {
		clearInterval(this.interval);
		clearInterval(this.intervalSystemInfo);
	}

	componentWillUnmount() {
		this.clearIntervals();
		window.removeEventListener("resize", this.resizeChart);
	}

	async getTask() {
		await TaskService.get_tasks(this.token).then(async (result) => {
			// console.log("task", result);
			if (result.status == 1) {
				var task = result.data;
				var timeCaledTask = await this.processColumnOfTime(task);

				this.setState({ taskList: timeCaledTask });
			} else {
				InformUser.unsuccess({ text: result.msg });
			}
		});
	}
	async processColumnOfTime(logData) {
		var logs = JSON.parse(JSON.stringify(logData));
		// var  logs = logData;
		for (var i = 0; i < logs.length; i++) {
			const newAddDate = await this.calculateTime(logs[i]["add_date"]);
			// const newStartDate = await this.calculateTime(logs[i]["act_start_date"])
			// const newFinishDate = await this.calculateTime(logs[i]["finish_date"])
			logs[i]["add_date"] = newAddDate;
			// logs[i]["act_start_date"] = newStartDate;
			// logs[i]["finish_date"] = newFinishDate;
		}
		return logs;
	}
	async calculateTime(serverTime) {
		var rawServerTime = await moment(serverTime);
		// console.log(rawServerTime)
		// console.log(timeOffset)
		var offset = moment.duration(this.time_offset);
		// // var temp = moment(server_time).add(1, 'm')
		// console.log(offset)
		var currentTime = await rawServerTime
			.add(offset)
			.format("YYYY-MM-DD HH:mm:ss");
		// console.log(currentTime)
		return currentTime;
		// console.log(currentTime)
	}

	resizeChart = async () => {
		// Set actual size in memory (scaled to account for extra pixel density).
		var scale = window.devicePixelRatio; // Change to 1 on retina screens to see blurry canvas.
		// canvas.width = Math.floor(size * scale);
		// canvas.height = Math.floor(size * scale);
		// console.log(scale)

		var a = (await this.getWidth()) / 200;
		// console.log(a, b )
		if (this.portChart) {
			this.portChart.options.legend.labels.fontSize = a;
			this.portChart.options.legend.labels.boxWidth = a;
			// console.log(this.portChart.options.legend.labels)
			this.portChart.update();
		}
	};

	renderPortCanvas = (portData) => {
		this.resizeChart();
		// console.log(this.state.portSummary)
		this.portCanvas = document.getElementById("portChart");
		this.portChart = new Chart(this.portCanvas, {
			type: "doughnut",
			data: {
				// labels: ['Available', 'Connected', 'Interconnection', 'Interconnected', 'Disable'],
				labels: [
					"Available",
					"Connected",
					"Interconnection",
					"Used Interconnection",
					"Disable",
				],
				datasets: [
					{
						// label: '# of Votes',
						data: portData,
						backgroundColor: [
							"#00CC00",

							"#da1e27",
							"#00BFFF",
							"#551a8b",
							"#CCCCCC",
						],
						borderWidth: 0,
					},
				],
			},
			options: {
				cutoutPercentage: 0,
				responsive: true,
				maintainAspectRatio: true,
				animation: {
					duration: 0,
				},
				// plugins: {
				//     datalabels: {
				//         color: 'black',
				//         anchor: "end",
				//         align: "right",
				//         offset: 10,
				//         font: function(context) {
				//             var width = context.chart.width;
				//             var size = Math.round(width / 32);
				//               return {
				//                 size: size,
				//               weight: 600
				//             };
				//           },
				//     }
				// },
				scales: {},

				legend: {
					position: "left",
					labels: {
						fontSize: 11,
						boxWidth: 38,
						fontColor: "#b3b3b3",
					},
				},
				// pieceLabel: {
				//     render: "value",
				//     fontSize: this.chartFontSize,
				//     arc: false,
				//     fontStyle: 'normal',
				//     fontColor: ['white', 'white', 'white'],
				//     position: 'default',
				//     precision: 0,
				//     mode: 'label',
				// },
			},
		});
	};

	getWidth() {
		return Math.max(
			document.body.scrollWidth,
			document.documentElement.scrollWidth,
			document.body.offsetWidth,
			document.documentElement.offsetWidth,
			document.documentElement.clientWidth
		);
	}
	async getCustomerCount() {
		await CustomerService.get_customer_count(this.token).then((result) => {
			// console.log(result.data)
			if (result && result.status === 1) {
				this.setState({ customerCount: result.data.total_customer });
			}
			if (!result || result.status !== 1) {
				this.setState({ customerCount: 0 });
			}
		});
	}

	async getUnitState() {
		const operating = 3;
		const ready = 2;
		const start = 1;

		InfoService.get_unit_by_all(this.token).then((result) => {
			// console.log("Operating System", result.data);
			if (result.status == 1) {
				if (result.data) {
					this.setState({ operatingUnitList: result.data });
				}
			} else {
				InformUser.unsuccess({ text: result.msg });
			}
		});
	}

	async getPortSummary() {
		await InfoService.get_port_summary(this.token).then(async (result) => {
			// console.log("PortSum", result);
			var rerender = false;

			if (result.status == 1) {
				const data = result.data;

				if (
					this.rerenderCondition.available_port !==
						data.available_port ||
					this.rerenderCondition.available_inter_port !==
						data.available_inter_port ||
					this.rerenderCondition.disable_port !== data.disable_port
				) {
					rerender = true;
					// console.log('found you')
				}

				this.rerenderCondition = {
					available_port: data.available_port,
					available_inter_port: data.available_inter_port,
					disable_port: data.disable_port,
				};

				var portData = await this.formatPortData(result.data);

				if (!this.portChart) {
					await this.renderPortCanvas(portData);
				}

				if (this.portChart) {
					this.portChart.data.datasets[0].data = portData;
					this.portChart.update();
				}

				await this.progressBarData(result.data);
				const {
					available_port,
					available_inter_port,
					connected_port,
					used_inter_port,
					disable_port,
					assigned_port,
					connected_assign_port,
					intercon_theshold_pct,
					intercon_below_theshold
				} = data;
				var temp1 =
					available_port +
					connected_port -
					available_inter_port -
					used_inter_port;
				this.setState({
					portSummary: portData,
					ConnectionTypePort: temp1,
					availablePort: available_port,
					connectedPort: connected_port,
					disablePort: disable_port,
					usedInterconPort: used_inter_port,
					availableInterconnectPort: available_inter_port,
					assignedPort: assigned_port,
					connectedAssignPort: connected_assign_port,
					interconThesholdPct: intercon_theshold_pct,
					interconBelowTheshold: intercon_below_theshold,
				});
			} else {
				InformUser.unsuccess({ text: result.msg });
			}
		});
	}

	async getUnitSummary() {
		await InfoService.get_unit_summary(this.token).then((result) => {
			// console.log("UnitSum", result)
			if (result.status == 1) {
				if (result.data) {
					const {
						total_alarm,
						total_offline,
						total_online,
						total_unit,
					} = result.data;
					this.setState({
						totalUnitOnline: total_online,
						totalUnitAlarm: total_alarm,
						totalUnitOffline: total_offline,
					});
				}
			} else {
				InformUser.unsuccess({ text: result.msg });
			}
		});
	}

	async getUnitOverviewStatus() {
		const unit_per_page = 100;
		const page = 1;
		await InfoService.get_unit_overview_status(
			unit_per_page,
			page,
			this.token
		).then((result) => {
			// console.log("UnitStatusList", result)
			if (result.status == 1) {
				if (result.data) {
					this.setState({ unitStatusList: result.data });
				} else {
					InformUser.unsuccess({ text: "No unit data for overview" });
				}
			} else {
				InformUser.unsuccess({ text: result.msg });
			}
		});
	}

	async getSystemInfo() {
		await InfoService.get_system_info(this.token).then((result) => {
			// console.log("System Info", result);
			if (result && result.status === 1) {
				if (result.data) {
					this.setState({
						systemInfo: result.data,
					});
				} else {
					InformUser.unsuccess({
						text: "No system information for overview",
					});
				}
			} else {
				InformUser.unsuccess({ text: result.msg });
			}
		});
	}
	async getSystemStatus() {
		await InfoService.get_system_status(this.token).then((result) => {
			// console.log("System Status", result);
			if (result && result.status === 1) {
				if (result.data) {
					this.setState({ systemStatus: result.data });
				}
				if (!result.data) {
					this.noSystemStatus();
					InformUser.unsuccess({
						text: "There's no information of system status.",
					});
				}
			}
			if (!result || result.status !== 1) {
				this.noSystemStatus();
				// InformUser.unsuccess({ text: result.msg });
			}
		});
	}
	noSystemStatus() {
		this.setState({
			systemStatus: {
				alarm_count: 0,
				avail_update: 0,
				camera_status: null,
				connection_count: 0,
				last_operate_unit: null,
				last_operate_unit_url: null,
				remain_task: 0,
				server_time: "There's no information of system status.",
			},
		});
	}
	formatPortData(data) {
		// console.log("port data", data)
		return [
			data.available_port,
			data.connected_port,
			data.available_inter_port,
			data.used_inter_port,
			data.disable_port,
		];
	}
	async progressBarData(data) {
		const {
			available_inter_port,
			available_port,
			connected_port,
			disable_port,
			used_inter_port,
		} = data;
		// console.log(available_inter_port, available_port, connected_port, disable_port, used_inter_port)
		const availableNotIntercon = available_port - available_inter_port;
		const connectedNotIntercon = connected_port - used_inter_port;
		const totalPort = available_port + connected_port + disable_port;

		// console.log(connectionPort, connectedPort, totalPort);
		const temp1 = availableNotIntercon + connectedNotIntercon;

		const totalIntercon = available_inter_port + used_inter_port;

		var connectionTypePercentage = (temp1 * 100) / totalPort;

		var availablePercentage = 0; // available per connected
		if (totalPort) {
			availablePercentage = (available_port * 100) / totalPort;
		}
		var availableInterconnectPercentage = 0; // available for interconn per connected intercon
		if (totalIntercon) {
			availableInterconnectPercentage =
				(available_inter_port * 100) / totalIntercon;
		}

		// console.log(available_inter_port, used_inter_port, totalIntercon, availableInterconnectPercentage)

		// console.log(connectionTypePercentage,availablePercentage,availableInterconnectPercentage)

		await this.setState({
			connectionTypePercentage: parseFloat(
				connectionTypePercentage
			).toFixed(1),
			availablePercentage: parseFloat(availablePercentage).toFixed(1),
			totalPort: totalPort,
			totalIntercon: totalIntercon,
			availableInterconnectPercentage: parseFloat(
				availableInterconnectPercentage
			).toFixed(1),
			availableInterconnectPercentage2: totalIntercon,
		});
	}

	statusLightClasses = (status) => {
		if (status === "Active") {
			return " online ";
		} else {
			return " offline ";
		}
	};
	progressBarColorClasses = (status) => {
		// const layoutClass = ;

		// if (status == 3) {
		return " connecting-bar ";
		// } else if (status == 2) {
		//     return " disconnecting-bar "
		// }
	};

	openLiveView() {
		this.setState({ liveView: true });
	}
	closeLiveView = () => {
		if (document.getElementById(`sourceStream-0`)) {
			document.getElementById(`sourceStream-0`).src = "null";
		}
		if (document.getElementById(`sourceStream-1`)) {
			document.getElementById(`sourceStream-1`).src = "null";
		}

		this.setState({ liveView: false });
	};
	openUnitDashboard(unit) {
		// console.log(unit)
		this.openingUnit = unit;
		this.setState({ unitDashboardOpen: true }, () => {
			this.clearIntervals();
		});
	}
	closeUnitDashboard = (port, doForward) => {
		this.setState({ unitDashboardOpen: false }, () => {
			if (doForward) {
				this.elementVisual = port.idx;
				this.setState({ goToElementVisual: true });
			}
			this.setupInterval();
		});
	};

	closeInterconSummary = () => {
		this.setState({ interconSummary: false });
	};
	closeNotificationModal = () => {
		this.setState({ notificationModal: false });
	};

	render() {
		if (this.state.toConnectivity) {
			return <Redirect push to={{ pathname: "/connectivity" }} />;
		}
		if (this.state.toCustomerManagement) {
			return <Redirect push to={{ pathname: "/customer-management" }} />;
		}
		if (this.state.goToElementVisual) {
			return (
				<Redirect
					push
					to={{
						pathname: "/topology",
						state: { elementVisual: this.elementVisual },
					}}
				/>
			);
		}
		if (this.state.navigatesToTask) {
			return <Redirect push to={{ pathname: "/remaining-task" }} />;
		}

		return (
			<div className="overview-container">
				<Modal
					isOpen={this.state.liveView}
					className="live-view-body"
					overlayClassName="default-overlay"
					onRequestClose={this.closeLiveView}
					// closeTimeoutMS={500}
				>
					<LiveView
						close={this.closeLiveView}
						operatingUnit={this.state.operatingUnitList}
					/>
				</Modal>
				<Modal
					isOpen={this.state.unitDashboardOpen}
					className="unit-dashboard-body"
					overlayClassName="white-overlay"
					onRequestClose={() => {
						this.closeUnitDashboard(null, false);
					}}
					// closeTimeoutMS={500}
				>
					<UnitDashboard
						unit={this.openingUnit}
						close={this.closeUnitDashboard}
					/>
				</Modal>
				<Modal
					isOpen={this.state.interconSummary}
					className="notification-body"
					overlayClassName="default-overlay"
					onRequestClose={this.closeInterconSummary}
					// closeTimeoutMS={500}
				>
					<InterconSummary interconThesholdPct={this.state.interconThesholdPct} close={this.closeInterconSummary} />
				</Modal>
				<Modal
					isOpen={this.state.notificationModal}
					className="notification-body"
					overlayClassName="default-overlay"
					onRequestClose={this.closeNotificationModal}
					// closeTimeoutMS={500}
				>
					<Notification close={this.closeNotificationModal} />
				</Modal>
				<header className="h1">
					<h1>OVERVIEW: DASHBOARD</h1>
				</header>
				<div className="dashboard">
					<div className="container-left">
						<div className="div3-container">
							<div className="issue-container">
								<div className="header-section">
									<h2>DASHBOARD</h2>
								</div>
								<div className="issue-group">
									<div className=" lr-container ">
										<div className="left">
											<span>System Alarm</span>
										</div>
										<div
											className="right"
											onClick={() => {
												this.setState({
													notificationModal: true,
												});
											}}
										>
											<span id="total-system-alarm">
												{
													this.state.systemStatus
														.alarm_count
												}
											</span>
										</div>
									</div>
									{/* <div className="lr-container">
                                        <div className="left"><span>Total Connection</span></div>
                                        <div className="right"><span>{this.state.systemStatus.connection_count}</span></div>
                                    </div> */}

									<div className=" lr-container ">
										<div className="left">
											<span>Remaining Task</span>
										</div>
										<div
											className="right"
											onClick={() => {
												this.setState({
													navigatesToTask: true,
												});
											}}
										>
											<span id="total-remaining-task">
												{
													this.state.systemStatus
														.remain_task
												}
											</span>
										</div>
									</div>
									{/* <div className="lr-container disable">
                                        <div className="left"><span>Approval Required</span></div>
                                        <div className="right"><span>{this.state.systemStatus.connection_count}</span></div>
                                    </div> */}
									<div className=" lr-container ">
										<div className="left">
											<span>
												Available Software Update
											</span>
										</div>
										<div className="right">
											<span id="total-software-update">
												{
													this.state.systemStatus
														.avail_update
												}
											</span>
										</div>
									</div>
								</div>
							</div>
							<div className="div3-right">
								<div
									className="port-number-box connection-box connection-bar"
									onClick={() => {
										this.setState({ toConnectivity: true });
									}}
								>
									<div className="header-number-box">
										<h2>AVAILABLE PORTS</h2>
									</div>
									<div className="bar-container">
										<div className="left">
											<span
												id="available-port-percentage"
												className="card-number"
											>
												{this.state.availablePercentage}
											</span>
											<span className="card-number-total">
												%
											</span>
										</div>
										<div className="right">
											<LinearProgress
												onClick={() => {
													this.setState({
														toConnectivity: true,
													});
												}}
												variant="determinate"
												value={
													this.state
														.availablePercentage
												}
											/>
										</div>
									</div>
									<hr />
									<div className="detail-row">
										<div className="detail-box">
											<div className="header flex">
												<span>Available Ports</span>
											</div>
											<div className="content">
												<h2
													id="total-available-ports"
													className="sec-number"
												>
													{this.state.availablePort}
												</h2>
												<h2
													id="total-ports"
													style={{ opacity: "0.5" }}
												>
													/{this.state.totalPort}
												</h2>
											</div>
										</div>
										<div className="detail-box">
											<div className="header flex">
												<span>Connected Ports</span>
											</div>
											<div className="content">
												<h2
													id="connected-ports"
													className="sec-number"
												>
													{this.state.connectedPort}
												</h2>
											</div>
										</div>

										<div className="detail-box">
											<div className="header flex">
												<span>Disabled Ports</span>
											</div>
											<div className="content">
												<h2
													id="disabled-ports"
													className="sec-number"
												>
													{this.state.disablePort}
												</h2>
											</div>
										</div>
									</div>
								</div>
								<div
									className="port-number-box connection-box intercon-bar"
									onClick={() => {
										this.setState({
											interconSummary: true,
										});
									}}
								>
									<div className="header-number-box">
										<h2>INTERCONNECTIONS</h2>
									</div>
									<div className="bar-container intercon">
										<div className="left">
											<span
												id="intercon-percentage"
												className="card-number"
											>
												{
													this.state
														.availableInterconnectPercentage
												}
											</span>
											<span className="card-number-total">
												%
											</span>
										</div>
										<div className="right">
											<LinearProgress
												variant="determinate"
												value={
													this.state
														.availableInterconnectPercentage
												}
											/>
										</div>
									</div>
									<hr />
									<div className="detail-row">
										<div className="detail-box">
											<div className="header flex">
												<span>Available Ports</span>
											</div>
											<div className="content">
												<h2
													id="available-intercon-ports"
													className="sec-number"
												>
													{
														this.state
															.availableInterconnectPort
													}
												</h2>
												<h2
													id="total-intercon=ports"
													style={{ opacity: "0.5" }}
												>
													/{this.state.totalIntercon}
												</h2>
											</div>
										</div>
										<div className="detail-box">
											<div className="header flex">
												<span>
													Below Theshold Links({this.state.interconThesholdPct}%)
												</span>
											</div>
											<div className="content">
												<h2
													className="sec-number"
													id="below-threshold-link"
												>
													{this.state.interconBelowTheshold}
												</h2>
											</div>
										</div>
										<div className="detail-box">
											<div className="header flex">
												<span>Connected Ports</span>
											</div>
											<div className="content">
												<h2
													id="connected-intercon-ports"
													className="sec-number"
												>
													{
														this.state
															.usedInterconPort
													}
												</h2>
											</div>
										</div>
									</div>
								</div>
								<div
									className="port-number-box interconnection-box intercon-bar"
									onClick={() => {
										this.setState({
											toCustomerManagement: true,
										});
									}}
								>
									<div className="header-number-box">
										<h2>CUSTOMERS</h2>
									</div>
									<div className="bar-container">
										<div className="left">
											<span
												id="total-customer-account"
												className="card-number"
											>
												{this.state.customerCount}
											</span>
											{/* <span className="card-number-total">%</span> */}
										</div>
										<div className="right">
											{/* <LinearProgress variant="determinate" value={this.state.availableInterconnectPercentage} /> */}
											<span> Accounts</span>
										</div>
									</div>
									<hr />
									<div className="detail-row">
										<div className="detail-box">
											<div className="header flex">
												<span>Allocated Ports</span>
											</div>
											<div className="content">
												<h2
													id="available-allocated-ports"
													className="sec-number"
												>
													{this.state.assignedPort -
														this.state
															.connectedAssignPort}
												</h2>
												<h2
													id="total-allocated-port"
													style={{ opacity: "0.5" }}
												>
													/{this.state.assignedPort}
												</h2>
											</div>
										</div>
										<div className="detail-box">
											<div className="header flex">
												<span>Used Ports</span>
											</div>
											<div className="content">
												<h2
													id="used-allocated-port"
													className="sec-number"
												>
													{
														this.state
															.connectedAssignPort
													}
												</h2>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div className="below-port">
							<div className="operation-container">
								<div className="port-summary">
									<div className="port-canvas">
										<div className="header-section">
											<h2>PORT SUMMARY</h2>
										</div>
										<div className="v-center-container">
											<div className="limiting-size">
												<canvas id="portChart"></canvas>
											</div>
										</div>
									</div>
									<div className="live-view-section">
										<div className="lr-container">
											<div className="left">
												<div className="header-section">
													<h2
														style={{
															marginRight: "1vw",
														}}
													>
														LIVE VIEW{" "}
													</h2>
													{this.state.systemStatus
														.camera_status ===
														1 && (
														<span>
															({" "}
															{
																this.state
																	.systemStatus
																	.last_operate_unit
															}{" "}
															)
														</span>
													)}
												</div>
											</div>
											<div className="right">
												<button
													onClick={() => {
														this.openLiveView();
													}}
													className="submit sm-btn"
													style={{ margin: "0" }}
												>
													<span>Live View</span>
												</button>
											</div>
										</div>
										<div className="live-view-box">
											{this.state.systemStatus
												.camera_status === 1 && (
												<img
													id="last-operate-unit"
													src={
														!this.state.liveView
															? this.state
																	.systemStatus
																	.last_operate_unit_url
															: "null"
													}
													alt=""
												/>
											)}
											{this.state.systemStatus
												.camera_status !== 1 && (
												<spann
													id="camera-offline"
													className="camera-offline"
												>
													OFFLINE
												</spann>
											)}
										</div>
									</div>
								</div>
							</div>
							<div className="util-container">
								<div className="lr-container margin-bottom">
									<div className="left">
										<h2>UNITS OPERATING</h2>
									</div>
									<div className="right"></div>
								</div>
								<div className="operating-section">
									<div className="unit-visual-container">
									
										{this.state.operatingUnitList &&
											this.state.operatingUnitList.map(
												(unit, index) => (
													<div
														key={`model-unit-${index}`}
														id={`model-unit-${index}`}
														className="unit-container"
												
														onClick={() => {
															this.openUnitDashboard(
																unit
															);
														}}
													>
														
														<div
															style={{
																backgroundImage:
																	"url(" +
																	environment.api +
																	unit.img_path +
																	")",
															}}
															className="layer layer-front"
														>
															

															<span
																data-for="modelNameTooltip"
																data-tip={
																	unit.name
																		.length >
																	4
																		? unit.name
																		: ""
																}
																id="model-unit-name"
																className="unit-name"
															>
																{unit.name}
															</span>
															{/* <span>{unit.progress}</span>
                                                        <span className="percentt">%</span> */}
															{unit.state === 3 &&
																unit.progress !==
																	0 && (
																	<div
																		className={
																			this.progressBarColorClasses(
																				unit.state
																			) +
																			" progress-bar-container "
																		}
																	>
																		<span className="text-center status">
																			{
																				unit.progress
																			}
																			%
																		</span>
																		<LinearProgress
																			striped
																			variant="determinate"
																			value={
																				unit.progress
																			}
																		/>
																	</div>
																)}
														</div>
														<div className="layer layer-back"></div>
														<div className="layer layer-left"></div>
														<div className="layer layer-right"></div>
														<div className="layer layer-top"></div>
														<div className="layer layer-bottom"></div>
													</div>
												)
											)}
									</div>
									<div className="operating-table">
										<div className="lr-container margin-bottom">
											<div className="left">
												<h2>REMAINING TASKS</h2>
											</div>
											<div className="right">
												<Link to="/remaining-task">
													<button
														id="view-more-btn"
														style={{ margin: "0" }}
														className="sm-btn submit"
													>
														<span
															style={{
																margin: "0",
															}}
														>
															View more
														</span>
													</button>
												</Link>
											</div>
										</div>
										<div className="table-size">
											<table id="remaining-task-table">
												<tr className="tr-head">
													<th
														style={{ width: "5%" }}
														className="text-center"
													>
														<span>No.</span>
													</th>
													<th
														style={{ width: "10%" }}
														className="text-center"
													>
														<span>Operation</span>
													</th>
													<th
														style={{ width: "20%" }}
														className="text-center"
													>
														<span>Description</span>
													</th>
													<th
														style={{ width: "15%" }}
														className="text-center"
													>
														<span>Date</span>
													</th>
													<th
														style={{ width: "5%" }}
														className="text-center"
													>
														<span>Add by</span>
													</th>
													<th
														style={{ width: "15%" }}
														className="text-center"
													>
														<span>Status</span>
													</th>
													<th
														style={{ width: "15%" }}
														className="text-center"
													>
														<span>Progress</span>
													</th>
													<th
														style={{ width: "15%" }}
														className="text-center"
													>
														<span>Remarks</span>
													</th>
												</tr>
												{this.state.taskList.length >
													0 && (
													<ReactTooltip
														id="remark-tooltip"
														html={true}
														className="remarkClass defaultTooltip"
														arrowColor="#404040"
														place="top"
														effect="solid"
													/>
												)}
												{this.state.taskList &&
													this.state.taskList.map(
														(unit, index) => (
															<tr
																key={`task-list-${index}`}
																id={`task-list-${index}`}
															>
																<td
																	id={`id-${index}`}
																	className="text-center"
																>
																	<span>
																		{
																			unit.no
																		}
																	</span>
																</td>
																<td
																	id={`operation-name-${index}`}
																	className="text-center"
																>
																	<span>
																		{
																			unit.operation_name
																		}{" "}
																	</span>
																</td>
																<td
																	id={`description-${index}`}
																	style={{
																		overflowX:
																			"auto",
																	}}
																	className="text-center"
																>
																	<span>
																		{
																			unit.description
																		}
																	</span>
																</td>
																<td
																	id={`add-date-${index}`}
																	className="text-center"
																>
																	<span>
																		{
																			unit.add_date
																		}
																	</span>
																</td>
																<td
																	id={`add-by-${index}`}
																	className="text-center"
																>
																	<span>
																		{
																			unit.add_by
																		}
																	</span>
																</td>

																<td
																	id={`status-desc-${index}`}
																	className="text-center"
																>
																	<span>
																		{
																			unit.status_desc
																		}
																	</span>
																</td>
																<td
																	id={`progress-bar-${index}`}
																>
																	<div className="progress-container">
																		<div
																			className={
																				this.progressBarColorClasses(
																					unit.state
																				) +
																				" progress-bar-container "
																			}
																		>
																			<LinearProgress
																				striped
																				variant="determinate"
																				value={
																					unit.progress
																				}
																			/>
																		</div>
																		<span
																			id={`progress-no-${index}`}
																			className="text-center"
																		>
																			{
																				unit.progress
																			}
																			%
																		</span>
																	</div>
																</td>
																<td
																	id={`remark-${index}`}
																	className="text-center"
																>
																	{/* {
																		( unit.remark.length > 11 ) && 
																	}
																	 */}

																	<span
																		data-for="remark-tooltip"
																		data-html={
																			true
																		}
																		data-tip={
																			unit
																				.remark
																				.length >
																			18
																				? `<span>${unit.remark}</span>`
																				: ""
																		}
																		className="ellipsis-text"
																		style={{
																			width: "7.5vw",
																		}}
																	>
																		{
																			unit.remark
																		}
																	</span>
																</td>
															</tr>
														)
													)}
											</table>
											{this.state.taskList.length <=
												0 && (
												<div
													id="no-remaining-task"
													className="no-remaining-task"
												>
													<span className="camera-offline">
														NO TASK
													</span>
												</div>
											)}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="container-right">
						<div className="unit-summary">
							<div className="unit-detail">
								<div className="header-section">
									<h2>UNIT STATUS</h2>
								</div>
								<div className="unit-number-container">
									<div className="unit-number-box">
										<div className="sm header-number-box ">
											<h3>Online</h3>
										</div>
										<div className="number-box">
											<span
												id="total-online-unit"
												className="card-number"
											>
												{this.state.totalUnitOnline}
											</span>
										</div>
									</div>
									<div className="unit-number-box">
										<div className=" sm header-number-box">
											<h3>Offline</h3>
										</div>
										<div className="number-box">
											<span
												id="total-offline-unit"
												className="card-number"
											>
												{this.state.totalUnitOffline}
											</span>
										</div>
									</div>
									<div className="unit-number-box">
										<div className="sm header-number-box">
											<h3>Alarm</h3>
										</div>
										<div className="number-box">
											<span
												id="total-alarmed-unit"
												className="card-number"
											>
												{this.state.totalUnitAlarm}
											</span>
										</div>
									</div>
								</div>
								<div className="unit-list-size">
									{this.state.unitStatusList && (
										<ReactTooltip
											id="alarmTooltip"
											multiline={true}
											arrowColor="#ff8800"
											className="errorClass defaultTooltip"
											place="top"
											// type="warning"
											effect="solid"
										/>
									)}
									{this.state.unitStatusList && (
										<ReactTooltip
											id="unitNameTooltip"
											arrowColor="#404040"
											className="portClass defaultTooltip"
											place="right"
											// type="warning"
											effect="solid"
										/>
									)}
									{this.state.unitStatusList &&
										this.state.unitStatusList.map(
											(unit, index) => (
												<div
													key={"unit-list-" + index}
													id={"unit-list-" + index}
													className="unit-profile"
												>
													<div
														className="unit-left"
														onClick={() => {
															this.openUnitDashboard(
																unit
															);
														}}
													>
														{/* <div className="image-profile">
                                                    <img src={unit.img_path} alt="" />
                                                </div> */}
														<div className="content">
															<span
																id={`unit-list-name-${index}`}
																className="overflow-name ellipsis-text"
																style={{
																	width: "10vw",
																}}
																data-for="unitNameTooltip"
																data-tip={
																	unit.name
																		.length >
																	18
																		? unit.name
																		: ""
																}
															>
																{unit.name}
															</span>
															<span
																id={`unit-list-model-${index}`}
															>
																{unit.model}
															</span>
														</div>
													</div>

													{
														<div
															className={
																(!unit.alarm_msg
																	? " hide "
																	: "  ") +
																" alarm-container "
															}
														>
															<div
																id={`unit-list-alarm-${index}`}
																data-for="alarmTooltip"
																data-tip={`${unit.alarm_msg}`}
																className="alarm-tag"
															>
																<span>
																	ALARM
																</span>
															</div>
														</div>
													}
													<div className="status">
														{unit.status ===
															"Connecting" && (
															<ClipLoader
																color="#B3B3B3"
																css={{
																	animation:
																		"animation-s8tf20-no-shrink 0.75s 0s infinite linear",
																}}
																loading={true}
																size={15}
															/>
														)}
														{unit.status !==
															"Connecting" && (
															<div
																id={`unit-list-light-${index}`}
																className={
																	this.statusLightClasses(
																		unit.status
																	) +
																	" status-light "
																}
															></div>
														)}
														<ReactTooltip
															id="errorTooltip"
															arrowColor="#ff8800"
															className="errorClass defaultTooltip"
															place="top"
															// type="warning"
															effect="solid"
														/>
														{unit.status ===
															"Inactive" && (
															<span
																id={`unit-list-inactive-${index}`}
																data-for="errorTooltip"
																data-tip={`${unit.error_msg}`}
																style={{
																	marginLeft:
																		"0.2vw",
																}}
															>
																{unit.status}
															</span>
														)}
														{unit.status !==
															"Inactive" && (
															<span
																id={`unit-list-active-${index}`}
																style={{
																	marginLeft:
																		"0.2vw",
																}}
															>
																{unit.status}{" "}
															</span>
														)}
													</div>
												</div>
											)
										)}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Overview;
