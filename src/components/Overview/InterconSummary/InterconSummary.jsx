import React, { Component } from 'react'
import './InterconSummary.scss'
import * as InfoService from "./../../../services/InfoService";
import { Loading } from "./../../../shared/Loading";

export class InterconSummary extends Component {


    token = JSON.parse(localStorage.getItem('userData')).token;


    constructor(props){
        super(props);
        this.state = {
            interconThesholdPct: this.props.interconThesholdPct,
            interconList: '',
            loading: false
        }
    }

    componentDidMount(){
        this.setState({ loading: true }, async ()=>{
            await this.getInterconSummary();
            this.setState({ loading: false });
        })
    }

    async getInterconSummary(){
        await InfoService.get_intercon_summary(this.token).then(async result => {
            // console.log(result.data)
            const tableData = await this.calculateTableData(result.data);
            if(result.status === 1){
                this.setState({ interconList: tableData })
            }
        })
    }

    calculateTableData(data){
        var tableData = [];
        data.map( intercon => {
            var avail = (intercon.total_intercon - intercon.used_intercon);
            var avail_percentage = ( avail * 100) /intercon.total_intercon;
            if( (avail_percentage % 1) !== 0 ){
                avail_percentage = parseFloat(avail_percentage).toFixed(2);
            }
            const td = {
                "description": `${intercon.unit_name1} - ${intercon.unit_name2}`,
                "used_intercon": avail+ ` / ${intercon.total_intercon}`,
                "avail_percentage": avail_percentage,
            }
            tableData.push(td);
            


        })
        tableData.sort(function(a, b){return a.avail_percentage - b.avail_percentage});
        return tableData;
    }

    spanStyle(percentage){
        if(percentage <= 20){
            return " text-danger "
        } else if(percentage < this.state.interconThesholdPct) {
            // below theshold
            return " text-warning "
        }
    }

    render() {
        return (
            <div className="intercon-summary-container">
                <Loading open={this.state.loading} />
                <button onClick={()=>{ this.props.close()}} className="close fit-btn"><i class="fas fa-times"></i></button>
                <header className="h2"><h2>Interconnection Summary</h2></header>
                <div className="content">
                    <div className="table-container">
                        <div className="table-size">
                            <table id="intercon-summary-table">
                                <tr>
                                    <th><span>Interconnection</span></th>
                                    <th><span>Available Interconnection (Link)</span></th>
                                    <th><span>Available Percentage</span></th>
                                </tr>
                                {
                                    this.state.interconList && this.state.interconList.map( (intercon, index) =>
                                        <tr key={`intercon-list-${index+1}`}  id={`intercon-list-${index}`} >
                                            <td id={`intercon-name-${index+1}`} className="text-center"><span >{intercon.description}</span></td>
                                            <td id={`intercon-proportion-${index+1}`} className="text-center"><span >{intercon.used_intercon}</span></td>
                                            <td id={`intercon-available-percentage-${index+1}`} className="text-center"><span className={ this.spanStyle(intercon.avail_percentage)}>{intercon.avail_percentage}%</span></td>
                                        </tr>
                                        )
                                }
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default InterconSummary
