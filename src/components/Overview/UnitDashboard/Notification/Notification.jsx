import React, { Component } from 'react'
// import * as InfoService from "./../../services/InfoService";
import './Notification.scss'
import moment from "moment";
export class Notification extends Component {

    token = JSON.parse(localStorage.getItem('userData')).token
    time_offset = JSON.parse(localStorage.getItem("userData")).time_offset;

    interval;

    constructor(props){
        super(props);
        this.state = {
            notiData: '',
        }
    }

    async componentDidMount(){
        // this.getAllNotification();
        // this.interval = setInterval(() => {
        //     this.getAllNotification();
        // }, 3000);
        var notification = this.props.data;
        var timeCaledLogs = await this.processAddDateOfTime(notification);
                        // console.log(timeCaledLogs)
        this.setState({ notiData: timeCaledLogs });
    }

    async processAddDateOfTime(logData) {
		// var  logs = JSON.parse(JSON.stringify(logData));
		var logs = logData;

		for (var i = 0; i < logs.length; i++) {
			const newAddDate = await this.calculateTime(logs[i]["noti_date"]);
			logs[i]["noti_date"] = newAddDate;
		}
		// await logs.map( async  (log,index) => {
		//     const newAddDate = await this.calculateTime(log.add_date)
		//     const newStartDate = await this.calculateTime(log.start_date)
		//     const newFinishDate = await this.calculateTime(log.finish_date)

		//     logs[index]["start_date"] = newStartDate;
		//     logs[index]["finish_date"] = newFinishDate;

		//     // console.log(newTime)
		//     // await temp.push(newTime)
		// } )
		// console.log(logs)
		// console.log(logs)
		return logs;

		// return temp;
	}

    async calculateTime(serverTime) {
		var rawServerTime = await moment(serverTime);
		// console.log(rawServerTime)
		// console.log(timeOffset)
		var offset = moment.duration(this.time_offset);
		// // var temp = moment(server_time).add(1, 'm')
		// console.log(offset)
		var currentTime = await rawServerTime
			.add(offset)
			.format("YYYY-MM-DD HH:mm:ss");
		console.log(currentTime)
		return currentTime;
		// console.log(currentTime)
	}
    componentWillUnmount(){
        // if(this.interval){ clearInterval(this.interval)}
    }
    // async getAllNotification() {
    //     await InfoService.get_all_notifications(this.token).then( result => {
    //         console.log(result)
    //         if(result.status == 1){
    //             this.setState({ notiData: result.data })
    //         }
    //     })
    // }
    render() {

        const { data, name } = this.props;
    // console.log(data)

        return (
            <div className="notification-container">
                <button onClick={()=>{ this.props.close()}} className="close fit-btn"><i class="fas fa-times"></i></button>
                <header><h2>{name} Notifications</h2></header>
                <div className="table-container">
                    <table>
                        <tr className="tr-head">
                            <th><span>Unit</span></th>
                            <th><span>Error Code</span></th>
                            <th><span>Description</span></th>
                            <th><span>Date & Time</span></th>
                            <th><span>Severity</span></th>
                        </tr>
                        
                        {
                            data && data.map( noti =>
                                <tr>
                                    <td className="text-center"><span>{noti.noti_from}</span></td>
                                    <td className="text-center"><span>{noti.error_code}</span></td>
                                    <td className="text-center"><span>{noti.error_name}</span></td>
                                    <td className="text-center"><span>{noti.noti_date}</span></td>
                                    <td className="text-center"><span>{noti.severity}</span></td>



                                </tr>
                                )
                        }
                        
                    </table>
                </div>
            </div>
        )
    }
}

export default Notification
