import React, { Component } from 'react'
import * as UnitService from "./../../../services/UnitService";
import './UnitDashboard.scss'
import Chart from 'chart.js';
import { Loading } from "./../../../shared/Loading";
import Modal from 'react-modal';
import { Notification } from "./Notification/Notification";
import LinearProgress from '@material-ui/core/LinearProgress';
import { openConfirmation } from "./../../../shared/Confirmation";
import ReactTooltip from 'react-tooltip';
import { ElementVisual } from "./../../ElementVisual/ElementVisual";
import { environment } from '../../../Environment';

export class UnitDashboard extends Component {

    token = JSON.parse(localStorage.getItem('userData')).token;
    portCanvas;
    
    interval;
    interval2;
    interval3;


    constructor(props) {
        super(props);
        this.portChart = null;
        this.state = {
            portPanel: '',
            unitStatus: '',
            unitPortSummary: '',
            unitState: '',
            unitInfo: '',
            unitNotification: '',
            portSummary: '',
            notificationModal: false,

            loading: false,
            openElementVisual: false,
        }
    }

    chartFontSize = this.getWidth() / 168;
    boxWidthSize = this.getWidth() / 50;

    componentDidMount() {
        // console.log(this.props.unit)

        this.setState({ loading: true }, async () => {
            await this.getUnitPortPanel()
            this.getUnitStatus()
            this.getUnitPortSummary()
            this.getUnitState();
            this.getUnitInfo();
            this.getUnitNotification()
            this.setState({ loading: false }, () => {
                this.setupInterval()
            })
        })




    }

    setupInterval(){
        this.interval = setInterval(() => {
            this.getUnitPortSummary()
            this.getUnitPortPanel()
            this.getUnitStatus()
        }, 2000)
        this.interval2 = setInterval(() => {
            this.getUnitState();
        }, 5000)
        this.interval3 = setInterval(() => {
            this.getUnitNotification()
        }, 10000)
    }
    clearIntervals(){
        clearInterval(this.interval) 
        clearInterval(this.interval2) 
        clearInterval(this.interval3) 
    }
    componentWillUnmount() {
        this.clearIntervals()
    }
    


    getWidth() {

        return Math.max(
            document.body.scrollWidth,
            document.documentElement.scrollWidth,
            document.body.offsetWidth,
            document.documentElement.offsetWidth,
            document.documentElement.clientWidth
        );
    }
    chunk = (arr, size) => arr.reduce((acc, e, i) => (i % size ? acc[acc.length - 1].push(e) : acc.push([e]), acc), []);

    async getUnitPortPanel() {
        // console.log(this.props.unit)
        await UnitService.get_unit_port_panel(this.props.unit.unit_id, this.token).then(async result => {
            // console.log("port", result)
            if (result.status === 1) {

                const rowAmount = 6;
                var groupAmount = 4;

                const portPerRow = result.data.length / rowAmount;

                var array = await this.chunk(result.data, portPerRow);
                var f_array = [];

                // for( var i = 0 ; i < rowAmount ; i++ ){
                //     var arrayTemp = await this.chunk(array[i], groupAmount) ;
                //     f_array.push(arrayTemp)
                // }

                // array.fore
                await array.map( async row => {
                    var arrayTemp = await this.chunk(row, groupAmount) ;
                    f_array.push(arrayTemp)
                })

                // console.log(array)
                this.setState({ portPanel: f_array })
            } 
            else if( result.status === 401 ) {

            }
        })
    }
    async getUnitStatus() {
        await UnitService.get_unit_status(this.props.unit.unit_id, this.token).then(result => {
            // console.log(result)
            if (result.status === 1) {
                this.setState({ unitStatus: result.data })
            }
        })
    }
    async getUnitNotification() {
        await UnitService.get_unit_notification(this.props.unit.unit_id, this.token).then(result => {
            // console.log(result)
            if (result.status === 1) {
                this.setState({ unitNotification: result.data })
            }
        })
    }
    async getUnitInfo() {
        await UnitService.get_unit_info(this.props.unit.unit_id, this.token).then(result => {
            // console.log(result)
            if (result.status === 1) {
                this.setState({ unitInfo: result.data })
            }
        })
    }
    async getUnitPortSummary() {
        await UnitService.get_unit_port_summary(this.props.unit.unit_id, this.token).then( async result => {
            // console.log(result.data)
            if (result.status === 1) {

                var portData = this.formatPortData(result.data);

                if( !this.portChart ){
                    await this.renderPortCanvas(portData)
                }

                if( this.portChart ){
                    this.portChart.data.datasets[0].data = portData;
                    this.portChart.update()
                }
                
                this.setState({
                    unitPortSummary: result.data,
                    portSummary: portData,
                })
            }
        })
    }
    formatPortData(data) {
        // console.log("port data", data)
        return [
            data.available_port,
            // data.available_inter_port,
            data.connected_port,
            // data.used_inter_port,
            data.disable_port,
        ]
    }
    renderPortCanvas(portData) {
        this.portCanvas = document.getElementById('portCharts');
        this.portChart = new Chart(this.portCanvas, {
            type: 'doughnut',
            data: {
                // labels: ['Available', 'Connected', 'Interconnection', 'Interconnected', 'Disable'],
                labels: ['Available', 'Connected', 'Disable'],
                datasets: [{
                    // label: '# of Votes',
                    data: portData,
                    backgroundColor: [
                        '#00CC00',

                        '#da1e27',
                        // '#00BFFF',
                        // '#551a8b',
                        '#CCCCCC',
                    ],
                    borderWidth: 0
                }]
            },
            options: {
                cutoutPercentage: 0,
                // responsive: true,
                // maintainAspectRatio: false,
                scales: {

                },

                legend: {
                    position: "left",
                    labels: {
                        fontSize: this.chartFontSize,
                        boxWidth: this.boxWidthSize,
                        fontColor: '#b3b3b3'
                    },
                },
                // pieceLabel: {
                //     render: "value",
                //     fontSize: this.chartFontSize,
                //     arc: false,
                //     fontStyle: 'normal',
                //     fontColor: ['white', 'white', 'white'],
                //     position: 'default',
                //     precision: 0,
                //     mode: 'label',
                // },
                responsive: true,
            },

        });
    }
    async getUnitState() {
        await UnitService.get_unit_state(this.props.unit.unit_id, this.token).then(result => {
            // console.log("sate", result.data)
            if (result.status === 1) {
                this.setState({ unitState: result.data })
            }
        })
    }

    activeStatusLight() {
        // console.log(this.state.unitStatus.active_status)
        switch (this.state.unitStatus.active_status) {
            case -1:
                return " offline ";
                break;
            case 1:
                return " online ";
                break;
            case 0:
                return " offline ";
                break;

            default:
                return " offline ";
                break;
        }

    }
    lightStyleClasses(port, isSource) {

        var defaultStyle = " port ";
        var operation = port.port_operation_id;
        // var intercon = port.
        // console.log(port)
        if (isSource) {
            // if (port.idx === this.state.selectedSource.idx) {
            //     defaultStyle = " selected port ";
            // }
            // if (port.smu_type === this.state.selectedTarget.smu_type) {
            //     defaultStyle = defaultStyle + " same-type "
            // }
            if(port.reserved === 1){
                defaultStyle += " disabled "
            }

            switch (operation) {
                case -1:
                    return defaultStyle += " connected "
                    break;
                case 0:
                    return defaultStyle + " disabled "
                    break;
                case 1:
                    return defaultStyle + " available "
                    break;
                case 2:
                    return defaultStyle + " connecting "
                    break;
                case 3:
                    return defaultStyle + " disconnecting "
                    break;
                case 4:
                    return defaultStyle + " recovering "
                    break;
                case 5:
                    return defaultStyle + " locking "
                    break;
                case 11:
                    return defaultStyle + " available_intercon "
                    break;
                case 12:
                    return defaultStyle + " interconnecting "
                    break;
                case 13:
                    return defaultStyle + " used_intercon "
                    break;
                case 14:
                    return defaultStyle + " disinterconnecting "
                    break;

                default:
                    return defaultStyle
            }
        } else {
            // if (port.idx === this.state.selectedTarget.idx) {
            //     defaultStyle = " selected port ";
            // }
            // if (port.smu_type === this.state.selectedSource.smu_type) {
            //     defaultStyle = defaultStyle + " same-type "
            // }
            if(port.reserved === 1){
                defaultStyle += " disabled "
            }
            switch (operation) {
                case -1:
                    return defaultStyle += " connected "
                    break;
                case 0:
                    return defaultStyle + " disabled "
                    break;
                case 1:
                    return defaultStyle + " available "
                    break;
                case 2:
                    return defaultStyle + " connecting "
                    break;
                case 3:
                    return defaultStyle + " disconnecting "
                    break;
                case 4:
                    return defaultStyle + " recovering "
                    break;
                case 5:
                    return defaultStyle + " locking "
                    break;
                default:
                    return defaultStyle
            }
        }





    }

    stateStyles() {
        var state = this.state.unitState.state;
        switch (state) {
            case 1:
                return " STATE_READY "
                break;
            case 2:
                return " STATE_INITIAL "
                break;
            case 3:
                return " STATE_OPERATING "
                break;
            case 4:
                return " STATE_STOP "
                break;

            default:
                return " STATE_READY "
                break;
        }
    }


    async showDetailConnection(port) {
        // console.log(port)
        var operation = port.port_operation_id;
        if ((operation === -1) || (operation === 13)) {  //Connected ad Used_intercon
            const dialog = await openConfirmation({
                title: "Connection Path",
                text: `Do you want to see connection path of ${port.description}`
            })
            if(dialog){
                // this.setState({ openElementVisual: true })
                this.props.close(port, true)
            }

        }


    }
    closeElementVisual = () =>{
        this.setState({ openElementVisual: false})
    }
    // tooltipDescription(port) {
    //     var description = `${port.description}`;
    //     if (port.reserved === 1) {
    //         description = description + `<br/>Reserved by ${port.reserved_change_by}`;
    //     }
    //     if (port.port_operation_id === -1 && (port.connected_task_desc)) {
    //         if (port.connected_task_desc) {
    //             description = description + `<br/>Connected to ${port.connected_name}<br/>Task: ${port.connected_task_desc}`;

    //         } else {
    //             description = description + `<br/>Connected to ${port.connected_name}`;

    //         }
    //     }

       
    //     // description = description + `<br/>Connected Count: ${port.connected_count}`;
    //     return description;
    // }

    tooltipDescription(port) {

        var description = `Description:  ${port.description}`;
        
        // description = description + `<br/>Unit:  ${port.unit_name} ( ${port.smu_type_desc} )`;
        description = description + `<br/>Unit:  ${this.props.unit.name} ( ${port.smu_type_desc} )`;

        description = description + `<br/>Connected Count:  ${port.connected_count}`;

        if (port.reserved === 1) {
            description = description + `<hr/>Reserved by ${port.reserved_change_by}`;
        }
        
        if (port.port_operation_id === -1 && (port.connected_task_desc)) {
            if (port.connected_task_desc) {
                description = description + `<hr/>Connected to ${port.connected_name}<br/>Task: ${port.connected_task_desc}`;

            } else {
                description = description + `<br/>Connected to ${port.connected_name}`;

            }
        }
        return `<span>${description}</span>`;
    }

    openNotification() {
        this.setState({ notificationModal: true })
    }
    closeNotification = () => {
        this.setState({ notificationModal: false })
    }

    layoutClasses(index){
        if( index%4 === 0 ){

            if( (index !== 48) && (index !== 96) && (index !== 144) && (index !== 192) && (index !== 240) && (index !== 288) ){
                return " group-space "
            }
        }
    }

    render() {
        const { unit } = this.props;
        const { alarm_msg, noti_count, timestamp, timezone, uptime } = this.state.unitStatus;
        // console.log(unit)
        return (
            <div className="unit-dashboard-container">


                {/* <Loading open={this.state.loading} /> */}
                <Loading open={this.state.loading} />
                <Modal
                    isOpen={this.state.notificationModal}
                    className="unit-notification-body"
                    overlayClassName="default-overlay"
                    onRequestClose={this.closeNotification}
                // closeTimeoutMS={500}
                >
                    <Notification name={unit.name} data={this.state.unitNotification} close={this.closeNotification} />

                </Modal>
                <Modal
                    isOpen={this.state.openElementVisual}
                    className="unit-notification-body"
                    overlayClassName="default-overlay"
                    onRequestClose={this.closeElementVisual}
                // closeTimeoutMS={500}
                >
                    <ElementVisual close={this.closeElementVisual} />

                </Modal>
                
                <div className="header">
                    <div className="left">
                        <div className="image-profile">
                            <div className="image-size">
                                <img id="unit-image" src={environment.api + unit.img_path} alt="" />
                            </div>
                        </div>
                        <div className="profile-info">
                            <div className="upper">
                                <h1 id="unit-name">{unit.name}</h1> <div className={this.activeStatusLight() + " active-status-light "}></div>
                            </div>
                            <span id="unit-model">{unit.model}</span>
                        </div>
                        <div className="progress-section">
                            <div className={this.stateStyles() + " progress-btn "}>
                                <span style={{ marginRight: "0.2vw" }}>Status:</span>

                                {
                                    (this.state.unitState.state !== 3) && <span id="unit-status"><strong>{this.state.unitState.state_name}</strong></span>
                                }
                                {
                                    (this.state.unitState.state === 3) && <div className=" progress-bar-section ">

                                        <div className={"connecting-bar progress-bar-container "}>
                                            <span className="status" style={{ marginRight: "0.2vw" }}><strong>{this.state.unitState.state_name}</strong></span>
                                            <LinearProgress striped variant="determinate" value={this.state.unitState.progress} />
                                        </div>
                                        <span id="unit-progress-percent" className="text-center">{this.state.unitState.progress}%</span>
                                    </div>
                                }
                                {/* this.progressBarColorClasses(unit.state) + */}

                            </div>
                        </div>
                        {
                            alarm_msg && <div className="alarm-section">
                                <div className="alarm-btn">
                                    <span id="unit-alarm" ><strong>{alarm_msg}</strong></span>
                                </div>
                            </div>
                        }
                    </div>
                    <div className="right">
                        <div className="noti-section">
                            <button id="notification-panel-btn" onClick={() => { this.openNotification() }} className="btn-tab"><i class="fas fa-bell noti-icon-margin"></i> ( {this.state.unitNotification.length} )</button>
                            <button id="dialog-close-btn" onClick={ ()=>{ this.props.close(null, false) }} className="btn-tab close-tab"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                </div>
                <div className="content">
                    <div className="port-section">


                        {
                            unit['model_id'] === "2" && <div className="panel-size">
                            {
                                (this.state.portPanel.length > 0) && <ReactTooltip id='unitTooltip' place="top" html={true} className="portClass defaultTooltip" arrowColor="#404040" effect="solid" />
                            }
                        
                            {
                                this.state.portPanel && this.state.portPanel.map((row, rowIndex) =>
                                    <div className="row" key={`row-${rowIndex+1}`}  id={`row-${rowIndex+1}`} >
                                        {
                                            row && row.map((group, groupIndex) =>

                                                <div key={`port-${groupIndex+1}`} id={`port-${groupIndex+1}`} className="group model-576d">
                                                    {

                                                        group && group.map( (port,portIndex ) => 
                                                            <div
                                                            key={`port-${portIndex+1}`}
                                                            id={`port-${portIndex+1}`}
                                                            data-tip={this.tooltipDescription(port)}
                                                            data-for='unitTooltip'
                                                            onClick={() => { this.showDetailConnection(port) }} 
                                                            className={this.lightStyleClasses(port, true) + " model-576d "}
                                                            // className={this.state.selectedSource.idx === port.idx ? "selected port" : "port"}
                                                            >
                                                                <span>{port.front_no}</span>
                                                                <div className="light-box">
                                                                    <div className="light-status"></div>
                                                                </div>
    
                                                            </div>
    
                                                        
                                                        )
                                                    }

                                                </div>

                                                
                                               

                                            )
                                        }

                                    </div>



                                )
                            }
                             </div>

                        }
                        {
                            !["2","21"].includes(unit['model_id']) && <div className="panel-size">
                            {
                                (this.state.portPanel.length > 0) && <ReactTooltip id='unitTooltip' place="top" html={true} className="portClass defaultTooltip" arrowColor="#404040" effect="solid" />
                            }
                        
                            {
                                this.state.portPanel && this.state.portPanel.map((row, rowIndex) =>
                                    <div className="row" key={`row-${rowIndex+1}`}  id={`row-${rowIndex+1}`} >
                                        {
                                            row && row.map((group, groupIndex) =>

                                                <div key={`port-${groupIndex+1}`} id={`port-${groupIndex+1}`} className="group">
                                                    {

                                                        group && group.map( (port,portIndex ) => 
                                                            <div
                                                            key={`port-${portIndex+1}`}
                                                            id={`port-${portIndex+1}`}
                                                            data-tip={this.tooltipDescription(port)}
                                                            data-for='unitTooltip'
                                                            onClick={() => { this.showDetailConnection(port) }} 
                                                            className={this.lightStyleClasses(port, true)}
                                                            // className={this.state.selectedSource.idx === port.idx ? "selected port" : "port"}
                                                            >
                                                                <span>{port.front_no}</span>
                                                                <div className="light-box">
                                                                    <div className="light-status"></div>
                                                                </div>
    
                                                            </div>
    
                                                        
                                                        )
                                                    }

                                                </div>

                                                
                                               

                                            )
                                        }

                                    </div>



                                )
                            }
                             </div>

                        }
                        
                        {
                            unit['model_id'] === "21" && <div className="panel-size">
                            {
                                (this.state.portPanel.length > 0) && <ReactTooltip id='unitTooltip' place="top" html={true} className="portClass defaultTooltip" arrowColor="#404040" effect="solid" />
                            }
                        
                            {
                                this.state.portPanel && this.state.portPanel.map((row, rowIndex) =>
                                    <div className="row" key={`row-${rowIndex+1}`}  id={`row-${rowIndex+1}`} >
                                        {
                                            row && row.map((group, groupIndex) =>

                                                <div key={`port-${groupIndex+1}`} id={`port-${groupIndex+1}`} className="group model-288d">
                                                    {

                                                        group && group.map( (port,portIndex ) => 
                                                            <div
                                                            key={`port-${portIndex+1}`}
                                                            id={`port-${portIndex+1}`}
                                                            data-tip={this.tooltipDescription(port)}
                                                            data-for='unitTooltip'
                                                            onClick={() => { this.showDetailConnection(port) }} 
                                                            className={this.lightStyleClasses(port, true)}
                                                            // className={this.state.selectedSource.idx === port.idx ? "selected port" : "port"}
                                                            >
                                                                <span>{port.front_no}</span>
                                                                <div className="light-box">
                                                                    <div className="light-status"></div>

                                                                    <div className="light-status"></div>
                                                                </div>
                                                            </div>
    
                                                        
                                                        )
                                                    }

                                                </div>

                                                
                                               

                                            )
                                        }

                                    </div>



                                )
                            }
                             </div>

                        }

                    </div>

                    <div className="info-section">
                        <div className="info-status section">

                            <div style={{ marginBottom: "0.5vw" }} className="summary sub-section">
                                <div className="number-container">
                                    <span id="available-port" className="main-number">
                                        {this.state.unitPortSummary.available_port}
                                    </span>
                                    <span className="description">
                                        Available Ports
                                </span>

                                </div>

                                <div className="number-container">
                                    <span id="connected-port" className="main-number">
                                        {this.state.unitPortSummary.connected_port}
                                    </span>
                                    <span className="description">
                                        Connected Ports
                                </span>

                                </div>

                                <div className="number-container">
                                    <span id="disabled-port" className="main-number">
                                        {this.state.unitPortSummary.disable_port}
                                    </span>
                                    <span className="description">
                                        Disabled Ports
                                </span>

                                </div>
                            </div>
                            <div className="envi sub-section">
                                <div className="number-container">
                                    <span id="dust-particle" className="main-number">
                                        {this.state.unitStatus.dust}
                                    </span>
                                    <span className="description"> Dust Particle </span>
                                    <span className="description">(μg/m³)</span>

                                </div>

                                <div className="number-container">
                                    <span id="temperature" className="main-number">  {this.state.unitStatus.temperature}</span>
                                    <span className="description">Temperature</span>
                                    <span className="description">(°C)</span>

                                </div>

                                <div className="number-container">
                                    <span id="humidity" className="main-number">
                                        {this.state.unitStatus.humidity}
                                    </span>
                                    <span className="description">Humidity</span>
                                    <span className="description">(%)</span>

                                </div>
                            </div>
                        </div>
                        <div className="task-section section">
                            <h2 className="header-text">Port Summary</h2>
                            <div className="port-canvas">
                                {/* <div className="header-section">
                                    <h2>PORT SUMMARY</h2>
                                </div> */}
                                <div className="v-center-container">
                                    <div className="limiting-size">
                                        <canvas id="portCharts" ></canvas>
                                    </div>
                                </div>

                            </div>
                        </div>
                        {/* <div className="envi-section section">

                        </div> */}

                        <div className="port-summary section">
                            <h2 className="header-text">System Information</h2>
                            <div className="content">
                                <div className="input-group">
                                    <div className="left">
                                        <span>IP Address :</span>
                                    </div>
                                    <div className="right">
                                        <span id="unit-ip-address">{this.state.unitInfo.ip_address}</span>
                                    </div>
                                </div>
                                {/* <div className="input-group">
                                    <div className="left">
                                        <span>MAC Address :</span>
                                    </div>
                                    <div className="right">
                                        <span id="unit-mac-address">{this.state.unitInfo.mac_address}</span>
                                    </div>
                                </div> */}
                                <div className="input-group">
                                    <div className="left">
                                        <span>Software Version :</span>
                                    </div>
                                    <div className="right">
                                        <span id="unit-software-version">{this.state.unitInfo.sw_version}</span>
                                    </div>
                                </div>
                                <div className="input-group">
                                    <div className="left">
                                        <span>Serial No. :</span>
                                    </div>
                                    <div className="right">
                                        <span id="unit-serial-no">{this.state.unitInfo.serial_no}</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div className="footer">
                    <span id="unit-time" >{timestamp} {timezone}</span>
                </div>

            </div>
        
        )
    }
}

export default UnitDashboard
