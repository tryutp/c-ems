import React, { Component, useState } from "react";
import "./NetworkSetting.scss";
import { Loading } from "./../../shared/Loading";
import * as SystemService from "./../../services/SystemService";
import Datetime from "react-datetime";
import "react-datetime/css/react-datetime.css";
import * as InformUser from "./../../shared/InformUser";
import { openConfirmation } from "./../../shared/Confirmation";
import { openCountdownDialog } from "./../../shared/CountdownDialog/CountdownDialog";
import { Redirect, Route } from 'react-router-dom'
import * as UserService from "./../../services/UserService";
import moment from "moment";
import Modal from "react-modal";
import { createModal } from "react-modal-promise";
import * as InfoService from "./../../services/InfoService";
// import ProgressBar from "@ramonak/react-progress-bar";
import UpdateFirmwareProgress from "./UpdateFirmwareProgress";

// const hiddenFileInput = React.useRef(null);
export class NetworkSetting extends Component {
    token = JSON.parse(localStorage.getItem("userData")).token;
    time_offset = JSON.parse(localStorage.getItem("userData")).time_offset;

    fileFirmwareInput = React.createRef();
    

    constructor(props) {
        super(props);
        this.state = {
            ip_address_setting: 1,

            timetype: "",
            timezone: "",
            timezoneWithOffset: "",
            timeserver: "",
            timeserver2: "",
            timeserver3: "",
            timezoneList: "",
            // datetime: moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),
            // datetime: new Date(),
            datetime: "",
            currentDatetime: new Date(),
            static_ip: "",
            dtcp_ip: "",
            subnet: "",
            dns1: "",
            dns2: "",
            dns3: "",
            gateway: "",
            loading: false,
            redirectToLogin:false,
            firmwareData: {
                version: "",
                date: "",
                features: "",
                loading_time: ""
            },
            openFirmwareDetailsModal: false,
            systemInfo: "",
            server_time: "",
            openFirmwareProgressModal: false,
            isUpdateNetwork: false
            // formFirmwareFile: null,
        };
    }

    componentDidMount() {
        // console.log('window interval ',window.interval)
        this.setState({ loading: true }, async () => {
            await this.getDatetimeSetting();
            await this.getNetworkSetting();
            await this.getTimezoneList();
            // await this.getCurrentDatetime();
            await this.getSystemInfo();
            this.setState({ loading: false });
        });
    }

    async getSystemInfo() {
        await InfoService.get_system_info(this.token).then(result => {
            if (result.status == 1) {
                if (result.data) {
                    this.setState({
                        systemInfo: result.data,
                        server_time: result.data.server_time
                    }, ()=>{
                        // this.calculateTime();
                        this.getCurrentDatetime();
                    })
                } else {
                    InformUser.unsuccess({ text: "No system information" })
                }
            }
        })
    }

    async getDatetimeSetting() {
        await SystemService.get_datetime_setting(this.token).then((result) => {
            // console.log("datetime", result.data)
            if (result.status === 1) {
                const {
                    timezone,
                    timeserver1,
                    timeserver2,
                    timeserver3,
                    timetype,
                    timezone_id,
                } = result.data;
                this.setState({
                    timetype: timetype,
                    timezone: timezone_id,

                    timeserver: timeserver1,
                    timeserver2: timeserver2,
                    timeserver3: timeserver3,
                });
            }
        });
    }
    async getNetworkSetting() {
        await SystemService.get_network_setting(this.token).then((result) => {
            // console.log("network", result.data)

            if (result.status === 1) {
                const {
                    dns1,
                    dns2,
                    dns3,
                    gateway,
                    ip_type,
                    static_ip,
                    subnet,
                } = result.data;
                this.setState({
                    ip_address_setting: ip_type,
                    static_ip: static_ip,
                    subnet: subnet,
                    dns1: dns1,
                    dns2: dns2,
                    dns3: dns3,
                    gateway: gateway,
                });
            }
        });
    }

    async getTimezoneList() {
        await SystemService.get_timezone_list(this.token).then((result) => {
            // console.log("timezone", result.data)
            if (result.status === 1) {
                this.setState(
                    {
                        timezoneList: result.data,
                    },
                    () => {
                        result.data.map((time) => {
                            if (time.id === this.state.timezone) {
                                this.setState({
                                    timezoneWithOffset: time.offset,
                                });
                            }
                        });
                    }
                );
            }
        });
    }
    getCurrentDatetime() {
        var server_time = this.state.server_time;
        var offset = moment.duration(this.time_offset)

        const datetime = moment(server_time).add(offset).format("YYYY-MM-DD HH:mm:ss");
        this.setState({
            currentDatetime: server_time,
            datetime: new Date(datetime),
        })

    }

    validateIPAddress(ipAddress) {
        if(!ipAddress.match(/^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/gm)){
            return false;
        }
        return true;
    }

    validateNoQoute(input){
        if(!input.match(/^[^'"]*$/)){
            return false;
        }
        return true;
    }

    // validateSpecialChar(input) {
    //     var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
    //     if(format.test(input)){
    //         return true;
    //     } 
    //     return false;
    // }

    validateName(input) {
		if (!input.match(/^[\w-.]{0,255}$/)) {
			return false;
		}
        return true;
	}

    async saveNetwork() {
        const staticSetting = {
            static_ip: this.state.static_ip,
            subnet: this.state.subnet,
            gateway: this.state.gateway,
            dns1: this.state.dns1,
            dns2: this.state.dns2,
            dns3: this.state.dns3,
        };

        if (this.state.ip_address_setting === 1) {
            const dialog = await openConfirmation({
                title: "Automatic IP Address Setting",
                text: "Are you sure you want to set the IP address automatically with DHCP?",
            });
            if (dialog) {
                this.setState({ loading: true}, ()=>{
                    SystemService.save_network_dhcp(this.token).then((result) => {
                        this.setState({ loading: false})
                        if (result.status === 1) {
                            this.setState({isUpdateNetwork: true});
                            InformUser.success({ text: result.msg });
                        } else {
                            this.setState({ ip_address_setting : 2 });
                            InformUser.unsuccess({ text: result.msg });
                        }
                        
                    });
                })
            }
        } else if (this.state.ip_address_setting === 2) {
            let error_field;
            error_field = !error_field && (staticSetting.static_ip && !this.validateIPAddress(staticSetting.static_ip)) ? 'IP Address' : null;
            error_field = !error_field && (staticSetting.subnet && !this.validateIPAddress(staticSetting.subnet)) ? 'Subnetmask' : error_field;
            error_field = !error_field && (staticSetting.gateway && !this.validateIPAddress(staticSetting.gateway)) ? 'Gateway' : error_field;

            let error_no_qoute;
            if( !error_field &&
                ((staticSetting.dns1 && !this.validateNoQoute(staticSetting.dns1)) || 
                (staticSetting.dns2 && !this.validateNoQoute(staticSetting.dns2)) || 
                (staticSetting.dns3 && !this.validateNoQoute(staticSetting.dns3)))
            ) {
                error_no_qoute = true;
            }

            if(error_field) {
                InformUser.unsuccess({ text: error_field+' is invalid format' });
            } else if(error_no_qoute) {
                InformUser.unsuccess({ text: 'Single quote and Double quote are not allowed.' });
            } else {
                const dialog = await openConfirmation({
                    title: "Static IP Address Setting",
                    text: "Are you sure you want to change the static IP Address setting?",
                });
                if (dialog) {
                    this.setState({ loading: true}, ()=>{
                        SystemService.save_network_static(
                            staticSetting,
                            this.token
                        ).then((result) => {
                            this.setState({ loading: false})
                            if (result.status === 1) {
                                this.setState({isUpdateNetwork: true});
                                InformUser.success({ text: result.msg });
                            } else {
                                this.setState({ ip_address_setting : 1 });
                                InformUser.unsuccess({ text: result.msg });
                            }
                        });
                        
                    })
                }
            }
        }
    }

    async saveDatetime() {
        const timezone_id = this.state.timezone;
        const offset = this.state.timezoneWithOffset;

        const timeserver = this.state.timeserver;
        const timeserver2 = this.state.timeserver2;
        const timeserver3 = this.state.timeserver3;
        const datetime = this.state.datetime;

        if (this.state.timetype === 1) {
            let ntp_error = false;
            if(
                (timeserver && !this.validateName(timeserver)) ||
                (timeserver2 && !this.validateName(timeserver2)) ||
                (timeserver3 && !this.validateName(timeserver3))
            ) {
                ntp_error = true;
            }

            if(!timeserver) {
                InformUser.unsuccess({ text: 'NTP Server 1 is required' });
            } else if(ntp_error) {
                InformUser.unsuccess({ text: 'NTP Server only allow _ . and - as special character' });
            } else {
                const dialog = await openConfirmation({
                    title: "NTP Server Setting",
                    text: `Are you sure you want to set the date and time automatically with ${timeserver}, ${timeserver2}, and ${timeserver3}?`,
                });
                // console.log(timeserver)
                if (dialog) {
                    this.setState({ loading: true}, async ()=>{
                        await SystemService.save_datetime_ntp(
                            timeserver,
                            timeserver2,
                            timeserver3,
                            timezone_id,
                            this.token
                        ).then(async (result) => {
                            this.setState({ loading: false})
                            if (result.status === 1) {
                                await SystemService.save_timeOffset(offset, this.token).then(
                                    (result) => {
                                        if (result.status === 1) {
                                            this.changeTimeOffset(offset);
                                        }
                                    }
                                );
                                InformUser.success({ text: result.msg });
                            } else {
                                InformUser.unsuccess({ text: result.msg });
                            }
                            
                        });
                        
                    })
                    
                }
            }
        } else if (this.state.timetype === 2) {
            var offsetDateTime = moment.duration(offset);
            // var offsetDateTime = moment.duration(this.time_offset);
            // // var temp = moment(server_time).add(1, 'm')
            // console.log(offset)
            var currentTime = await moment(datetime)
                .subtract(offsetDateTime)
                .format("YYYY-MM-DD HH:mm:ss");

            if(currentTime == 'Invalid date') {
                InformUser.unsuccess({ text: 'Invalid datetime format' });
            } else {
                const dialog = await openConfirmation({
                    title: "Manual Date and Time Setting",
                    text: "Are you sure you want to manually change the date and time setting?",
                });
                // var currentTime = moment(datetime).format("YYYY-MM-DD HH:mm:ss")
                // console.log(currentTime)
                if (dialog) {
                    // var offsetDateTime = moment.duration(this.time_offset);
                    // const utc_time = 
                    // console.log('send time to backend ',currentTime,offset,offsetDateTime,this.time_offset)
                    this.setState({ loading: true}, async ()=>{
                        SystemService.save_datetime_manual(
                            currentTime,
                            timezone_id,
                            this.token
                        ).then(async (result) => {
                            this.setState({ loading: false})
                            if (result.status === 1) {
                                await SystemService.save_timeOffset(offset, this.token).then(
                                    (result) => {
                                        if (result.status === 1) {
                                            this.changeTimeOffset(offset);
                                        }
                                    }
                                );
                                InformUser.success({ text: result.msg });
                            } else {
                                InformUser.unsuccess({ text: result.msg });
                            }
                        });
                        

                    })
                    
                }
            }
        }
    }

    changeTimeOffset(offset) {
        this.time_offset = offset; // current time_offset
        var userObj = JSON.parse(localStorage.getItem("userData"));
        userObj["time_offset"] = offset;
        // console.log(userObj)
        localStorage.setItem("userData", JSON.stringify(userObj));
        
    }

    // async saveTimezone() {
    //     const timezone_id = this.state.timezone;
    //     const offset = this.state.timezoneWithOffset;

    //     const timeserver = this.state.timeserver;
    //     const timeserver2 = this.state.timeserver2;
    //     const timeserver3 = this.state.timeserver3;
    //     const datetime = this.state.datetime;

    //     const dialog = await openConfirmation({
    //         title: "Timezone Setting",
    //         text: "The timezone will update along with the current data and time automatically. Are you sure you want to change timezone setting?",
    //     });
    //     // console.log(offset)
    //     if (dialog) {
    //         await SystemService.save_timezone(timezone_id, this.token).then(
    //             (result) => {
    //                 // inform nothing
    //             }
    //         );
    //         await SystemService.save_timeOffset(offset, this.token).then(
    //             (result) => {
    //                 if (result.status === 1) {
    //                     InformUser.success({ text: result.msg });
    //                     this.changeTimeOffset(offset);
    //                 } else {
    //                     InformUser.unsuccess({ text: result.msg });
    //                 }
    //             }
    //         );

    //         // Save date and time too
    //         if (this.state.timetype === 2) {
    //             var offsetDateTime = moment.duration(this.time_offset);
    //             // // var temp = moment(server_time).add(1, 'm')
    //             // console.log(offset)
    //             var currentTime = await moment(datetime)
    //                 .subtract(offsetDateTime)
    //                 .format("YYYY-MM-DD HH:mm:ss");

    //             // var currentTime = moment(datetime).format("YYYY-MM-DD HH:mm:ss")
    //             // console.log(currentTime)
    //             if (dialog) {
    //                 SystemService.save_datetime_manual(
    //                     currentTime,
    //                     this.token
    //                 ).then((result) => {
    //                     if (result.status !== 1) {
    //                         InformUser.unsuccess({ text: result.msg });
    //                     }
    //                 });
    //             }
    //         }
    //     }
    // }

    onIntegerChange = (event) => {
        // console.log(event)
        if (event.target.name === "timetype") {
            this.setState({
                [event.target.name]: parseInt(event.target.value),
                currentDatetime: new Date(),
            });
        } else {
            this.setState({
                [event.target.name]: parseInt(event.target.value),
            });
        }
    };
    onStringChange = (event) => {
        // console.log(event)

        this.setState({ [event.target.name]: event.target.value });
    };
    onTimezoneChange(event) {
        // console.log(event)
        const timezone_id = parseInt(event.target.value);
        var timezones = this.state.timezoneList;
        var newOffset = "";

        timezones.map((timezone) => {
            if (timezone.id === timezone_id) {
                newOffset = timezone.offset;
                // console.log(newOffset, timezone.id, event.target.value)
            }
        });

        this.setState({
            [event.target.name]: event.target.value,
            timezoneWithOffset: newOffset,
        });
    }
    onDatetimeChange = (event) => {
        const datetime = moment(event).format("YYYY-MM-DD HH:mm:ss");
        this.setState({ datetime: datetime });
    };

    async rebootSystem() {
        const countdownDuration = 10;

        const dialog = await openConfirmation({
            title: "System Reboot",
            text: `Are you sure you want to reboot the system?`,
        });
        if (dialog) {


            const countdown = await openCountdownDialog({
                title: "System Reboot",
                text: `The system will reboot in `,
                countdownTime: countdownDuration,
            });

            if (countdown) {
                if (countdown === "confirm") {
                    SystemService.reboot_system(this.token);
                    // await localStorage.removeItem("userData");
                    setTimeout(async (result) => {
                        // this.logout()
                        // InformUser.success({ text: result.msg})
                        // await localStorage.removeItem("userData");
                        // this.setState({ redirectToLogin:true })
                        this.countdownToRefresh()
                    }, 1000);
                }
            }
           
        }
    }

    async countdownToRefresh(){
        const countdownDuration = 60;
        const countdown = await openCountdownDialog({
            title: "System Reboot",
            text: `The system will be ready again in `,
            countdownTime: countdownDuration,
            cantClose: true,
        });
        // console.log('debug countdown to reload',countdown)
        // if (!countdown) {
            // console.log('reload')
            window.location.reload(false);
        // }
    }
    async shutdownSystem() {
        const countdownDuration = 10;
        const dialog = await openConfirmation({
            title: "System Shutdown",
            text: `Are you sure you want to shutdown the system?`,
        });
        if (dialog) {
            const countdown = await openCountdownDialog({
                title: "System Shutdown",
                text: `The system will shutdown in `,
                countdownTime: countdownDuration,
            });
            if (countdown) {
                if (countdown === "confirm") {
                    SystemService.shutdown_system( countdownDuration, this.token ).then( async result => {
                        if(result.status === 1){
                            InformUser.success({ text: result.msg})
                            await localStorage.removeItem("userData");
                            this.setState({ redirectToLogin:true },()=>{
                                window.close()
                            })
                        }else{
                            InformUser.unsuccess({ text: result.msg })
                        }
                    })
                    
                }
            }
        }
    }

    async delay(milsec) {
        return new Promise( res => setTimeout(res, milsec) );
    }

    closeUpdateModal = async () => {
        this.setState({ openFirmwareProgressModal: false });
        window.location.reload(false);
    };

    onFileSelectFirmware(event) {
        if (event.target.files.length > 0) {
          this.submitFirmwareFile(event.target.files[0]);
          // this.onSubmit()
          event['target']['value'] = null;
        }
      }

    async submitFirmwareFile(file) {
        const dialog = await openConfirmation({
            title: "Update Firmware",
            text: "Are you sure you want to update the firmware?",
        });
        if (dialog) {
            const formData = new FormData();
            formData.append('firmware_file', file);
            this.setState({ loading: true})
            SystemService.upload_firmware(formData, file.type, file.size, file, this.token).then(async (result) => {
                // console.log('Debug result upload',result)
                // result.data = {
                //     "version": "3.2.1.1",
                //     "data": "07 Feb 2023",
                //     "features": [
                //         "----- Feature -----",
                //         "Improve web UI 288D model",
                //         "----- Bug Fix -----",
                //         "Fixed port recovering status change with front_no instead of idx",
                //         "Method parameter for create connection through API"
                //     ],
                //     "loading_time": 150
                // }
                this.setState({ loading: false })
                if(result.status === 1) {
                    this.setState({firmwareData: result.data})
                    const dialog_update = await openFirmwareDetails({
                        firmwareData: result.data,
                    })
                    // console.log('Debug dialog update',dialog_update)
                    if( dialog_update ){
                        this.clearIntervalToken(); // stop check token
                        this.setState({loading: true})
                        let error_msg;
                        SystemService.execute_firmware(1,this.token).then((result) => {
                            if(result.status != 1) {
                                error_msg = result.msg;
                            } 
                        })
                        await this.delay(2000);
                        this.setState({loading: false})
                        if(!error_msg) {
                            this.setState({openFirmwareProgressModal: true})
                        } else {
                            InformUser.unsuccess({ text: error_msg })
                            await this.delay(2000);
                            window.location.reload(false);
                        }
                        // await openFirmwareProgress({firmwareData: result.data})
                        
                    } else {
                        SystemService.execute_firmware(0,this.token).then((result) => {
                        })
                    }
                } else {
                    InformUser.unsuccess({ text: result.msg });
                }
            })
        }
    }

    clearIntervalToken() {
        // console.log('test clear interval')
        if(window.interval != null) {
            clearInterval(window.interval)
        }
    }

    click(){
        
    }
    async logout() {
        UserService.logout(this.token).then((result) => {
            // console.log(result)
            if (result.status == 1) {
                localStorage.removeItem("userData");
                this.setState({ redirect: true }, () => {
                    window.location.reload(false);
                });
            }
        });
    }

    clickUploadFirmware(event) {
        this.fileFirmwareInput.current.click();
    }

    render() {
        
        if(this.state.redirectToLogin){
            return <Redirect to="/" />;
        }
        return (
            <div className="network-setting-container">
                <header className="h1">
                    <h1>SETTING: GENERAL</h1>
                </header>
                <div className="content-container">
                    <div className="content">
                        <header className="h2">
                            <h2>System Operation</h2>
                        </header>
                        <hr />
                        <div className="operation-container">
                            <span>Appliance</span>
                            <hr />
                            <div className="button-container appliance">
                                <div className="input-group">
                                    <div className="left">
                                        <button
                                            id="reboot-btn"
                                            onClick={() => {
                                                this.rebootSystem();
                                            }}
                                            className="btn submit"
                                        >
                                            <span>Reboot</span>
                                        </button>
                                    </div>
                                    <div className="right">
                                        <span>Reboot the system</span>
                                    </div>
                                </div>
                                <div className="input-group">
                                    <div className="left">
                                        <button
                                            id="shutdown-btn"
                                            onClick={() => {
                                                this.shutdownSystem();
                                            }}
                                            className="btn submit"
                                        >
                                            <span>Shutdown</span>
                                        </button>
                                    </div>
                                    <div className="right">
                                        <span>Shutdown the system</span>
                                    </div>
                                </div>
                                <div className="input-group">
                                    <div className="left">
                                    {/* <button onClick={handleClick}>
                                        Upload a file
                                    </button>
                                    <input
                                        type="file"
                                        ref={this.fileFirmwareInput}
                                        onChange={(event) => {this.onFileSelectFirmware(event)}}
                                        style={{display: 'none'}} /> */}
                                        {/* <form [formGroup]="this.form">
                                        <input id="upload_ca" type="file" name="avatar" (change)="onFileSelect($event)"
                                            [disabled]="this.checkInputDisabled()" style="display: none" />
                                    </form>
                                    <div class="browse_btn">
                                        <label id="btn-upload-firmware" for="upload_ca"
                                            [ngClass]="this.permission =='Read-Only' ? 'select_file_btn_disabled' : '' "
                                            (click)="this.checkInputClick()" class="
                                            select_file_btn">Upload Firmware</label>
                                    </div> */}
                                    <form>
                                        <input id="upload_ca" ref={this.fileFirmwareInput} type="file" name="avatar" onChange={(event) => {this.onFileSelectFirmware(event)}}
                                            style={{display: "none"}} />
                                             {/* style={{display: "none"}}  */}
                                    </form>
                                        <button
                                            id="upfirmware-btn"
                                            for="upload_ca"
                                            onClick={() => {
                                                this.clickUploadFirmware();
                                            }}
                                            style={{padding: "0.5vw 0.5vw"}}
                                            className="btn submit"
                                        >
                                            <span>Update Firmware</span>
                                        </button>
                                    </div>
                                    <div className="right">
                                        <span>Update the system firmware</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/* <div className="input-group">

                            <header className="h3"><h3>Appliance</h3></header>
                            <hr />
                            <div className="left"> </div>
                            <div className="right">
                                
                            </div>
                        </div> */}
                    </div>
                    <div className="content">
                        <header className="h2">
                            <h2>Network</h2>
                        </header>
                        <hr />
                        <div className="input-group ip-address-setup">
                            <div className="left">
                                <span>IP Address Setting:</span>
                            </div>
                            <div className="right">
                                <div className="sub-group">
                                    <input
                                        type="radio"
                                        className="radio"
                                        name="ip_address_setting"
                                        id="static_ip"
                                        value={2}
                                        checked={
                                            this.state.ip_address_setting === 2
                                        }
                                        onChange={(event) => {
                                            this.onIntegerChange(event);
                                        }}
                                    />
                                    <label htmlFor="static_ip">
                                        <span>Static IP Address</span>
                                    </label>
                                </div>
                                <div className="sub-group">
                                    <input
                                        type="radio"
                                        className="radio"
                                        name="ip_address_setting"
                                        id="dhcp_ip"
                                        value={1}
                                        checked={
                                            this.state.ip_address_setting === 1
                                        }
                                        onChange={(event) => {
                                            this.onIntegerChange(event);
                                        }}
                                    />
                                    <label htmlFor="dhcp_ip">
                                        <span>Automatic IP Address (DHCP)</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div className="input-group ip-address-setup popup">
                            <div className="left">
                                <span>Static IP Address:</span>
                            </div>
                            <div className="right">
                                <div className="input-group">
                                    <div className="left">
                                        {" "}
                                        <span>IP Address:</span>
                                    </div>
                                    <div className="right">
                                        <input
                                            className={
                                                this.state
                                                    .ip_address_setting === 1
                                                    ? " disabled "
                                                    : ""
                                            }
                                            disabled={
                                                this.state
                                                    .ip_address_setting === 1
                                                    ? true
                                                    : false
                                            }
                                            type="text"
                                            id="static_ip"
                                            name="static_ip"
                                            value={this.state.static_ip}
                                            onChange={this.onStringChange}
                                        />
                                    </div>
                                </div>
                                <div className="input-group">
                                    <div className="left">
                                        {" "}
                                        <span>Subnetmask:</span>
                                    </div>
                                    <div className="right">
                                        <input
                                            className={
                                                this.state
                                                    .ip_address_setting === 1
                                                    ? " disabled "
                                                    : ""
                                            }
                                            disabled={
                                                this.state
                                                    .ip_address_setting === 1
                                                    ? true
                                                    : false
                                            }
                                            type="text"
                                            id="subnet"
                                            name="subnet"
                                            value={this.state.subnet}
                                            onChange={this.onStringChange}
                                        />
                                    </div>
                                </div>
                                <div className="input-group">
                                    <div className="left">
                                        {" "}
                                        <span>Gateway:</span>
                                    </div>
                                    <div className="right">
                                        <input
                                            className={
                                                this.state
                                                    .ip_address_setting === 1
                                                    ? " disabled "
                                                    : ""
                                            }
                                            disabled={
                                                this.state
                                                    .ip_address_setting === 1
                                                    ? true
                                                    : false
                                            }
                                            type="text"
                                            id="gateway"
                                            name="gateway"
                                            value={this.state.gateway}
                                            onChange={this.onStringChange}
                                        />
                                    </div>
                                </div>
                                <div className="input-group">
                                    <div className="left">
                                        {" "}
                                        <span>DNS1:</span>
                                    </div>
                                    <div className="right">
                                        <input
                                            className={
                                                this.state
                                                    .ip_address_setting === 1
                                                    ? " disabled "
                                                    : ""
                                            }
                                            disabled={
                                                this.state
                                                    .ip_address_setting === 1
                                                    ? true
                                                    : false
                                            }
                                            type="text"
                                            id="dns1"
                                            name="dns1"
                                            value={this.state.dns1}
                                            onChange={this.onStringChange}
                                        />
                                    </div>
                                </div>
                                <div className="input-group">
                                    <div className="left">
                                        {" "}
                                        <span>DNS2:</span>
                                    </div>
                                    <div className="right">
                                        <input
                                            className={
                                                this.state
                                                    .ip_address_setting === 1
                                                    ? " disabled "
                                                    : ""
                                            }
                                            disabled={
                                                this.state
                                                    .ip_address_setting === 1
                                                    ? true
                                                    : false
                                            }
                                            type="text"
                                            id="dns2"
                                            name="dns2"
                                            value={this.state.dns2}
                                            onChange={this.onStringChange}
                                        />
                                    </div>
                                </div>
                                <div className="input-group">
                                    <div className="left">
                                        {" "}
                                        <span>DNS3:</span>
                                    </div>
                                    <div className="right">
                                        <input
                                            className={
                                                this.state
                                                    .ip_address_setting === 1
                                                    ? " disabled "
                                                    : ""
                                            }
                                            disabled={
                                                this.state
                                                    .ip_address_setting === 1
                                                    ? true
                                                    : false
                                            }
                                            type="text"
                                            id="dns3"
                                            name="dns3"
                                            value={this.state.dns3}
                                            onChange={this.onStringChange}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        {
                            this.state.isUpdateNetwork &&
                            <div className="warning-update-network">
                                {/* <i class="fa-sharp fa-regular fa-circle-exclamation"></i> */}
                                <i class="fas fa-exclamation-circle"></i>
                                <span>The update will be activated after the system restart.</span>
                            </div>
                        }
                        {/* {
                            (this.state.ip_address_setting === 1) && <div className="input-group ip-address-setup popup">
                                <div className="left"><span>Automatic IP Address (DHCP)</span></div>
                                <div className="right dhcp-default">
                                    <span>The IP Address has been generated automatically.</span>
                                    <div className="input-group">
                                        <div className="left"> <span>Default IP Address:</span></div>
                                        <div className="right"><input style={{opacity:"0.8"}} type="text" id="dtcp_ip" disabled name="dtcp_ip" value={this.state.dtcp_ip} onChange={this.onStringChange} /></div>
                                    </div>
                                </div>
                            </div>
                        } */}
                        <div className="btn-row">
                            <button
                                id="save-ip-btn"
                                className="btn submit"
                                onClick={() => {
                                    this.saveNetwork();
                                }}
                            >
                                <span>Save</span>
                            </button>
                        </div>
                    </div>

                    <div className="content no-bg">
                        <div className="sub-content">
                            <header className="h2">
                                <h2>Date and Time</h2>
                            </header>
                            <hr />
                            <div className="input-group ip-address-setup">
                                <div className="left">
                                    <span>Date and Time Setting:</span>
                                </div>
                                <div className="right">
                                    <div className="sub-group">
                                        <input
                                            type="radio"
                                            className="radio"
                                            name="timetype"
                                            id="time_manaual"
                                            value={2}
                                            checked={this.state.timetype === 2}
                                            onChange={(event) => {
                                                this.onIntegerChange(event);
                                            }}
                                        />
                                        <label htmlFor="time_manaual">
                                            <span>Manual Setting:</span>
                                        </label>
                                    </div>
                                    <div className="sub-group">
                                        <input
                                            type="radio"
                                            className="radio"
                                            name="timetype"
                                            id="time_ntp"
                                            value={1}
                                            checked={this.state.timetype === 1}
                                            onChange={(event) => {
                                                this.onIntegerChange(event);
                                            }}
                                        />
                                        <label htmlFor="time_ntp">
                                            <span>
                                                Network Time Protocol server
                                                (NTP)
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            {this.state.timetype === 1 && (
                                <div className="input-group ip-address-setup popup">
                                    <div className="left">
                                        <span>NTP Setting:</span>
                                    </div>
                                    <div className="right">
                                        <div className="input-group ntp-server">
                                            <div className="left ">
                                                {" "}
                                                <span>NTP Server 1:</span>
                                            </div>
                                            <div className="right">
                                                <input
                                                    type="text"
                                                    id="timeserver"
                                                    name="timeserver"
                                                    required
                                                    value={
                                                        this.state.timeserver
                                                    }
                                                    onChange={
                                                        this.onStringChange
                                                    }
                                                />{" "}
                                            </div>
                                        </div>
                                        <div className="input-group ntp-server">
                                            <div className="left ">
                                                {" "}
                                                <span>NTP Server 2:</span>
                                            </div>
                                            <div className="right">
                                                <input
                                                    type="text"
                                                    id="timeserver2"
                                                    name="timeserver2"
                                                    value={
                                                        this.state.timeserver2
                                                    }
                                                    onChange={
                                                        this.onStringChange
                                                    }
                                                />
                                                <span
                                                    style={{
                                                        marginLeft: "0.5vw",
                                                    }}
                                                >
                                                    (optional)
                                                </span>
                                            </div>
                                        </div>
                                        <div className="input-group ntp-server">
                                            <div className="left ">
                                                {" "}
                                                <span>NTP Server 3:</span>
                                            </div>
                                            <div className="right">
                                                <input
                                                    type="text"
                                                    id="timeserver3"
                                                    name="timeserver3"
                                                    value={
                                                        this.state.timeserver3
                                                    }
                                                    onChange={
                                                        this.onStringChange
                                                    }
                                                />
                                                <span
                                                    style={{
                                                        marginLeft: "0.5vw",
                                                    }}
                                                >
                                                    (optional)
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )}
                            {(this.state.timetype === 2 && this.state.datetime) && (
                                <div className="input-group ip-address-setup popup">
                                    <div className="left">
                                        <span>Manual Setting</span>
                                    </div>
                                    <div className="right">
                                        {/* <input type="datetime" className="datetime" name="datetime" id="datetime" value={} /> */}
                                        <Datetime
                                            className="datetime"
                                            name="datetime"
                                            id="datetime"
                                            initialValue={this.state.datetime}
                                            // value={this.state.datetime}
                                            dateFormat="YYYY-MM-DD "
                                            timeFormat="HH:mm:ss"
                                            onChange={(event) => {
                                                this.onDatetimeChange(event);
                                            }}
                                        />
                                    </div>
                                </div>
                            )}

                            <hr className="timezone-hr"/>

                            <div className="input-group">
                                <div className="left">
                                    {" "}
                                    <span>Timezone:</span>
                                </div>
                                <div className="right">
                                    <select
                                        name="timezone"
                                        id="timezone"
                                        value={this.state.timezone}
                                        onChange={(event) => {
                                            this.onTimezoneChange(event);
                                        }}
                                    >
                                        {this.state.timezoneList &&
                                            this.state.timezoneList.map(
                                                (zone, index) => (
                                                    <option key={`timezone-${index+1}`}  id={`timezone-${index+1}`} value={zone.id}>
                                                        {zone.name}
                                                    </option>
                                                )
                                            )}
                                    </select>
                                </div>
                            </div>
                            {/* <div className="btn-row">
                                <button
                                    id="save-timezone-btn"
                                    className="btn submit"
                                    onClick={() => {
                                        this.saveTimezone();
                                    }}
                                >
                                    <span>Save</span>
                                </button>
                            </div> */}
                        
                            <div className="btn-row">
                                <button
                                    id="save-datetime-btn"
                                    className="btn submit"
                                    onClick={() => {
                                        this.saveDatetime();
                                    }}
                                >
                                    <span>Save</span>
                                </button>
                            </div>
                        </div>
                        {/* <div className="sub-content">
                            <header className="h2">
                                <h2>Timezone</h2>
                            </header>
                            <hr />

                            <div className="input-group">
                                <div className="left">
                                    {" "}
                                    <span>Timezone:</span>
                                </div>
                                <div className="right">
                                    <select
                                        name="timezone"
                                        id="timezone"
                                        value={this.state.timezone}
                                        onChange={(event) => {
                                            this.onTimezoneChange(event);
                                        }}
                                    >
                                        {this.state.timezoneList &&
                                            this.state.timezoneList.map(
                                                (zone, index) => (
                                                    <option key={`timezone-${index+1}`}  id={`timezone-${index+1}`} value={zone.id}>
                                                        {zone.name}
                                                    </option>
                                                )
                                            )}
                                    </select>
                                </div>
                            </div>
                            <div className="btn-row">
                                <button
                                    id="save-timezone-btn"
                                    className="btn submit"
                                    onClick={() => {
                                        this.saveTimezone();
                                    }}
                                >
                                    <span>Save</span>
                                </button>
                            </div>
                        </div> */}

                        {/* <div className="input-group">

                            <div className="left"> <span>Offset:</span></div>
                            <div className="right">
                                <input type="text" id="timezoneWithOffset" name="timezoneWithOffset" value={this.state.timezoneWithOffset} onChange={this.onStringChange} />
                            </div>
                        </div> */}
                    </div>
                </div>
                <Modal
                    isOpen={this.state.openFirmwareProgressModal}
                    className="confirmationFirmware-modal"
                    overlayClassName="default-overlay"
                    // onRequestClose={this.closeEditModal}
                    // closeTimeoutMS={500}
                >
                    <UpdateFirmwareProgress
                        firmwareData={this.state.firmwareData}
                        closeModal={this.closeUpdateModal.bind(this)}
                        // updateInput={this.updateInput}
                        // closeEditModal={this.closeEditModal.bind(this)}
                    />
                </Modal>
                {/* <Modal
                    isOpen={open}
                    toggle={cancel}
                    className="confirmationFirmware-modal"
                    overlayClassName="confirmation-overlay"
                    onRequestClose={cancel}
                // closeTimeoutMS={500}
                ></Modal> */}
                <Loading open={this.state.loading} />
            </div>
        );
    }
}

export default NetworkSetting;


const ConfirmFirmwareDetail = ({ open, close, firmwareData }) => {
    const submit = () => {
        close({"confirm": 1})
    };
  
    const cancel = () => close(null);
  
    return (
      <Modal
        isOpen={open}
        toggle={cancel}
        className="confirmationFirmware-modal"
        overlayClassName="confirmation-overlay"
        onRequestClose={cancel}
      // closeTimeoutMS={500}
      >
        <div className="firmware-update">
  
          {/* <header className="h1"> */}
            <h2>Firmware Update</h2>
          {/* </header> */}
          <hr />
          <div className="paragraph">
            <div className="firmware-content">
                <div className="sub-header">
                    
                    <span className="check-icon">
                        <i class="far fa-check-circle"></i>
                    </span>
                    <span>Firmware Details</span>
                </div>
                <div className="list-detail">
                    <li>Version: {firmwareData.version}</li>
                    <li>Date: {firmwareData.date}</li>
                </div>

                <div className="sub-header" style={{marginTop: "0.8vw"}}>
                    <span className="check-icon">
                        <i class="far fa-check-circle"></i>
                    </span>
                    <span>What's new</span>
                </div>
                <div className="list-detail">
                    {
                        firmwareData.features.map(
                            (detail, index) => (
                                <li>{detail}</li>
                            )
                        )
                    }
                </div>
                
            </div>
            
          </div>
  
          <div className="btn-row">
            <button id="confirm-submit-btn"  style={{marginRight:"0.5vw"}} autoFocus className="submit btn" onClick={submit}>
              <span>Update</span>
            </button>
            <button id="confirm-cancel-btn" className="cancel btn" onClick={cancel}>
              <span>Cancel</span>
            </button>
          </div>
        </div>
      </Modal>
    );
  };
  
  export const openFirmwareDetails = createModal(ConfirmFirmwareDetail);