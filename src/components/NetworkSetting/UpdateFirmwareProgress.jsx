import React, { Component } from 'react'
import './UpdateFirmwareProgress.scss'
// import * as SystemService from "../../services/SystemService";
import * as InformUser from "../../shared/InformUser";
// import { openConfirmation } from "../../shared/Confirmation";
import ProgressBar from "@ramonak/react-progress-bar";

export class UpdateFirmwareProgress extends Component {

    // token = JSON.parse(localStorage.getItem('userData')).token
    progress_interval;

    constructor(props) {
        super(props);
        this.state = {
            firmwareData: this.props.firmwareData,
            // progress: 50
        }
    }

    componentDidMount() {
        this.startCalProgress(50);
    }
    
    async progressSuccess() {
        clearInterval(this.progress_interval);
        InformUser.success({ text: 'Upgrade firmware completed' });
        await this.delay(2000);
        this.props.closeModal()
    }

    async delay(milsec) {
      return new Promise( res => setTimeout(res, milsec) );
  }

    startCalProgress(sleep) {
        // this.clearIntervalHeader()
        // clearInterval(this.progress_interval)
        // await this.delay(5000)
        // console.log('startCalProgress',this.state.firmwareData);
        let start_time = new Date();
        // let running_time = 1;
        let time_diff = 0;
        const loading_time = this.state.firmwareData.loading_time ? this.state.firmwareData.loading_time : 150;
        this.progress_interval = setInterval(() => {
          let now = new Date();
          time_diff = (now.getTime() - start_time.getTime()) / 1000;
          const progress = (((time_diff / loading_time) * 100)).toFixed(1);
          this.setState({progress: progress});
        //   console.log('debug ',progress)
          if (progress >= 100) {
            this.progressSuccess();
          }
          // running_time += 1;
        }, sleep);
    };

    render() {

        return (
              <div className="firmware-update">
        
                  <h2>Firmware Update...</h2>
                {/* </header> */}
                <hr />
                <div className="paragraph">
                  {/* <div>
                      Updating...
                  </div> */}
                  <span className="warning-txt">Warning: System is updating. Do not shut down the system</span>
                <ProgressBar completed={this.state.progress} className="bar-wrapper progress-bar-style"
                    barContainerClassName="firmware-bar-container"
                    // completedClassName="firmware-bar-Completed"
                    labelClassName="firmware-bar-label" 
                    bgColor="#3b9747"/>
                </div>
              </div>
            // </Modal>
          );
    }
}

export default UpdateFirmwareProgress
