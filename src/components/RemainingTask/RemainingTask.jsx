import React, { Component } from 'react'
import './RemainingTask.scss'
import LinearProgress from '@material-ui/core/LinearProgress';
import * as TaskService from "./../../services/TaskService";
import * as InformUser from "./../../shared/InformUser";
import { openConfirmationRemark } from "./../../shared/Confirmation";
import moment from 'moment';
import ReactTooltip from "react-tooltip";

export class RemainingTask extends Component {

    token = JSON.parse(localStorage.getItem('userData')).token;
    time_offset = JSON.parse(localStorage.getItem('userData')).time_offset;
    interval;

    constructor(props){
        super(props);
        this.state = {
            taskList: '',
        }
    }

    componentDidMount() {

        this.setState({ loading: true }, async () => {
            await this.getTask();
            this.setState({ loading: false }, () => {
                this.setupInterval()
            });
        })
    }
    setupInterval() {
        this.interval = setInterval(() => {
            this.getTask()
        }, 1500)
    }

    async getTask() {
        await TaskService.get_tasks(this.token).then( async result => {
            // console.log(result);
            if (result.status == 1) {

                var task = result.data;
                var timeCaledTask = await this.processColumnOfTime(task)



                this.setState({ taskList: timeCaledTask });
            } else {
                InformUser.unsuccess({ text: result.msg });
            }
        })
    }

    async rollbackTask(task){
        var textMessage = ""

        if( !task.remark || ( task.remark === "-" ) ){
            textMessage = `Are you sure you want to rollback ${task.description}?`;
        }

        if( task.remark && ( task.remark !== "-" ) ){
            textMessage = `Are you sure you want to rollback ${task.description} (${task.remark})?`;
        }

        const dialog = await openConfirmationRemark({
            title: "Remove Task",
            text: textMessage,
        })
        if( dialog ){
            // console.log( dialog )
            TaskService.rollback_task( task.id, dialog.remark, this.token ).then( result => {
                // console.log( result )
                if(result.status === 1){
                    InformUser.success({ text:  result.msg })
                    this.getTask()
                }
                if(result.status !== 1){
                    InformUser.unsuccess({ text: result.msg })
                }
            })
        }
    }
    async removeTask(task) {
        var textMessage = ""

        if( !task.remark || ( task.remark === "-" ) ){
            textMessage = `Are you sure you want to remove ${task.description}`;
        }

        if( task.remark && ( task.remark !== "-" ) ){
            textMessage = `Are you sure you want to remove ${task.description} (${task.remark}) ?`;
        }

        const dialog = await openConfirmationRemark({
            title: "Remove Task",
            text: textMessage,
        })
        if( dialog ){
            TaskService.remove_task( task.id , this.token ).then( result => {
                // console.log( result )
                if(result.status === 1){
                    InformUser.success({ text:  result.msg })
                    this.getTask()
                }
                if(result.status !== 1){
                    InformUser.unsuccess({ text: result.msg })
                }
            })
        }
    }
    async processColumnOfTime(logData){
        var  logs = JSON.parse(JSON.stringify(logData));
        // var  logs = logData;
        for( var i=0; i< logs.length; i++){
            const newAddDate = await this.calculateTime(logs[i]["add_date"])
            const newStartDate = await this.calculateTime(logs[i]["start_date"])
            // const newFinishDate = await this.calculateTime(logs[i]["finish_date"])
            logs[i]["add_date"] = newAddDate;
            logs[i]["start_date"] = newStartDate;
            // logs[i]["finish_date"] = newFinishDate;
        }
        return logs;
    }
    async calculateTime(serverTime) {

        var rawServerTime = await moment(serverTime);
        // console.log(rawServerTime)
        // console.log(timeOffset)
        var offset = moment.duration(this.time_offset)
        // // var temp = moment(server_time).add(1, 'm')
        // console.log(offset)
        var currentTime =  await rawServerTime.add(offset).format("ddd YYYY-MM-DD HH:mm:ss")
        // console.log(currentTime)
        return currentTime;
        // console.log(currentTime)
    }

    render() {
        return (
            <div className="remaining-task-container">
                <header className="h1"><h1>CONNECTIVITY: REMAINING TASKS</h1></header>
                <div className="content">
                    <div className="table-container">

                    
                    <div className="table-size">
                        <table id="remaining-task-table">
                                    <tr className="tr-head">
                                        <th style={{ width: "5%" }} ><span>No.</span></th>
                                        <th style={{ width: "5%" }} ><span>Operation</span></th>
                                        <th style={{ width: "20%" }} ><span>Description</span></th>
                                        <th style={{ width: "10%" }} ><span>Add Date</span></th>
                                        {/* <th style={{ width: "10%" }} ><span>Start Date</span></th> */}
                                        <th style={{ width: "10%" }} ><span>Add By</span></th>
                                        <th style={{ width: "10%" }} ><span>Status</span></th>
                                        <th style={{ width: "15%" }} ><span>Progress</span></th>
                                        <th style={{ width: "20%" }} ><span>Remarks</span></th>
                                        <th style={{ width: "5%" }} ><span>Action</span></th>
                                    </tr>
                                    {
                                        (this.state.taskList.length > 0) && <ReactTooltip id={`remark-tooltip`} html={true} className="remarkClass defaultTooltip" place="top" arrowColor="#404040"  effect="solid"
                                        />
                                    }
                                    {/* <ReactTooltip id="yutyutyut" place="top" multiline="true" type="dark" effect="solid" /> */}
                                    {
                                        this.state.taskList && this.state.taskList.map((task, index) =>
                                            <tr
                                                key={`remaining-task-${index+1}`}
                                                id={`remaining-task-${index+1}`}
                                            >
                                                <td id={`task-no-${index+1}`} className="text-center"><span >{task.no}</span></td>
                                                <td id={`task-name-${index+1}`} className="text-center"><span>{task.operation_name}</span></td>
                                                <td id={`task-description-${index+1}`}  className="text-center"><span>{task.description}</span></td>
                                                <td id={`task-add-date-${index+1}`}  className="text-center"><span>{task.add_date}</span></td>
                                                {/* <td id={`task-start-date-${index+1}`}  className="text-center"><span>{task.start_date}</span></td> */}
                                                <td id={`task-add-by-${index+1}`}  className="text-center"><span>{task.add_by}</span></td>
                                                <td id={`task-status-${index+1}`}  className="text-center"><span>{task.status_desc}</span></td>
                                                <td id={`task-progress-bar-${index+1}`}  >
                                                    <div className="progress-container">
                                                        <div className="connecting-bar progress-bar-container">
                                                            <LinearProgress striped variant="determinate" value={task.progress} />
                                                        </div>
                                                        <span  id={`task-progress-percent-${index+1}`}  className="text-center">{task.progress}%</span>
                                                    </div>
                                                    
                                                 </td>
                                                <td  id={`task-remark-${index+1}`}  className="text-center">
                                                    <span 
                                                    className="ellipsis-text" 
                                                    style={{width:"11vw"}}
                                                    data-for="remark-tooltip"
													// data-html={true}
													data-tip={ task.remark.length > 28 ? `<span>${task.remark}</span>` : ""}
                                                    >{task.remark}</span>
                                                </td>

                                                <td id={`task-action-${index+1}`}  className="text-center action-row">
                                                    {
                                                        (task.status === -1) && <button id={`rollback-btn-${index+1}`} className="fit-btn submit " onClick={() => { this.rollbackTask(task) }}> <span>Rollback</span></button>
                                                    }
                                                {
                                                    (task.status !== -1) && <button id={`romove-btn-${index+1}`} className="fit-btn submit " onClick={() => { this.removeTask(task) }}> <span>Remove</span></button>
                                                }
                                                    
                                                </td>
                                            </tr>
                                        )
                                    }
                                    
                                </table>
                                {
                                        (this.state.taskList.length === 0) && <div id="no-data-task" className="no-remaining-task" style={{height:"7vw"}}><span className="camera-offline">NO REMAINING TASK</span></div>
                                    }
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default RemainingTask
