import React, { Component } from 'react'
import './FrontPanel.scss'
import * as InfoService from "./../../../services/InfoService";

import ReactTooltip from 'react-tooltip';
import { Loading } from "./../../../shared/Loading";
import ClipLoader from "react-spinners/ClipLoader";
import Modal from 'react-modal';
import { ConfirmConnectivity } from "./../../Connectivity/ConfirmConnectivity";
import * as TaskService from "../../../services/TaskService";
import * as SystemService from "../../../services/SystemService";
import * as InformUser from "../../../shared/InformUser";
import { Option } from "./Option/Option";
import Switch from "react-switch";
import { openConfirmation, openConfirmationRemark } from '../../../shared/Confirmation';
import LinearProgress from '@material-ui/core/LinearProgress';
import moment from 'moment';
import { Link } from "react-router-dom";

export class FrontPanel extends Component {

    token = JSON.parse(localStorage.getItem('userData')).token;
    time_offset = JSON.parse(localStorage.getItem('userData')).time_offset;
    interval;

    filterSetting;

    constructor(props) {
        super(props);
        this.state = {

            portPerPage: 100,
            view_type: 2,
            sourceData: '',
            targetData: '',
            currentPageSource: 0,
            currentPageTarget: 0,

            taskList: '',



            // sourceIndex: '',
            // targetIndex: '',

            selectedSource: '',
            selectedTarget: '',
            // portWidth: 4,
            // portHeight: 4.5,
            pageForwardIndex: 1,
            loading: false,

            loadingSource: false,
            loadingTarget: false,
            routeSearchModal: false,



            // unitList: '',
            pageIndexSource: '',
            pageIndexTarget: '',
            totalPortSource: '',
            totalPageSource: '',
            totalPortTarget: '',
            totalPageTarget: '',

            filterSourceUnit: null,
            filterSourceSmu: 0,
            filterSourceSearch: null,
            ActualfilterSourceSearch: null,
            filterTargetUnit: null,
            filterTargetSmu: 1,
            filterTargetSearch: null,
            ActualfilterTargetSearch: null,
            filterSourceUnitResult: '',
            filterSourceSmuResult: '',
            filterTargetUnitResult: '',
            filterTargetSmuResult: '',

            filterOptionModal: false,

            showFrontPanelNumber: false,

            routeConnectionData: ''
        }

    }

    componentWillMount() {
        // console.log(this.props.source,this.props.target)
    }
    componentDidMount() {
        this.setState({ loading: true }, async () => {
            // await this.getTotalPort();
            await this.getUnitList();
            await this.getPortData(true);
            await this.getPortData(false);
            await this.getTask();
            this.setState({ loading: false }, () => {
                this.setupInterval()
            });
        })
    }
    setupInterval() {
        this.interval = setInterval(() => {
            this.getPortData(true);
            this.getPortData(false);
            this.getTask()
        }, 3000)
    }
    clearIntervals() {
        clearInterval(this.interval)
    }
    componentWillUnmount() {
        this.clearIntervals()


    }

    chunk = (arr, size) => arr.reduce((acc, e, i) => (i % size ? acc[acc.length - 1].push(e) : acc.push([e]), acc), []) ;
    async getTask() {
        await TaskService.get_tasks(this.token).then( async result => {
            // console.log(result);
            if (result.status == 1) {

                var task = result.data;
                var timeCaledTask = await this.processColumnOfTime(task)



                this.setState({ taskList: timeCaledTask });
            } else {
                InformUser.unsuccess({ text: result.msg });
            }
        })
    }
    async processColumnOfTime(logData){
        var  logs = JSON.parse(JSON.stringify(logData));
        // var  logs = logData;
        for( var i=0; i< logs.length; i++){
            const newAddDate = await this.calculateTime(logs[i]["add_date"])
            const newStartDate = await this.calculateTime(logs[i]["start_date"])
            // const newFinishDate = await this.calculateTime(logs[i]["finish_date"])
            logs[i]["add_date"] = newAddDate;
            logs[i]["start_date"] = newStartDate;
            // logs[i]["finish_date"] = newFinishDate;
        }
        return logs;
    }
    async calculateTime(serverTime) {

        var rawServerTime = await moment(serverTime);
        // console.log(rawServerTime)
        // console.log(timeOffset)
        var offset = moment.duration(this.time_offset)
        // // var temp = moment(server_time).add(1, 'm')
        // console.log(offset)
        var currentTime =  await rawServerTime.add(offset).format("ddd YYYY-MM-DD HH:mm:ss")
        // console.log(currentTime)
        return currentTime;
        // console.log(currentTime)
    }
    clearSelected() {
        this.setState({
            selectedSource: '',
            selectedTarget: '',
            sourceSelectingPair: null,
            targetSelectingPair: null
        })
    }

    getTotalPort(unit_id, smu_type, search, isSource) {
        // console.log(unit_id, smu_type, search,)

        InfoService.get_total_port_count(unit_id, smu_type, search, this.token).then(result => {
            // console.log("count", result);
            if (result.status === 1) {

                const port_qty = this.state.portPerPage;
                var totalPage = Math.ceil(result.data.port_count / port_qty)
                if(totalPage === 0){
                    totalPage = 1;
                }
                var pageIndex = [...Array(totalPage).keys()]
                // console.log(totalPage)
                
                if (isSource) {
                    this.setState({
                        totalPortSource: result.data.port_count,
                        totalPageSource: totalPage,
                        pageIndexSource: pageIndex,
                    })
                } else {
                    this.setState({
                        totalPortTarget: result.data.port_count,
                        totalPageTarget: totalPage,
                        pageIndexTarget: pageIndex,
                    })
                }

            }
        })
    }




    async getUnitList() {
        await SystemService.get_unit(this.token).then(result => {
            // console.log("yut", result)
            if (result.status === 1) {
                this.setState({ unitList: result.data })
            }
        })
    }

    selectedSmuType() {
        const connector = this.state.smuConnector;
        const adapter = this.state.smuAdapter;

        if (!connector && !adapter) {
            return null;
        } else {
            if (connector && !adapter) {
                return 0;
            }
            if (!connector && adapter) {
                return 1;
            }
            if (connector && adapter) {
                return null;
            }
        }
    }

    async getPortData(isSource) {
        const view_type = this.state.view_type;
        const port_qty = this.state.portPerPage;

        var start_no = 0;
        var unit_id = '';
        var smu_type = '';
        var search = '';
        // console.log("notice", this.state.filterSourceSmu, this.state.filterTargetSmu)
        if (isSource) {
            start_no = ((this.state.currentPageSource) * port_qty) + 1;
            unit_id = this.state.filterSourceUnit;
            smu_type = this.state.filterSourceSmu;
            search = this.state.ActualfilterSourceSearch;
        } else {
            start_no = ((this.state.currentPageTarget) * port_qty) + 1;
            unit_id = this.state.filterTargetUnit;
            smu_type = this.state.filterTargetSmu;
            search = this.state.ActualfilterTargetSearch;
        }

        // console.log("notice", unit_id, typeof (unit_id))
        const rowAmount = 5;
        const portPerRow = port_qty / rowAmount;




        // console.log(view_type, start_no, port_qty, unit_id, smu_type, search,)
        this.filterResultDescription(unit_id, smu_type, isSource)

        await this.getTotalPort(unit_id, smu_type, search, isSource)


        await InfoService.get_port_panel(view_type, start_no, port_qty, unit_id, smu_type, search, this.token).then(async result => {
            // console.log("filter", result);
            var array = '';
            if (result.status === 1) {
                if(result.data){
                    array = await this.chunk(result.data, portPerRow);

                }
                
                if (isSource) {
                    this.setState({
                        sourceData: array,
                    });
                } else {
                    this.setState({
                        targetData: array,
                    });
                }
            }
        });
        // console.log("yut", this.state.currentPageSource)
    }

    filterResultDescription(unit_id, smu_type, isSource) {
        // console.log("unit_id", unit_id, smu_type)
        var unit_id_text;
        var smu_type_text;

        if (!unit_id) {
            unit_id_text = "All units"
        } else {
            this.state.unitList.map(unit => {
                // console.log(unit)
                if (unit.id === parseInt(unit_id)) {
                    unit_id_text = `${unit.name}`
                }
            })
        }

        if (smu_type === 0) {
            smu_type_text = "East"
        } else if (smu_type === 1) {
            smu_type_text = "West"
        } else {
            smu_type_text = "East and West"
        }

        if (isSource) {
            this.setState({
                filterSourceUnitResult: unit_id_text,
                filterSourceSmuResult: smu_type_text
            })
        } else {
            this.setState({
                filterTargetUnitResult: unit_id_text,
                filterTargetSmuResult: smu_type_text
            })
        }
        // return `${unit_id_text}, ${smu_type_text}`


    }


    handlePage = (event, isSource) => {
        let selected = parseInt(event.target.value);
        this.setState({ [event.target.name]: selected });

        if (isSource) {
            this.setState({ currentPageSource: selected, loadingSource: true }, async () => {
                await this.getPortData(true);
                this.setState({ loadingSource: false }, () => {
                    // console.log(this.state.currentPageSource, this.state.totalPageSource)
                });
            });
        } else {
            this.setState({ currentPageTarget: selected, loadingTarget: true }, async () => {
                await this.getPortData(false);
                this.setState({ loadingTarget: false });
            });
        }
    }
    forwardPage(isSource) {


        if (isSource) {
            const page = parseInt(this.state.currentPageSource)
            if (page < (this.state.totalPageSource - 1)) {
                var pageNext = page + 1;
                // console.log(pageNext)
                this.setState({ currentPageSource: pageNext, loadingSource: true }, async () => {
                    await this.getPortData(true);
                    this.setState({ loadingSource: false }, () => {
                        // console.log(this.state.currentPageSource, this.state.totalPageSource)
                    })
                });
            }

        } else {
            const page = parseInt(this.state.currentPageTarget)
            if (page < (this.state.totalPageTarget - 1)) {
                var pageNext = page + 1;
                this.setState({ currentPageTarget: pageNext, loadingTarget: true }, async () => {
                    await this.getPortData(false);
                    this.setState({ loadingTarget: false })
                });
            }

        }
    }
    backwardPage(isSource) {
        // console.log(this.state.currentPageSource)

        if (isSource) {
            const page = parseInt(this.state.currentPageSource)
            if (page > 0) {
                var pageNext = page - 1;
                this.setState({ currentPageSource: pageNext, loadingSource: true }, async () => {
                    await this.getPortData(true);
                    this.setState({ loadingSource: false }, () => {
                        // console.log(this.state.currentPageSource, this.state.totalPageSource)
                    })
                });
            }

        } else {
            const page = parseInt(this.state.currentPageTarget)
            if (page > 0) {
                var pageNext = page - 1;
                this.setState({ currentPageTarget: pageNext, loadingTarget: true }, async () => {
                    await this.getPortData(false);
                    this.setState({ loadingTarget: false }, () => {
                        // console.log(this.state.currentPageTarget, this.state.totalPageTarget)
                    })
                });
            }

        }
    }
    // handlePageChange = (event) => {
    //     this.setState({ [event.target.name]: event.target.value })
    //     console.log(this.state.pageForwardIndex);
    // }
    findPort(idx,isSource) {
        this.state.targetData.map(function(x) {return x.idx; }).indexOf(idx);
    }

    selectPort(port, isSource) {
        const source = this.state.selectedSource;
        const target = this.state.selectedTarget;

        if([1,-1].includes(port.port_operation_id) && port.reserved === 0) {
            if (isSource) {
                if ((source && (source.idx == port.idx)) || port.idx == this.state.sourceSelectingPair) {
                    if(port.port_operation_id == -1) {
                        // this.setState({ selectedSource: "", selectedTarget: "", sourceSelectingPair: null, targetSelectingPair: null });
                        this.clearSelected();
                    } else {
                        this.setState({ selectedSource: "" });
                    }
                } else {
                    // (target.smu_type !== port.smu_type) &&
                    if ((target.idx !== port.idx) && (port.port_operation_id !== 0) && (port.reserved !== 1)) {
                        if((port.port_operation_id == -1)) {
                            // select connected port to change polarity
                            this.setState({ selectedSource: port, selectedTarget: "", sourceSelectingPair: port.idx, targetSelectingPair: port.connected_idx })
                            
                        } else {
                            if(this.state.selectedTarget != '' && this.state.selectedTarget.port_operation_id == -1) {
                                // clear target if source changed
                                this.setState({ selectedSource: port, selectedTarget: '', sourceSelectingPair: null, targetSelectingPair: null });
                            } else {
                                this.setState({ selectedSource: port, sourceSelectingPair: null, targetSelectingPair: null });
                            }
                        }
                    }
                }

            } else {
                if ((target && (target.idx == port.idx)) || port.idx == this.state.targetSelectingPair) {
                    if(port.port_operation_id == -1) {
                        this.clearSelected();
                    } else {
                        this.setState({ selectedTarget: "" });
                    }
                } else {
                    // (source.smu_type !== port.smu_type) &&
                    if ((source.idx !== port.idx) && (port.port_operation_id !== 0) && (port.reserved !== 1)) {
                        if((port.port_operation_id == -1)) {
                            this.setState({ selectedTarget: port, selectedSource: "", sourceSelectingPair: port.connected_idx, targetSelectingPair: port.idx });
                        } else {
                            if(this.state.selectedSource != '' && this.state.selectedSource.port_operation_id == -1) {
                                // clear source if target changed
                                this.setState({ selectedTarget: port, selectedSource: '', sourceSelectingPair: null, targetSelectingPair: null });
                            } else {
                                this.setState({ selectedTarget: port, sourceSelectingPair: null, targetSelectingPair: null });
                            }
                        }
                    }
                }
            }
        }
        // if(this.state.selectedSource){
        //     if(port.smu_type !== this.state.selectedSource.smu_type){

        //     }
        // }
        // if(this.state.selectedTarget){
        //     if(port.smu_type !== this.state.selectedTarget.smu_type){
        //         if (isSource) {
        //             this.setState({ selectedSource: port });
        //         } else {
        //             this.setState({ selectedTarget: port });
        //         }
        //     }
        // }

        // console.log(this.state.selectedSource, this.state.selectedTarget)
    }

    choosePort() {
        // this.props.choosePort(this.state.selectedSource, this.state.selectedTarget);
        // this.props.close()
        // if( !this.state.selectedSource || !this.state.selectedTarget ){
        //     InformUser.unsuccess({ text: "Please select both source and target port to ceate the connectivity." })
        // }
        if( this.state.selectedSource && this.state.selectedTarget ){
            this.setState({ loading: true }, async () => {
                await this.searchRoute()
    
                this.setState({ loading: false })
            })
        }
        
        

    }

    async switchPolarity() {
        // <strong>{` Port-${this.props.source.display_no}`}</strong>{` (${this.props.source.description})`} and <strong>{`Port-${this.props.target.display_no}`}</strong>{` (${this.props.target.display_no})`}?
        if(this.checkSwitchPolarity()) {
            let source = this.state.selectedSource;
            let target = this.state.selectedTarget;
            if(!target) {
                await InfoService.get_port_single(this.state.targetSelectingPair, this.token).then((result) => {
                    if(result.status === 1) {
                        target = result.data; 
                    }
                })
            }

            if(!source) {
                await InfoService.get_port_single(this.state.targetSelectingPair, this.token).then((result) => {
                    if(result.status === 1) {
                        source = result.data; 
                    }
                })
            }
            // let port = this.state.selectedSource ? this.state.selectedSource : this.state.selectedTarget;
            // console.log('debug selected port',port)
            let polarity = source.polarity_status == 0 ? 'Reversed (Type B)' : 'Direct (Type A)';
            const dialog = await openConfirmationRemark({
                title: "Swap Polarity",
                // text: `Are you sure you want to switch polarity between Port-${source.display_no} (${source.description}) and Port-${target.display_no} (${target.description}) to `+polarity+`?`,
                text: `Are you sure you want to swap polarity of this connection to `+polarity+`?`,
            })
            if( dialog ){
                this.setState({ loading: true })
                TaskService.switch_polarity(this.state.sourceSelectingPair, this.state.targetSelectingPair, null, dialog.remark, this.token).then((result) => {
                    this.setState({ loading: false })
                    if(result.status === 1){
                        InformUser.success({ text:  result.msg })
                        this.clearSelected()
                        this.getTask()
                    }
                    if(result.status !== 1){
                        InformUser.unsuccess({ text: result.msg })
                    }
                })
            }
            
        }
    }

    checkSwitchPolarity() {
        return (this.state.selectedSource && this.state.selectedSource.port_operation_id == -1 && this.state.selectedSource.allow_swap_polarity == 1) || (this.state.selectedTarget && this.state.selectedTarget.port_operation_id == -1 && this.state.selectedTarget.allow_swap_polarity == 1);
    }

    async searchRoute() {

        const sourceFiber = this.state.selectedSource.port_fiber;
        const targetFiber = this.state.selectedTarget.port_fiber;

        // const sourceFiber = this.props.source.port_fiber;
        // const targetFiber = this.props.target.port_fiber;


        // console.log(this.state.selectedSource, targetFiber)

        await TaskService.get_route_connectivity(1, sourceFiber, targetFiber, 2, this.token).then(result => {
            // console.log("Search Route:", result)
            if (result.status == 1) {
                if (result.data !== null) {
                    this.setState({

                        routeConnectionData: result.data,
                        routeSearchModal: true,
                        // isSelectingFiber: true,
                    }, () => { this.clearIntervals() });
                } else {
                    InformUser.unsuccess({ text: "No data received" })
                }

            } else {
                InformUser.unsuccess({ text: result.msg })
            }



            // this.setState({ routeData: data}, ()=> {
            //   this.renderCytoscapeElement()
            // })
        })
        // searchRoute() {

        //   const port_no1 = this.state.sourcePort.port_no;
        //   const port_no2 = this.state.targetPort.port_no;



        //   TaskService.get_route_connectivity(1, port_no1, port_no2, 2, this.token).then(result => {
        //     console.log("Search Route:", result)
        //     if (result.status == 1) {
        //       if (result.data !== null) {
        //         var data = {
        //           nodes: result.data.nodes,
        //           edges: result.data.edges,
        //         }
        //         this.setState({
        //           routeData: data,
        //           horizontalLayout: result.data.horizontal,
        //           displayingRoute: 'Route: [ Port-' + this.state.sourcePort.port_no + " to Port-" + this.state.targetPort.port_no + " ]",
        //           // isSelectingFiber: true,
        //         }, () => {
        //           this.renderCytoscapeElement()
        //         });
        //       } else {
        //         InformUser.unsuccess({ text: "No data received" })
        //       }

        //     } else {
        //       InformUser.unsuccess({ text: result.msg })
        //     }



        //     // this.setState({ routeData: data}, ()=> {
        //     //   this.renderCytoscapeElement()
        //     // })
        //   })




        // var data = {
        //   nodes: cytoRoute.nodes,
        //   edges: cytoRoute.edges,
        // }
        // this.setState({ routeData: data }, () => {
        //   console.log(this.state.routeData)
        //   this.renderCytoscapeElement()
        // })


    }

    lightStyleClasses(port, isSource) {

        var defaultStyle = " port ";
        var operation = port.port_operation_id;
        // console.log(port)
        if (isSource) {
            if((this.state.selectedSource.idx && (port.idx === this.state.selectedSource.idx)) || port.idx == this.state.sourceSelectingPair){
                defaultStyle = " selected port ";
            }
            if((this.state.selectedSource.idx && (port.idx !== this.state.selectedSource.idx)) || (this.state.sourceSelectingPair && port.idx != this.state.sourceSelectingPair)){
                defaultStyle = " not-selected port ";
            }
            // if (port.idx === this.state.selectedSource.idx) {
                
            // }
            // if (port.idx !== this.state.selectedSource.idx) {
               
            // }
            if (port.idx === this.state.selectedTarget.idx) {
                // defaultStyle = defaultStyle + " same-type "
                defaultStyle = defaultStyle + " disabled "

            }
            if (port.reserved === 1) {
                defaultStyle += " disabled "
            }
            if (port.idx === this.state.connectedPair) {
                defaultStyle += " pair-is-hovered "
            }
            switch (operation) {
                case -1:
                    return defaultStyle += " connected "
                    break;
                case 0:
                    return defaultStyle + " disabled "
                    break;
                case 1:
                    return defaultStyle + " available "
                    break;
                case 2:
                    return defaultStyle + " connecting "
                    break;
                case 3:
                    return defaultStyle + " disconnecting "
                    break;
                case 4:
                    return defaultStyle + " recovering "
                    break;
                case 5:
                    return defaultStyle + " locking "
                    break;
                default:
                    return defaultStyle
            }
        } else {
            if((this.state.selectedTarget.idx && (port.idx === this.state.selectedTarget.idx)) || port.idx == this.state.targetSelectingPair){
                defaultStyle = " selected port ";
            }
            if((this.state.selectedTarget.idx && (port.idx !== this.state.selectedTarget.idx)) || (this.state.targetSelectingPair && port.idx != this.state.targetSelectingPair)){
                defaultStyle = " not-selected port ";
            }
            if (port.idx === this.state.selectedSource.idx) {
                defaultStyle = defaultStyle + " disabled unclickable "
            }
            if (port.reserved === 1) {
                defaultStyle += " disabled "
            }
            if (port.idx === this.state.connectedPair) {
                defaultStyle += " pair-is-hovered "
            }
            switch (operation) {
                case -1:
                    return defaultStyle += " connected "
                    break;
                case 0:
                    return defaultStyle + " disabled "
                    break;
                case 1:
                    return defaultStyle + " available "
                    break;
                case 2:
                    return defaultStyle + " connecting "
                    break;
                case 3:
                    return defaultStyle + " disconnecting "
                    break;
                case 4:
                    return defaultStyle + " recovering "
                    break;
                case 5:
                    return defaultStyle + " locking "
                    break;
                default:
                    return defaultStyle
            }
        }





    }

    closeRouteSearch = (shouldClearSelected) => {
        if (shouldClearSelected) {
            this.setState({
                selectedSource: "",
                selectedTarget: "",
                routeSearchModal: false,
            },()=>{
                this.getTask();
            })

        } else {
            this.setState({ routeSearchModal: false })

        }
        this.setupInterval();

    }

    setConnectedPair(port) {
        if (port.connected_idx) {
            this.setState({ connectedPair: port.connected_idx })
        }
    }
    resetConnectedPair() {
        this.setState({ connectedPair: '' })
    }
    tooltipDescription(port) {
        var description = `Description:  ${port.description}`;
        
        description = description + `<br/>Unit:  ${port.unit_name} ( ${port.smu_type_desc} )`;
        description = description + `<br/>Connected Count:  ${port.connected_count}`;

        if (port.reserved === 1) {
            description = description + `<hr/>Reserved by ${port.reserved_change_by}`;
        }
        
        if (port.port_operation_id === -1 && (port.connected_task_desc)) {
            let polarity_desc = '';
            if(port.allow_swap_polarity == 1) {
                polarity_desc = `<br/>Polarity: ` + (port.polarity_status == 0 ? 'Direct (Type A)' : 'Reversed (Type B)');
            }

            if (port.connected_task_desc) {
                description = description + `<hr/>Connected to ${port.connected_name}${polarity_desc}<br/>Task: ${port.connected_task_desc}`;

            } else {
                description = description + `<br/>Connected to ${port.connected_name}${polarity_desc}`;

            }

            
        }
        return `<span>${description}</span>`;
    }

    searchPortDescription = (event, isSource) => {
        event.preventDefault();
        // console.log(this.state.filterTargetSearch)
        if (isSource) {
            this.setState({ 
                loadingSource: true, 
                ActualfilterSourceSearch: this.state.filterSourceSearch,
                currentPageSource: 0, 
            }, async ()=>{
                this.setState({sourceData: []}); //reset to fix tooltip bug
                await this.getPortData(true)
                this.setState({ loadingSource: false})
            })
            
        } else {
            this.setState({ 
                loadingTarget: true, 
                ActualfilterTargetSearch: this.state.filterTargetSearch, 
                currentPageTarget: 0,
            }, async ()=>{
                this.setState({targetData: []}); //reset to fix tooltip bug
                await this.getPortData(false)
                this.setState({ loadingTarget: false})
            })
        }

    }
    openFilterOption(isSource) {
        if (isSource) {
            this.filterSetting = {
                "unit": this.state.filterSourceUnit,
                "smu_type": this.state.filterSourceSmu,
                // "search": this.state.filterSourceSearch,
            }
            this.setState({ filterOptionModal: true })
        } else {
            this.filterSetting = {
                "unit": this.state.filterTargetUnit,
                "smu_type": this.state.filterTargetSmu,
                // "search": this.state.filterTargetSearch,
            }
            this.setState({ filterTargetOptionModal: true })
        }

    }
    closeFilterOption = (isSource, setting, doFilter) => {
        // console.log(setting)

        var type_temp;
        if (setting.smu_type) {
            type_temp = parseInt(setting.smu_type);
        } else {
            type_temp = setting.smu_type;
        }

        if (doFilter) {
            if (isSource) {
                this.setState({
                    filterSourceUnit: parseInt(setting.unit),
                    filterSourceSmu: type_temp,
                    currentPageSource: 0,
                    filterOptionModal: false,
                }, () => {
                    this.setState({ loadingSource:true }, async ()=>{
                        await this.getPortData(true);
                        this.setState({ loadingSource: false })
                    })
                    
                })
            } else {
                this.setState({
                    filterTargetUnit: parseInt(setting.unit),
                    filterTargetSmu: type_temp,
                    currentPageTarget: 0,
                    filterTargetOptionModal: false,
                }, () => {
                    this.setState({ loadingTarget:true }, async ()=>{
                        await this.getPortData(false);
                        this.setState({ loadingTarget: false })
                    })
                    
                })
            }
        } else {
            this.setState({
                filterOptionModal: false,
                filterTargetOptionModal: false,
            })

        }

    }



    handleValueInput = (event) => {

        const searchText = event.target.value;
        const searchName = event.target.name;

        this.setState({ [event.target.name]: searchText });

    //    console.log(searchText)
        if( searchText === "" && searchName === "filterSourceSearch" ){
            this.setState({ ActualfilterSourceSearch: "" },()=>{
                this.getPortData(true)
            })
        }

        if( searchText === "" && searchName === "filterTargetSearch" ){
            this.setState({ ActualfilterTargetSearch: "" },()=>{
                this.getPortData(false)
            })
            
        }
        // if (!this.state.smuConnector && !this.state.smuAdapter) {
        //     this.setState({
        //         [event.target.name]: event.target.value,
        //         smuConnector: true,
        //         smuAdapter: true,
        //         currentPageSource: 0,
        //         currentPageTarget: 0,
        //     }, () => {
        //         console.log(this.state.selectedUnit)
        //         // this.getTotalPort();
        //         this.getPortData(true);
        //         this.getPortData(false);
        //     })
        // } else {
        //     this.setState({ [event.target.name]: event.target.value }, () => {
        //         console.log(this.state.selectedUnit)
        //     }, () => {
        //         // this.getTotalPort();
        //         this.getPortData(true);
        //         this.getPortData(false);
        //     })
        // }


    }


    handleSwitch = (event) => {

        this.setState({ showFrontPanelNumber: event });

    }
    handleSwitchHidden = (event) => {
        // console.log(event)
        this.setState({ showFrontPanelNumber: event.target.checked });
    }

    // handleCheckedInput = (event) => {
    //     this.setState({
    //         [event.target.name]: event.target.checked,
    //         currentPageSource: 0,
    //         currentPageTarget: 0,
    //     }, () => {
    //         // this.getTotalPort();
    //         this.getPortData(true);
    //         this.getPortData(false)
    //     })
    // }

    async removeTask(task) {
        var textMessage = ""

        if( !task.remark || ( task.remark === "-" ) ){
            textMessage = `Are you sure you want to remove ${task.description}`;
        }

        if( task.remark && ( task.remark !== "-" ) ){
            textMessage = `Are you sure you want to remove ${task.description} (${task.remark}) ?`;
        }

        const dialog = await openConfirmationRemark({
            title: "Remove Task",
            text: textMessage,
        })
        if( dialog ){
            TaskService.remove_task( task.id , this.token ).then( result => {
                // console.log( result )
                if(result.status === 1){
                    InformUser.success({ text:  result.msg })
                    this.getTask()
                }
                if(result.status !== 1){
                    InformUser.unsuccess({ text: result.msg })
                }
            })
        }
    }
    progressBarColorClasses = (status) => {

        // const layoutClass = ;


        // if (status == 3) {
            return " connecting-bar "
        // } else if (status == 2) {
        //     return " disconnecting-bar "
        // }

    }

    async rollbackTask(task){
        var textMessage = ""

        if( !task.remark || ( task.remark === "-" ) ){
            textMessage = `Are you sure you want to rollback ${task.description}?`;
        }

        if( task.remark && ( task.remark !== "-" ) ){
            textMessage = `Are you sure you want to rollback ${task.description} (${task.remark})?`;
        }

        const dialog = await openConfirmationRemark({
            title: "Remove Task",
            text: textMessage,
        })
        if( dialog ){
            // console.log( dialog )
            TaskService.rollback_task( task.id, dialog.remark, this.token ).then( result => {
                // console.log( result )
                if(result.status === 1){
                    InformUser.success({ text:  result.msg })
                    this.getTask()
                }
                if(result.status !== 1){
                    InformUser.unsuccess({ text: result.msg })
                }
            })
        }
    }

    render() {
        // const { source, target } = this.props;
        const { sourceData, targetData, pageIndex } = this.state
        // console.log(this.state.connectedPair)
        // var portSize = {
        //     width:`${this.state.portWidth}em`,
        //     height: `${this.state.portHeight}em`,
        // }

        // const renderPageControl = pageIndex.map(page => {
        //     return (
        //         <span>{page + 1}</span>
        //     )
        // });
        return (
            <div className="front-panel-container">
                <Loading open={this.state.loading} />
                <Modal
                    isOpen={this.state.routeSearchModal}
                    className="confirm-con-dis-body"
                    overlayClassName="modal-overlay"
                    onRequestClose={() => { this.closeRouteSearch(false) }}
                // closeTimeoutMS={500}
                >
                    <ConfirmConnectivity source={this.state.selectedSource} target={this.state.selectedTarget} close={this.closeRouteSearch} data={this.state.routeConnectionData} />

                </Modal>
                <Modal
                    isOpen={this.state.filterOptionModal}
                    className="filter-option-body"
                    overlayClassName="filter-option-overlay"
                    onRequestClose={() => { this.closeFilterOption(true, this.filterSetting, false) }}
                // closeTimeoutMS={500}
                >
                    <Option isSource={true} header={"Source"} filterSetting={this.filterSetting} close={this.closeFilterOption} />

                </Modal>
                <Modal
                    isOpen={this.state.filterTargetOptionModal}
                    className="filter-option-body"
                    overlayClassName="filter-option-overlay"
                    onRequestClose={() => { this.closeFilterOption(false, this.filterSetting, false) }}
                // closeTimeoutMS={500}
                >
                    <Option isSource={false} header={"Target"} filterSetting={this.filterSetting} close={this.closeFilterOption} />

                </Modal>

                <header className="h1">
                    <h1 style={{ marginRight: "1vw" }}>CONNECTIVITY: CONNECTION</h1>
                    <span style={{ marginRight: "0.5vw" }}>Front Panel View: </span>
                    <Switch
                        id="front-panel-switch"
                        onChange={this.handleSwitch}
                        uncheckedIcon={false}
                        checkedIcon={false}
                        className={(this.state.showFrontPanelNumber ? " checked " : " uncheck ") + " toggle-switch "}
                        // height={10}
                        // width={32}
                        checked={this.state.showFrontPanelNumber} />
                        <input type="checkbox" name="showFrontPanelNumber" id="hidden-switch" style={{position:"absolute",opacity:"0"}} checked={ this.state.showFrontPanelNumber } onChange={this.handleSwitchHidden} />
                </header>
                {/* <button onClick={this.props.close} className="close fit-btn"><i class="fas fa-window-close"></i></button> */}
                <div className="content-container">
                    <div className="left-content">



                        {/* <button className="fit-btn submit"><span> </span></button> */}
                        <div className="section-container source">
                            <div className="lr-container">
                                <div className="left">
                                    <h2>Source Selection</h2>
                                    {/* {renderPageControl} */}
                                    <div className="pagination-container">
                                    <span>Page:</span>
                                    <button  id="source-previous-btn" onClick={() => { this.backwardPage(true) }} className={(this.state.currentPageSource === 0 ? " btn-disable " : " ") + "fit-btn submit next"}>
                                        <span>Previous</span>
                                    </button>

                                    <select name="currentPageSource" id="source-selection-page" value={this.state.currentPageSource} onChange={(event) => { this.handlePage(event, true) }}>
                                        {
                                            this.state.pageIndexSource && this.state.pageIndexSource.map((page, index) =>
                                                <option key={`source-page-${index}`} id={`source-page-${index}`} className={page === this.state.currentPageSource ? "selected-page" : ""} value={page}>{page + 1}</option>
                                            )
                                        }
                                    </select>
                                    <button  id="source-next-btn" onClick={() => { this.forwardPage(true) }} className={(this.state.currentPageSource === (this.state.totalPageSource - 1) ? " btn-disable " : " ") + "fit-btn submit next"}>
                                        <span>Next</span>
                                    </button>
                                    <ClipLoader color="#B3B3B3" loading={this.state.loadingSource} size={20} />
                                    </div>
                                </div>
                                <div className="right">
                               
                                    <button id="source-filter-btn" className="sm-btn submit" onClick={() => { this.openFilterOption(true) }}> 
                                        <span className="result-box"> 
                                            Filter: 
                                            <span 
                                                id="source-filter-unit" 
                                                className="result unit ellipsis-text" 
                                                style={{maxWidth:"10vw"}}
                                                >
                                                {this.state.filterSourceUnitResult}
                                            </span> 
                                            <span id="source-filter-smu" className="result smu">{this.state.filterSourceSmuResult}</span> 
                                        </span> 
                                    </button>

                                    <form onSubmit={(event) => { this.searchPortDescription(event, true) }}>
                                        <ReactTooltip id='searchSource' place="top" className="noteClass defaultTooltip" arrowColor="#404040"  effect="solid" />
                                        <input id="source-search-input" data-for="searchSource" data-tip="Port description, and port alias" type="text" className="search-input" placeholder="Search" name="filterSourceSearch" maxLength={20} value={this.state.filterSourceSearch} onChange={this.handleValueInput} />
                                        <button id="source-search-btn" className="fit-btn submit search-btn" type="submit"><i class="fas fa-search"></i></button>
                                    </form>




                                </div>
                            </div>

                            <div className="box">
                                {
                                    sourceData && sourceData.map((row, rowIndex) =>
                                        <div key={`source-row-${rowIndex+1}`} id={`source-row-${rowIndex+1}`} className="row">
                                            <ReactTooltip id='sourceTooltip' place="top" html={true} className="portClass defaultTooltip" arrowColor="#404040" effect="solid" />
                                            {
                                                row.map((port, portIndex) =>

                                                    <div
                                                        // data-html="true"
                                                        key={`source-port-${portIndex+1}`}
                                                        id={`source-port-${portIndex+1}`}
                                                        data-tip={this.tooltipDescription(port)}
                                                        data-for='sourceTooltip'
                                                        onMouseEnter={() => this.setConnectedPair(port)}
                                                        onMouseLeave={() => this.resetConnectedPair()}
                                                        onClick={() => { this.selectPort(port, true) }}
                                                        className={this.lightStyleClasses(port, true)}
                                                        currentitem="false"
                                                        // className={this.state.selectedSource.idx === port.idx ? "selected port" : "port"}
                                                    >
                                                        {
                                                            !this.state.showFrontPanelNumber && <span>{port.display_no}</span>
                                                        }
                                                        {
                                                            this.state.showFrontPanelNumber && <span>{port.front_no}</span>
                                                        }
                                                        <div className="light-status"></div>

                                                    </div>

                                                )
                                            }
                                        </div>
                                    )


                                }
                                 {
                                    ((sourceData.length === 0) && (!this.state.filterSourceSearch)) && <span id="no-source-port" className="camera-offline" style={{textAlign: 'center', padding: '6.5vw'}}>NO PORT AVAILABLE</span>
                                }
                                {
                                    ((sourceData.length === 0) && (this.state.filterSourceSearch)) && <span id="no-source-port" style={{textAlign: 'center', padding: '6.5vw'}}>Your search did not match any port data.</span>
                                }
                            </div>
                        </div>
                        <div className="section-container target">
                            <div className="lr-container">
                                <div className="left">
                                    <h2>Target Selection</h2>
                                    {/* {renderPageControl} */}
                                    <div className="pagination-container">
                                    <span>Page:</span>
                                    <button id="target-previous-btn"  onClick={() => { this.backwardPage(false) }} className={(this.state.currentPageTarget === 0 ? " btn-disable " : " ") + "fit-btn submit next"}>
                                        <span>Previous</span>
                                        {/* <input type="number" id="page-forward-index" name="pageForwardIndex" value={this.state.pageForwardIndex} onChange={this.handlePageChange}/> */}
                                    </button>
                                    <select name="currentPageTarget"  id="target-selection-page"  value={this.state.currentPageTarget} onChange={(event) => { this.handlePage(event, false) }}>
                                        {
                                            this.state.pageIndexTarget && this.state.pageIndexTarget.map((page, index) =>
                                                <option  key={`target-page-${index+1}`} id={`target-page-${index+1}`}  className={page === this.state.currentPageTarget ? "selected-page" : ""} value={page}>{page + 1}</option>
                                            )
                                        }
                                    </select>
                                    <button id="target-next-btn"  onClick={() => { this.forwardPage(false) }} className={(this.state.currentPageTarget === (this.state.totalPageTarget - 1) ? " btn-disable " : " ") + "fit-btn submit next"}>
                                        <span>Next</span>
                                        {/* <input type="number" id="page-forward-index" name="pageForwardIndex" value={this.state.pageForwardIndex} onChange={this.handlePageChange}/> */}
                                    </button>
                                    <ClipLoader color="#B3B3B3" loading={this.state.loadingTarget} size={20} />
                                    </div>
                                </div>
                                <div className="right">
                                    <button  id="target-filter-btn"  className="sm-btn submit" onClick={() => { this.openFilterOption(false) }}> 
                                        <span className="result-box"> 
                                            Filter: 
                                            <span  id="target-filter-unit"  className="result unit ellipsis-text" style={{maxWidth:"10vw"}}>{this.state.filterTargetUnitResult}</span> 
                                            <span  id="target-filter-smu" className="result">{this.state.filterTargetSmuResult}</span> 
                                        </span> 
                                    </button>
                                    <form onSubmit={(event) => { this.searchPortDescription(event, false) }}>
                                        <ReactTooltip id='targetSource' place="top" className="noteClass defaultTooltip" arrowColor="#404040"  effect="solid" />
                                        <input type="text"  id="target-search-input"  className="search-input" data-for="targetSource" data-tip="Port description, and port alias" maxLength={20} placeholder="Search" name="filterTargetSearch" value={this.state.filterTargetSearch} onChange={this.handleValueInput} />
                                        <button  id="target-search-btn"  className="fit-btn submit search-btn" type="submit"><i class="fas fa-search"></i></button>
                                    </form>

                                </div>
                            </div>

                            <div className="box">
                                {
                                    targetData && targetData.map((row, rowIndex) =>
                                        <div key={`target-row-${rowIndex}`} className="row" >
                                            <ReactTooltip id='targetTooltip' place="top" html={true} className="portClass defaultTooltip" arrowColor="#404040" effect="solid" />
                                            {
                                                row.map((port,portIndex) =>
                                                    <div
                                                        key={`target-port-${portIndex+1}`}
                                                        id={`target-port-${portIndex+1}`}
                                                        data-tip={this.tooltipDescription(port)}
                                                        data-for='targetTooltip'
                                                        onMouseEnter={() => this.setConnectedPair(port)}
                                                        onMouseLeave={() => this.resetConnectedPair()}
                                                        onClick={() => { this.selectPort(port, false) }}
                                                        className={this.lightStyleClasses(port, false)}
                                                    // className={this.state.selectedTarget.idx === port.idx ? "selected port" : "port"}
                                                    >
                                                        {
                                                            !this.state.showFrontPanelNumber && <span>{port.display_no}</span>
                                                        }
                                                        {
                                                            this.state.showFrontPanelNumber && <span>{port.front_no}</span>
                                                        }
                                                        <div className="light-status"></div>
                                                    </div>
                                                )
                                            }
                                        </div>
                                    )


                                }
                                {
                                    ((targetData.length === 0) && this.state.filterTargetSearch ) && <span id="no-data-target"  style={{textAlign: 'center', padding: '6.5vw'}}>Your search did not match any port data.</span>
                                }
                                {
                                    ((targetData.length === 0) && !this.state.filterTargetSearch ) && <span id="no-data-target" className="camera-offline" style={{textAlign: 'center', padding: '6.5vw'}}>NO PORT AVAILABLE</span>
                                }
                            </div>
                        </div>
                    </div>
                    <div className="btn-row">
                        
                        {/* <input id="source-search-input" data-for="searchSource" data-tip="Port description, and port alias" type="text" className="search-input" placeholder="Search" name="filterSourceSearch" value={this.state.filterSourceSearch} onChange={this.handleValueInput} /> */}
                        
                        <button
                            id="connect-btn"
                            className={( (!this.state.selectedSource || !this.state.selectedTarget) ? 'opacity5' : '') +" btn submit connectivity-btn connect-btn "}
                            style={{marginRight:"0.5vw"}}
                            onClick={() => { this.choosePort() }}
                        ><span>Connect</span></button>
                        <button 
                            id="cancel-btn"
                            className={( (this.state.selectedSource || this.state.selectedTarget) ? 'opacity10' : 'opacity5') +" btn submit connectivity-btn cancel-btn "}
                        onClick={() => { this.clearSelected() }}><span>Cancel</span></button>
                        {
                            (this.checkSwitchPolarity()) && <button
                                id="change-pol-btn"
                                data-for="switchPol"
                                data-tip="This action is enabled for ports of model 288D only and the selected ports must be connected"
                                className="btn submit connectivity-btn switch-pol-btn"
                                style={{marginLeft:"0.5vw"}}
                                onClick={() => { this.switchPolarity() }}
                            ><span>Swap Polarity</span></button>
                            // <ReactTooltip id='switchPol' place="top" className="noteClass defaultTooltip" arrowColor="#404040"  effect="solid" />
                        }
                    </div>
                    <div className="right-content">

                        <header className="h2" style={{marginBottom:'0.5vw',padding:"0"}}>
                        <div className="lr-container">
                            <div className="left">
                            <h2>Remaining Tasks</h2>
                            </div>
                            <div className="right">
                                <Link to="/remaining-task">
                                    <button id="view-more-btn"  style={{margin:'0'}} className="sm-btn submit">
                                        <span style={{margin:'0'}}>View more</span>
                                        
                                    </button>
                                </Link>
                            </div>
                        </div>
                        </header>
                        
                        <div className="remaining-task-content">
                            <div className="table-size">

                                <table id="remaining-task-table">
                                    <tr className="tr-head">
                                        <th style={{ width: "5%" }} ><span>No.</span></th>
                                        <th style={{ width: "5%" }} ><span>Operation</span></th>
                                        <th style={{ width: "20%" }} ><span>Description</span></th>
                                        <th style={{ width: "10%" }} ><span>Add Date</span></th>
                                        {/* <th style={{ width: "10%" }} ><span>Start Date</span></th> */}
                                        <th style={{ width: "10%" }} ><span>Add By</span></th>
                                        <th style={{ width: "10%" }} ><span>Status</span></th>
                                        <th style={{ width: "15%" }} ><span>Progress</span></th>
                                        <th style={{ width: "20%" }} ><span>Remarks</span></th>
                                        <th style={{ width: "5%" }} ><span>Action</span></th>
                                    </tr>
                                    {
                                        (this.state.taskList.length > 0) && <ReactTooltip id={`remark-tooltip`} html={true} className="remarkClass defaultTooltip" place="top" arrowColor="#404040"  effect="solid"
                                        />
                                    }
                                    {
                                        (this.state.taskList.length > 0) && this.state.taskList.map((task, index) =>
                                            <tr
                                                key={`task-${index+1}`}
                                                id={`task-${index+1}`}
                                            >
                                                <td id={`task-no-${index+1}`} className="text-center"><span >{task.no}</span></td>
                                                <td id={`task-name-${index+1}`}  className="text-center"><span>{task.operation_name}</span></td>
                                                <td id={`task-description-${index+1}`}  className="text-center"><span>{task.description}</span></td>
                                                <td id={`task-add-date-${index+1}`}  className="text-center"><span>{task.add_date}</span></td>
                                                {/* <td id={`task-start-date-${index+1}`}  className="text-center"><span>{task.start_date}</span></td> */}
                                                <td id={`task-add-by-${index+1}`}  className="text-center"><span>{task.add_by}</span></td>
                                                <td id={`task-status-${index+1}`}  className="text-center"><span>{task.status_desc}</span></td>
                                                <td id={`task-progress-${index+1}`}  >
                                                    <div className="progress-container">
                                                        <div className={this.progressBarColorClasses(task.state) + " progress-bar-container "}>
                                                            <LinearProgress striped variant="determinate" value={task.progress} />
                                                        </div>
                                                        <span  id={`task-progress-percent-${index+1}`}  className="text-center">{task.progress}%</span>
                                                    </div>
                                                
                                                 </td>
                                                <td   id={`task-remark-${index+1}`}   className="text-center">
                                                    <span 
                                                        className="ellipsis-text" 
                                                        style={{width:"13vw"}}
                                                        data-for="remark-tooltip"
                                                        data-tip={ task.remark.length > 19 ? `<span>${task.remark}</span>` : ""} 
                                                    >
                                                            {task.remark}
                                                        </span>
                                                </td>
                                                <td   id={`task-action-${index+1}`}   className="text-center action-row">
                                                {
                                                        (task.status === -1) && <button id={`rollback-btn-${index+1}`} className="fit-btn submit " onClick={() => { this.rollbackTask(task) }}> <span>Rollback</span></button>
                                                    }
                                                {
                                                    (task.status !== -1) && <button id={`romove-btn-${index+1}`} className="fit-btn submit " onClick={() => { this.removeTask(task) }}> <span>Remove</span></button>
                                                }
                                                </td>
                                            </tr>



                                        )
                                    }
                                    
                                </table>
                                {
                                        (this.state.taskList.length === 0) && <div id="no-data-task" className="no-remaining-task" style={{height:"6.5vw"}}>
                                            <span className="camera-offline">NO TASK</span>
                                        </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default FrontPanel
