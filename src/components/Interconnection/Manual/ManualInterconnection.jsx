import React, { Component } from 'react'
import TreeView from '@material-ui/lab/TreeView';
import TreeItem from '@material-ui/lab/TreeItem';
import { MinusSquare, PlusSquare, CloseSquare } from "../../../shared/TreeViewIcon";
import './ManualInterconnection.scss'
import * as TopologyService from "../../../services/TopologyService";
import * as InformUser from "../../../shared/InformUser";
import { Loading } from "../../../shared/Loading";
import { environment } from "../../../Environment";
import { openConfirmation } from '../../../shared/Confirmation';
import { onUserResponse } from '../../../shared/UserResponse';

import * as UnitService from "../../../services/UnitService";



export class ManualInterconnection extends Component {


    token = JSON.parse(localStorage.getItem('userData')).token
    idCounter = 0;

    constructor(props) {
        super(props);
        this.state = {
            searchInput: "",
            checkedPort: [],

            selectingOrigin: '',
            selectingTarget: '',

            disableOrigin: false,


            interconnectionQueue: [],

            openOriginPort: false,

            isSelectingOrigin: true,
            loading: false,
            disableAddButton: true,

            originPortData: '',
            targetPortData: '',

            smuTypeSource: "0",
            smuTypeTarget: "1",
        }
    }

    componentDidMount() {
        this.setState({ loading: true }, async () => {
            const { origin, target } = this.props;
            await this.sourceFilterPort()
            await this.targetFilterPort()
            this.setState({ loading: false })
        })
    }
    onSearchOrigin = (event) => {
        this.setState({ [event.target.name]: event.target.value });

        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("origin-search");
        filter = input.value.toUpperCase();
        table = document.getElementById("origin-table");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    onSearchTarget = (event) => {
        this.setState({ [event.target.name]: event.target.value });

        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("target-search");
        filter = input.value.toUpperCase();
        table = document.getElementById("target-table");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    // async getPortOrigin(unit_id, token) {
    //     await UnitService.get_port(unit_id, token).then(result => {
    //         console.log("OriginPort", result)
    //         if (result.status == 1) {
    //             this.setState({ originPortData: result.data })
    //             // this.setState({ originPortData: result.data })
    //         } else {
    //             InformUser.unsuccess({ text: result.msg })
    //         }
    //     })
    // }
    // async getPortTarget(unit_id, token) {
    //     // console.log(unit_id)
    //     await UnitService.get_port(unit_id, token).then(result => {
    //         console.log("TargetPort", result)
    //         if (result.status == 1) {
    //             this.setState({ targetPortData: result.data })
    //             // this.setState({ originPortData: result.data })
    //         } else {
    //             InformUser.unsuccess({ text: result.msg })
    //         }
    //     })
    //     // await UnitService.get_port(unit_id, token).then( result => {
    //     //     console.log(result)
    //     //     if( result.status == 1){
    //     //         this.targetPortData = result.data;
    //     //         // this.setState({ targetPortData: result.data })
    //     //     }else{
    //     //         InformUser.unsuccess({ text: result.msg })
    //     //     }
    //     // })
    // }


    // clickCheckbox (event, newPort, isOriginSystem) {
    //     console.log(event.target)

    //     if(isOriginSystem){
    //         if(event.target.checked){
    //             this.setState({ selectingOrigin : newPort, disableOrigin: true }, ()=> {
    //                 this.addInterconnectionQueue()
    //             } )
    //         }
    //     }else{
    //         if(event.target.checked){
    //             this.setState({ selectingTarget : newPort, disableTarget: true }, ()=> {
    //                 this.addInterconnectionQueue()
    //             } )
    //         }
    //     }
    // }
    choosePort(newPort, isOrigin) {
        // console.log(newPort)
        if (newPort.intercon_status !== -1) {

            if (isOrigin) {
                if (newPort.idx !== this.state.selectingOrigin.idx) {
                    this.setState({ selectingOrigin: newPort }, () => {
                        this.shouldDisableAddButton();
                    })
                } else {
                    this.setState({ selectingOrigin: "" }, () => {
                        this.shouldDisableAddButton();
                    })
                }

            } else {
                if (newPort.idx !== this.state.selectingTarget.idx) {
                    this.setState({ selectingTarget: newPort }, () => {
                        this.shouldDisableAddButton();
                    })
                } else {
                    this.setState({ selectingTarget: "" }, () => {
                        this.shouldDisableAddButton();
                    })
                }

            }
        }



    }
    shouldDisableAddButton() {
        if (!this.state.selectingOrigin) {
            this.setState({ disableAddButton: true })
        } else {
            this.setState({ disableAddButton: false })
        }
    }


    async addInterconnectionQueue() {
        var origin = this.state.selectingOrigin;
        var target = this.state.selectingTarget;
        // console.log(origin, target)
        if(!origin || !target){
            InformUser.unsuccess({ text: "Please select port from both source and target."})
        }
        else if( origin && target ){
            const dialog = await openConfirmation({
                title: "Confirm creating interconnection.",
                text: "Are you sure you want to interconnect " + origin.description + " to " + target.description + "? "
            })
            if (dialog) {
                TopologyService.add_manual_interconnection(origin.port_fiber, target.port_fiber, this.token).then(async result => {
    
                    // console.log(result)
                    if (result.status == 1) {
                        InformUser.success({ text: result.msg })
                        this.props.pushInterconnectionTable()
                        this.deleteSelected(true);
                        this.deleteSelected(false);
                        this.sourceFilterPort();
                        this.targetFilterPort();
    
                    } else {
                        InformUser.unsuccess({ text: result.msg })
    
                    }
                })
            }
        }
        


    }
    // deletePair(pair) {
    //     var queue = this.state.interconnectionQueue;
    //     queue.map((q, index) => {
    //         if (q.id == pair.id) {
    //             queue.splice(index, 1)
    //         }
    //     })
    //     this.setState({ interconnectionQueue: queue })
    // }
    deleteSelected = (isOrigin) => {

        if (isOrigin) {
            this.setState({
                selectingOrigin: '',
                disableOrigin: false,
            }, () => {
                this.shouldDisableAddButton();
            });
        } else {
            this.setState({
                selectingTarget: '',
                disableTarget: false,
            }, () => {
                this.shouldDisableAddButton();
            });
        }

    }
    // submitInterconnection() {
    //     // this.setState({ loading:true }, async ()=> {
    //     this.state.interconnectionQueue.map(async queue => {
    //         console.log(queue)
    //         const origin = queue.origin.port_no;
    //         const target = queue.target.port_no;
    //         console.log(origin, target)
    //         await TopologyService.add_manual_interconnection(origin, target, this.token).then(async result => {
    //             console.log(result)
    //         })
    //     })
    //     // this.setState({ loading:false })
    //     // })


    // }
    showPort() {
        this.setState({ openOriginPort: true })
    }

    // async getPortOrigin(unit_id, token) {
    //     await UnitService.get_port(unit_id, token).then(result => {
    //         console.log("OriginPort", result)
    //         if (result.status == 1) {
    //             this.setState({ originPortData: result.data })
    //             // this.setState({ originPortData: result.data })
    //         } else {
    //             InformUser.unsuccess({ text: result.msg })
    //         }
    //     })
    // }
    // async getPortTarget(unit_id, token) {
    //     // console.log(unit_id)
    //     await UnitService.get_port(unit_id, token).then(result => {
    //         console.log("TargetPort", result)
    //         if (result.status == 1) {
    //             this.setState({ targetPortData: result.data })
    //             // this.setState({ originPortData: result.data })
    //         } else {
    //             InformUser.unsuccess({ text: result.msg })
    //         }
    //     })
    //     // await UnitService.get_port(unit_id, token).then( result => {
    //     //     console.log(result)
    //     //     if( result.status == 1){
    //     //         this.targetPortData = result.data;
    //     //         // this.setState({ targetPortData: result.data })
    //     //     }else{
    //     //         InformUser.unsuccess({ text: result.msg })
    //     //     }
    //     // })
    // }

    async sourceFilterPort() {
        await UnitService.get_port_by_type(this.props.origin.unit_id, this.state.smuTypeSource, this.token).then(result => {
            // console.log("TargetPort", result)
            if (result.status == 1) {
                this.setState({ originPortData: result.data })
                // this.setState({ originPortData: result.data })
            } else {
                InformUser.unsuccess({ text: result.msg })
            }
        })
    }
    async targetFilterPort() {
        await UnitService.get_port_by_type(this.props.target.unit_id, this.state.smuTypeTarget, this.token).then(result => {
            // console.log("TargetPort", result)
            if (result.status == 1) {
                this.setState({ targetPortData: result.data })
                // this.setState({ originPortData: result.data })
            } else {
                InformUser.unsuccess({ text: result.msg })
            }
        })
    }


    selectChange = (event, isFromSource) => {
        this.setState({ [event.target.name]: event.target.value }, () => {
            if (isFromSource) {
                this.sourceFilterPort()
            } else {
                this.targetFilterPort()
            }

        });
    }
    updatePort() {
        this.sourceFilterPort();
        this.targetFilterPort();
    }
    portStyleClasses(port, isSource) {
        // port.intercon_status === -1 ? " unavailable " : "" 
        // this.state.selectingOrigin.port_no === port.port_no ? " selected " : ""
        var classes = "";
        switch (port.intercon_status) {
            case -1:
                classes = classes + " unavailable "
                break;
            default:
                break;
        }

        if (isSource) {
            if (port.idx === this.state.selectingOrigin.idx) {
                classes = classes + " selected "
            }
        } else {
            if (port.idx === this.state.selectingTarget.idx) {
                classes = classes + " selected "
            }
        }

        return classes;
    }


    render() {
        const { origin, target, originPortData, targetPortData } = this.props
        // console.log(this.props)
        const renderTree = (nodes) => (
            <TreeItem id={nodes.id} key={nodes.id} checkable nodeId={nodes.id} label={nodes.name} >
                {Array.isArray(nodes.children) ? nodes.children.map((node) => renderTree(node)) : null}
            </TreeItem>
        );
        return (
            <div className="manual-interconnection">
                <Loading open={this.state.loading} />
                <div style={{marginBottom:"1vw"}} className="search-section">


                    {/* <div className="select-box"> */}

                    <div className="origin-box space-bottom">

                        <div className="system-box">
                            <div className="top-box radio1">
                                <div className="select-header">
                                    <h3>Source Unit</h3>
                                    <hr />
                                </div>
                                <div className="image-box">
                                    <div className="system-image">
                                        <img src={environment.api + origin.img_path} alt="" />
                                    </div>
                                    <div className="system-detail">
                                        <div className="input-group">
                                            <div className="left">
                                                <span>Name :</span>
                                            </div>
                                            <div className="right">
                                                <span  id="source-name">{origin.name}</span>
                                            </div>
                                        </div>
                                        <div className="input-group">
                                            <div className="left">
                                                <span>Model :</span>
                                            </div>
                                            <div className="right">
                                                <span  id="source-model">{origin.model_name}</span>
                                            </div>
                                        </div>

                                        <div className="input-group">
                                            {/* port-row */}
                                            <div className="left">
                                                <span>Port : </span>
                                            </div>
                                            <div className="right">

                                                {
                                                    !this.state.selectingOrigin && <input type="text" id="origin-search" name="origin-search" placeholder="Search port" onChange={this.onSearchOrigin} />
                                                }
                                                {
                                                    this.state.selectingOrigin && <div id="selected-source-port" className="selected-port submit " onClick={() => { this.deleteSelected(true) }}>
                                                        <span >{this.state.selectingOrigin.idx} - {this.state.selectingOrigin.description}</span>
                                                        {/* <span> */}
                                                            <i class="fas fa-times-circle close"></i>
                                                        {/* </span> */}
                                                    </div>
                                                }
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div className="input-group radio-section radio1">
                                <div className="left">
                                    <span>SMU type:</span>
                                </div>
                                <div className="right">

                                    <input className="radio-btn" type="radio" id="connector-source" name="smuTypeSource" value="0" checked={this.state.smuTypeSource === "0"}
                                        onChange={(event) => { this.selectChange(event, true) }} />
                                    <label className="margin-right-20" htmlFor="connector-source"><span>East</span></label>
                                    <input className="radio-btn" type="radio" id="adapter-source" name="smuTypeSource" value="1" checked={this.state.smuTypeSource === "1"}
                                        onChange={(event) => { this.selectChange(event, true) }} />
                                    <label htmlFor="adapter-source"><span>West</span></label>
                                </div>
                            </div>

                            <div className="system-info">

                                {
                                    // !this.state.selectingOrigin && 
                                    <div className="table-box">

                                        <div className="fix-length-table" >
                                            <table className="port-select-table" id="origin-table" cellSpacing="0" cellPadding="0">
                                                <tr className="tr-head">
                                                    <th style={{ width: "50%" }}> <span>Unit Port No.</span> </th>
                                                    <th style={{ width: "50%" }}> <span>Description</span></th>
                                                </tr>
                                                {
                                                    this.state.originPortData && this.state.originPortData.map(port =>
                                                        <tr className={this.portStyleClasses(port, true)} onClick={() => { this.choosePort(port, true) }}>
                                                            <td className="text-center"> <span>{port.front_no}</span> </td>
                                                            <td className="text-center"> <span>{port.description}</span> </td>
                                                        </tr>
                                                    )
                                                }
                                            </table>
                                        </div>

                                    </div>
                                }

                            </div>
                        </div>
                    </div>
                    {/* <hr /> */}
                    <div className="target-box">

                        <div className="system-box">
                            <div className="top-box radio2">
                                <div className="select-header">
                                    <h3>Target Unit</h3>
                                    <hr />
                                </div>
                                <div className="image-box">
                                    <div className="system-image">
                                        <img src={environment.api + target.img_path} alt="" />
                                    </div>
                                    <div className="system-detail">
                                        <div className="input-group">
                                            <div className="left">
                                                <span>Name :</span>
                                            </div>
                                            <div className="right">
                                                <span>{target.name}</span>
                                            </div>
                                        </div>
                                        <div className="input-group">
                                            <div className="left">
                                                <span>Model :</span>
                                            </div>
                                            <div className="right">
                                                <span>{target.model_name}</span>
                                            </div>
                                        </div>
                                        <div className="input-group">
                                            {/* port-row */}
                                            <div className="left">
                                                <span>Port : </span>
                                            </div>
                                            <div className="right">

                                                {
                                                    !this.state.selectingTarget && <input type="text" id="target-search" name="target-search" placeholder="Search port" onChange={this.onSearchTarget} />
                                                }
                                                {
                                                    this.state.selectingTarget && <div className="selected-port submit" onClick={() => { this.deleteSelected(false) }}>
                                                        <span>{this.state.selectingTarget.idx} - {this.state.selectingTarget.description}</span>
                                                        {/* <span> */}
                                                            <i class="fas fa-times-circle close"></i>
                                                        {/* </span> */}
                                                    </div>
                                                }
                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </div>

                            <div className="input-group radio-section radio2">
                                <div className="left">
                                    <span>SMU type:</span>
                                </div>
                                <div className="right">

                                    <input className="radio-btn" type="radio" id="connector" name="smuTypeTarget" value="0" checked={this.state.smuTypeTarget === "0"}
                                        onChange={(event) => { this.selectChange(event, false) }} />
                                    <label className="margin-right-20" for="connector"><span>East</span></label>
                                    <input className="radio-btn" type="radio" id="adapter" name="smuTypeTarget" value="1" checked={this.state.smuTypeTarget === "1"}
                                        onChange={(event) => { this.selectChange(event, false) }} />
                                    <label for="adapter"><span>West</span></label>
                                </div>

                            </div>
                            <div className="system-info">

                                {
                                    // !this.state.selectingTarget && 
                                    <div className="table-box">

                                        {
                                            // !this.state.selectingTarget && 
                                            <div className="fix-length-table">
                                                <table className="port-select-table" id="target-table" cellSpacing="0" cellPadding="0">

                                                    <tr className="tr-head">
                                                        <th style={{ width: "50%" }}> <span>Unit Port No.</span> </th>
                                                        <th style={{ width: "50%" }}> <span>Description</span></th>

                                                    </tr>
                                                    {
                                                        this.state.targetPortData && this.state.targetPortData.map(port =>
                                                            <tr className={this.portStyleClasses(port, false)} onClick={() => { this.choosePort(port, false) }}>
                                                                <td className="text-center"><span>{port.front_no}</span></td>
                                                                <td className="text-center"> <span>{port.description}</span></td>
                                                            </tr>
                                                        )
                                                    }

                                                </table>
                                            </div>
                                        }

                                    </div>
                                }

                            </div>
                        </div>
                    </div>
                    {/* </div> */}

                </div>
                {/* <hr /> */}
                {/* <div className="search-box"> */}
                    <div className="btn-row">
                        <button className={( (!this.state.selectingOrigin || !this.state.selectingTarget) ? " cannot-click " : "")+" btn submit "} onClick={this.addInterconnectionQueue.bind(this)}><span>Add</span></button>
                    </div>
                {/* </div> */}
                {/* <div className="checked-port-list">
                    
                    <div className="checked-port-label">
                        <span>Checked Port: </span>
                    </div>
                    <div className="pair-box">
                        {
                            this.state.interconnectionQueue && this.state.interconnectionQueue.map( queue => 
                                <div className="selected-port" onClick={ ()=>{ this.deletePair(queue) }}>
                                    <span>{queue.origin.id}-{queue.target.id}</span>
                                    <button>x</button>
                                </div>
                            )
                        }
                    </div>
                    
                </div> */}
                {/* <div className="save-box">
                        <button className="submit-button submit-connection" onClick={ this.submitInterconnection.bind(this)} >Submit Connection</button>
                </div> */}


            </div >
        )
    }
}

export default ManualInterconnection
