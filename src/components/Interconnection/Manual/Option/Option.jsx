import React, { Component } from 'react'
import * as SystemService from "./../../../../services/SystemService";
import './Option.scss'

export class Option extends Component {

    token = JSON.parse(localStorage.getItem('userData')).token;

    constructor(props) {
        super(props);
        this.state = {
            unitList: '',
            filterUnit: null,
            filterConnector: true,
            filterAdapter: true,
        }
    }


    componentDidMount() {
        // console.log(this.props)
        this.initialSetting();
        this.getUnitList();
    }

    initialSetting() {
        const { filterSetting } = this.props;
        var smuConnector;
        var smuAdapter;
        if (filterSetting.smu_type === 1) {
            smuConnector = false;
            smuAdapter = true;
        } else if (filterSetting.smu_type === 0) {
            smuConnector = true;
            smuAdapter = false;
        } else {
            smuConnector = true;
            smuAdapter = true;
        }

        this.setState({
            filterUnit: filterSetting.unit,
            filterConnector: smuConnector,
            filterAdapter: smuAdapter,
        })

    }

    async getUnitList() {
        await SystemService.get_unit(this.token).then(result => {
            // console.log("yut", result)
            if (result.status === 1) {
                const enabledUnits = result.data.filter( unit => unit.enabled === 1 );
                this.setState({ unitList: enabledUnits })
            }
        })
    }

    handleValueInput = (event) => {

        if (!this.state.smuConnector && !this.state.smuAdapter) {
            this.setState({
                [event.target.name]: event.target.value,
                smuConnector: true,
                smuAdapter: true,
                currentPageSource: 0,
                currentPageTarget: 0,
            })
        } else {
            this.setState({ [event.target.name]: event.target.value }, () => {
                // console.log(this.state.selectedUnit)
            })
        }


    }

    handleCheckedInput = (event) => {
        this.setState({
            [event.target.name]: event.target.checked,
            currentPageSource: 0,
            currentPageTarget: 0,
        })
    }

    applyFilter() {

        const { filterUnit, filterConnector, filterAdapter } = this.state;

        var filterSmu;

        if (filterConnector && !filterAdapter) {
            filterSmu = 0;
        } else if (!filterConnector && filterAdapter) {
            filterSmu = 1;
        } else {
            filterSmu = null;
        }

        const setting = {
            "unit": this.state.filterUnit,
            "smu_type": filterSmu,
        }

        this.props.close(this.props.isSource, setting, true)
    }

    render() {

        return (
            <div className="filter-container">
                <button id="dialog-close-btn" onClick={() => { this.props.close(this.props.isSource, this.props.filterSetting) }} className="close fit-btn"><i class="fas fa-times"></i></button>

                <header className="h2"><h2>{this.props.header} Filter Setting</h2></header>

                <div className="content">


                    <div className="input-group">
                        <div className="left" style={{paddingLeft:"2vw"}}>
                            <span>Unit : </span>
                        </div>
                        <div className="right">
                            <select style={{ width: "10vw", marginRight: "0.5vw" }} onChange={this.handleValueInput} value={this.state.filterUnit} name="filterUnit" id="filterUnit">
                                <option i="unit-list-0" value={null}>All units</option>
                                {
                                    this.state.unitList && this.state.unitList.map((unit, index) =>
                                        <option id={`unit-list-${index+1}`} key={`unit-list-${index+1}`} value={unit.id}>{unit.name}</option>
                                    )
                                }
                            </select>
                        </div>
                    </div>
                    <div className="input-group">
                        <div className="left" >
                            {/* <span>SMU type :</span> */}
                        </div>
                        <div className="right">
                            <input className="checkmark" type="checkbox" onChange={this.handleCheckedInput} checked={this.state.filterConnector} name="filterConnector" id="filterConnector" />
                            <label htmlFor="filterConnector" className="checkmark"  style={{ marginRight: "1vw" }}><span>East</span></label>

                            <input className="checkmark" type="checkbox" onChange={this.handleCheckedInput} checked={this.state.filterAdapter} name="filterAdapter" id="filterAdapter" />
                            <label htmlFor="filterAdapter"  className="checkmark" ><span>West</span></label>
                        </div>
                    </div>

                </div>
                <div className="btn-row" style={{ paddingTop: "1vw" }}>
                    <button id="confirm-submit-btn" onClick={() => { this.applyFilter() }} className="btn submit" style={{marginRight:"0.5vw"}}><span>Apply Filter</span></button>
                    <button id="cancel-submit-btn" onClick={() => { this.props.close(this.props.isSource, this.props.filterSetting, false) }} className="btn cancel"><span>Cancel</span></button>

                </div>
            </div>
        )
    }
}

export default Option
