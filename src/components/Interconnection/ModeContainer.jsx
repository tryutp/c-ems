import React, { Component } from 'react'
import './ModeContainer.scss'
import ReactTooltip from "react-tooltip";
import Manual from "./Manual/Manual";
import ManualInterconnection from "./Manual/ManualInterconnection";
import Auto from "./Auto/Auto";
import FrontPanel from "./Manual/FrontPanel";
import { environment } from "../../Environment";
import * as TopologyService from "../../services/TopologyService";
import * as UnitService from "../../services/UnitService";
import * as InformUser from "../../shared/InformUser";
import { Loading } from "../../shared/Loading";
import { top } from '@popperjs/core';
import { openConfirmation } from '../../shared/Confirmation';
import { Video } from "./../../shared/Video/Video";
import Modal from "react-modal";

export class ModeContainer extends Component {


    token = JSON.parse(localStorage.getItem('userData')).token

    constructor(props) {
        super(props)
        this.state = {
            autoMode: false,
            openSelection: false,

            originData: '',
            targetData: '',
            treeData: [],

            loading: false,
            openDropUnitVideo:false,
            addedInterconnection: this.props.interconnectionHistory,

        }
        this.manualInterconnection = React.createRef();
    }

    componentDidMount() {

        // TopologyService.get_containerTree(this.token).then(result => {

        //     if (result.status == 1) {
        //         this.setState({ treeData: result.data })
        //     } else {
        //         console.log(result)
        //     }
        // })
        // this.getData()
        // console.log(this.state.addedInterconnection)


    }


    // getData(){
    //     const { origin, target } = this.props
    //     this.setState({ loading:true }, async ()=> {
    //         await TopologyService.get_addedInterconnection(origin.unit_id, target.unit_id, this.token).then(result => {
    //             if(result.status == 1){
    //                 this.setState({ addedInterconnection: result.data })
    //             }
    //         })
    //     })

    //     this.setState({ loading:false }, async ()=> {

    //         // await UnitService.get_port(origin.unit_id,this.token).then(  result => {
    //         //     console.log(result)
    //         //     if(result.status == 1){
    //         //         this.setState({ originData: result.data })
    //         //     }else{
    //         //         InformUser.unsuccess({ text: result.msg})
    //         //     }
    //         // })
    //         // await UnitService.get_port(target.unit_id, this.token).then( result => {
    //         //     console.log(result)
    //         //     if(result.status == 1){
    //         //         this.setState({ targetData: result.data })
    //         //     }else{
    //         //         InformUser.unsuccess({ text: result.msg})
    //         //     }
    //         // })
    //         console.log( this.state.originData);
    //         this.setState({ loading:false, openInterconnection:true })
    //     });

    // }
    pushInterconnectionTable() {
        // console.log(data)
        this.getAddedInterconnection()
    }

    toggleMode(isAuto) {
        if ((isAuto && !this.state.autoMode) || (!isAuto && this.state.autoMode)) {
            this.setState({ autoMode: !this.state.autoMode })
        }
    }
    handleFrontPanel() {
        this.setState({ openSelection: true })
    }
    async removeInterconnection(interconnection) {
        // console.log(interconnection)

        if (interconnection && interconnection.id) {
            const dialog = await openConfirmation({
                title: "Remove Interconnection",
                text: `Are you sure you want to remove an interconnection between ${interconnection.name}?`
            })
            if (dialog) {
                TopologyService.remove_interconnection(interconnection.id, this.token).then(async result => {
                    // console.log("remove intercon", result);
                    if (result.status === 1) {
                        InformUser.success({ text: result.msg })
                        await TopologyService.get_addedInterconnection(this.props.origin.id, this.props.target.id, this.token).then(result => {
                            // console.log("added interconnect", result)
                            if (result.status == 1) {
                                if (result.data) {
                                    this.setState({ addedInterconnection: result.data })
                                    // this.addedInterconnection = result.data;
                                    if(this.manualInterconnection.current) {
                                        this.manualInterconnection.current.updatePort();
                                    }
                                }
                            }
                        })
                    } else {
                        InformUser.unsuccess({ text: result.msg });
                    }
                })
            }
        } else {
            InformUser.unsuccess({ text: "Data is incomplete" })
        }
    }




    async getAddedInterconnection() {
        await TopologyService.get_addedInterconnection(this.props.origin.id, this.props.target.id, this.token).then(result => {
            // console.log("added interconnect", result)
            if (result.status == 1) {
                if (result.data) {
                    this.setState({ addedInterconnection: result.data })
                    // this.addedInterconnection = result.data;
                }
            }
        })
    }

    openManual(video){
        
        if( video === "CREATE_INTERCON" ){
            this.videoTitle = "Add Interconnection link with Auto Selection."
            this.videoSrc = "/xen/resources/video/create_interconnection.mp4"
        }
        
        this.setState({ openDropUnitVideo:true })
    }

    
    closeVideo = () => {
        this.setState({ openDropUnitVideo:false })
    }



    render() {
        const { origin, target, interconnectionHistory } = this.props
        // console.log(targetPortData)
        // console.log(this.state.addedInterconnection)
        return (
            <div className="mode-container">
                <button onClick={this.props.close} id="page-close-btn" className="close fit-btn"><i class="fas fa-times"></i></button>
                <Loading open={this.state.loading} />
                <Modal
                    isOpen={this.state.openDropUnitVideo}
                    className="video-body 181818"
                    overlayClassName="white-overlay"
                    onRequestClose={() => {
                        this.closeVideo();
                    }}
                    // closeTimeoutMS={500}
                >
                    <Video videoTitle={this.videoTitle} videoSrc={this.videoSrc} close={this.closeVideo} />
                </Modal>
                <header><h1>Create Interconnection</h1></header>
                <div className="auto-manual-selection">
                    <div className="toggle-btn">
                        <button id="auto-selection-btn" className={this.state.autoMode ? "auto selected-mode" : "auto"} onClick={() => { this.toggleMode(true) }} ><span >Auto Selection</span></button>
                        <button id="manual-selection-btn"  className={!this.state.autoMode ? "manual selected-mode" : "manual"} onClick={() => { this.toggleMode(false) }} ><span>Manual Selection</span></button>
                    </div>
                </div>
                <div className="content">
                    {/* <div id="selectedSystems" className="selected-box">
                        <div className="selected-system origin-profile">
                            <div className="system-image">
                                <img src={ environment.api + origin.img_path} alt=""/>
                            </div>
                            <div className="system-info">
                                <p>Origin System : {origin.name}</p>
                                <p>Unit Model : {origin.model_id}</p>
                            </div>
                        </div>
                        <div className="selected-system target-profile">
                            <div className="system-image">
                                <img src={environment.api + target.img_path} alt=""/>
                            </div>
                            <div className="system-info">
                                <p>Target System : {target.name}</p>
                                <p>Unit Model : {target.model_id}</p>
                            </div>
                        </div>
                    </div> */}

                    <div className="user-action-box">

                        {/* <div className="interconnect-form"> */}
                        {
                            this.state.autoMode && <Auto origin={origin} target={target} pushInterconnectionTable={this.pushInterconnectionTable.bind(this)} />
                        }
                        {
                            !this.state.autoMode && <ManualInterconnection ref={this.manualInterconnection} origin={origin} target={target} pushInterconnectionTable={this.pushInterconnectionTable.bind(this)} />
                            // <Manual openSelection={this.handleFrontPanel.bind(this)} />
                        }

                        {/* </div> */}
                    </div>

                    <div className="intercon-history-box">
                        <header className="h2">
                            <div className="lr-container">
                                <div className="left">
                                    <h2>Interconnection List</h2>
                                </div>
                                <div className="right">
                                    <ReactTooltip  id="registeredUnitTooltip" place="left" effect="solid" />
                                    <span   data-for="registeredUnitTooltip"  data-tip="Click Tutorial: How to create interconnection link." className="question" onClick={ ()=>{this.openManual("CREATE_INTERCON")} }><i class="fas fa-question-circle"></i></span>
                                </div>
                                </div>
                            </header>

                        {
                            interconnectionHistory && <div className="interconnection-his" >
                                <table id="origin-table" cellSpacing="0" cellPadding="0">
                                    <tr>
                                        <th ><span>Name</span> </th>
                                        {/* <th ><span>Source Port</span>  </th> */}
                                        <th ><span> Source SMU Type</span>  </th>
                                        {/* <th ><span>Target Port</span> </th> */}
                                        <th ><span>Target SMU Type</span>  </th>
                                        <th ><span>Action</span> </th>
                                    </tr>
                                    {
                                        this.state.addedInterconnection && this.state.addedInterconnection.map((interconnection, index) =>
                                            <tr key={`interconn-link-${index+1}`}  id={`interconn-link-${index+1}`}>
                                                <td id={`interconn-name-${index+1}`} className="text-center"><span>{interconnection.name}</span></td>
                                                {/* <td id={`interconn-source-port-${index+1}`} className="text-center"><span>{interconnection.port_no1}</span></td> */}
                                                <td id={`interconn-source-smu-${index+1}`} className="text-center"><span>{interconnection.smu_type1}</span></td>
                                                {/* <td id={`interconn-target-port-${index+1}`} className="text-center"><span >{interconnection.port_no2}</span></td> */}
                                                <td id={`interconn-target-smu-${index+1}`} className="text-center"><span>{interconnection.smu_type2}</span></td>
                                                <td id={`interconn-action-${index+1}`} className="text-center action">
                                                    <button id={`interconn-remove-btn-${index+1}`} className="fit-btn submit" onClick={() => { this.removeInterconnection(interconnection) }}><span ><i class="fas fa-trash-alt"></i></span></button>
                                                </td>
                                            </tr>
                                        )
                                    }
                                </table>
                            </div>
                        }



                    </div>
                </div>




                {/* <div className="submit-box">
                <button>Connect</button>
            </div> */}


            </div>
        )
    }
}

export default ModeContainer
