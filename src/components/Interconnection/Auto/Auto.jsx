import React, { Component } from 'react'
import * as TopologyService from "../../../services/TopologyService";
import * as InformUser from "../../../shared/InformUser";
import { Loading } from "../../../shared/Loading";
import { environment } from "../../../Environment";
import { openConfirmation } from '../../../shared/Confirmation';
import { onUserResponse } from '../../../shared/UserResponse';
import './Auto.scss'


export class Auto extends Component {

    token = JSON.parse(localStorage.getItem('userData')).token;


    constructor(props) {
        super(props);
        this.state = {
            qtyNumber: 0,
            loading: false,
            caseCC: 0, caseCA: 0, caseAC: 0, caseAA: 0,
        }
    }

    onChangeQuantity(event) {
        this.setState({ [event.target.name]: event.target.value }, () => {
            // console.log(this.state.qtyNumber)

        })
    }
    handleInput = (event) => {
        this.setState({ [event.target.name]: event.target.value })
    }
    async addAutoInterconnection() {
        const { origin, target } = this.props
        const a = parseInt(this.state.caseCC)
        const b = parseInt(this.state.caseCA)
        const c = parseInt(this.state.caseAC)
        const d = parseInt(this.state.caseAA)

        if ( isNaN(a) || isNaN(b) || isNaN(c) || isNaN(d) ){
            InformUser.unsuccess({ text: "Incorrect number of interconnection link" })
        }
        else if ( a < 0 || b < 0 || c < 0 || d < 0 ) {
            InformUser.unsuccess({ text: "Incorrect number of interconnection link" })
        }
        else{
            
            
            const data = {
                "unit_id1": origin.unit_id,
                "unit_id2": target.unit_id,
                "case1_qty": a,
                "case2_qty": b,
                "case3_qty": c,
                "case4_qty": d,
            }
    
            const intercon_qyt = a + b + c + d;
    
            const dialog = await openConfirmation({
                title: "Create Interconnection",
                text: `Are you sure you want to create ${intercon_qyt} interconnection(s) automatically?`
            })
            if (dialog) {
                // console.log(data)
                TopologyService.add_auto_interconnection(data, this.token).then(async result => {
                    // console.log(result)
                    if (result.status === 1) {
                        InformUser.success({ text: result.msg })
                        this.props.pushInterconnectionTable();
                        // this.setState({ loading: false }, () => {
                        
                        // })
    
                    } else {
                        InformUser.unsuccess({ text: result.msg })
                        
                    }
                })
            }
        }
        
        // console.log(origin)
    }

    render() {
        const { origin, target } = this.props
        return (
            <div className="auto-container">
                <div className="system-info">
                    <div className="top-box radio1">
                        <div className="select-header">
                            <h3>Source Unit</h3>
                            <hr />
                        </div>
                        <div className="image-box">
                            <div className="system-image">
                                <img src={environment.api + origin.img_path} alt="" />
                            </div>
                            <div className="system-detail">
                                <div className="input-group">
                                    <div className="left">
                                        <span>Name :</span>
                                    </div>
                                    <div className="right">
                                        <span id="source-name">{origin.name}</span>
                                    </div>
                                </div>
                                <div className="input-group">
                                    <div className="left">
                                        <span>Model :</span>
                                    </div>
                                    <div className="right">
                                        <span id="source-model">{origin.model_name}</span>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div className="top-box radio2">
                        <div className="select-header">
                            <h3>Target Unit</h3>
                            <hr />
                        </div>
                        <div className="image-box">
                            <div className="system-image">
                                <img src={environment.api + target.img_path} alt="" />
                            </div>
                            <div className="system-detail">
                                <div className="input-group">
                                    <div className="left">
                                        <span>Name :</span>
                                    </div>
                                    <div className="right">
                                        <span id="target-name">{target.name}</span>
                                    </div>
                                </div>
                                <div className="input-group">
                                    <div className="left">
                                        <span>Model :</span>
                                    </div>
                                    <div className="right">
                                        <span id="target-model">{target.model_name}</span>
                                    </div>
                                </div>


                            </div>
                        </div>

                    </div>

                </div>
                <div className="quantity-input">
                    {/* <input id="qty-number"  name="qtyNumber" type="number" placeholder="Enter Quantity Number" onChange={this.onChangeQuantity.bind(this)} /> */}
                    <div className="content">
                        <div className="header h2">
                            <h2>Interconnection Link</h2>
                        </div>
                        <div className="group">
                            <div className="input-group">
                                <div className="left"><span>East - East :</span></div>
                                <div className="right"><span><input type="number" id="case-cc" name="caseCC" value={this.state.caseCC} onChange={this.handleInput} min={0} /></span></div>
                            </div>
                            <div className="input-group">
                                <div className="left"><span>East - West :</span></div>
                                <div className="right"><span><input type="number" id="case-ca" name="caseCA" value={this.state.caseCA} onChange={this.handleInput} min={0} /></span></div>
                            </div>
                            <div className="input-group">
                                <div className="left"><span>West - East :</span></div>
                                <div className="right"><span><input type="number" id="case-ac" name="caseAC" value={this.state.caseAC} onChange={this.handleInput} min={0} /></span></div>
                            </div>
                            <div className="input-group">
                                <div className="left"><span>West - West :</span></div>
                                <div className="right"><span><input type="number" id="case-aa" name="caseAA" value={this.state.caseAA} onChange={this.handleInput} min={0} /></span></div>
                            </div>
                        </div>
                        <div className="btn-row">
                            <button id="add-auto-btn" className="btn submit" onClick={this.addAutoInterconnection.bind(this)}><span>Add</span></button>

                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default Auto
