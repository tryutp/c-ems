import React from 'react';

const Port =(props)=> {
    return (
        <div className='product'>
            <h1>{props.port_no}</h1>
            <p>{props.description}</p>
        </div>
    )
}

export default Port;