import React, { Component } from "react";
import "./LiveView.scss";
import * as InfoService from "./../../services/InfoService";
import * as InformUser from "../../shared/InformUser";

export class LiveView extends Component {
	token = JSON.parse(localStorage.getItem("userData")).token;

	constructor(props) {
		super(props);
		this.state = {
			unitStatusList: "",
			displayingUnit: [],
			paused: [false, false],
			finished: [[], []],
			source_snapshot: "",
		};
	}
	imageNr = [0, 0]; // Serial number of current image

	// References to img objects which have finished downloading

	componentDidMount() {
		this.getUnitOverviewStatus();
	}

	async getUnitOverviewStatus() {
		const unit_per_page = 100;
		const page = 1;
		await InfoService.get_unit_overview_status(
			unit_per_page,
			page,
			this.token
		).then((result) => {
			// console.log("UnitStatusList", result);
			if (result.status == 1) {
				if (result.data) {
					this.setState({ unitStatusList: result.data });
				} else {
					InformUser.unsuccess({ text: "No unit data for overview" });
				}
			} else {
				InformUser.unsuccess({ text: result.msg });
			}
		});
	}
	// async getUnitOverviewStatus() {
	//     const unit_per_page = 100;
	//     const page = 1;
	//     await InfoService.get_unit_overview_all(unit_per_page, page, this.token).then(result => {
	//         console.log("UnitStatusList", result)
	//         if (result.status == 1) {
	//             if (result.data) {
	//                 this.setState({ unitStatusList: result.data });
	//             } else {
	//                 InformUser.unsuccess({ text: "No unit data for overview" });
	//             }
	//         } else {
	//             InformUser.unsuccess({ text: result.msg });
	//         }
	//     })
	// }
	selectLiveview(unit) {
		const displayingUnit = this.state.displayingUnit;
		var found = false;

		displayingUnit.map((displayedUnit, index) => {
			if (displayedUnit.name === unit.name) {
				this.removeLiveShow(index);
				found = true;
			}
		});
		if (!found) {
			if (displayingUnit.length <= 1) {
				this.addShowList(unit);
			}
		}
		// else{
		//     this.addShowList(unit);
		// }
	}
	addShowList(unit) {
		// console.log(unit);

		const operatingUnit = this.props.operatingUnit;
		// console.log("operatingUnit", operatingUnit);
		var selected = "";
		operatingUnit.map((operating) => {
			if (unit.name === operating.name) {
				selected = operating;
			}
		});
		if (selected === "") {
			selected = unit;
		}

		var showList = this.state.displayingUnit;
		if (this.state.displayingUnit.length >= 2) {
			showList.shift();
			showList.push(selected);
			this.setState({ displayingUnit: showList }, () => {
				// console.log(this.state.displayingUnit);
			});
		} else {
			// this.changeIpStream(selected)
			showList.push(selected);
			// console.log('add')

			this.setState({ displayingUnit: showList }, () => {
				// console.log(this.state.displayingUnit);
				// this.createImageLayer(showList.length - 1);

				this.addingLiveviewId(unit);
			});
		}
	}

	addingLiveviewId(unit) {
		if (document.getElementById(`sourceStream-0`)) {
			this.centeringLiveview("0");
		}
		if (document.getElementById(`sourceStream-1`)) {
			this.centeringLiveview("1");
		}
	}

	centeringLiveview(id) {
		// console.log("center", id);
		var container = document.getElementById("liveview-body-"+id);

		if (document.getElementById("sourceStream-"+id)) {
			var imgStream = document.getElementById("sourceStream-"+id);
			var containerBody = {
				x: container.clientWidth,
				y: container.clientHeight,
			};
			var imgBody = {
				x: imgStream.clientWidth,
				y: imgStream.clientHeight,
			};
			var topScroll = (container.scrollTop =
				imgBody.y / 2 - containerBody.y / 2);
			var leftScroll = imgBody.x / 2 - containerBody.x / 2;
			container.scrollTop = topScroll;
			// console.log(container)
			container.scrollLeft = leftScroll;
			// console.log(containerBody, imgBody, topScroll, leftScroll);
		}
	}

	// changeIpStream() {
	// 	let url_str = String(window.location.href).split("/");
	// 	let ip_addr = url_str[2];
	// 	this.source_snapshot = url_str[0] + "//" + ip_addr + "/snapshot?n=";
	// }
	async removeLiveShow(index) {
		var showList = this.state.displayingUnit;
		// await this.emptyImageSource();

		showList.splice(index, 1);
		this.setState({
			displayingUnit: showList,
			// paused: false,
			// finished: [],
			// source_snapshot: "",
		});
	}

	// emptyImageSource() {
	// 	this.setState({ paused: true }, () => {
	// 		var webcam = document.getElementById("webcam");
	// 		var temp = webcam.querySelectorAll("img");
	// 		console.log(temp);
	// 		Array.from(temp).forEach((imgElement) => {
	// 			imgElement.src = null;
	// 		});
	// 		console.log(temp);
	// 	});
	// }

	buttonStyles(unit) {
		var style = "  btn margin-bottom btn-click ";
		this.state.displayingUnit.map((liveview) => {
			if (unit.name === liveview.name) {
				style = style + " showing ";
			}
		});
		return style;
	}
	statusLightClasses(unit) {
		var classes = " light-status ";
		var active_status = unit.active_status;
		var { operatingUnit } = this.props;
		operatingUnit.map((operator) => {
			if (unit.name === operator.name && operator.state === 3) {
				classes = classes + " operating ";
			}
		});
		if (active_status === 1) {
			classes = classes + " active ";
		}

		return classes;
	}

	// createImageLayer(index) {
	// 	var img = new Image();
	// 	var _self = this;
	// 	img.style.position = "absolute";
	// 	img.style.zIndex = "-1";
	// 	img.style.width = "30vw";
	// 	// img.style.imageRendering = "pixelated";
	// 	// console.log('creat')
	// 	img.onload = function () {
	// 		var _this = this;
	// 		_this.style.zIndex = _self.imageNr[index].toString();
	// 		// _this.style.zIndex = _self.imageNr; // Image finished, bring to front!
	// 		while (1 < _self.state.finished[index].length) {
	// 			var del = _self.state.finished[index].shift(); // Delete old image(s) from document
	// 			if (del.parentNode) {
	// 				del.parentNode.removeChild(del);
	// 			}
	// 		}
	// 		_self.state.finished[index].push(this);
	// 		if (!_self.state.paused[index]) _self.createImageLayer(index);
	// 	};
	// 	img.onerror = function imageError() {
	// 		// console.log('load image error')
	// 	};
	// 	// img.onunload = function onUnload() {
	// 	//   console.log('on unload')
	// 	// };
	// 	img.onclick = function imageOnclick() {
	// 		// console.log('click on img')
	// 		//   _self.close()
	// 	};
	// 	img.src = "http://192.168.60.21/snapshot?n=" + ++this.imageNr[index];
	// 	// img.src = this.source_snapshot + (++this.imageNr);
	// 	// console.log(index)
	// 	var webcam = document.getElementById("webcam" + index);
	// 	if (webcam) {
	// 		webcam.insertBefore(img, webcam.firstChild);
	// 	}
	// }

	render() {
		// console.log(this.props)
		return (
			<div className="live-view-container">
				<button
					id="dialog-close-btn"
					onClick={() => {
						this.props.close();
					}}
					className="close fit-btn"
				>
					<i class="fas fa-times"></i>
				</button>
				<header>
					<h2>Live View</h2>
				</header>
				<div className="content">
					<div className="operating-tab">
						{/* <h3 className="margin-bottom">Operating System</h3> */}
						{this.state.unitStatusList &&
							this.state.unitStatusList.map((unit, index) => (
								<button
									id={`liveview-btn-${index + 1}`}
									key={`liveview-btn-${index + 1}`}
									onClick={() => {
										this.selectLiveview(unit);
									}}
									className={this.buttonStyles(unit)}
								>
									<span
										className={this.statusLightClasses(
											unit
										)}
									></span>
									<span style={{ overflow: "hidden" }}>
										{unit.name}
									</span>
								</button>
							))}
					</div>
					<div className="live-container">
						{this.state.displayingUnit &&
							this.state.displayingUnit.map((unit, index) => (
								<div
									id={`liveview-unit-${index + 1}`}
									key={`liveview-unit-${index + 1}`}
									className="img-size"
								>
									<div className="img-size15">
									<div
										id={`liveview-body-${index}`}
										className="img-size2"
									>
										<button
											id={`liveview-close-btn`}
											onClick={() => {
												this.removeLiveShow(index);
											}}
											className="close fit-btn shafted"
										>
											<i class="fas fa-times"></i>
										</button>

										{/* <div
											className="webcam"
											id={`webcam${index}`}
										>
											&nbsp;
										</div> */}

										<img
											id={`sourceStream-${index}`}
											src={unit.liveview}
											alt=""
										/>
										<span>{unit.name}</span>
									</div>
									</div>
								</div>
							))}
					</div>
				</div>
			</div>
		);
	}
}

export default LiveView;
