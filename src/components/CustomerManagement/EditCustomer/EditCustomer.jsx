import React, { Component } from 'react'
import './EditCustomer.scss';
import * as CustomerService from "./../../../services/CustomerService";

import { openConfirmation } from "./../../../shared/Confirmation";
import * as InformUser from "./../../../shared/InformUser";
import { onUserResponse } from "./../../../shared/UserResponse";

export class EditCustomer extends Component {

    token = JSON.parse(localStorage.getItem('userData')).token;

    constructor(props) {
        super(props);
        this.state = {
            c_email: '',
            c_firstname: '',
            c_lastname: '',
            c_password: '',
            c_confirm_password: '',
            changePassword: false,
            invalided: false,
        }
    }

    componentDidMount() {
        // console.log(this.props)
        // this.getEditingCustomer();
        this.setState({
            c_email: this.props.customer.email,
            c_firstname: this.props.customer.firstname,
            c_lastname: this.props.customer.lastname,
        })
    }

    handleInput = (event) => {
        // console.log(event)
        this.setState({ [event.target.name]: event.target.value })
    }

    async getEditingCustomer() {
        const { customer } = this.props;
        await CustomerService.get_customer(customer.id, this.token).then(result => {
            // console.log(result)
            const { email, firstname, lastname } = result.data;
            if (result.status === 1) {
                this.setState({
                    c_username : this.props.username,
                    c_email: email,
                    c_firstname: firstname,
                    c_lastname: lastname,
                    c_password: '',
                    c_confirm_password: ''

                })
            }
        })
    }

    async submitEditing(event){

        event.preventDefault()
        const { c_firstname, c_lastname, c_email, c_password, c_confirm_password } = this.state;
        this.setState({ invalided: true })
       
            
            var dataIsNotOkay = await this.verifyData(
                c_firstname,
                c_lastname,
                c_email,
            );
            
            if (dataIsNotOkay.found) {
                InformUser.unsuccess({ text: `${dataIsNotOkay.msg}` });
                
            }

            if (!dataIsNotOkay.found) {
                this.setState({ invalided: false })
                if ( c_firstname && c_lastname && c_email ) {

                    if( this.state.changePassword && ( c_password && c_confirm_password && ( c_password === c_confirm_password ) ) ){
                        await CustomerService.update_customer_info( this.props.customer.id, c_firstname, c_lastname, c_email, c_password, this.token ).then( async result => {
                            // console.log(result)
                            if(result.status === 1){
                                InformUser.success({ text: result.msg });
                                this.props.close();
                            }else{
                                InformUser.unsuccess({text: result.msg})
                            }
                        })
                    }
    
                    if( !this.state.changePassword ){
                        CustomerService.update_customer_info( this.props.customer.id, c_firstname, c_lastname, c_email, '', this.token ).then( async result => {
                            // console.log(result)
                            if(result.status === 1){
                                InformUser.success({ text: result.msg });
                                this.props.close();
                            }else{
                                InformUser.unsuccess({text: result.msg})
                            }
                        })
                    }
                    
                } 
            
            }
    }

    async verifyData(  firstname, lastname, email ) {
        var found = false;
        var msg = "";

        if(!email.match(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/)){
            found = true;
            msg = "Email is incorrect.";
        }

        if(!lastname.match( /^[A-Za-z-. ]{1,40}$/)){
            found = true;
            msg = "Lastname only allow . and - as special character with maximum of 40 characters.";
        }
        
        if(!firstname.match( /^[A-Za-z-. ]{1,40}$/)){
            found = true;
            msg = "Firstname only allow . and - as special character with maximum of 40 characters.";
        }

        if( this.validateNoQoute(this.state.c_password) || this.validateNoQoute(this.state.c_confirm_password) ){
            found = true;
            msg = "Single quote and Double quote are not allowed in password.";
        }

        if ( this.state.changePassword && ( this.state.c_password !== this.state.c_confirm_password ) ) {
            found = true;
            msg = "Password and Confirm password do not match. Please try again.";
        }

        if(  !this.state.changePassword && ( !this.state.c_firstname.trim() || !this.state.c_lastname.trim() || !this.state.c_email ) ){
            found = true;
            msg = "The customer information is incomplete.";
        }

        if(  this.state.changePassword && ( !this.state.c_firstname.trim() || !this.state.c_lastname.trim() || !this.state.c_email || !this.state.c_password || !this.state.c_confirm_password ) ){
            found = true;
            msg = "The customer information is incomplete.";
        }
        
        return {
            found: found,
            msg: msg,
        };
    }

    validateInput(input){

        const { c_firstname, c_lastname, c_email, c_password, c_confirm_password } = this.state;

        if(this.state.invalided){

            if( input === "FNAME_INPUT" ){
                if( !c_firstname.trim() || this.validateText(c_firstname)  ) {
                    return " invalid-input "
                }
            }

            if( input === "LNAME_INPUT" ){
                if( !c_lastname.trim() || this.validateText(c_lastname) ) {
                    return " invalid-input "
                }
            }

            if( input === "EMAIL_INPUT" ){
                if( !c_email || this.validateEmail(c_email) ) {
                    return " invalid-input "
                }
            }

            if(  input === "PASSWORD_INPUT" ){
                if( !c_password || this.validateNoQoute(c_password) ||
                    !c_confirm_password || this.validateNoQoute(c_confirm_password) ||
                    (c_password !== c_confirm_password) ) {
                    return " invalid-input "
                }
            }

        }
    }

    validateNoQoute(input){
        
        if(!input.match(/^[^'"]*$/)){
            return true
        }
    }

    validateEmail(input){
        if(!input.match( /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/)){
            return true
        }
    }

    validateText(input){
        if(!input.match( /^[A-Za-z-. ]{1,40}$/)){
            return true
        }
    }

    handleCheckbox = (event) => {
        // console.log(event.target.value, typeof event.target.value);
        const changePassword = this.state.changePassword;
        if(changePassword){
            this.setState({ [event.target.name]: false });  
        }if(!changePassword){
            this.setState({ [event.target.name]: true });
        }
    };


    render() {


        return (
            <div className="edit-customer-container">
                <header ><h1>Customer Edit</h1></header>
                <form onSubmit={(event) => { this.submitEditing(event) }} autoComplete="off" noValidate >
                <div className="registration-form section sm-tab-black">

                <div className="input-group">
                        <div className="left"><span>Username :</span></div>
                        <div className="right disabled"><input type="text" id="username-input1" name="c_username" maxLength="40" value={this.props.customer.username} disabled /></div>
                    </div>
                    <div className="input-group">
                        <div className="left"><span>Firstname :</span></div>
                        <div className="right"><input type="text" id="firstname-input1" name="c_firstname" maxLength="40"  value={this.state.c_firstname} className={this.validateInput("FNAME_INPUT")} onChange={this.handleInput} placeholder="First name" /></div>
                    </div>
                    <div className="input-group">
                        <div className="left"><span>Lastname :</span></div>
                        <div className="right"><input type="text" id="lastname-input1" name="c_lastname" maxLength="40"  value={this.state.c_lastname} className={this.validateInput("LNAME_INPUT")} onChange={this.handleInput} placeholder="Last name"/></div>
                    </div>
                    <div className="input-group">
                        <div className="left"><span>Email :</span></div>
                        <div className="right"><input type="email" id="email-input1" name="c_email" maxLength="40"  value={this.state.c_email} className={this.validateInput("EMAIL_INPUT")} onChange={this.handleInput}  placeholder="Email"/></div>
                    </div>

                    <hr />
                    <div className="input-group">
                            <div className="left">
                                <span>Change password : </span>
                            </div>
                            <div className="right checkbox">
                                <input
                                    id="change-password-checkbox1"
                                    name="changePassword"
                                    type="checkbox"
                                    // value={!this.state.changePassword}
                                    placeholder=""
                                    checked={this.state.changePassword}
                                    onChange={this.handleCheckbox}
                                />
                            </div>
                        </div>
                        {
                            this.state.changePassword && <div className="input-group">
                            <div className="left"><span>Password :</span></div>
                            <div className="right"><input type="password" id="password-input1" name="c_password" maxLength="40"  value={this.state.c_password} className={this.validateInput("PASSWORD_INPUT")} onChange={this.handleInput} placeholder="New Password" /></div>
                        </div>
                        }
                        {
                            this.state.changePassword && <div className="input-group">
                            <div className="left"><span>Confirm password :</span></div>
                            <div className="right"><input type="password" id="confirm-password-imput1" name="c_confirm_password"  maxLength="40" className={this.validateInput("PASSWORD_INPUT")} value={this.state.c_confirm_password} onChange={this.handleInput} placeholder="Confirm Password"/></div>
                        </div>
                        }
                    

                    


                </div>
                <div className="btn-row" style={{ paddingTop: "1vw" }}>
                    <button id="submit-btn1" type="submit" className="btn submit" style={{marginRight:"0.5vw"}}><span>Submit</span></button>
                    <button onClick={()=>{this.props.close()}} id="cancel-btn1" type="button" className="btn cancel"><span>Cancel</span></button>
                </div>
                </form>
            </div>
        )
    }
}

export default EditCustomer
