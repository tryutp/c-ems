import React, { Component } from 'react'
import './CustomerManagement.scss'

import { Redirect } from "react-router-dom";
import * as CustomerService from "./../../services/CustomerService";
import { openConfirmation } from "./../../shared/Confirmation";
import * as InformUser from "./../../shared/InformUser";
import { EditCustomer } from "./EditCustomer/EditCustomer";
import { Loading } from "./../../shared/Loading";
import moment from 'moment';

import Modal from "react-modal";

export class CustomerManagement extends Component {

    token = JSON.parse(localStorage.getItem('userData')).token;
    time_offset = JSON.parse(localStorage.getItem('userData')).time_offset;

    

    constructor(props) {
        super(props);
        this.state = {
            c_username: '',
            c_firstname: '',
            c_lastname: '',
            c_password: '',
            c_vPassword: '',
            c_email: '',
            customerList: '',

            editingCustomer: '',
            editCustomerModal: false,
            entryToAssignPage: false,

            loading: false,
        }
    }

    componentDidMount() {

        this.setState({ loading: true }, async () => {

            await this.getCustomerList();

            this.setState({ loading: false })
        })

    }

    async getCustomerList() {
        await CustomerService.get_customer_list(this.token).then(async result => {
            // console.log(result)
            if (result.status === 1) {

                var customers = result.data;
                var timeCaledCustomers = await this.processColumnOfTime(customers)


                this.setState({ customerList: timeCaledCustomers })
            } else {
                InformUser.unsuccess({ text: result.msg })
            }
        })

    }

    async processColumnOfTime(logData){
        var  logs = JSON.parse(JSON.stringify(logData));
        // var  logs = logData;
        for( var i=0; i< logs.length; i++){
            // const newAddDate = await this.calculateTime(logs[i]["add_date"])
            const newRegisteredDate = await this.calculateTime(logs[i]["regist_date"])
            // const newFinishDate = await this.calculateTime(logs[i]["finish_date"])
            // logs[i]["add_date"] = newAddDate;
            logs[i]["regist_date"] = newRegisteredDate;
            // logs[i]["finish_date"] = newFinishDate;
        }
        return logs;
    }
    async calculateTime(serverTime) {

        var rawServerTime = await moment(serverTime);
        // console.log(rawServerTime)
        // console.log(timeOffset)
        var offset = moment.duration(this.time_offset)
        // // var temp = moment(server_time).add(1, 'm')
        // console.log(offset)
        var currentTime =  await rawServerTime.add(offset).format("ddd YYYY-MM-DD HH:mm:ss")
        // console.log(currentTime)
        return currentTime;
        // console.log(currentTime)
    }


    handleInput = (event) => {
        // console.log(event)
        this.setState({ [event.target.name]: event.target.value })
    }
    async submitCustomer(event) {

        event.preventDefault()
        const { c_username, c_firstname, c_lastname, c_email, c_password, c_vPassword } = this.state;
        this.setState({ invalided: true })
        
        if( !c_username || !c_firstname.trim() || !c_lastname.trim() || !c_email || !c_password || !c_vPassword ){
            InformUser.unsuccess({ text: "The customer information is incomplete." })
        }
        if (this.state.c_password !== this.state.c_vPassword) {
            InformUser.unsuccess({ text: "Password and Confirm password do not match. Please try again." })
        }
        if ( c_username && c_firstname.trim() && c_lastname.trim() && c_email && c_password && c_vPassword && ( c_password === this.state.c_vPassword )) {

            var dataIsNotOkay = await this.verifyData(
                c_username,
                c_firstname,
                c_lastname,
                c_email,
            );
            if (dataIsNotOkay.found) {
                InformUser.unsuccess({ text: `${dataIsNotOkay.msg}` });
                
            }

            if (!dataIsNotOkay.found) {

                this.setState({ invalided: false })
                const dialog = await openConfirmation({
                    title: "Register Customer",
                    text: `Are you sure you want to register ${c_firstname} ${c_lastname}?`
                })
                if (dialog) {
                    CustomerService.register_customer(c_username, c_firstname, c_lastname, c_email, c_password, this.token).then(result => {
                        // console.log(result)
                        if (result.status === 1) {
                            InformUser.success({ text: result.msg })
                            this.clearInput();
                            this.getCustomerList();
                        } else {
                            InformUser.unsuccess({ text: result.msg })
                        }
                    })
                }

            }
            
        }



    }

    async verifyData(username, firstname, lastname, email,) {
        var found = false;
        var msg = "";

        if(!email.match(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/)){
            found = true;
            msg = "Email is incorrect.";
        }

        if(!lastname.match( /^[A-Za-z-. ]{1,40}$/)){
            found = true;
            msg = "Lastname only allow . and - as special character with maximum of 40 characters.";
        }
        
        if(!firstname.match( /^[A-Za-z-. ]{1,40}$/)){
            found = true;
            msg = "Firstname only allow . and - as special character with maximum of 40 characters.";
        }

        if(!username.match( /^[\w-.]{3,20}$/)){
            found = true;
            msg = "Username only allow _ . and - as special character in the range of 3 to 20 characters.";
        }
        if( this.validateNoQoute(this.state.c_password) || this.validateNoQoute(this.state.c_vPassword) ){
            found = true;
            msg = "Single quote and Double quote are not allowed in password.";
        }

        return {
            found: found,
            msg: msg,
        };
    }

    validateInput(input){

        if(this.state.invalided){

            if( input === "NAME_INPUT" ){
                if( !this.state.c_username || this.validateName(this.state.c_username) ) {
                    return " invalid-input "
                }
            }

            if( input === "FNAME_INPUT" ){
                if( !this.state.c_firstname.trim() || this.validateText(this.state.c_firstname) ) {
                    return " invalid-input "
                }
            }

            if( input === "LNAME_INPUT" ){
                if( !this.state.c_lastname.trim() || this.validateText(this.state.c_lastname)  ) {
                    return " invalid-input "
                }
            }

            if( input === "EMAIL_INPUT" ){
                if( !this.state.c_email || this.validateEmail(this.state.c_email) ) {
                    return " invalid-input "
                }
            }

            if(  input === "PASSWORD_INPUT" ){
                if( !this.state.c_password || this.validateNoQoute(this.state.c_password) ||
                    !this.state.c_vPassword || this.validateNoQoute(this.state.c_vPassword) ||
                    (this.state.c_password !== this.state.c_vPassword) ) {
                    return " invalid-input "
                }
            }

        }
    }

    validateNoQoute(input){
        
        if(!input.match(/^[^'"]*$/)){
            return true
        }
    }

    validateName(input){
        if(!input.match( /^[\w-.]{0,20}$/)){
            return true
        }
    }

    validateText(input){
        if(!input.match( /^[A-Za-z-. ]{1,40}$/)){
            return true
        }
    }

    validateEmail(input){
        if(!input.match( /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/)){
            return true
        }
    }
    
    async removeCustomerAccount(customer) {
        const dialog = await openConfirmation({
            title: "Remove Customer Account",
            text: `Are you sure you want to remove ${customer.username}?`
        })
        if (dialog) {
            CustomerService.remove_customer_account(customer.id, this.token).then(result => {
                // console.log(result)
                if (result.status === 1) {
                    InformUser.success({ text: result.msg })
                    this.getCustomerList()
                } else {
                    InformUser.unsuccess({ text: result.msg })
                }
            })

        }
    }

    clearInput() {
        this.setState({
            c_username: '',
            c_firstname: '',
            c_lastname: '',
            c_password: '',
            c_vPassword: '',
            c_email: '',
            // invalided: false,
        })
    }

    entryToAssignPage(customer) {
        this.setState({ assigningCustomer: customer }, () => {
            this.setState({ entryToAssignPage: true })
        })
    }

    editCustomerInfo(customer) {
        this.setState({
            editingCustomer: customer,
            editCustomerModal: true
        })
    }
    closeEditCustomerModal = async () => {
        await this.getCustomerList()
        this.setState({ editCustomerModal: false })
    }



    render() {

        if (this.state.entryToAssignPage) {
            return <Redirect push to={{
                pathname: "/port-assignment",
                state: { customer: this.state.assigningCustomer }
            }} />;
        }

        return (
            <div className="customer-management-container">
                <Loading open={this.state.loading} />
                <Modal
                    isOpen={this.state.editCustomerModal}
                    className="edit-customer-body"
                    overlayClassName="default-overlay"
                    onRequestClose={() => { this.closeEditCustomerModal(false) }}
                // closeTimeoutMS={500}
                >
                    <EditCustomer customer={this.state.editingCustomer} close={this.closeEditCustomerModal} />

                </Modal>
                <header className="h1"><h1>MANAGEMENT: CUSTOMER ACCOUNT</h1></header>
                <div className="content">
                    {/* <div className="lr-container tab-black upper-tab" >
                        <div className="left">
                            <button className="btn submit">New Customer</button>
                        </div>
                        <div className="right">
                            <span style={{paddingRight: "1vw"}}>Search : </span>
                            <input type="text" disabled />

                        </div>
                    </div> */}

                    <div className="register-tab section tab-black">
                        <header className="h2"><h2>Registration</h2></header>
                        <form onSubmit={(event) => { this.submitCustomer(event) }}  autoComplete="off" noValidate >
                        <div className="registration-form section sm-tab-black">
                            <div className="input-group">
                                <div className="left"><span>Username :</span></div>
                                <div className="right"><input type="text" name="c_username" id="username-input" maxLength="20" value={this.state.c_username} className={this.validateInput("NAME_INPUT")} onChange={this.handleInput} placeholder="Username" /></div>
                            </div>
                            <div className="input-group">
                                <div className="left"><span>Firstname :</span></div>
                                <div className="right"><input type="text" name="c_firstname" id="firstname-input" maxLength="40"  value={this.state.c_firstname} className={this.validateInput("FNAME_INPUT")} onChange={this.handleInput} placeholder="First name" /></div>
                            </div>
                            <div className="input-group">
                                <div className="left"><span>Lastname :</span></div>
                                <div className="right"><input type="text" name="c_lastname" id="lastname-input" maxLength="40"  value={this.state.c_lastname} className={this.validateInput("LNAME_INPUT")} onChange={this.handleInput} placeholder="Last name" /></div>
                            </div>
                            <div className="input-group">
                                <div className="left"><span>Email :</span></div>
                                <div className="right"><input type="email" name="c_email" id="email-input" maxLength="40"  value={this.state.c_email} className={this.validateInput("EMAIL_INPUT")} onChange={this.handleInput} placeholder="Email" /></div>
                            </div>
                            <hr />
                            <div className="input-group">
                                <div className="left"><span>Password :</span></div>
                                <div className="right"><input type="password" name="c_password" id="password-input" maxLength="40"  value={this.state.c_password} className={this.validateInput("PASSWORD_INPUT")} onChange={this.handleInput} placeholder="Password" /></div>
                            </div>

                            <div className="input-group">
                                <div className="left"><span>Confirm password :</span></div>
                                <div className="right"><input type="password" name="c_vPassword" id="confirm-password-input" maxLength="40"  value={this.state.c_vPassword} className={this.validateInput("PASSWORD_INPUT")}onChange={this.handleInput} placeholder="Password" /></div>
                            </div>


                        </div>
                        <div className="btn-row" style={{ paddingTop: "1vw" }}>
                        <button id="submit-btn" type="submit" style={{marginRight:"0.5vw"}} className="btn submit"><span>Submit</span></button>

                            <button onClick={() => { this.clearInput() }} id="cancel-btn" type="button" className="btn cancel"><span>Clear</span></button>
                        </div>
                        </form>
                    </div>
                    <div className="customer-tab section tab-black">
                        <header className="h2"><h2>Customer Accounts</h2></header>
                        <div className="list-container ">
                            <div className="table-size">
                                <table id="customer-table">
                                    <thead>
                                    <tr className="tr-head">
                                        <th><span>No</span></th>
                                        <th><span>Username</span></th>
                                        <th><span>First name</span></th>
                                        <th><span>Last name</span></th>

                                        <th><span>Register Date</span></th>
                                        <th><span>Email</span></th>
                                        <th><span>Action</span></th>
                                    </tr>
                                    </thead>
                                    
                                    <tbody>

                                    {
                                        this.state.customerList && this.state.customerList.map((customer, index) =>
                                            <tr key={`customer-${index+1}`} id={`customer-${index+1}`}>
                                                <td id={`no-${index+1}`} className="text-center"><span>{customer.no}</span></td>
                                                <td id={`username-${index+1}`}  className="text-center" >
                                                    <span className="ellipsis" style={{width:"10vw"}} >{customer.username}</span>
                                                </td>
                                                <td id={`firstname-${index+1}`} className="text-center" ><span>{customer.firstname}</span></td>
                                                <td id={`lastname-${index+1}`} className="text-center" ><span>{customer.lastname}</span></td>

                                                <td id={`register-date-${index+1}`} className="text-center"><span>{customer.regist_date}</span></td>
                                                <td id={`email-${index+1}`} className="text-center"><span>{customer.email}</span></td>
                                                <td id={`action-${index+1}`} className="text-center" style={{ width: "15vw" }}>
                                                    <div className="action-row">
                                                        <button id={`allocate-${index+1}`}  onClick={() => { this.entryToAssignPage(customer) }} className="sm-btn submit"><span>Allocate</span></button>
                                                        <button id={`edit-${index+1}`}  onClick={() => { this.editCustomerInfo(customer) }} className="sm-btn submit"> <span>Edit</span></button>
                                                        <button id={`remove-${index+1}`}  onClick={() => { this.removeCustomerAccount(customer) }} className="sm-btn submit"> <span>Remove</span></button>
                                                    </div>
                                                </td>
                                            </tr>



                                        )
                                    }
                                    </tbody>
                                </table>
                                {
                                    (this.state.customerList.length === 0) && <div id="no-data-task" className="no-remaining-task" style={{height:"7vw"}}><span className="camera-offline">NO CUSTOMER REGISTERED</span></div>
                                }
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default CustomerManagement
