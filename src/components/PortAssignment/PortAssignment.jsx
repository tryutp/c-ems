import React, { Component } from 'react'
import './PortAssignment.scss';

import * as InfoService from "./../../services/InfoService";
import * as SystemService from "./../../services/SystemService";
import * as CustomerService from "./../../services/CustomerService";
import * as InformUser from "./../../shared/InformUser";
import { openConfirmation } from "./../../shared/Confirmation";
import ReactTooltip from 'react-tooltip';
import { Loading } from "./../../shared/Loading";
import ClipLoader from "react-spinners/ClipLoader";
import { Redirect } from "react-router-dom";
import Modal from 'react-modal';
import Switch from "react-switch";

import { Option } from "./../Interconnection/Manual/Option/Option";

export class PortAssignment extends Component {

    token = JSON.parse(localStorage.getItem('userData')).token;
    last_elememt_clicked;
    constructor(props) {
        super(props);
        this.state = {
            totalPort: '',
            portPerPage: 200,
            view_type: 2,
            unitList: '',
            currentPageSource: 0,

            sourceData: '',
            assignedPort: [],
            customerPort: [],


            totalPage: '',
            pageIndex: '',

            selectedSource: '',
            selectedTarget: '',

            loading: false,

            loadingSource: false,
            loadingTarget: false,

            multipleSelection: false,
            backToManagement: false,

            filterSourceUnit: null,
            filterSourceSmu: null,
            filterSourceSearch: null,
            ActualfilterSourceSearch: null,

            filterSourceUnitResult: '',
            filterSourceSmuResult: '',

            filterOptionModal: false,
            showFrontPanelNumber: false,

            CustomerPortToggler: false,
        }
    }
    componentDidMount() {
        this.setState({ loading: true }, async () => {
            // await this.getTotalPort();
            this.setupCloseTabDetection(true);
            await this.getUnitList();
            await this.getPortData(true);
            // await this.getPortData(false);

            await this.getAvailablePort();
            await this.getCustomerPort()

            this.setState({ loading: false });
        })
    }
    componentWillUnmount() {
		this.setupCloseTabDetection(false);
	}
  
    setupCloseTabDetection (isSettingUp)  {

        if( isSettingUp ){
            window.addEventListener('beforeunload', this.closingBrowserHandler );
        }

        if( !isSettingUp ){
            window.removeEventListener('beforeunload', this.closingBrowserHandler   );
        }
	};

    closingBrowserHandler =(e) => {
        e.preventDefault();
        e.returnValue = 'something';
    }
    async getUnitList() {
        await SystemService.get_unit(this.token).then(result => {
            // console.log("yut", result)
            if (result.status === 1) {
                this.setState({ unitList: result.data })
            }
        })
    }

    getTotalPort() {
        InfoService.get_total_port_count(this.token).then(result => {
            // console.log(result);
            if (result.status === 1) {

                const port_qty = this.state.portPerPage;
                var totalPage = Math.ceil(result.data.port_count / port_qty);
                if(totalPage === 0){
                    totalPage = 1;
                }
                // console.log(totalPage)

                // console.log(totalPage)
                var pageIndex = [...Array(totalPage).keys()]

                this.setState({
                    totalPort: result.data.port_count,
                    totalPage: totalPage,
                    pageIndex: pageIndex,
                })
            }
        })
    }

    chunk = (arr, size) => arr.reduce((acc, e, i) => (i % size ? acc[acc.length - 1].push(e) : acc.push([e]), acc), []);

    async getPortData(isSource) {
        const view_type = this.state.view_type;
        const port_qty = this.state.portPerPage;

        var start_no = ((this.state.currentPageSource) * port_qty) + 1;;
        var unit_id = this.state.filterSourceUnit;
        var smu_type = this.state.filterSourceSmu;
        var search = this.state.ActualfilterSourceSearch;
        // console.log("notice", this.state.filterSourceSearch, typeof (this.state.filterSourceSearch))



        // console.log("notice", unit_id, typeof (unit_id))
        // const rowAmount = 8;
        const rowAmount = 8;
        const portPerRow = port_qty / rowAmount;


        // console.log(view_type, start_no, port_qty, unit_id, smu_type, search,)
        var filterResult = this.filterResultDescription(unit_id, smu_type, isSource)

        await this.getTotalPort(unit_id, smu_type, search, isSource)

        // console.log(this.props)

        await CustomerService.get_port_assign_panel(this.props.location.state.customer.id, view_type, start_no, port_qty, unit_id, smu_type, search, this.token).then(async result => {
            // console.log("filter", result);
            if (result.status === 1) {
                var array = await this.chunk(result.data, portPerRow);

                // console.log(array)
                this.setState({
                    sourceData: array,
                    filterSourceResult: filterResult,
                });

            }
        });
    }


    filterResultDescription(unit_id, smu_type, isSource) {
        // console.log("unit_id", unit_id, smu_type)
        var unit_id_text;
        var smu_type_text;

        if (!unit_id) {
            unit_id_text = "All units"
        } else {
            this.state.unitList.map(unit => {
                // console.log(unit)
                if (unit.id === parseInt(unit_id)) {
                    unit_id_text = `${unit.name}`
                }
            })
        }



        if (smu_type === 0) {
            smu_type_text = "East"
        } else if (smu_type === 1) {
            smu_type_text = "West"
        } else {
            smu_type_text = "East and West"
        }

        if (isSource) {
            this.setState({
                filterSourceUnitResult: unit_id_text,
                filterSourceSmuResult: smu_type_text
            })
        }


    }

    getTotalPort(unit_id, smu_type, search, isSource) {
        // console.log(unit_id, smu_type, search,)

        InfoService.get_total_port_count(unit_id, smu_type, search, this.token).then(result => {
            // console.log("count", result);
            if (result.status === 1) {

                const port_qty = this.state.portPerPage;
                var totalPage = Math.ceil(result.data.port_count / port_qty)
            // console.log(totalPage);

                if(totalPage === 0){
                    totalPage = 1;
                }
                var pageIndex = [...Array(totalPage).keys()]

                this.setState({
                    totalPort: result.data.port_count,
                    totalPage: totalPage,
                    pageIndex: pageIndex,
                })


            }
        })
    }

    getAvailablePort() {

    }
    getCustomerPort() {
        CustomerService.get_customer_port(this.props.location.state.customer.id, this.token).then(result => {
            // console.log("port", result)
            if (result.status === 1) {
                var assigned = result.data;
                assigned.sort((a, b) => {
                    return a.idx - b.idx;
                })
                this.setState({ customerPort: assigned })
            } else {
                InformUser.unsuccess({ text: result.msg });
            }
        })

    }


    handlePage = (event, isSource) => {
        let selected = parseInt(event.target.value);
        this.setState({ [event.target.name]: selected })

        if (isSource) {
            this.setState({ currentPageSource: selected, loadingSource: true }, async () => {
                await this.getPortData(true);
                this.setState({ loadingSource: false });
            });
        } else {
            this.setState({ currentPageTarget: selected, loadingTarget: true }, async () => {
                await this.getPortData(false);
                this.setState({ loadingTarget: false });
            });
        }
    }
    forwardPage(isSource) {
        // console.log(this.state.totalPage)

        if (isSource) {
            const page = parseInt(this.state.currentPageSource)
            if (page < (this.state.totalPage - 1)) {
                var pageNext = page + 1;
                // console.log(pageNext)
                this.setState({ currentPageSource: pageNext, loadingSource: true }, async () => {
                    await this.getPortData(true);
                    this.setState({ loadingSource: false })
                });
            }

        }
        // else {
        //     const page = parseInt(this.state.currentPageTarget)
        //     if( page < (this.state.totalPage - 1)){
        //         var pageNext = page + 1;
        //         this.setState({ currentPageTarget: pageNext, loadingTarget: true }, async () => {
        //             await this.getPortData(false);
        //             this.setState({ loadingTarget: false })
        //         });
        //     }

        // }
    }
    backwardPage(isSource) {
        // console.log('yut')

        if (isSource) {
            const page = parseInt(this.state.currentPageSource)
            if (page > 0) {
                var pageNext = page - 1;
                this.setState({ currentPageSource: pageNext, loadingSource: true }, async () => {
                    await this.getPortData(true);
                    this.setState({ loadingSource: false })
                });
            }

        }
        // else {
        //     const page = parseInt(this.state.currentPageTarget)
        //     if( page > 1){
        //         var pageNext = page - 1;
        //         this.setState({ currentPageTarget: pageNext, loadingTarget: true }, async () => {
        //             await this.getPortData(false);
        //             this.setState({ loadingTarget: false })
        //         });
        //     }

        // }
    }

    lightStyleClasses(port, isSource) {

        var defaultStyle = " port ";
        var operation = port.port_operation_id;
        // console.log(port)

        if (isSource) {
            var assigned = this.state.assignedPort
            assigned.map(a => {
                if (port.idx === a.idx) {
                    defaultStyle = defaultStyle += " selected "
                }
            })

            // if (port.smu_type === this.state.selectedTarget.smu_type) {
            //     defaultStyle = defaultStyle + " same-type "
            // }
            if (!port.assign) {
                defaultStyle += " unassignable "
            }
            switch (operation) {
                case -1:
                    return defaultStyle += " connected "
                    break;
                case 0:
                    return defaultStyle + " disabled "
                    break;
                case 1:
                    return defaultStyle + " available "
                    break;
                // case 2:
                //     return defaultStyle + " connecting "
                //     break;
                // case 3:
                //     return defaultStyle + " disconnecting "
                //     break;
                // case 4:
                //     return defaultStyle + " recovering "
                //     break;
                // case 5:
                //     return defaultStyle + " locking "
                //     break;
                default:
                    return defaultStyle
            }
        }
        // else {
        //     if (port.idx === this.state.selectedTarget.idx) {
        //         defaultStyle = " selected port ";
        //     }
        //     if (port.smu_type === this.state.selectedSource.smu_type) {
        //         defaultStyle = defaultStyle + " same-type "
        //     }
        //     switch (operation) {
        //         case -1:
        //             return defaultStyle += " connected "
        //             break;
        //         case 0:
        //             return defaultStyle + " disabled "
        //             break;
        //         case 1:
        //             return defaultStyle + " available "
        //             break;
        //         case 2:
        //             return defaultStyle + " connecting "
        //             break;
        //         case 3:
        //             return defaultStyle + " disconnecting "
        //             break;
        //         case 4:
        //             return defaultStyle + " recovering "
        //             break;
        //         case 5:
        //             return defaultStyle + " locking "
        //             break;
        //         default:
        //             return defaultStyle
        //     }
        // }
    }

    assignedClasses(port, isSource) {

        var defaultStyle = " port ";
        var operation = port.port_operation_id;
        // console.log(port)
        if (isSource) {
            // var assigned = this.state.assignedPort
            // assigned.map(a => {
            //     if (port.idx === a.idx) {
            //        defaultStyle = defaultStyle += " selected "
            //     }
            // })

            // if (port.smu_type === this.state.selectedTarget.smu_type) {
            //     defaultStyle = defaultStyle + " same-type "
            // }
            switch (operation) {
                case -1:
                    return defaultStyle += " connected "
                    break;
                case 0:
                    return defaultStyle + " disabled "
                    break;
                case 1:
                    return defaultStyle + " available "
                    break;
                case 2:
                    return defaultStyle + " connecting "
                    break;
                case 3:
                    return defaultStyle + " disconnecting "
                    break;
                case 4:
                    return defaultStyle + " recovering "
                    break;
                case 5:
                    return defaultStyle + " locking "
                    break;
                default:
                    return defaultStyle
            }
        }
        // else {
        //     if (port.idx === this.state.selectedTarget.idx) {
        //         defaultStyle = " selected port ";
        //     }
        //     if (port.smu_type === this.state.selectedSource.smu_type) {
        //         defaultStyle = defaultStyle + " same-type "
        //     }
        //     switch (operation) {
        //         case -1:
        //             return defaultStyle += " connected "
        //             break;
        //         case 0:
        //             return defaultStyle + " disabled "
        //             break;
        //         case 1:
        //             return defaultStyle + " available "
        //             break;
        //         case 2:
        //             return defaultStyle + " connecting "
        //             break;
        //         case 3:
        //             return defaultStyle + " disconnecting "
        //             break;
        //         case 4:
        //             return defaultStyle + " recovering "
        //             break;
        //         case 5:
        //             return defaultStyle + " locking "
        //             break;
        //         default:
        //             return defaultStyle
        //     }
        // }
    }

    unallocateClasses(port) {

        var defaultStyle = " port ";
        var operation = port.port_operation_id;

        switch (operation) {
            case -1:
                return defaultStyle += " connected "
                break;
            case 0:
                return defaultStyle + " disabled "
                break;
            case 1:
                return defaultStyle + " available "
                break;
            case 2:
                return defaultStyle + " connecting "
                break;
            case 3:
                return defaultStyle + " disconnecting "
                break;
            case 4:
                return defaultStyle + " recovering "
                break;
            case 5:
                return defaultStyle + " locking "
                break;
            default:
                return defaultStyle
        }

    }

    assignPort(event, port) {

        var last_clicked = this.last_elememt_clicked;
        var assigned = this.state.assignedPort

        // if (event.ctrlKey && (this.last_elememt_clicked)) {

        //     console.log(true)
        //     if (port.idx > this.last_elememt_clicked.idx) {
        //         var startIndex = assigned.findIndex(x => x.idx === this.last_elememt_clicked.idx)
        //         console.log(startIndex)
        //         for (var i = startIndex; i < assigned.length; i++) {
        //             console.log(assigned[i].idx)

        //         }
        //     }


        // }



        var shouldAssign = true;
        assigned.map((a, index) => {
            if (port.idx === a.idx) {  // if found, remove it.
                shouldAssign = false;
                assigned.splice(index, 1);
            }
        })
        if (port.assign) {
            if (shouldAssign) {
                this.addToAssigning(port) // if not found, add it
                this.last_elememt_clicked = port;
            } else {
                this.setState({ assignedPort: assigned }, () => {
                    ReactTooltip.rebuild();
                })
                this.last_elememt_clicked = '';
            }
        }
        // ReactTooltip.rebuild();





        // if(this.state.multipleSelection){  // single selection


        // }else{

        // }

    }

    addToAssigning(port) {
        // this.sortAssigned()
        var assigned = [...this.state.assignedPort, port];
        assigned.sort((a, b) => {
            return a.display_no - b.display_no;
        })
        this.setState({ assignedPort: assigned }, () => {
            ReactTooltip.rebuild();
        })
        // this.setState({
        //     assignedPort: [...this.state.assignedPort, port]
        // })
    }

    unassignPort(unassigning) {
        var assigned = this.state.assignedPort;
        assigned.map((port, index) => {
            if (port.idx === unassigning.idx) {
                assigned.splice(index, 1);
            }
        })
        this.setState({ assignedPort: assigned }, () => {
            // ReactTooltip.rebuild();
        });
    }
    async unallocatePort(port) {
        // console.log(port)
        const dialog = await openConfirmation({
            title: "Unallocate Customer Port",
            text: `Are you sure you want to unallocate Port-${port.idx}? ${this.props.location.state.customer.username} will no longer be able to access Port-${port.idx}.`
        })
        if (dialog) {
            CustomerService.remove_customer_port(port.id, this.token).then(result => {
                // console.log(result)
                if (result.status === 1) {
                    InformUser.success({ text: result.msg })
                    this.getPortData(true);
                    this.getCustomerPort()
                } else {
                    InformUser.unsuccess({ text: result.msg })
                }
            })
        }
    }

    toggleMultipleSelection() {
        if (this.state.multipleSelection) {
            this.setState({ multipleSelection: false })
        } else {
            this.setState({ multipleSelection: false })
        }
    }

    async submitAssigningPort() {
        const assigningPort = this.submitArray();
        // console.log(assigningPort)
        if( assigningPort.length > 0 ){
            const dialog = await openConfirmation({
                title: "Customer Port Allocation",
                text: `Are you sure you want to allocate ${assigningPort.length} port(s) to ${this.props.location.state.customer.username}?`
            })
            if (dialog) {
                CustomerService.assign_port_to_customer(this.props.location.state.customer.id, assigningPort, this.token).then(result => {
                    // console.log(result)
                    if (result.status === 1) {
                        InformUser.success({ text: result.msg })
                        this.getPortData(true);
                        this.getCustomerPort()
                        this.setState({ assignedPort: [] })
                    } else {
                        InformUser.unsuccess({ text: result.msg })
                    }
                })
            }
        }
        
    }

    submitArray() {
        const assigned = this.state.assignedPort;
        var temp = [];
        assigned.map(port => {
            temp.push(port.idx)
        })
        return temp;
    }

    customerToggle(toWhat) {
        this.setState({ CustomerPortToggler: toWhat })
    }
    openFilterOption(isSource) {

        this.filterSetting = {
            "unit": this.state.filterSourceUnit,
            "smu_type": this.state.filterSourceSmu,
            // "search": this.state.filterSourceSearch,
        }
        this.setState({ filterOptionModal: true })


    }
    closeFilterOption = (isSource, setting) => {
        // console.log(setting)

        var type_temp;
        if (setting.smu_type) {
            type_temp = parseInt(setting.smu_type);
        } else {
            type_temp = setting.smu_type;
        }
        this.setState({
            filterSourceUnit: parseInt(setting.unit),
            filterSourceSmu: type_temp,
            currentPageSource: 0,
            filterOptionModal: false,
        }, () => {
            this.setState({ loadingSource:true }, async ()=>{
                await this.getPortData(true);
                this.setState({ loadingSource:false })
            })
            
        })

    }
    searchPortDescription = (event) => {
        event.preventDefault();
        // console.log(this.state.filterSourceSearch)
        
        this.setState({ 
            loadingSource: true, 
            ActualfilterSourceSearch: this.state.filterSourceSearch,
            currentPageSource: 0, 
        }, async ()=>{
            this.setState({sourceData: []});
            await this.getPortData(true)
            this.setState({ loadingSource: false})
        })


    }
    handleSwitch = (event) => {
        this.setState({ showFrontPanelNumber: event });
    }
    handleSwitchHidden = (event) => {
        // console.log(event)
        this.setState({ showFrontPanelNumber: event.target.checked });
    }
    handleValueInput = (event) => {

        const searchText = event.target.value;
        const searchName = event.target.name;

        this.setState({ [event.target.name]: searchText });

        if( searchText === "" && searchName === "filterSourceSearch" ){
            this.setState({ ActualfilterSourceSearch: "" },()=>{
                this.getPortData(true)
            })
        }
    }

    tooltipDescription(port) {


        var description = `Description:  ${port.description}`;
        
        description = description + `<br/>Unit:  ${port.unit_name} ( ${port.smu_type_desc} )`;
        if(port.connected_count != null && port.connected_count != undefined) {
            description = description + `<br/>Connected Count:  ${port.connected_count}`;
        }

        if(port.allocated) {
            description = description + `<br/>Allocated to: ${port.allocated}`;
        }

        if (port.reserved === 1) {
            description = description + `<hr/>Reserved by ${port.reserved_change_by}`;
        }
        
        if (port.port_operation_id === -1 && (port.connected_task_desc)) {
            if (port.connected_task_desc) {
                description = description + `<hr/>Connected to ${port.connected_name}<br/>Task: ${port.connected_task_desc}`;

            } else {
                description = description + `<br/>Connected to ${port.connected_name}`;

            }
        }
        return `<span>${description}</span>`;
    }


    render() {
        // console.log(this.props.location.state.customer);
        const { customer } = this.props.location.state;
        const { sourceData, assignedPort } = this.state;

        if (this.state.backToManagement) {
            return <Redirect push to={{
                pathname: "/customer-management",
                // state: { customer: this.state.assigningCustomer }
            }} />;
        }




        return (
            <div className="port-assignment-container">
                <Loading open={this.state.loading} />
                <Modal
                    isOpen={this.state.filterOptionModal}
                    className="filter-option-body"
                    overlayClassName="filter-option-overlay"
                    onRequestClose={() => { this.closeFilterOption(true, this.filterSetting) }}
                // closeTimeoutMS={500}
                >
                    <Option isSource={true} header={"Port"} filterSetting={this.filterSetting} close={this.closeFilterOption} />

                </Modal>
                <header className="h1 flex-row">
                    <h1 style={{ marginRight: "1vw" }}>Customer Management : Port Allocation</h1>
                    <button id="back-btn" onClick={() => { this.setState({ backToManagement: true }) }} className="sm-btn cancel"><span>Back</span></button>
                </header>
                <div className="content">
                    <div className="available-tab section tab-black">

                        <div className="section-container source">
                            <div className="lr-container">
                                <div className="left">

                                    <h2 >Available Port</h2>
                                    <span>Page:</span>
                                    <button id="source-previous-btn"  onClick={() => { this.backwardPage(true) }} className={(this.state.currentPageSource === 0 ? " btn-disable " : " ") + "fit-btn submit next"}>
                                        <span>Previous</span>
                                        {/* <input type="number" id="page-forward-index" name="pageForwardIndex" value={this.state.pageForwardIndex} onChange={this.handlePageChange}/> */}
                                    </button>

                                    <select id="source-selection-page"  name="currentPageSource" id="page" value={this.state.currentPageSource} onChange={(event) => { this.handlePage(event, true) }}>
                                        {
                                            this.state.pageIndex && this.state.pageIndex.map((page, index) =>
                                                <option key={`page-${index}`} className={page === this.state.currentPageSource ? "selected-page" : ""} value={page}>{page + 1}</option>
                                            )
                                        }
                                    </select>
                                    <button id="source-next-btn"  onClick={() => { this.forwardPage(true) }} className={(this.state.currentPageSource === (this.state.totalPage - 1) ? " btn-disable " : " ") + "fit-btn submit next"}>
                                        <span>Next</span>
                                        {/* <input type="number" id="page-forward-index" name="pageForwardIndex" value={this.state.pageForwardIndex} onChange={this.handlePageChange}/> */}
                                    </button>
                                    <ClipLoader color="#B3B3B3" loading={this.state.loadingSource} size={20} />

                                </div>
                                <div className="right">
                                    <button id="source-filter-btn"  className="sm-btn submit search-filter-button" onClick={() => { this.openFilterOption(true) }}>  <span> Filter: <span id="source-filter-unit"  className="result">{this.state.filterSourceUnitResult}</span> <span id="source-filter-smu"  className="result">{this.state.filterSourceSmuResult}</span> </span> </button>

                                    <form onSubmit={(event) => { this.searchPortDescription(event, true) }} style={{ marginRight: "1vw" }}>
                                    <ReactTooltip id='searchSource' place="top" className="noteClass defaultTooltip" arrowColor="#404040"  effect="solid" />
                                        <input id="source-search-input"  data-for="searchSource" data-tip="Port description, and port alias"  type="text" className="search-input" placeholder="Search" maxLength={20}  name="filterSourceSearch" value={this.state.filterSourceSearch} onChange={this.handleValueInput} />
                                        <button id="source-search-btn" className="fit-btn submit search-btn" type="submit"><i class="fas fa-search"></i></button>
                                    </form>
                                    <div class="toggle-group" style={{display: 'flex',flexDirection: 'row',position: 'relative'}}>
                                    <span style={{ marginRight: "0.5vw" }}>Front Panel View: </span>
                                    <Switch
                                        
                                        onChange={this.handleSwitch}
                                        uncheckedIcon={false}
                                        checkedIcon={false}
                                        // height={20}
                                        // width={32}
                                        className={(this.state.showFrontPanelNumber ? " checked " : " uncheck ") + " toggle-switch "}
                                        checked={this.state.showFrontPanelNumber} />
                                        <input type="checkbox" name="showFrontPanelNumber" id="hidden-switch" style={{position:"absolute",opacity:"0",width:"7.5vw",height:"1vw",left:"0"}} checked={ this.state.showFrontPanelNumber } onChange={this.handleSwitchHidden} />
                                    </div>
                                    
                                    <h2 className="username">{customer.username}</h2>
                                    {/* <span onClick={() => { this.toggleMultipleSelection() }} className="icon"><i class="fas fa-list-ul"></i></span> */}
                                </div>
                            </div>
                            <div className="box">
                                {
                                    sourceData && sourceData.map((row, rowIndex) =>
                                        <div key={`source-row-${rowIndex+1}`} id={`source-row-${rowIndex+1}`} className="row">
                                            <ReactTooltip id='portTooltip' place="top" html={true} className="portClass defaultTooltip" arrowColor="#404040" effect="solid" />
                                            {/* <ReactTooltip id='sourceTooltip' place="top" multiline={true} currentitem={false} type="dark" effect="solid" /> */}

                                            {
                                                row.map((port, portIndex) =>

                                                    <div
                                                        // data-html="true"
                                                        // data-tip={`Port-${port.idx}  ${port.unit_name} - ${port.smu_type_desc}`}
                                                        key={`available-port-${portIndex+1}`}
                                                        id={`available-port-${portIndex+1}`}
                                                        data-for='portTooltip'
                                                        data-tip={this.tooltipDescription(port)}
                                                        onClick={(event) => { this.assignPort(event, port) }}
                                                        className={this.lightStyleClasses(port, true)}
                                                    // className={this.state.selectedSource.idx === port.idx ? "selected port" : "port"}
                                                    >
                                                        {
                                                            !this.state.showFrontPanelNumber && <span>{port.display_no}</span>
                                                        }
                                                        {
                                                            this.state.showFrontPanelNumber && <span>{port.front_no}</span>
                                                        }
                                                        <div className="light-status"></div>

                                                    </div>

                                                )
                                            }
                                        </div>
                                    )


                                }
                                {
                                    (sourceData.length === 0 ) && <span style={{textAlign:'center',padding: '10.75vw'}}>There's no port that matched your filter.</span>
                                }
                            </div>
                        </div>


                    </div>
                    <div className="customer-tab section tab-black">
                        <div className="toggle-btn-container">
                            <div id="allocating-port-tab" onClick={() => { this.customerToggle(false) }} className={(!this.state.CustomerPortToggler ? " sm-tab-black " : " tab-black ") + " left tab  "}>
                                <h2 className="specific-line-height">Allocating Port</h2>
                                {
                                    (assignedPort.length > 0) && <span className="selected-number">+{this.state.assignedPort.length}</span>
                                }
                            </div>
                            <div id="customer-port-tab" onClick={() => { this.customerToggle(true) }} className={(this.state.CustomerPortToggler ? " sm-tab-black " : " tab-black ") + " right tab  "}>
                                <h2 style={{ marginRight: "0.2vw" }}>Customer Port</h2>
                                {
                                    (this.state.customerPort.length > 0) && <span span className="added-number">{this.state.customerPort.length}</span>
                                }
                            </div>
                        </div>

                        {
                            !this.state.CustomerPortToggler && <div className="added-port">
                                <div className="port-container section-bottom sm-tab-black">
                                    <ReactTooltip id='assignedTooltip' place="top" html={true} className="portClass defaultTooltip" arrowColor="#404040" effect="solid" />
                                    {
                                        assignedPort && assignedPort.map((port, index) =>

                                            <div
                                                // data-html="true"
                                                key={`allocating-port-${index+1}`}
                                                id={`allocating-port-${index+1}`}
                                                data-for='assignedTooltip'
                                                data-tip={this.tooltipDescription(port)}
                                                onClick={() => { this.unassignPort(port) }}
                                                className={(this.assignedClasses(port, true)) + " port-size "}
                                            // className={this.state.selectedSource.idx === port.idx ? "selected port" : "port"}
                                            >
                                                {
                                                    !this.state.showFrontPanelNumber && <span>{port.display_no}</span>
                                                }
                                                {
                                                    this.state.showFrontPanelNumber && <span>{port.front_no}</span>
                                                }
                                                <div className="light-status"></div>

                                            </div>

                                        )


                                    }
                                </div>

                            </div>
                        }
                        {
                            this.state.CustomerPortToggler && <div className="customer-owned">
                                <div className="port-container section-bottom sm-tab-black">
                                    <ReactTooltip id='availableTooltip' place="top" html={true} className="portClass defaultTooltip" arrowColor="#404040" effect="solid" />


                                    {
                                        this.state.customerPort && this.state.customerPort.map((port, index) =>
                                            <div
                                                // data-html="true"
                                                data-for='availableTooltip'
                                                key={`customer-port-${index+1}`}
                                                id={`customer-port-${index+1}`}
                                                data-tip={this.tooltipDescription(port)}
                                                onClick={() => { this.unallocatePort(port) }}
                                                className={this.unallocateClasses(port, true) + " port-size "}
                                            // className={this.state.selectedSource.idx === port.idx ? "selected port" : "port"}
                                            >
                                                {
                                                    !this.state.showFrontPanelNumber && <span>{port.display_no}</span>
                                                }
                                                {
                                                    this.state.showFrontPanelNumber && <span>{port.front_no}</span>
                                                }
                                                <div className="light-status"></div>
                                            </div>
                                        )
                                    }
                                </div>


                            </div>
                        }
                        <div style={{ paddingTop: "1vw" }} className="btn-row">
                            {
                                !this.state.CustomerPortToggler  && <button id="submit-btn" onClick={() => { this.submitAssigningPort() }} className={ (this.state.assignedPort.length === 0 ? " inactive " : " ") + " btn submit "}><span>Submit</span></button>
                            }

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default PortAssignment
