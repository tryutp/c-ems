import React, { Component } from 'react'
import './EditSystem.scss'
import * as SystemService from "./../../services/SystemService";
import * as InformUser from "./../../shared/InformUser";
import { openConfirmation } from "../../shared/Confirmation";

export class EditSystem extends Component {

    token = JSON.parse(localStorage.getItem('userData')).token

    constructor(props) {
        super(props);
        this.state = {
            name: this.props.data.name,
            mac_addr: this.props.data.mac_address,
            ip_address: this.props.data.ip_address,
            model_id: this.props.data.model_id,
            clientPassword: this.props.data.password,
            modelList: this.props.data.modelList,
            invalided: false,
        }
    }


    handleInput = (event) => {
        this.setState({ [event.target.name]: event.target.value })
    }
    

    async updateUnitInfo(event) {
        event.preventDefault();
        const name = this.state.name;
        const mac_addr = this.state.mac_addr;
        const ip_address = this.state.ip_address;
        const clientPassword = this.state.clientPassword ? this.state.clientPassword : null;

        this.setState({ invalided: true })
        if( !name || !ip_address || !clientPassword ){
            InformUser.unsuccess({ text: "The unit information is incomplete." })
        }
        if( name && ip_address && clientPassword ){

            var dataIsNotOkay = await this.verifyData(
                name,
                mac_addr,
                ip_address
            );
            if (dataIsNotOkay.found) {
                InformUser.unsuccess({ text: `${dataIsNotOkay.msg}` });
                
            }


            if (!dataIsNotOkay.found) {
                this.setState({ invalided: false })

                const dialog = await openConfirmation({
                    title: "Update Unit Information",
                    text: `Are you sure you want to change ${name} information?`,
                });
                if (dialog) {

                await SystemService.update_system(this.props.data.id, this.state.name, this.state.mac_addr, this.state.ip_address, clientPassword, this.token).then(result => {
                // console.log(result);
                if (result.status === 1) {
                    InformUser.success({ text: result.msg });
                    this.props.closeEditModal();
                } else {
                    InformUser.unsuccess({ text: result.msg });
                }
                
                })
            }

            }
        }

        
    }

    async verifyData(systemName, macAddress, ipAddress) {
        var found = false;
        var msg = "";
        
        if(this.validateNoQoute(macAddress)){
            found = true;
            msg = "Single quote and Double quote are not allowed in MAC address.";
        }
        if(this.validateNoQoute(this.state.clientPassword)){
            found = true;
            msg = "Single quote and Double quote are not allowed in Client password.";
        }
        
        // if(!macAddress.match(/[\s\S]{3,40}/)){
        //     found = true;
        //     msg = "MAC address is incorrect";
        // }

        if(!ipAddress.match(/^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/gm)){
            found = true;
            msg = "IP address is incorrect.";
        }
        
        if(!systemName.match( /^[\w-.]{3,20}$/)){
            found = true;
            msg = "Username only allow _ . and - as special character in the range of 3 to 20 characters.";
        }
        return {
            found: found,
            msg: msg,
        };
    }

    validateInput(input){

        if(this.state.invalided){

            if( input === "NAME_INPUT" ){
                if( !this.state.name || this.validateName(this.state.name) ) {
                    return " invalid-input "
                }
            }

            if( input === "IP_INPUT" ){
                if( !this.state.ip_address || this.validateIpAddr(this.state.ip_address) ) {
                    return " invalid-input "
                }
            }

            if( input === "MAC_INPUT" ){
                if( !this.state.mac_addr.trim() || this.validateMacAddr(this.state.mac_addr) || this.validateNoQoute(this.state.mac_addr) ) {
                    return " invalid-input "
                }
            }

            if( input === "PASSWORD_INPUT" ){
                if( !this.state.clientPassword || this.validateNoQoute(this.state.clientPassword) ) {
                    return " invalid-input "
                }
            }

        }
        
        
            
        
    }

    validateName(input){
        if(!input.match( /^[\w-.]{0,20}$/)){
            return true
        }
    }
    validateIpAddr(input){
        if(!input.match(/^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/gm)){
            return true
        }
    }
    validateMacAddr(input){
        if(!input.match(/[\s\S]{3,40}/)){
            return true
        }
    }
    validateNoQoute(input){
        if(!input.match(/^[^'"]*$/)){
            return true
        }
    }


    render() {
        const { data, closeEditModal } = this.props;
        // console.log(data)

        return (
            <div className="edit-system-info">
                <header><h1>Unit Edit</h1></header>
                <form onSubmit={(event) => { this.updateUnitInfo(event) }} autoComplete="off">
                <div className="content">
                    <div className="regis-form">
                        <div className="input-group">
                            <div className="left"><span>Name : </span></div>
                            <div className="right"><input id="system-name1" name="name" maxLength="20" className={this.validateInput("NAME_INPUT")} value={this.state.name} type="text" placeholder="Unit Name" onChange={this.handleInput} /></div>
                        </div>
                        {/* <div className="input-group">
                            <div className="left"><span>MAC address : </span></div>
                            <div className="right"><input id="mac-address1" name="mac_addr" maxLength="40"  className={this.validateInput("MAC_INPUT")} value={this.state.mac_addr} type="text" placeholder="Mac address" onChange={this.handleInput} /></div>
                        </div> */}
                        <div className="input-group">
                            <div className="left"><span>IP address : </span></div>
                            <div className="right"><input id="ip-address1" name="ip_address" maxLength="40"  className={this.validateInput("IP_INPUT")} value={this.state.ip_address} type="text" placeholder="IP address" onChange={this.handleInput} /></div>
                        </div>
                        <div className="input-group">
                            <div className="left"><span>Unit model : </span></div>
                            <div className="right">
                                <select disabled name="model_id" id="model_id1" value={this.state.model_id} onChange={this.handleInput}>
                                    {/* {modelList} */}
                                    {this.state.modelList &&
                                        this.state.modelList.map(
                                            (model, index) => (
                                                <option key={`model-${index+1}`} id={`model-${index+1}`} value={model.id}>
                                                    {model.name}
                                                </option>
                                            )
                                        )}
                                    {/* <option id="model-1" value="1">XSOS-288S</option> */}
                                    {/* <option id="model-2" value="3">XSOS-576D</option> */}
                                    {/* <option id="model-3" value="4">XSOS-1152Q</option> */}
                                </select>
                            </div>
                        </div>
                        <hr />
                        <div className="input-group">
                            <div className="left"><span>Client Password : </span></div>
                            <div className="right">
                                <input id="authen-code1" name="clientPassword" type="password" maxLength="40"  value={this.state.clientPassword} className={this.validateInput("PASSWORD_INPUT")} placeholder="Password" onChange={this.handleInput} />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="btn-row edit-unit">
                    <button id="btn-submit" type="submit" className="btn submit" style={{marginRight:"0.5vw"}}  > <span>Submit</span> </button>
                    <button id="btn-cancel" type="reset" className="btn cancel" onClick={() => { this.props.closeEditModal() }}> <span>Cancel</span></button>
                </div>
                </form>
            </div>
        )
    }
}

export default EditSystem
