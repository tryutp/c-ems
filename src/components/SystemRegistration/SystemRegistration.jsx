import React, { Component } from "react";
import "./SystemRegistration.scss";
import * as SystemService from "./../../services/SystemService";
import * as InformUser from "./../../shared/InformUser";
import {
    openConfirmation,
    openConfirmationRemark,
} from "./../../shared/Confirmation";
import { EditSystem } from "./EditSystem";
import { DisableHandler } from "./DisableHandler";
import Modal from "react-modal";
import { Loading } from "./../../shared/Loading";
import ReactTooltip from 'react-tooltip';

export class SystemRegistration extends Component {
    token = JSON.parse(localStorage.getItem("userData")).token;
    edittingSystem = "";

    constructor(props) {
        super(props);
        this.state = {
            systemList: "",
            modelList: "",
            systemName: "",
            macAddress: "",
            ipAddress: "",
            clientPassword: "",
            model_id: 21,
            inputErrorMsg: '',

            openEditModal: false,
            openDisableModal: false,
            loading: false,
            invalided: false,
        };
    }

    componentWillMount() {
        Modal.setAppElement("body");
    }

    componentDidMount() {
        this.setState({ loading: true }, async () => {
            await this.getSystemList();
            await this.getModelList();
            this.setState({ loading: false });
        });
    }

    async getSystemList() {
        await SystemService.get_system_list(this.token).then((result) => {
            // console.log(result);
            if (result.status == 1) {
                this.setState({ systemList: result.data });
            } else {
                InformUser.unsuccess({ text: result.msg });
            }
        });
    }

    async getModelList() {
        await SystemService.get_model_list(this.token).then((result) => {
            // console.log(result);
            if (result.status == 1) {
                this.setState({ modelList: result.data });
            } else {
                InformUser.unsuccess({ text: result.msg });
            }
        });
    }

    handleInput = (event) => {
        this.setState({ [event.target.name]: event.target.value });
        if(event.target.name === "systemName"){
            
        }
    };
    selectChange(event) {
        this.setState({ [event.target.name]: event.target.value }, () => {
            this.filterPort();
        });
    }
     submitRegistration = async (event) => {
        //Validate Input
        event.preventDefault();
        const systemName = this.state.systemName;
        // const macAddress = this.state.macAddress;
        const macAddress = '0.0.0.0';
        const ipAddress = this.state.ipAddress;
        const model_id = this.state.model_id;
        const clientPassword = this.state.clientPassword;

            var dataIsNotOkay = await this.verifyData(
                systemName,
                macAddress,
                ipAddress
            );
            if (dataIsNotOkay.found) {
                InformUser.unsuccess({ text: `${dataIsNotOkay.msg}` });
                this.setState({ invalided: true })
            }

            if (!dataIsNotOkay.found) {
                this.setState({ invalided: false })
                const dialog = await openConfirmation({
                    title: "Unit Registration",
                    text: `Are you sure you want to submit ${systemName} to the system?`,
                });
                if (dialog) {
                    SystemService.register_system(
                        systemName,
                        macAddress,
                        ipAddress,
                        model_id,
                        clientPassword,
                        this.token
                    ).then(async (result) => {
                        // console.log(result)
                        if (result.status == 1) {
                            InformUser.success({ text: result.msg });
                            await this.setState(
                                {
                                    systemName: "",
                                    macAddress: "",
                                    ipAddress: "",
                                    model_id: 21,
                                    clientPassword: "",
                                },
                                () => {
                                    this.getSystemList();
                                }
                            );
                        } else {
                            InformUser.unsuccess({ text: result.msg });
                        }
                    });
                }
            }
        

        //Send Registration Info
    }

    async verifyData(systemName, macAddress, ipAddress) {
        var found = false;
        var msg = "";
        await this.state.systemList.map((system) => {
            // console.log(system);
            if (system.ip_address === ipAddress) {
                found = true;
                msg = `IP address "${ipAddress}" is already existed.`;
                // console.log("found");
            }
            // if (system.mac_address === macAddress) {
            //     found = true;
            //     msg = `MAC address "${macAddress}" is already existed.`;
            // }
            
            if (system.name === systemName) {
                found = true;
                msg = `Unit "${systemName}" is already existed.`;
            }
        });
        


        if(!ipAddress.match(/^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/gm)){
            found = true;
            msg = "IP address is incorrect.";
        }

        if(!macAddress.match(/[\s\S]{3,40}/)){
            found = true;
            msg = "MAC address is incorrect.";
        }
        if(!macAddress.match(/^[^'"]*$/)){
            found = true;
            msg = "Single quote and Double quote are not allowed in MAC address.";
		}
        if(!this.state.clientPassword.match(/^[^'"]*$/)){
            found = true;
            msg = "Qoutes are not allowed in Client password.";
		}
        if(!systemName.match( /^[\w-.]{3,20}$/)){
            found = true;
            msg = "Username only allow _ . and - as special character in the range of 3 to 20 characters.";
        }
        
        if (!systemName || !ipAddress || !macAddress.trim() || !this.state.model_id || !this.state.clientPassword) {
            found = true;
            msg = "Unit information is incomplete.";
        }
        return {
            found: found,
            msg: msg,
        };
    }
    resetInput() {
        this.setState({
            systemName: "",
            macAddress: "",
            ipAddress: "",
            model_id: 1,
            clientPassword: "",
        });
    }
    async removeSystem(system) {
        const dialog = await openConfirmation({
            title: "Remove system",
            text: "Are you sure you want to remove '" + system.name + "'?",
        });
        if (dialog) {
            SystemService.remove_system(system.id, this.token).then(
                (result) => {
                    // console.log(result);
                    if (result.status == 1) {
                        InformUser.success({ text: result.msg });
                        this.getSystemList();
                    } else {
                        InformUser.unsuccess({ text: result.msg });
                    }
                }
            );
        }
    }
    editSystem(system) {
        // console.log(system);
        if (system.enabled) {
            system.modelList = this.state.modelList;
            // console.log(system);
            this.edittingSystem = system;
            this.setState({ openEditModal: true });
        }
    }

    // updateInput(data) {
    //     console.log("update", data);
    // }
    closeEditModal = async () => {
        await this.getSystemList();
        this.setState({ openEditModal: false });
    };
    closeDisableModal = async () => {
        await this.getSystemList();
        this.setState({ openDisableModal: false });
    };
    async disableUnit(unit) {
        // if(unit.enabled){
        //     this.edittingSystem = unit;
        //     this.setState({ openDisableModal: true})
        // }

        const dialog = await openConfirmationRemark({
            title: "Disable Unit",
            text: `Are you sure you want to disable ${unit.name}?`,
        });
        if (dialog) {
            SystemService.disable_unit(unit.id, this.token).then((result) => {
                // console.log(result);
                if (result.status === 1) {
                    InformUser.success({ text: result.msg });
                    this.getSystemList();
                } else {
                    InformUser.unsuccess({ text: result.msg });
                }
            });
        }
    }
    async enableUnit(unit) {
        // const dialog = await openConfirmation({
        //     title: "Enable Unit",
        //     text: `Are you sure you want to enable ${unit.name}?`
        // })
        SystemService.enable_unit(unit.id, this.token).then((result) => {
            // console.log(result);
            if (result.status === 1) {
                InformUser.success({ text: result.msg });
                this.getSystemList();
            } else {
                InformUser.unsuccess({ text: result.msg });
            }
        });
    }


    validateInput(input){

        if(this.state.invalided){

            if( input === "NAME_INPUT" ){
                if( !this.state.systemName || this.validateName(this.state.systemName) ) {
                    return " invalid-input "
                }
            }

            if( input === "IP_INPUT" ){
                if( !this.state.ipAddress || this.validateIpAddr(this.state.ipAddress) ) {
                    return " invalid-input "
                }
            }

            if( input === "MAC_INPUT" ){
                if( !this.state.macAddress.trim() || this.validateMacAddr(this.state.macAddress) || this.validateNoQoute(this.state.macAddress) ) {
                    return " invalid-input "
                }
            }

            if( input === "PASSWORD_INPUT"){
                if( !this.state.clientPassword  || this.validateNoQoute(this.state.clientPassword) ) {
                    return " invalid-input "
                }
            }

        }
        
        
            
        
    }

    validateName(input){
        if(!input.match( /^[\w-.]{0,20}$/)){
            return true
        }
    }
    validateIpAddr(input){
        if(!input.match(/^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/gm)){
            return true
        }
    }
    validateMacAddr(input){
        if(!input.match(/[\s\S]{3,40}/)){
            return true
        }
    }
    validateNoQoute(input){
        if(!input.match(/^[^'"]*$/)){
            return true
        }
    }

    render() {
        return (
            <div className="system-management">
                <header className="h1">
                    <h1>MANAGEMENT: UNIT MANAGEMENT</h1>
                </header>

                <div className="system-management-layout">

                    <div className="registration">
                        <header className="h2">
                            <h2>Registration</h2>
                        </header>
                        <form onSubmit={this.submitRegistration} autocomplete="off" >

                        <div className="regis-form">
                            
                            <div className="input-group">
                                <div className="left">
                                    <span>Name : </span>
                                </div>
                                <div className="right">
                                    <input
                                        id="system-name"
                                        name="systemName"
                                        type="text"
                                        maxlength="20"
                                        className={this.validateInput("NAME_INPUT")}
                                        value={this.state.systemName}
                                        placeholder="Name"
                                        onChange={this.handleInput}
                                    />
                                </div>
                            </div>
{/* 
                            <div className="input-group">
                                <div className="left">
                                    <span>MAC Address : </span>
                                </div>
                                <div className="right">

                                    <input
                                        // data-for="nameTooltip"
                                        // data-tip="yutyut"
                                        id="system-mac-address"
                                        name="macAddress"
                                        type="text"
                                        maxlength="40"
                                        className={this.validateInput("MAC_INPUT")}
                                        value={this.state.macAddress}
                                        placeholder="MAC address"
                                        onChange={this.handleInput}
                                    />
                                </div>
                            </div> */}

                            <div className="input-group">
                                <div className="left">
                                    <span>IP Address : </span>
                                </div>
                                <div className="right">
                                    <input
                                        id="system-ip-address"
                                        name="ipAddress"
                                        type="text"
                                        maxlength="40"
                                        value={this.state.ipAddress}
                                        className={this.validateInput("IP_INPUT")}
                                        placeholder="IP address"
                                        onChange={this.handleInput}
                                    />
                                </div>
                            </div>

                            <div className="input-group">
                                <div className="left">
                                    <span>Unit Model : </span>
                                </div>
                                <div className="right">
                                    <select
                                        name="model_id"
                                        id="system-model-id"
                                        value={this.state.model_id}
                                        onChange={this.handleInput}
                                    >
                                        {this.state.modelList &&
                                            this.state.modelList.map(
                                                (model, index) => (
                                                    <option key={`model-${index+1}`} id={`model-${index+1}`} value={model.id}>
                                                        {model.name}
                                                    </option>
                                                )
                                            )}
                                    </select>
                                </div>
                            </div>

                            <hr />
                            <div className="input-group">
                                <div className="left">
                                    <span>Client Password : </span>
                                </div>
                                <div className="right">
                                    <input
                                        id="system-client-password"
                                        name="clientPassword"
                                        type="password"
                                        maxlength="40"
                                        value={this.state.clientPassword}
                                        className={this.validateInput("PASSWORD_INPUT")}
                                        placeholder="Password"
                                        onChange={this.handleInput}
                                    />
                                </div>
                            </div>
                        
                        </div>

                        <div className="btn-row submit-regis">
                            <button
                                id="submit-btn"
                                type="submit"
                                style={{marginRight:"0.5vw"}}
                                className="btn submit"
                                // onClick={this.submitRegistration.bind(this)}
                            >
                                <span>Submit</span>
                            </button>
                            <button
                                id="cancel-btn"
                                type="button"
                                className="btn cancel"
                                onClick={this.resetInput.bind(this)}
                            >
                                <span>Clear</span>
                            </button>
                        </div>
                        
                        </form>
                    </div>
                    <div className="management-container">
                        {/* <div className="filter-container">

                            <div className="input-group">
                                <div className="left">
                                    <span>System:</span>
                                </div>
                                <div className="right">
                                    <select
                                        id="system-id"
                                        name="systemId"
                                        onChange={this.selectChange.bind(this)}
                                        value={this.state.systemId}
                                    >
                                        
                                    </select>
                                </div>

                            </div>
                            <div className="input-group">
                                <div className="left">
                                    <span>SMU type:</span>
                                </div>
                                <div className="right">
                                    <select
                                        id="smu-type"
                                        name="smuType"
                                        onChange={this.selectChange.bind(this)}
                                        value={this.state.smuType}
                                    >
                                        <option value="null">Both</option>
                                        <option value="0">Connector</option>
                                        <option value="1">Adapter</option>
                                    </select>
                                </div>

                            </div>
                           



                        </div>
                         */}
                        <div className="editting-container">
                            <header className="h2">
                                <h2>Registered Units</h2>
                            </header>
                            <div className="table-container">
                                <table id="registered-unit-table">
                                    <thead>
                                        <tr className="tr-head">
                                            <th style={{width:"10%"}}>
                                                <span>No</span>
                                            </th>
                                            <th style={{width:"30%"}}>
                                                <span>Name</span>
                                            </th>
                                            <th style={{width:"20%"}}>
                                                <span>IP address</span>
                                            </th>
                                            {/* <th style={{width:"15%"}}>
                                                <span>MAC address</span>
                                            </th> */}
                                            <th style={{width:"15%"}}>
                                                <span>Model</span>
                                            </th>
                                            <th style={{width:"25%"}}>
                                                <span>Actions</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    {this.state.systemList &&
                                        this.state.systemList.map(
                                            (system, index) => (
                                                <tr
                                                    id={`unit-row-${index+1}`}
                                                    
                                                >
                                                    <td
                                                        id={`id-${index+1}`}
                                                        // className="text-center"
                                                        className={
                                                            (system.enabled === 0
                                                                ? "disabled"
                                                                : "") + " text-center "
                                                        }
                                                    >
                                                        <span>{system.id}</span>
                                                    </td>
                                                    <td
                                                        id={`name-${index+1}`}
                                                        className={
                                                            (system.enabled === 0
                                                                ? "disabled"
                                                                : "") + " text-center "
                                                        }
                                                    >
                                                        <span>
                                                            {system.name}
                                                        </span>
                                                    </td>
                                                    <td
                                                        id={`ip-address-${index+1}`}
                                                        className={
                                                            (system.enabled === 0
                                                                ? "disabled"
                                                                : "") + " text-center "
                                                        }
                                                    >
                                                        <span>
                                                            {system.ip_address}
                                                        </span>
                                                    </td>
                                                    {/* <td
                                                        id={`mac-address-${index+1}`}
                                                        className={
                                                            (system.enabled === 0
                                                                ? "disabled"
                                                                : "") + " text-center "
                                                        }
                                                    >
                                                        <span>
                                                            {system.mac_address}
                                                        </span>
                                                    </td> */}
                                                    <td
                                                        id={`model-${index+1}`}
                                                        className={
                                                            (system.enabled === 0
                                                                ? "disabled"
                                                                : "") + " text-center "
                                                        }
                                                    >
                                                        <span>
                                                            {system.model}
                                                        </span>
                                                    </td>
                                                    <td
                                                        id={`action-${index+1}`}
                                                        className="text-center action-row"
                                                    >
                                                        <button
                                                            id={`btn-edit-${index+1}`}
                                                            className=""
                                                            className={
                                                                (system.enabled === 0
                                                                    ? "disabled"
                                                                    : "") + " sm-btn submit  actions "
                                                            }
                                                            onClick={() => {
                                                                this.editSystem(
                                                                    system
                                                                );
                                                            }}
                                                        >
                                                            <span>Edit</span>
                                                        </button>

                                                        {system.enabled ===
                                                            1 && (
                                                            <button
                                                                id={`disable-btn-${index+1}`}
                                                                className="sm-btn submit "
                                                                onClick={() => {
                                                                    this.disableUnit(
                                                                        system
                                                                    );
                                                                }}
                                                            >
                                                                <span>
                                                                    Inactive
                                                                </span>
                                                            </button>
                                                        )}
                                                        {system.enabled ===
                                                            0 && (
                                                            <button
                                                                id={`enable-btn-${index+1}`}
                                                                className="sm-btn enable-color"
                                                                onClick={() => {
                                                                    this.enableUnit(
                                                                        system
                                                                    );
                                                                }}
                                                            >
                                                                <span>
                                                                    Enable
                                                                </span>
                                                            </button>
                                                        )}
                                                    </td>
                                                </tr>
                                            )
                                        )}
                                </table>
                                {
                                        (this.state.systemList.length === 0) && <div id="no-data-task" className="no-remaining-task" style={{height:"7vw"}}><span className="camera-offline">NO UNIT REGISTERED</span></div>
                                    }
                            </div>
                        </div>
                    </div>
                </div>
                <Modal
                    isOpen={this.state.openEditModal}
                    className="edit-unit-body"
                    overlayClassName="default-overlay"
                    onRequestClose={this.closeEditModal}
                    // closeTimeoutMS={500}
                >
                    <EditSystem
                        data={this.edittingSystem}
                        // updateInput={this.updateInput}
                        closeEditModal={this.closeEditModal.bind(this)}
                        system={this.edittingSystem}
                    />
                </Modal>
                <Modal
                    isOpen={this.state.openDisableModal}
                    className="edit-unit-body"
                    overlayClassName="default-overlay"
                    onRequestClose={this.closeDisableModal}
                    // closeTimeoutMS={500}
                >
                    <DisableHandler
                        data={this.edittingSystem}
                        closeEditModal={this.closeDisableModal.bind(this)}
                    />
                </Modal>
                <Loading open={this.state.loading} />
            </div>
        );
    }
}

export default SystemRegistration;
