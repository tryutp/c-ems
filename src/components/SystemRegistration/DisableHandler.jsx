import React, { Component } from 'react'

export class DisableHandler extends Component {


    constructor(props){
        super(props);
        this.state = {
            enableSelect: this.props.data.enabled,
        }
    }

    handleInput =(event)=> {
        this.setState({ 
            [event.target.name] : event.target.value
        })
    }
    render() {
        // console.log(this.props.data)
        return (
            <div className="dsiable-unit-container">
                <header className="h2"><h2>Disable Unit</h2></header>
                <div className="content">
                    <div className="regis-form">
                        <div className="input-group">
                            <div className="left">
                                <span>Unit Status: </span>
                            </div>
                            <div className="right">
                                <select name="enableSelect" id="enable-select" value={ this.state.enableSelect} onChange={ this.handleInput}>
                                    <option value={1}>Enable</option>
                                    <option value={0}>Disable</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default DisableHandler
