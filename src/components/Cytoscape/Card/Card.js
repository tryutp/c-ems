import React from 'react'
import './Card.scss';

import {environment} from "./../../../Environment";
function Card({ editTemplate, templateInfo }) {
    return (
        <div className="card" onClick={editTemplate}>
            <div className="image-box">
                <img src={environment.api +templateInfo.img_path} alt=""/>
            </div>
            
            <div className="content">
                <p>{templateInfo.name}</p>
            </div>
            
        </div>
    )
}

export default Card
