import React, { Component } from "react";
import { environment } from "../../Environment";
import * as TopologyService from "../../services/TopologyService";
import "./MapManagement.scss";
import cytoscape from "cytoscape";
import { Redirect } from "react-router-dom";
import Modal from "react-modal";
import ReactTooltip from "react-tooltip";
import TreeItem from "@material-ui/lab/TreeItem";
import { InterconnectionList } from "./../InterconnectionList/InterconnectionList";
import { openConfirmation } from "../../shared/Confirmation";
import { onUserResponse } from "../../shared/UserResponse";
import * as InformUser from "../../shared/InformUser";
import * as UnitService from "../../services/UnitService";
import { Loading } from "../../shared/Loading";
import ModeContainer from "../Interconnection/ModeContainer";
import { UnitDashboard } from "./../Overview/UnitDashboard/UnitDashboard";
import { Video } from "./../../shared/Video/Video";

export class MapManagement extends Component {
	token = JSON.parse(localStorage.getItem("userData")).token;
	cxt_unit_data;

	constructor(props) {
		super(props);
		this.state = {
			data: [],
			loading: false,
			registeredName: "",
			registeredDescription: "",
			registeredIcon: "",

			editingTemplate: "",
			graphData: this.props.location.state.editingGraphData,
			models: [],
			unitList: "",
			containerList: [],
			mapPicture: {
				url: "",
				height: "",
				width: "",
			},
			containerHistory: [],

			interconnectionOrigin: "",
			interconnectionTarget: "",
			addedInterconnection: "",

			openInterconnection: false,
			isScrollDown: true,
			loading: false,
			choseSelfConnect: false,
			navigatesToTopology: false,

			addingElement: "",
			elementIsUnit: false,

			showIntercon: false,

			resizeBox: false,
			changingSizeId: "",
			percentageNodeSize: "",
			originalSize: 0,

			allowToLeave: false,
			properLeavingPage: false,
			unitDashboardOpen: false,
			openDropUnitVideo: false,
		};
	}
	cy;
	sourceNodeId;
	targetNodeId;
	openingUnit;
	originalSizeForAdjusting = [];
	videoSrc = "";
	videoTitle = "";

	graphStyle = cytoscape
		.stylesheet()
		.selector("edge")
		.css({
			width: "2",
			"curve-style": "bezier",
			// "control-point-distances": [40, -40],
			// "control-point-weights": [0.250, 0.75],
			// "haystack-radius": 0,
			"control-point-step-size": 100,
			// "source-distance-from-node": "20px",
			// "line-cap": "round",
			// 'source-endpoint': '180deg',
			// 'target-endpoint': '0deg',
			// 'curve-style': 'segments',
			// 'control-point-step-size': '200',
			// "edge-distances": "100",
			"line-fill": "linear-gradient",
			"line-gradient-stop-colors": "data(colorCode)",
			"line-gradient-stop-positions": ["0%", "50%", "50%", "100%"],
			"loop-direction": "data(loopDirection)",
			// "loop-sweep": "90deg",
			// 'line-color': 'data(colorCode)',
			"text-margin-x": "data(shiftLabel)",
			label: "data(label)",
			"text-background-opacity": 1,
			// "color": "#000000",
			"text-background-color": "#b3b3b3",
			// "text-background-shape": "roundrectangle",
			// "text-border-color": "#000",
			"text-border-width": 0,
			// "text-border-opacity": 1,
			// "text-background-padding": "2px 5px",
			color: "#EEEEEE",
			"text-background-color": "#282828",
			"text-background-opacity": "0.8",
			"text-background-shape": "roundrectangle",
			"text-background-padding": "5px 10px 5px 10px",

			// 'color': 'white',
			"font-size": "20px",
			// 'font-family': 'helvetica, arial, verdana, sans-serif',
			// 'font-weight': 300,
			// 'text-outline-width': 2,
			// 'text-outline-color': 'black',
			// 'control-point-distance': 100,
			// 'target-arrow-shape': 'data(arrow_target)',
			// 'source-arrow-shape': 'data(arrow_source)',
			// 'target-arrow-shape': 'circle',
			// 'source-arrow-shape': 'circle',
			// 'source-arrow-color': 'data(sourceColor)',
			// 'target-arrow-color': 'data(targetColor)',
		})
		.selector(":selected")
		.css({
			"edge.opacity": 0.5,
			width: "3",
			color: "white",
			"line-color": "#ffb3b3",
			// 'source-arrow-color': 'data(colorCode)',
			// 'target-arrow-color': 'data(colorCode)',
		})

		.selector("node")
		.css({
			shape: "rectangle",
			"background-clip": "none",
			// 'background-width':'100%',
			// 'background-height':'auto',
			// 'background-fit':'node',
			"background-fit": "contain",
			"background-opacity": "0",

			// 'width': '60',
			// 'height': '55',
			content: "data(name)",
			"text-valign": "center",
			// 'text-outline-width': 1.5,
			// 'text-outline-color': '#EEEEEE',

			color: "#EEEEEE",
			"font-size": "20px",
			"text-background-color": "#282828",
			"text-background-opacity": "0.8",
			"text-background-shape": "roundrectangle",
			"text-background-padding": "5px 10px 5px 10px",

			// 'background-image': 'url("assets/Robot_img_50.png")',
			// 'object-fit': 'cover',
			// 'background-fit':'contain',
			// 'background-repeat': 'no-repeat',
			// 'background-position': 'center center',
			// 'color': '#161616',
			// 'font-size': '1px',
			"font-weight": "600",
		})
		.selector(".select-intercon")
		.style()
		// .selector('.isNotSelected').style({
		//     'opacity': '0.6'
		// })
		.selector(":parent")
		.style({
			"border-width": "0",
			// 'overlay'
		});

	componentWillMount() {
		Modal.setAppElement("body");
		if (!this.props.location.state.editingGraphData) {
			InformUser.unsuccess({
				text: "Please select the topology for editing.",
			});
		}
		// console.log(this.props)
		this.setState({
			containerHistory: [
				{
					name: this.props.location.state.editingGraphData.name,
					ref_id: null,
				},
			],
		});
	}
	componentDidMount() {
		this.setState({ loading: true }, async () => {
			// await this.getRegisteredUnitList();
			await this.getContainerList();
			await this.getAvailableRegisteredUnit();
			await this.getGlobalTopology();
			// await this.getContainerTree();
			// await this.getElementList();
			this.setupCloseTabDetection(true);

			this.setState({ loading: false }, () => {
				// this.initialMapSetup();

				ReactTooltip.rebuild();
			});
		});
	}

	componentDidUpdate() {
		ReactTooltip.rebuild();
	}
	componentWillUnmount() {
		this.setupCloseTabDetection(false);
	}

	setupCloseTabDetection(isSettingUp) {
		if (isSettingUp) {
			window.addEventListener("beforeunload", this.closingBrowserHandler);
		}

		if (!isSettingUp) {
			window.removeEventListener(
				"beforeunload",
				this.closingBrowserHandler
			);
		}
	}

	closingBrowserHandler = async (e) => {
		e.preventDefault();
		e.returnValue = "something";

		// console.log(e)
		if (e.composed) {
			await this.cancelEdittingTopology();
		}
	};

	iconFile = "";
	backgroundMapFile = "";

	generateIdCount = 0;

	originPortData = "";
	targetPortData = "";

	// initialMapSetup() {
	//     if (this.state.graphData) {
	//         var thereIs = this.state.graphData;
	//         if (thereIs && thereIs.name && thereIs.bg_path && thereIs.bg_height && thereIs.bg_width) {
	//             var bg = {
	//                 url: environment.api + thereIs.bg_path,
	//                 height: thereIs.bg_height,
	//                 width: thereIs.bg_width
	//             }
	//             var iconUrl = environment.api + thereIs.img_path
	//             var cyData = {
	//                 nodes: thereIs.element,
	//                 edges: thereIs.edges,
	//             }
	//             this.setState({
	//                 graphData: cyData,
	//                 mapPicture: bg,
	//                 // registeredName: thereIs.name,
	//                 // registeredIcon: iconUrl,
	//                 // registeredDescription: "Still no description attribute"
	//             }, () => {
	//                 // console.log(this.state.graphData)
	//                 this.renderCytoscapeElement();
	//             })
	//         } else {
	//             InformUser.unsuccess({ text: "Data is incomplete" });
	//         }
	//     } else {
	//         InformUser.unsuccess({ text: "There is no data to generate editing map" });
	//     }
	// }

	async getGlobalTopology() {
		await TopologyService.open_next_global_container(this.token).then(
			(result) => {
				// console.log("choose result ", result)
				var thereIs = result.data;
				if (
					thereIs &&
					thereIs.name &&
					thereIs.bg_path &&
					thereIs.bg_height &&
					thereIs.bg_width
				) {
					// this.popPageHistory(node);
					this.reformGraphData(result.data);
				} else {
					// console.log("hi there")
					InformUser.unsuccess({ text: "Data is incomplete" });
				}
			}
		);
	}
	// async getRegisteredUnitList(){
	//     await TopologyService.get_registeredUnit_list(this.token).then(result => {
	//         console.log(result)
	//         if(result.status == 1){
	//             this.setState( { unitList : result.data } )
	//         }else{
	//             InformUser.unsuccess({ text: "Retrieve Registered Unit List Problem: " + result.msg})
	//         }
	//     })
	// }
	async getAvailableRegisteredUnit() {
		await TopologyService.get_available_registered(this.token).then(
			(result) => {
				// console.log(result)
				if (result.status == 1) {
					this.setState({ unitList: result.data });
				} else {
					InformUser.unsuccess({
						text:
							"Retrieve Registered Unit List Problem: " +
							result.msg,
					});
				}
			}
		);
	}
	async getContainerList() {
		await TopologyService.get_containerList(this.token).then((result) => {
			// console.log(result)
			if (result.status == 1) {
				this.setState({ containerList: result.data });
			} else {
				InformUser.unsuccess({
					text: "Retrieve Container List Problem: " + result.msg,
				});
			}
		});
	}
	async getContainerTree() {
		// this.setState({ loading : true} , ()=>{
		await TopologyService.get_containerTree(this.token).then((result) => {
			if (result.status == 1) {
				this.setState({ containerData: result.data });
			} else {
				InformUser.unsuccess({ text: result.msg });
			}
			// this.setState({ loading : false });
		});
		// })
	}
	async getElementList() {
		// this.setState({ loading: true }, ()=> {
		await TopologyService.get_elementList(this.token).then((result) => {
			// console.log("Unit Models, ", result.data)
			if (result.status == 1) {
				this.setState({ models: result.data });
			} else {
				InformUser.unsuccess({ text: result.msg });
			}
			// this.setState({ loading : false });
		});
		// })
	}

	openDashboardUnit(event) {
		this.openingUnit = event.target.data();
		this.openingUnit["model"] = this.openingUnit.model_name;
		this.openingUnit["img_path"] = event.target.style()["background-image"];
		// console.log(this.openingUnit)
		this.setState({ unitDashboardOpen: true });
	}
	renderCytoscapeElement() {
		// console.log("mapPicture ", this.state.registeredIcon)
		// console.log(this.state.graphData)

		this.cy = cytoscape({
			container: document.getElementById("cy"),
			style: this.graphStyle,
			zoom: 2,
			minZoom: 0.1,
			maxZoom: 2,
			wheelSensitivity: 0.1,
			// elements: this.state.data,
			elements: this.state.graphData,
			layout: {
				name: "preset",
				// boundingBox: {
				//     x1: 0,
				//     y1: 0,
				//     x2: 100,
				//     y2: 100
				// },
			},
		});

		let background = new Image();
		background.onload = () => {
			// console.log("height", background.height)
			// console.log("width", background.width)
			const bottomLayer = this.cy.cyCanvas({
				zIndex: -1,
				// pixelRatio: "auto",
			});
			const canvas = bottomLayer.getCanvas();
			const ctx = canvas.getContext("2d");
			this.cy.on("render cyCanvas.resize", (evt) => {
				bottomLayer.resetTransform(ctx);
				bottomLayer.clear(ctx);
				bottomLayer.setTransform(ctx);
				ctx.save();
				// Draw a background
				ctx.drawImage(background, 0, 0);

				// Draw text that follows the model
				// ctx.font = "24px Helvetica";
				// ctx.fillStyle = "black";
				// ctx.fillText("This text follows the model", 200, 300);

				// // Draw shadows under nodes
				// ctx.shadowColor = "black";
				// ctx.shadowBlur = 25 * cy.zoom();
				// ctx.fillStyle = "white";
				// cy.nodes().forEach(node => {
				//   const pos = node.position();
				//   ctx.beginPath();
				//   ctx.arc(pos.x, pos.y, 10, 0, 2 * Math.PI, false);
				//   ctx.fill();
				// });
				// ctx.restore();

				// // Draw text that is fixed in the canvas
				// bottomLayer.resetTransform(ctx);
				// ctx.save();
				// ctx.font = "24px Helvetica";
				// ctx.fillStyle = "red";
				// ctx.fillText("This text is fixed", 200, 200);
				// ctx.restore();
			});
		};
		background.src = this.state.mapPicture.url;
		//Override initial setting for background map
		// var pixelWidth = cy.width()/cy.zoom();
		// var pixelHeight = cy.height()/cy.zoom()
		// console.log(this.state.mapPicture.height)
		var zoomLevel = this.cy.height() / this.state.mapPicture.height;
		this.cy.zoom(zoomLevel);
		this.cy.minZoom(zoomLevel);

		var cyMapWidth = this.state.mapPicture.width / 2;
		var temp =
			(this.cy.width() * cyMapWidth) / (this.cy.width() / this.cy.zoom());

		this.cy.pan({
			x: this.cy.width() / 2 - temp,
			y: 0,
		});

		this.cy.on("tap", (event) => {
			if (this.state.addingElement) {
				this.addNode(event.position);
			}
		});

		// this.cy.on('tap', (event) => {
		//     // cy.fit()
		// })
		this.cy.on("tap", "node", (event) => {
			// console.log(event.target.position())
			event.target.select();
			const target = event.target.data();
			if (!event.target.grabbable()) {
				event.target.unselect();
			} else {
				if (target.model_id == "-1") {
					// console.log(node)
					this.changeContainerPage(target, true);
				} else {
					this.cy.nodes().map((node) => {
						if (!node.selected()) {
							// node.toggleClass("isNotSelected", true);
						} else {
							// node.toggleClass("isNotSelected", false);
						}
					});
					this.cy.edges().map((edge) => {
						// edge.toggleClass("isNotSelected", true);
					});
					this.openDashboardUnit(event);
				}
			}
			// console.log(target)
		});
		// this.cy.on('unselect', (event) => {
		//     this.cy.nodes().toggleClass("isNotSelected", false);
		//     this.cy.edges().toggleClass("isNotSelected", false);
		// })

		this.cy.on("tap", "edge", (event) => {
			var edgeData = event.target.data();
			// console.log(edgeData.source);
			this.autoOpenIntercon(edgeData);
		});

		// cy.on('tap', function (event) {
		//     // console.log("Zoom", cy.zoom())
		//     // console.log("Veiwpoint Width", cy.width())
		//     // console.log("Veiwpoint Height", cy.height())
		// })
		this.cy.on("cxttap", "node", (event) => {
			// console.log(event.target.id())
			let target = event.target || event.cyTarget;

			if (target.grabbable()) {
				this.cy.nodes(":selected").unselect();
				target.select();

				if (this.cy.nodes(":selected").length == 1) {
					//Single

					const node = target.data();
					this.cxt_unit_data = node;

					if (node.model_id != "-1") {
						// console.log(node)
						contextMenu.showMenuItem("interconnection-context");
					} else {
						contextMenu.hideMenuItem("interconnection-context");
					}

					contextMenu.showMenuItem("delete-node");
					// contextMenu.hideMenuItem('adjust-size')

					// if(target.grabbable()){
					//     contextMenu.showMenuItem('lock-node')
					//     contextMenu.hideMenuItem('unlock-node')
					//     contextMenu.hideMenuItem('toggle-lock-node')
					// }else{

					//     contextMenu.hideMenuItem('lock-node')
					//     contextMenu.showMenuItem('unlock-node')
					//     contextMenu.hideMenuItem('toggle-lock-node')
					// }
				} else {
					contextMenu.showMenuItem("delete-node");
					// contextMenu.showMenuItem('adjust-size')
					// contextMenu.hideMenuItem('adjust-size')
					// contextMenu.hideMenuItem('lock-node')
					// contextMenu.hideMenuItem('unlock-node')
					// contextMenu.showMenuItem('toggle-lock-node')
				}

				// contextMenu.showMenuItem('unselect-all-nodes')
			} else {
				contextMenu.hideMenuItem("interconnection-context");
				contextMenu.hideMenuItem("delete-node");
				// contextMenu.hideMenuItem('adjust-size')
			}
		});

		// this.cy.automove({
		//     nodesMatching: this.cy.nodes(),
		//     reposition: { x1: 0, x2: this.state.mapPicture.width, y1: 0, y2: this.state.mapPicture.height }
		// });

		var contextMenu = this.cy.contextMenus({
			menuItemClasses: ["context-menu-item"],
			contextMenuClasses: ["context-menu-box"],
			submenuIndicator: { src: "/angle-right-solid.svg" },
			menuItems: [
				{
					id: "interconnection-context",
					content: "Create Interconnections",
					tooltipText: "",
					selector: "node",
					coreAsWell: false,
					// show: true,
					hasTrailingDivider: true,
					onClickFunction: (event) => {},
					submenu: [
						{
							id: "select-origin",
							content: "Select as source unit",
							onClickFunction: (event) => {
								let target =
									event.target.data() ||
									event.cyTarget.data();

								var originInfo = JSON.parse(
									JSON.stringify(target)
								);
								// console.log(originInfo)
								originInfo["img_path"] =
									event.target.style()["background-image"];
								this.setState(
									{ interconnectionOrigin: originInfo },
									() => {
										// console.log(this.state.interconnectionOrigin)
									}
								);

								if (
									this.state.interconnectionTarget &&
									this.state.interconnectionOrigin &&
									this.state.interconnectionTarget.unit_id ==
										this.state.interconnectionOrigin.unit
								) {
									this.setState({ choseSelfConnect: true });
								}
							},
						},
						{
							id: "select-target",
							content: "Select as target unit",
							onClickFunction: (event) => {
								let target =
									event.target.data() ||
									event.cyTarget.data();

								// if (this.state.interconnectionOrigin) {
								// if (this.state.interconnectionOrigin.unit_id != target.unit_id) {
								var targetInfo = JSON.parse(
									JSON.stringify(target)
								);
								targetInfo["img_path"] =
									event.target.style()["background-image"];
								this.setState({
									interconnectionTarget: targetInfo,
								});
								// } else {
								//     InformUser.unsuccess({ title: "Selection Conflict!", text: "The Target system cannot be the same as the Origin system." })
								// }

								// }
							},
						},
					],
				},

				{
					id: "delete-node",
					content: "Delete this element",
					tooltipText: "Delete unit from templete.",
					// image: { src: "add.svg", width: 12, height: 12, x: 6, y: 4 },
					selector: "node",
					coreAsWell: false,
					onClickFunction: async (event) => {
						var nameTemp = [];
						this.cy.nodes(":selected").forEach((selected) => {
							// console.log(selected.data())
							nameTemp.push(selected.data().name);
						});
						const dialog = await openConfirmation({
							title: "Delete Element",
							text:
								"Are you sure you want to delete '" +
								nameTemp +
								"' element?",
						});
						if (dialog) {
							this.cy
								.nodes(":selected")
								.forEach(async (selected) => {
									await TopologyService.remove_unit(
										selected.data().ref_id,
										this.token
									).then(async (result) => {
										// console.log(result)
										if (result.status == 1) {
											// this.reformGraphData(result.data);
											var id = selected.id();
											// this.syncDeleteState(id)
											this.getAvailableRegisteredUnit();
											selected.remove();
										} else {
											InformUser.unsuccess({
												title: "Cannot remove this element.",
												text: result.msg,
											});
										}
									});
								});
							this.cy.nodes(":selected").unselect();
						}
					},
				},
				{
					id: "unit-detail",
					content: "View unit detail",
					tooltipText: "",
					selector: "node",
					coreAsWell: false,
					show: true,
					onClickFunction: (event) => {
						// this.openingUnit = unit;
						// console.log(event.target.style()["background-image"])
						this.openDashboardUnit(event);
					},
					// disabled: true
				},
				// {
				//     id: 'adjust-size',
				//     content: 'Adjust unit size',
				//     tooltipText: 'Increase/Decrease the size of system.',
				//     selector: 'node',
				//     coreAsWell: false,
				//     // submenu: [
				//     //     {
				//     //         id: 'increase-size',
				//     //         content: 'Increase size',
				//     //         onClickFunction: (event) => {
				//     //             this.cy.nodes(':selected').forEach(selected => {
				//     //                 var id = selected.id()
				//     //                 this.adjustSize(id, true)
				//     //             })
				//     //             this.cy.nodes(':selected').unselect();
				//     //         },
				//     //     },
				//     //     {
				//     //         id: 'decrease-size',
				//     //         content: 'Decrease size',
				//     //         onClickFunction: (event) => {
				//     //             this.cy.nodes(':selected').forEach(selected => {
				//     //                 var id = selected.id()
				//     //                 this.adjustSize(id, false)
				//     //             })
				//     //             this.cy.nodes(':selected').unselect();
				//     //         },
				//     //     },
				//     // ],

				//     onClickFunction: (event) => {
				//         // this.originalSizeForAdjusting = [];
				//         // this.cy.nodes(':selected').map(node => {
				//         //     node.addClass('resizing')
				//         //     this.originalSizeForAdjusting.push(node.width())
				//         // })
				//         // console.log(this.originalSizeForAdjusting)
				//         // this.setState({
				//         //     originalSize: event.target.width(),
				//         //     percentageNodeSize: 100,
				//         //     resizeBox: true,
				//         // });

				//     }
				// },

				// {
				//     id: 'lock-node',
				//     content: 'Lock this system',
				//     tooltipText: 'Prevent system from being dragged.',
				//     selector: 'node',
				//     coreAsWell: false,
				//     show: false,
				//     onClickFunction: (event) => {

				//         this.cy.nodes(':selected').forEach(selected => {
				//             if (selected.grabbable()) {
				//                 selected.ungrabify()
				//                 selected.addClass('locked')
				//                 var id = selected.id()
				//                 this.syncLockState(id, false)
				//             }
				//         })
				//         this.cy.nodes(':selected').unselect();
				//     }

				// },
				// {
				//     id: 'unlock-node',
				//     content: 'Unlock this system',
				//     tooltipText: 'Prevent system from being dragged.',
				//     selector: 'node',
				//     coreAsWell: false,
				//     show: false,
				//     onClickFunction: async (event) => {
				//         var nameTemp = []
				//         this.cy.nodes(':selected').forEach(selected => {
				//             nameTemp.push(selected.data().name);
				//         })
				//         const dialog = await openConfirmation({
				//             title: "Unlock Model box",
				//             text: "Are you sure you want to unlock '" + nameTemp + "' model box?"
				//         })
				//         if (dialog) {
				//             this.cy.nodes(':selected').forEach(selected => {
				//                 var idx = selected.id()
				//                 if (!selected.grabbable()) {
				//                     selected.grabify()
				//                     selected.removeClass('locked')
				//                     console.log('here')
				//                     this.syncLockState(idx, true)
				//                 }
				//             })
				//             this.cy.nodes(':selected').unselect();
				//         }

				//     }

				// },
				// {
				//     id: 'toggle-lock-node',
				//     content: 'Toggle-lock selected systems',
				//     tooltipText: 'Toggle locking status of selected systems.',
				//     selector: 'node',
				//     coreAsWell: false,
				//     show: false,
				//     onClickFunction: async () => {
				//         var nameTemp = []
				//         this.cy.nodes(':selected').forEach(selected => {
				//             nameTemp.push(selected.data().name);
				//         })
				//         const dialog = await openConfirmation({
				//             title: "Unlock Model box",
				//             text: "Are you sure you want to toggle '" + nameTemp + "' model box?"
				//         })
				//         if (dialog) {
				//             this.cy.nodes(':selected').forEach(selected => {
				//                 if (selected.grabbable()) {
				//                     selected.ungrabify()
				//                     selected.addClass('locked')
				//                     var id = selected.id()
				//                     this.syncLockState(id, false)
				//                 } else {
				//                     selected.grabify()
				//                     selected.removeClass('locked')
				//                     var id = selected.id()
				//                     this.syncLockState(id, true)
				//                 }

				//             })
				//             this.cy.nodes(':selected').unselect();
				//         }

				//     }

				// }
			],
		});
	}

	interconnectionModal(edge) {
		// console.log(edge)
		// console.log(this.cy.$id(edge.source).data())
		TopologyService.get_addedInterconnection();
	}
	adjustSize(id, isIncrease) {
		var newState = this.state.graphData;
		newState.nodes.forEach((node, index) => {
			if (node.data.id == id) {
				// console.log( index)
				if (isIncrease) {
					node.style.height = (
						parseInt(node.style.height) + 10
					).toString();
					node.style.width = (
						parseInt(node.style.width) + 10
					).toString();
				} else {
					node.style.height = (
						parseInt(node.style.height) - 10
					).toString();
					node.style.width = (
						parseInt(node.style.width) - 10
					).toString();
				}
			}
		});
		this.setState(
			{
				graphData: newState,
			},
			() => {
				this.renderCytoscapeElement();
			}
		);
	}
	syncDeleteState(id) {
		var newState = this.state.graphData;
		newState.nodes.forEach((node, index) => {
			if (node.data.id == id) {
				newState.nodes.splice(index, 1);
			}
		});
		this.setState({
			graphData: newState,
		});
	}
	syncLockState(id, value) {
		var newState = this.state.graphData;
		newState.nodes.forEach((node) => {
			if (node.data.id == id) {
				node.grabbable = value;
			}
		});
		this.setState({
			graphData: newState,
		});
	}
	reformGraphData(thereIs) {
		// console.log("reform data ", data)
		// var bg = {
		//     url: environment.api + data.bg_path,
		//     height: data.bg_height,
		//     width: data.bg_width
		// }
		// var iconUrl = environment.api + data.img_path
		// var cyData = {
		//     nodes: data.element,
		//     edegs: data.edges
		// }
		// this.setState({
		//     graphData: cyData,
		//     mapPicture: bg,
		//     registeredName: data.name,
		//     registeredIcon: iconUrl,
		//     registeredDescription: "Still no description attribute"
		// }, () => {
		//     this.renderCytoscapeElement();
		// })

		var bg = {
			url: environment.api + thereIs.bg_path,
			height: thereIs.bg_height,
			width: thereIs.bg_width,
		};
		var iconUrl = environment.api + thereIs.img_path;
		var cyData = {
			nodes: thereIs.element,
			edges: thereIs.edges,
		};
		if (thereIs.cont_element && thereIs.cont_element.length >= 1) {
			cyData = {
				nodes: thereIs.cont_element.concat(thereIs.element),
				edges: thereIs.edges,
			};
		}
		this.setState(
			{
				graphData: cyData,
				mapPicture: bg,
				// registeredName: thereIs.name,
				// registeredIcon: iconUrl,
				// registeredDescription: "Still no description attribute"
			},
			() => {
				// console.log(this.state.graphData)
				this.renderCytoscapeElement();
			}
		);
	}
	// reformGraphDataWithCont(thereIs) {
	//     // console.log("reform data ", data)
	//     var bg = {
	//         url: environment.api + thereIs.bg_path,
	//         height: thereIs.bg_height,
	//         width: thereIs.bg_width
	//     }
	//     var iconUrl = environment.api + thereIs.img_path
	//     var cyData = {
	//         nodes: thereIs.cont_element.concat(thereIs.element),
	//         edges: thereIs.edges,
	//     }
	//     // if(thereIs.element.length <= 0){
	//     //     cyData = {
	//     //         nodes: thereIs.cont_element.concat(thereIs.element),
	//     //         edges: thereIs.edges,
	//     //     }
	//     // }
	//     this.setState({
	//         graphData: cyData,
	//         mapPicture: bg,
	//         // registeredName: thereIs.name,
	//         // registeredIcon: iconUrl,
	//         // registeredDescription: "Still no description attribute"
	//     }, () => {
	//         console.log(this.state.graphData)
	//         this.renderCytoscapeElement();
	//     })
	//     // var bg = {
	//     //     url: environment.api + data.bg_path,
	//     //     height: data.bg_height,
	//     //     width: data.bg_width
	//     // }

	//     // var iconUrl = environment.api + data.img_path
	//     // var cyData = {
	//     //     nodes: data.cont_element.concat(data.element),
	//     //     edegs: data.edges
	//     // }
	//     // // if (data.element) {
	//     // //     data.element.map(element => {
	//     // //         cyData.nodes.push(element)
	//     // //     })
	//     // // }
	//     // console.log(cyData)
	//     // this.setState({
	//     //     graphData: cyData,
	//     //     mapPicture: bg,
	//     //     registeredName: data.name,
	//     //     registeredIcon: iconUrl,
	//     //     registeredDescription: "Still no description attribute"
	//     // }, () => {
	//     //     this.renderCytoscapeElement();
	//     // })
	// }
	addingNode(model, isUnit) {
		// console.log(model, this.state.addingElement)
		const addingElement = this.state.addingElement;
		if (!addingElement) {
			this.setState(
				{
					addingElement: model,
					elementIsUnit: isUnit,
				},
				() => {
					this.cursorMove(true);
				}
			);
		} else {
			if (addingElement.name == model.name) {
				// click again to remove
				this.setState({ addingElement: "" }, () => {
					this.cursorMove(false);
				});
			} else {
				// click another to replace
				this.setState({ addingElement: model }, () => {
					this.cursorMove(true);
				});
			}
		}
	}
	autoOpenIntercon(edge) {
		var sourceEdgeId = edge.source;
		var targetEdgeId = edge.target;

		// console.log(sourceEdgeId,sourceEdgeId)

		var sourceNodeId = this.cy.$id(sourceEdgeId).data().parent;
		var targetNodeId = this.cy.$id(targetEdgeId).data().parent;
		// console.log(this.sourceNodeId,this.targetNodeId)

		var sourceNodeData = this.cy.$id(sourceNodeId).data();
		var targetNodeData = this.cy.$id(targetNodeId).data();

		var sourceInfo = JSON.parse(JSON.stringify(sourceNodeData));
		var targetInfo = JSON.parse(JSON.stringify(targetNodeData));

		sourceInfo["img_path"] = this.cy.$id(sourceNodeId).style()[
			"background-image"
		];
		targetInfo["img_path"] = this.cy.$id(targetNodeId).style()[
			"background-image"
		];
		// console.log(sourceInfo,targetInfo)
		this.setState(
			{
				interconnectionOrigin: sourceInfo,
				interconnectionTarget: targetInfo,
			},
			() => {
				this.openInterconnection();
			}
		);

		// this.setState({ showIntercon: true })
	}
	cursorMove(visible) {
		if (visible) {
			document.getElementById("cy").classList.add("adding");
		} else {
			document.getElementById("cy").classList.remove("adding");
		}
	}
	addUnit(element, position) {
		//Generate ID for displaying in Cytoscape
		// console.log(model)
		// console.log(this.state.graphData)
		// console.log(element)
		let genModel = JSON.parse(JSON.stringify(element));

		// genModel.data.id = "added-" + this.generateIdCount.toString()
		genModel.position.x = position.x;
		genModel.position.y = position.y;

		// console.log(genModel)
		if (this.cy) {
			this.cy.add(genModel);
			this.setState({ addingElement: "" }, () => {
				this.generateIdCount += 1;
				this.cursorMove(false);
			});
		}
	}
	addContainer(element, position) {
		//Generate ID for displaying in Cytoscape
		// console.log(model)
		// console.log(this.state.graphData)
		// console.log(element)
		let genModel = JSON.parse(JSON.stringify(element));

		genModel.data.id = "added-" + this.generateIdCount.toString();
		genModel.position.x = position.x;
		genModel.position.y = position.y;

		// console.log(genModel)
		if (this.cy) {
			this.cy.add(genModel);
			this.setState({ addingElement: "" }, () => {
				this.generateIdCount += 1;
				this.cursorMove(false);
			});
		}
	}

	addNode(position) {
		const node = this.state.addingElement;
		const isUnit = this.state.elementIsUnit;

		// console.log(node);

		const currentPageId =
			this.state.containerHistory[this.state.containerHistory.length - 1][
				"ref_id"
			];

		if (node.id)
			if (this.state.containerHistory.length == 1) {
				if (isUnit) {
					TopologyService.add_global_topology(
						node.id,
						null,
						100,
						100,
						this.token
					).then((result) => {
						// console.log("result form add Unit ", result)
						if (result.status == 1) {
							if (result.data.last_element) {
								this.addUnit(
									result.data.last_element,
									position
								);
								// const grahpDataTemp = this.state.graphData

								// grahpDataTemp.nodes.push(result.data.last_element)

								// this.setState({ graphData: grahpDataTemp }, () => {
								//     this.renderCytoscapeElement();

								//     this.getAvailableRegisteredUnit();
								// })
								this.getAvailableRegisteredUnit();
							} else {
								InformUser.unsuccess({
									text: "There is no newly added unit.",
								});
							}
						} else {
							InformUser.unsuccess({ text: result.msg });
						}
					});
				} else {
					TopologyService.add_global_topology(
						null,
						node.id,
						100,
						100,
						this.token
					).then((result) => {
						// console.log("result form add Unit ", result)
						if (result.status == 1) {
							if (result.data.last_element) {
								this.addContainer(
									result.data.last_element,
									position
								);
								// const grahpDataTemp = this.state.graphData
								// grahpDataTemp.nodes.push(result.data.last_element)
								// console.log("grahpDataTemp", grahpDataTemp)
								// this.setState({ graphData: grahpDataTemp }, () => {
								//     this.renderCytoscapeElement();
								//     this.getAvailableRegisteredUnit();
								// })
								this.getAvailableRegisteredUnit();
							} else {
								InformUser.unsuccess({
									text: "There is no newly added unit.",
								});
							}
						} else {
							InformUser.unsuccess({ text: result.msg });
						}
					});
				}
			} else {
				if (isUnit) {
					TopologyService.add_local_topology(
						currentPageId,
						node.id,
						null,
						100,
						100,
						this.token
					).then((result) => {
						// console.log("result form add Unit ", result)
						if (result.status == 1) {
							if (result.data.last_element) {
								this.addUnit(
									result.data.last_element,
									position
								);
								// const grahpDataTemp = this.state.graphData
								// grahpDataTemp.nodes.push(result.data.last_element)
								// this.setState({ graphData: grahpDataTemp }, () => {
								//     this.renderCytoscapeElement();
								//     this.getAvailableRegisteredUnit();
								// })
								this.getAvailableRegisteredUnit();
							} else {
								InformUser.unsuccess({
									text: "There is no newly added unit.",
								});
							}
						} else {
							InformUser.unsuccess({ text: result.msg });
						}
					});
				} else {
					TopologyService.add_local_topology(
						currentPageId,
						null,
						node.id,
						100,
						100,
						this.token
					).then((result) => {
						// console.log("result form add Unit ", result)
						if (result.status == 1) {
							if (result.data.last_element) {
								this.addContainer(
									result.data.last_element,
									position
								);
								// const grahpDataTemp = this.state.graphData
								// grahpDataTemp.nodes.push(result.data.last_element)
								// console.log("grahpDataTemp", grahpDataTemp)
								// this.setState({ graphData: grahpDataTemp }, () => {
								//     this.renderCytoscapeElement();
								//     this.getAvailableRegisteredUnit();
								// })
								this.getAvailableRegisteredUnit();
							} else {
								InformUser.unsuccess({
									text: "There is no newly added unit.",
								});
							}
						} else {
							InformUser.unsuccess({ text: result.msg });
						}
					});
				}
			}
	}
	closeInterconModal = () => {
		this.setState({ showIntercon: false });
	};
	async updatePosition() {
		var positionArray = [];
		const data = this.cy.nodes();

		const mysteriousNumber = 60;

		// console.log(data)
		if (data) {
			data.map((node) => {
				// if(node.ref_id !== ""){
				if ("parent" in node.data()) {
				} else {
					var temp = {
						ref_id: node.data().ref_id,
						x_axist: node.position().x - mysteriousNumber,
						y_axist: node.position().y,
					};
					positionArray.push(temp);
				}

				// }else{
				//     console.log("yutyut")
				// }
			});
			// console.log("format ", positionArray);
		}
		// console.log("array position", positionArray);
		if (positionArray.length >= 1) {
			await TopologyService.save_position_unit(
				positionArray,
				this.token
			).then((result) => {
				// console.log("update position", result)
				if (result.status == 1) {
					// this.reformGraphData(result.data);
				} else {
					InformUser.unsuccess({
						text: result.msg,
					});
				}
			});
		}

		// console.log("array position", positionArray);
	}

	getInterconEdge = async () => {
		// await this.updatePosition();
		var currentPage =
			this.state.containerHistory[this.state.containerHistory.length - 1];

		// this.setState({ loading: false }, async () => {

		if (!currentPage.ref_id) {
			//Case: backward to the Default global

			await TopologyService.open_next_global_container(this.token).then(
				async (result) => {
					// console.log("get edge", result)
					var thereIs = result.data;
					if (
						thereIs &&
						thereIs.name &&
						thereIs.bg_path &&
						thereIs.bg_height &&
						thereIs.bg_width
					) {
						await this.reformGraphData(thereIs);
					} else {
						InformUser.unsuccess({ text: "Data is incomplete" });
					}
				}
			);
		} else {
			//Case: backward to nth Container
			await TopologyService.open_next_container(
				currentPage.ref_id,
				this.token
			).then(async (result) => {
				// console.log("get edge", result)
				var thereIs = result.data;
				if (
					thereIs &&
					thereIs.id &&
					thereIs.name &&
					thereIs.bg_path &&
					thereIs.bg_height &&
					thereIs.bg_width
				) {
					await this.reformGraphData(result.data);
				} else {
					InformUser.unsuccess({ text: "Data is incomplete" });
				}
			});
		}

		// this.setState({ loading: false })
		// })

		// this.setState({ graphData: data }, () => {
		//     console.log(this.state.graphData)
		//     this.renderCytoscapeElement();
		// })

		this.setState({ openInterconnection: false });
	};
	async confirmApplyTopology() {
		const dialog = await openConfirmation({
			title: "Confirmation",
			text: "Are you sure you want to apply this editting map to the current topology?",
		});
		if (dialog) {
			this.setState({ loading: true }, async () => {
				await this.applyTopology();
			});
		}
	}
	async applyTopology() {
		await this.updatePosition();

		await TopologyService.apply_topology(this.token).then(
			async (result) => {
				// console.log(result)
				if (result.status == 1) {
					this.setState({
						loading: false,
						properLeavingPage: true,
					});
					const dialog = await onUserResponse({
						text: "Successfully applied the Topology.",
					});
					this.setState({
						navigatesToTopology: true,
						allowToLeave: true,
					});
				} else {
					this.setState({
						loading: false,
					});
					InformUser.unsuccess({ text: result.msg });
				}
			}
		);
	}

	showInterconnectionButton() {
		const source = this.state.interconnectionOrigin;
		const target = this.state.interconnectionTarget;
		if (source && target) {
			if (source.id === target.id) {
				return (
					<button
						id="intercon-panel-btn"
						className="btn submit"
						onClick={this.openInterconnection.bind(this)}
					>
						<span>Open Self Connection</span>
					</button>
				);
			}
			if (source.id !== target.id) {
				return (
					<button
						id="intercon-panel-btn"
						className="btn submit"
						onClick={this.openInterconnection.bind(this)}
					>
						<span>Open Interconnection</span>
					</button>
				);
			}
		}
		if (!source || !target) {
			return (
				<span id="no-data-interconntion" className="no-source-target">
					Choose source and target unit to open Interconnection panel.
				</span>
			);
		}
	}

	onIconChange = (event) => {
		if (event.target.files && event.target.files[0]) {
			let img = event.target.files[0];
			this.iconFile = img;
			let imgSrc = URL.createObjectURL(img);
			const image = new Image();
			image.onload = () => {
				// this.iconWidth = image.width ;
				// this.iconHeight = image.height;
				// console.log(this.iconWidth, this.iconHeight)
				this.setState({
					registeredIcon: imgSrc,
				});
			};
			image.src = imgSrc;
		}
	};
	onBackgroundUpload = (event) => {
		// console.log(event)
		if (event.target.files && event.target.files[0]) {
			let map = event.target.files[0];
			this.backgroundMapFile = map;
			let imgSrc = URL.createObjectURL(map);
			var image = new Image();
			image.onload = () => {
				var bg = {
					url: imgSrc,
					height: image.height,
					width: image.width,
				};
				this.setState(
					{
						mapPicture: bg,
					},
					() => {
						this.renderCytoscapeElement();
					}
				);
			};
			image.src = imgSrc;
		}
	};

	changeContainerPage(node, isForwardingPage) {
		this.updatePosition();

		if (isForwardingPage) {
			this.setState({ loading: false }, async () => {
				await TopologyService.open_next_container(
					node.ref_id,
					this.token
				).then((result) => {
					// console.log("choose result ", result)
					var thereIs = result.data;
					if (
						thereIs &&
						thereIs.id &&
						thereIs.name &&
						thereIs.bg_path &&
						thereIs.bg_height &&
						thereIs.bg_width
					) {
						this.pushPageHistory(node);
						this.reformGraphData(result.data);
					} else {
						InformUser.unsuccess({ text: "Data is incomplete" });
					}
				});
				this.setState({ loading: false });
			});
		} else {
			this.setState({ loading: false }, async () => {
				// console.log(node)
				if (!node.ref_id) {
					//Case: backward to the Default global
					await TopologyService.open_next_global_container(
						this.token
					).then((result) => {
						// console.log("choose result ", result)
						var thereIs = result.data;
						if (
							thereIs &&
							thereIs.name &&
							thereIs.bg_path &&
							thereIs.bg_height &&
							thereIs.bg_width
						) {
							this.popPageHistory(node);
							this.reformGraphData(result.data);
						} else {
							InformUser.unsuccess({
								text: "Data is incomplete",
							});
						}
					});
				} else {
					//Case: backward to nth Container
					await TopologyService.open_next_container(
						node.ref_id,
						this.token
					).then((result) => {
						// console.log("choose result ", result)
						var thereIs = result.data;
						if (
							thereIs &&
							thereIs.id &&
							thereIs.name &&
							thereIs.bg_path &&
							thereIs.bg_height &&
							thereIs.bg_width
						) {
							this.popPageHistory(node);
							this.reformGraphData(result.data);
						} else {
							InformUser.unsuccess({
								text: "Data is incomplete",
							});
						}
					});
				}

				this.setState({ loading: false });
			});
		}

		//Call API to get new graphData

		//Reform
	}
	pushPageHistory(node) {
		// console.log("Push",this.state.containerHistory)
		const history = this.state.containerHistory;
		history.push(node);
		// console.log("Push2", this.state.containerHistory)
	}
	popPageHistory(node) {
		// console.log(this.state.containerHistory)
		const history = this.state.containerHistory;
		if (history.length <= 2) {
			//Try to backword to the Golbal Map
			history.pop();
			// console.log(this.state.containerHistory)
		} else {
			history.map((page, index) => {
				// console.log(page.ref_id, node.ref_id)
				if (page.ref_id == node.ref_id) {
					history.splice(index + 1);
				}
				// console.log(history)
			});
		}

		// this.setState({ containerHistory: history });
	}
	submitRegistration() {
		// console.log(this.state.graphData)
		var formData = new FormData();

		formData.append("name", this.state.registeredName);
		formData.append("width_size", this.state.mapPicture.width);
		formData.append("height_size", this.state.mapPicture.height);

		if (this.iconFile == "") {
			// formData.append("icon", undefined)
		} else {
			formData.append("icon", this.iconFile);
		}
		if (this.backgroundMapFile == "") {
			// formData.append("bg", undefined )
		} else {
			formData.append("bg", this.backgroundMapFile);
		}
		var formatedElement = this.submitFormat();
		if (!this.state.graphData.nodes) {
			formData.append("element", undefined);
		} else {
			// console.log("yut", JSON.stringify(formatedElement))
			formData.append("element", JSON.stringify(formatedElement));
		}

		for (var pair of formData.entries()) {
			// console.log(pair[0] + ', ' + pair[1]);
		}

		// re-fetch template list
		if (!this.state.editingTemplate) {
			TopologyService.create_container(formData, this.token).then(
				(result) => {
					if (result.status == 1) {
						InformUser.success({ text: result.msg });
						this.getData();
					} else {
						InformUser.unsuccess({ text: result.msg });
					}
				}
			);
		} else {
			TopologyService.update_container(
				formData,
				this.state.editingTemplate.id,
				this.token
			).then((result) => {
				if (result.status == 1) {
					InformUser.success({ text: result.msg });
					this.getData();
				} else {
					InformUser.unsuccess({ text: result.msg });
				}
			});
		}
	}

	submitFormat() {
		// console.log(this.state.graphData.nodes)
		if (this.state.graphData.nodes) {
			var rawData = JSON.parse(
				JSON.stringify(this.state.graphData.nodes)
			);
			var formatArr = [];
			// console.log(rawData)
			rawData.map((element) => {
				var temp = {
					id: element.data.elm_id,
					x_axist: element.position.x,
					y_axist: element.position.y,
					width_size: element.style.width,
					height_size: element.style.height,
				};
				formatArr.push(temp);
			});
			// console.log("format ", formatArr);
			return formatArr;
		}
	}
	handleTextInput = (event) => {
		this.setState({
			[event.target.name]: event.target.value,
		});
	};
	registerTemplate() {
		InformUser.success({
			text: "Please enter the Container information and upload background map.",
		});
		this.iconFile = "";
		this.setState(
			{
				registeredName: "",
				registeredDescription: "",
				registeredIcon: "",
				editingTemplate: "",
				graphData: "",
				mapPicture: {
					url: "",
					height: "",
					width: "",
				},
			},
			() => {
				this.renderCytoscapeElement();
			}
		);
	}
	async resetTopology() {
		const dialog = await openConfirmation({
			title: "Reset Topology",
			text: "All current setting will be cleared. Are you sure you want to reset the topology?",
		});
		if (dialog) {
			this.setState({ loading: true }, async () => {
				await TopologyService.reset_topology(this.token).then(
					async (result) => {
						// console.log(result)
						if (result.status === 1) {
							// console.log('yut')
							// InformUser
							await this.clearAllNodes();
						} else {
							InformUser.unsuccess({ text: result.msg });
						}
						// console.log(this.cy.nodes(':selected'))
					}
				);
				this.setState({ loading: false });
			});
		}
	}
	async clearAllNodes() {
		// console.log('yutyu')
		await this.cy.nodes().map((node) => {
			this.cy.remove(node.id());
		});
	}
	smoothScroll() {
		// console.log('hjfdg');
		var rightBox = document.getElementById("rightBox");
		rightBox.scrollIntoView({ behavior: "smooth" });
		// rightBox.animate({
		//     scrollTop : document.getElementById('cyHeader').offsetTop - document.getElementById('rightBox').offsetTop
		// }, {
		//     duration: 1000
		// })
		// rightBox.scrollTop = document.getElementById('cyHeader').offsetTop - document.getElementById('rightBox').offsetTop;
	}
	sliderInputHandler = (event) => {
		var size = event.target.value;
		this.setState({
			[event.target.name]: size,
		});
		var originalSize = this.state.originalSize;
		this.cy.nodes(":selected").map((node, index) => {
			// console.log(originalSize)
			var finalSize =
				(this.originalSizeForAdjusting[index] * size) / 100 + "px";
			node.style({
				width: finalSize,
				height: finalSize,
			});
		});
	};
	updateScroll() {
		this.setState({ isScrollDown: !this.state.isScrollDown });
	}
	cancelEdittingTopology() {
		this.setState({ loading: true }, async () => {
			await TopologyService.cancel_topology(this.token).then(
				async (result) => {
					// console.log(result)
					if (result.status == 1) {
						// const dialog = await onUserResponse({ text: result.msg })
						this.setState(
							{
								loading: false,
								properLeavingPage: true,
							},
							() => {
								this.setState({
									navigatesToTopology: true,
									allowToLeave: true,
								});
							}
						);
					} else {
						InformUser.unsuccess({ text: result.msg });
					}
				}
			);
		});
	}

	openInterconnection() {
		this.updatePosition();
		if (
			!this.state.interconnectionOrigin ||
			!this.state.interconnectionTarget
		) {
			InformUser.unsuccess({
				text: "Please select the origin and target system through right-click context menu.",
			});
		} 
		// else if(this.state.interconnectionOrigin.model_id != this.state.interconnectionTarget.model_id) {
		// 	InformUser.unsuccess({
		// 		text: "System is not allowed to create interconnections accross XSOS model.",
		// 	});
		// } 
		else {
			// console.log(this.state.interconnectionOrigin, this.state.interconnectionTarget)
			this.setState({ loading: true }, async () => {
				await this.getAddedInterconnection(
					this.state.interconnectionOrigin.id,
					this.state.interconnectionTarget.id,
					this.token
				);
				// console.log(this.state.interconnectionOrigin)
				await this.getPortOrigin(
					this.state.interconnectionOrigin.unit_id,
					this.token
				);
				await this.getPortTarget(
					this.state.interconnectionTarget.unit_id,
					this.token
				);
				this.setState({ loading: false }, () => {
					this.setState({ openInterconnection: true });
				});
			});
		}
	}

	async getAddedInterconnection(unit_id1, unit_id2, token) {
		await TopologyService.get_addedInterconnection(
			unit_id1,
			unit_id2,
			token
		).then((result) => {
			// console.log("added interconnect", result)
			if (result.status == 1) {
				if (result.data) {
					this.setState({ addedInterconnection: result.data });
					// this.addedInterconnection = result.data;
				} else {
					// console.log(result)
				}
			}
		});
	}

	async getPortOrigin(unit_id, token) {
		await UnitService.get_port(unit_id, token).then((result) => {
			// console.log("OriginPort", result)
			if (result.status == 1) {
				this.originPortData = result.data;
				// this.setState({ originPortData: result.data })
			} else {
				InformUser.unsuccess({ text: result.msg });
			}
		});
	}
	async getPortTarget(unit_id, token) {
		// console.log(unit_id)
		await UnitService.get_port(unit_id, token).then((result) => {
			// console.log("TargetPort", result)
			if (result.status == 1) {
				this.targetPortData = result.data;
				// this.setState({ originPortData: result.data })
			} else {
				InformUser.unsuccess({ text: result.msg });
			}
		});
		// await UnitService.get_port(unit_id, token).then( result => {
		//     console.log(result)
		//     if( result.status == 1){
		//         this.targetPortData = result.data;
		//         // this.setState({ targetPortData: result.data })
		//     }else{
		//         InformUser.unsuccess({ text: result.msg })
		//     }
		// })
	}

	unselect(isSource) {
		if (isSource) {
			this.setState({ interconnectionOrigin: "" });
		} else {
			this.setState({ interconnectionTarget: "" });
		}
	}
	closeUnitDashboard = (detailOf) => {
		// console.log(detailOf)
		// if(detailOf.idx){

		// }
		this.setState({ unitDashboardOpen: false });
	};
	tooltipDescription(unit) {
		// console.log(unit)
		var description = `Name: ${unit.name}
        <br/>Model: ${unit.model_name}
        <br/>Model: ${unit.ip_address}
        <br/>MAC Address: ${unit.mac_address}
        
        
        `;
		// if (port.reserved === 1) {
		//     description = description + `<br/>Model: ${port.reserved_change_by}`;
		// }
		// if (port.port_operation_id === -1 && (port.connected_task_desc)) {
		//     if (port.connected_task_desc) {
		//         description = description + `<br/>Connected to ${port.connected_name}<br/>Task: ${port.connected_task_desc}`;

		//     } else {
		//         description = description + `<br/>Connected to ${port.connected_name}`;

		//     }
		// }

		// description = description + ``;
		// description = description + `<br/>Connected Count: ${port.connected_count}`;
		return description;
	}

	openManual(video) {
		if (video === "DROP_UNIT") {
			this.videoTitle = "Drop Unit into Topology Map.";
			this.videoSrc = "/xen/resources/video/how_to_drop_unit.mp4";
		}
		if (video === "OPEN_INTERCONNECTION") {
			this.videoTitle = "Open Panel to Create Interconnection";
			this.videoSrc =
				"/xen/resources/video/open_interconnection_panel.mp4";
		}
		this.setState({ openDropUnitVideo: true });
	}

	closeVideo = () => {
		this.setState({ openDropUnitVideo: false });
	};

	render() {
		const renderTree = (nodes) => (
			<div className="tree-node">
				<button
					id={"delete-" + nodes.id}
					onClick={() => {
						this.deleteTemplate(nodes, false);
					}}
					className="delete-btn"
				>
					x
				</button>
				<TreeItem
					key={nodes.id}
					id={nodes.id}
					nodeId={nodes.id}
					label={nodes.name}
					onLabelClick={(event) => {
						this.onTreeClicked(event, nodes);
					}}
				>
					{Array.isArray(nodes.children)
						? nodes.children.map((node) => renderTree(node))
						: null}
				</TreeItem>
			</div>
		);

		if (this.state.navigatesToTopology) {
			return (
				<Redirect
					push
					to={{
						pathname: "/topology",
						// state: { editingGraphData: this.state.graphData }
					}}
				/>
			);
		}

		// const openInterconnectionButton = () => {
		//     if (this.state.interconnectionOrigin && this.state.interconnectionTarget) {
		//         if (this.state.choseSelfConnect) {
		//             return (<button className="btn submit" onClick={this.openInterconnection.bind(this)}>Open Self Connection</button>)
		//         } else {
		//             return (<button className="btn submit" onClick={this.openInterconnection.bind(this)}>Open Interconnection</button>)
		//         }
		//     } else {
		//         return (<button className="btn cancel" >Select Source/Target Unit</button>)
		//     }
		// }
		return (
			<div className="map-management">
				<Loading open={this.state.loading} />

				<Modal
					isOpen={this.state.showIntercon}
					className="interconn-list-body"
					overlayClassName="modal-overlay"
					onRequestClose={this.closeInterconModal}
					// closeTimeoutMS={500}
				>
					<InterconnectionList
						source={this.sourceEdge}
						target={this.targetEdge}
						isOpenInsideEditing={true}
						close={this.closeInterconModal}
					/>
				</Modal>
				{/* <Prompt when={true} message="Are you sure?" /> */}
				<Modal
					isOpen={this.state.openInterconnection}
					className="interconnection-body"
					overlayClassName="white-overlay"
					onRequestClose={this.getInterconEdge}
					// closeTimeoutMS={500}
				>
					<ModeContainer
						interconnectionHistory={this.state.addedInterconnection}
						origin={this.state.interconnectionOrigin}
						target={this.state.interconnectionTarget}
						originPortData={this.originPortData}
						targetPortData={this.targetPortData}
						close={this.getInterconEdge}
					/>
				</Modal>
				<Modal
					isOpen={this.state.unitDashboardOpen}
					className="unit-dashboard-body"
					overlayClassName="default-overlay"
					onRequestClose={() => {
						this.closeUnitDashboard(null);
					}}
					// closeTimeoutMS={500}
				>
					<UnitDashboard
						unit={this.openingUnit}
						close={this.closeUnitDashboard}
					/>
				</Modal>
				<Modal
					isOpen={this.state.openDropUnitVideo}
					className="video-body 181818"
					overlayClassName="default-overlay"
					onRequestClose={() => {
						this.closeVideo();
					}}
					// closeTimeoutMS={500}
				>
					<Video
						videoTitle={this.videoTitle}
						videoSrc={this.videoSrc}
						close={this.closeVideo}
					/>
				</Modal>
				<header className="h1">
					<h1>TOPOLOGY: MANAGEMENT</h1>
				</header>

				<div className="map-management-box">
					<div className="left-box">
						<div className="template-box">
							<div className="header-section">
								{/* <header> */}
								<div className="lr-container">
									<div className="left">
										<h2>Registered Units</h2>
									</div>
									<div className="right">
										<ReactTooltip
											id="registeredUnitTooltip"
											place="top"
											className="noteClass defaultTooltip"
											arrowColor="#404040"
											effect="solid"
										/>
										<span
											data-for="registeredUnitTooltip"
											data-tip="Tutorial: How to drop unit into topology map."
											className="question"
											onClick={() => {
												this.openManual("DROP_UNIT");
											}}
										>
											<i class="fas fa-question-circle"></i>
										</span>
									</div>
								</div>
								{/* </header> */}
								{/* <hr /> */}
							</div>

							<div className="unit-list-box">
								{this.state.unitList && (
									<ReactTooltip
										id="unitAddTooltip"
										place="top"
										multiline="true"
										type="dark"
										effect="solid"
									/>
								)}

								{this.state.unitList &&
									this.state.unitList.map((unit, index) => (
										// <div className="unit-form" onClick={() => { this.addingNode(unit, true) }}>
										<div
											id={`reggistered-unit-${index + 1}`}
											key={`reggistered-unit-${
												index + 1
											}`}
											data-tip={this.tooltipDescription(
												unit
											)}
											data-for="unitAddTooltip"
											className={
												!this.state.addingElement
													? "unit-form"
													: this.state.addingElement
															.name === unit.name
													? "selected-to-add unit-form"
													: "unit-form"
											}
											onClick={() => {
												this.addingNode(unit, true);
											}}
										>
											<div className="image-box">
												<img
													src={
														environment.api +
														unit.img_path
													}
													alt=""
												/>
											</div>
											<div className="name-box">
												<span
													key={`reggistered-unit-name-${
														index + 1
													}`}
												>
													{unit.name}
												</span>
											</div>
										</div>
									))}
									{
                                    (this.state.unitList.length === 0 ) && <span id="no-data-table" style={{marginLeft:'0.5vw',padding: '2vw'}}>There's no unit available.</span>
                                }
							</div>

							<div className="tips-section">
								<div className="lr-container">
									<span className="tips-msg">Tip: right-click on a unit to take an action</span>
								</div>
							</div>
						</div>
						{/* <div className="unit-box">
                            <header><h2>Containers</h2></header>
                            
                            <div className="container-list-box">
                                {
                                    this.state.containerList && this.state.containerList.map(container =>
                                        <div className={!this.state.addingElement ? "container-form" : (this.state.addingElement.name === container.name ? "selected-to-add container-form" : "container-form")}
                                            onClick={() => { this.addingNode(container, false) }} >
                                            <div className="image-box">
                                                <img src={environment.api + container.img_path} alt="" />
                                            </div>
                                            <div className="name-box">
                                                <span>{container.name}</span>
                                            </div>
                                        </div>
                                    )
                                }
                            </div>
                        </div> */}
						{/* <div className="btn-ro">
                            <button onClick={() => { this.resetTopology() }} className="btn cancel reset-topology"><span>Reset Topology</span></button>
                        </div> */}
					</div>
					<div
						name="topContainer"
						id="rightBox"
						className="right-box"
					>
						<div className="map-box">
							
							<div className="tips-box">
								<h2>Interconnection Link</h2>
								<div className="color-tag ">
									<div className="color-line east-east"></div>
									<span>East to East</span>
								</div>
								<div className="color-tag ">
									<div className="color-line east-west"></div>
									<span>East to West</span>
								</div>
								<div className="color-tag ">
									<div className="color-line west-east"></div>
									<span>West to East</span>
								</div>
								<div className="color-tag ">
									<div className="color-line west-west"></div>
									<span>West to West</span>
								</div>
							</div>
							
							{/* <div className="tips-box">
								<div className="color-tag ">
									<div className="color-line east-east"></div>
									<span>East to East Interconnection Link</span>
								</div>
								<div className="color-tag ">
									<div className="color-line east-west"></div>
									<span>East to West Interconnection Link</span>
								</div>
								<div className="color-tag ">
									<div className="color-line west-east"></div>
									<span>West to East Interconnection Link</span>
								</div>
								<div className="color-tag ">
									<div className="color-line west-west"></div>
									<span>West to West Interconnection Link</span>
								</div>
							</div>
							 */}
							{this.state.mapPicture.url && (
								<div className="map-header">
									<div className="left">
										<h2 className="header-name">
											{this.state.containerHistory.map(
												(page) => (
													//  onClick={() => { this.changeContainerPage(page, false) }}
													<span>{page.name}</span>
												)
											)}{" "}
											Topology
										</h2>
										{this.state.containerHistory.length >
											1 && (
											<button
												className="btn back-btn cancel "
												onClick={() => {
													this.changeContainerPage(
														this.state
															.containerHistory[
															this.state
																.containerHistory
																.length - 2
														],
														false
													);
												}}
											>
												<i class="fas fa-backward back"></i>
											</button>
										)}
									</div>
									<div className="right">
										{this.state.resizeBox && (
											<div className="resize">
												<span className="edit-size">
													Edit size:{" "}
												</span>
												<input
													type="range"
													id="points"
													name="percentageNodeSize"
													value={
														this.state
															.percentageNodeSize
													}
													min={0}
													max={200}
													onChange={this.sliderInputHandler.bind(
														this
													)}
													step="1"
												/>

												{/* <input style={{ width: "30px" }} type="number" name="percentageNodeSize" value={this.state.percentageNodeSize} min={this.state.originalSize - 100} max={this.state.originalSize + 100} onChange={this.handleTextInput.bind(this)} step="1" /> */}

												<span className="percentage">
													{
														this.state
															.percentageNodeSize
													}
													%
												</span>
											</div>
										)}
										<button
											id="apply-btn"
											onClick={() => {
												this.confirmApplyTopology();
											}}
											className="btn submit"
											style={{ marginRight: "0.5vw" }}
										>
											<span>Apply</span>
										</button>
										<button
											id="cancel-btn"
											onClick={() => {
												this.cancelEdittingTopology();
											}}
											className="btn cancel"
										>
											<span>Cancel</span>
										</button>
									</div>
								</div>
							)}
							<div id="cy"> </div>
						</div>
					</div>
					<div className="intercon-box" id="intercon-box">
						<div className="header-section">
							<div className="lr-container">
								<div className="left">
									<h2>Interconnection</h2>
								</div>
								<div className="right">
									<ReactTooltip
										id="interconTooltip"
										place="top"
										className="noteClass defaultTooltip"
										arrowColor="#404040"
										effect="solid"
									/>
									<span
										data-for="interconTooltip"
										data-tip="tutorial: How to create interconnection link."
										className="question"
										onClick={() => {
											this.openManual(
												"OPEN_INTERCONNECTION"
											);
										}}
									>
										<i class="fas fa-question-circle"></i>
									</span>
								</div>
							</div>

							{/* <hr /> */}
						</div>
						<div className="intercon-content">
							<div className="select-box-container">
								<div className="select-header lr-container">
									
                                    <div className="left"><h3>Source Unit</h3></div>
									<div className="right" style={{position:"relative"}}>
                                    {this.state.interconnectionOrigin && <button
											id="source-remove-btn"
											onClick={() => {
												this.unselect(true);
											}}
											className="close"
										>
											<i class="fas fa-times"></i>
										</button>}
                                    </div>
									
								</div>
								{this.state.interconnectionOrigin && 
									<div className="center-box">
                                        {/* <hr /> */}
                                    
                                    <div
										id="interconnection-source"
										className="select-box"
									>
										<div className="image-box">
											<img
												src={
													environment.api +
													this.state
														.interconnectionOrigin
														.img_path
												}
												alt=""
											/>
										</div>
										<span id="source-name" className="ellipsis-text name" style={{maxWidth:"13vw"}}>
											
											{
												this.state.interconnectionOrigin
													.name
											}
										</span>
										<span id="source-model" className="model">
											
											{
												this.state.interconnectionOrigin
													.model_name
											}
										</span>
									</div>
                                    </div>
                                }
							</div>

							<div className="select-box-container">
								<div className="select-header lr-container">
                                    <div className="left"><h3>Target Unit</h3></div>
									<div className="right"  style={{position:"relative"}}>
                                    {this.state.interconnectionTarget && <button
											id="target-remove-btn"
											onClick={() => {
												this.unselect(false);
											}}
											className="close"
										>
											<i class="fas fa-times"></i>
										</button>
                                    }
                                        
                                    </div>
                                    
								</div>

								{this.state.interconnectionTarget && 
                                    <div className="center-box">
                                    {/* <hr/> */}
									<div
										id="interconnection-target"
										className="select-box"
									>
										
										<div className="image-box">
											<img
												src={
													environment.api +
													this.state
														.interconnectionTarget
														.img_path
												}
												alt=""
											/>
										</div>
										<span id="target-name"  className="ellipsis-text name" style={{maxWidth:"13vw"}}>
											{
												this.state.interconnectionTarget
													.name
											}
										</span>
										<span id="target-model" className="model">
											{
												this.state.interconnectionTarget
													.model_name
											}
										</span>
									</div>
                                    </div>
                                }
							</div>
						</div>
						<div className="btn-row intercon-btn">
							{this.showInterconnectionButton()}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default MapManagement;
