
import React, { Component } from 'react'
import './Connectivity.scss';
import cytoscape from "cytoscape";

import * as SystemService from "../../services/SystemService";
import * as InformUser from "../../shared/InformUser";
import { openConfirmation } from "../../shared/Confirmation";
import * as TopologyService from "../../services/TopologyService";

export class MultipleConnectivity extends Component {

  token = JSON.parse(localStorage.getItem('userData')).token

  constructor(props) {
    super(props);
    this.state = {
      isSelectingSourcePort: true,
      systemId: 1,
      smuType: 1,
      tableData: '',

      systemList: '',

      sourcePort: '',
      targetPort: '',

      routeData: '',
      routeNumber: [0,1,2]
    }



  }

  componentDidMount() {


    this.getSystemList();

    this.filterPort();
  }

  getSystemList() {
    SystemService.get_unit(this.token).then(result => {
      // console.log(result)
      if (result.status) {
        this.setState({ systemList: result.data }, () => {
          // console.log(this.state.systemList)
        });
      } else {
        InformUser.unsuccess({ text: result.msg });
        // console.log("getSystemList", result.data)
      }
    })
  }

  searchTableData() {

    var input, filter, table, tr, td, cell, i, j;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    if (this.state.isSelectingSourcePort) {
      table = document.getElementById("sourceTable");
    } else {
      table = document.getElementById("targetTable");
    }

    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
      // Hide the row initially.
      tr[i].style.display = "none";

      td = tr[i].getElementsByTagName("td");
      for (var j = 0; j < td.length; j++) {
        cell = tr[i].getElementsByTagName("td")[j];
        if (cell) {
          if (cell.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
            break;
          }
        }
      }
    }
  }
  selectChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  async filterPort() {
    // console.log(this.state.systemId, this.state.smuType);
    await SystemService.get_port_filtering(this.state.systemId, this.state.smuType, this.token).then(result => {
      // console.log(result)
      if (result.status) {
        this.setState({ tableData: result.data }, () => {
        })
      } else {
        InformUser.unsuccess({ text: result.msg })
      }
    });

  }

  toggleSourceTargetPort(isSource) {

    this.setState({ isSelectingSourcePort: isSource }, () => {
      this.searchTableData();
    })

  }
  selectPort(port, isSource) {
    if (isSource) {

      if (port.port_no != this.state.targetPort.port_no) {
        if (port.port_no != this.state.sourcePort.port_no) {
          this.setState({ sourcePort: port });
        } else {
          this.removeSelectedPort(true);
        }

      } else {
        InformUser.unsuccess({
          text: "The port you are choosing is already selected to be target port. Please choose another port to create connectivity."
        })
      }

    } else {
      if (port.port_no != this.state.sourcePort.port_no) {
        if (port.port_no != this.state.targetPort.port_no) {
          this.setState({ targetPort: port });
        } else {
          this.removeSelectedPort(false)
        }

      } else {
        InformUser.unsuccess({
          text: "The port you are choosing is already selected to be target port. Please choose another port to create connectivity."
        })
      }

    }
  }
  removeSelectedPort(isSource) {
    if (isSource) {
      this.setState({ sourcePort: '' });
    } else {
      this.setState({ target: '' });
    }
  }

  searchRoute() {
    // const port_no1 = this.state.sourcePort.port_no;
    // const port_no2 = this.state.targetPort.port_no;
    // TopologyService.get_route_connectivity(1, port_no1, port_no2, 1, this.token).then(result => {
    //   console.log(result)
    //   const data = {
    //     nodes: result.data.nodes,
    //     edge: result.data.edge,
    //   }
    //   this.setState({ routeData: data}, ()=> {
    //     this.renderCytoscapeElement()
    //   })
    // })
    const data = {
      nodes: [
        { data: { id: "2", name: "Unit2" } },
        { data: { id: "1", name: "Unit1" } },
        { data: { id: "4", name: "Unit4" } },
        { data: { id: "3", name: "Unit3" } },
        { data: { id: "5", name: "Unit4" } },
        { data: { id: "6", name: "Unit3" } },
        { data: { id: "7", name: "Unit4" } },
        { data: { id: "8", name: "Unit3" } },
      ],
      edges: [
        { data: { id: "1-2", source: "1", target: "2" } },
        { data: { id: "3-4", source: "3", target: "4" } },
        { data: { id: "5-6", source: "5", target: "6" } },
        { data: { id: "6-7", source: "6", target: "7" } },
        { data: { id: "7-8", source: "7", target: "8" } },
      ],
    }
    this.setState({ routeData: data }, () => {
      // console.log(this.state.routeData)
      this.renderCytoscapeElement()
    })


  }

  renderCytoscapeElement() {
    var graphStyle = cytoscape.stylesheet()
      .selector('edge')
      .css({
        'width': '2',
        // 'curve-style': 'bezier',
        // 'curve-style': 'segments',
        // 'curve-style': 'unbundled-bezier',
        // 'control-point-step-size': '100px',
        // 'line-fill': 'linear-gradient',
        'line-gradient-stop-colors': 'data(colorCode)',
        // 'line-gradient-stop-positions': ['0%', '50%', '50%', '100%'],
        "loop-direction": "data(direction)",
        // "loop-sweep": "70deg",
        // 'line-color': 'data(colorCod)',
        'label': 'data(label)',
        // 'color': 'white',
        // 'font-size': '12px',
        // 'font-family': 'helvetica, arial, verdana, sans-serif',
        // 'font-weight': 300,
        // 'text-outline-width': 2,
        // 'text-outline-color': 'black',
        // 'control-point-distance': 100,
        // 'target-arrow-shape': 'data(arrow_target)',
        // 'source-arrow-shape': 'data(arrow_source)',
        // 'target-arrow-shape': 'circle',
        // 'source-arrow-shape': 'circle',
        // 'source-arrow-color': 'data(sourceColor)',
        // 'target-arrow-color': 'data(targetColor)',
      })
      .selector(':selected')
      .css({
        'edge.opacity': 0.5,
        'width': '3',
        'color': 'white',
        'line-color': '#ffb3b3',
        // 'source-arrow-color': 'data(colorCode)',
        // 'target-arrow-color': 'data(colorCode)',
      })

      .selector('node')
      .css({
        'shape': 'rectangle',
        'background-clip': 'none',
        // 'background-width':'100%',
        // 'background-height':'auto',
        // 'background-fit':'node',
        'background-fit': 'contain',
        'background-opacity': '0',

        // 'width': '60',
        // 'height': '55',
        'content': 'data(name)',
        'text-valign': 'center',
        'text-outline-width': 1.5,
        'text-outline-color': '#e9e9e9',

        // 'background-image': 'url("assets/Robot_img_50.png")',
        // 'object-fit': 'cover',
        // 'background-fit':'contain',
        // 'background-repeat': 'no-repeat',
        // 'background-position': 'center center',
        'color': '#31081f',
        'font-size': '12',


      })
      .selector(':selected')
      .style({
        // 'label': 'data(label)',
        // 'label': '',
        // 'color': 'black',
        // 'text-outline-width': 2,
        // 'text-outline-color': '#e9e9e9',
        // 'opacity': 1,
        // 'font-size': 18,
        // 'overlay-color': 'blue',
        // 'overlay-opacity': '0.5',
        // 'overlay-padding': '5',

        // 'shape': 'roundrectangle',
        'width': '60',
        'height': '55',
        'content': 'data(name)',
        'text-valign': 'center',
        'text-outline-width': 0.5,
        'text-outline-color': '#dce0d9',
        'background-color': '#1d2b36',
        // 'background-image': 'url("assets/Robot_img_50.png")',
        // 'object-fit': 'cover',
        // 'background-repeat': 'no-repeat',
        // 'background-position': 'center center',
        'color': '#31081f',
        'font-size': 14,
        'font-weight': '600',
        'border-width': '0',
        // 'border-color': 'white',
        // 'selection-box-color': 'white',
        // 'selection-box-border-color': 'white',
        // 'border-width': 1,
        // 'border-color': 'black',

      })
      // .selector(':active').style({
      //     // 'overlay-color': "red",
      // })
      .selector('.locked').style({
        'opacity': '0.5'
      }).selector('.select-intercon').style({

      }).selector('.isNotSelected').style({
        'opacity': '0.6'
      })



    let cy = cytoscape({
      container: document.getElementById('cy'),
      style: graphStyle,
      zoom: 2,
      minZoom: 0.1,
      maxZoom: 2,
      // wheelSensitivity: 0.2,
      elements: this.state.routeData,
      // zoomingEnabled: false,
      userZoomingEnabled: false,
      // panningEnabled: false,
      userPanningEnabled: false,
      boxSelectionEnabled: false,
      autoungrabify: true,
      autounselectify: true,
      // layout: {
      //   name: 'grid',
        
      // }
    });
  }


  render() {
    return (
      <div className="connectivity">
        <header>
          <h1>Connectivity: Shortest Route</h1>
        </header>
        <div className="connectivity-layout">
          <div className="system-section">
            <h2>Create Connection</h2>

            <div className="search-section">
              <div className="input-group">
                <div className="left">
                  <span>System:</span>
                </div>
                <div className="right">
                  <select
                    id="system-id"
                    name="systemId"
                    onChange={this.selectChange.bind(this)}
                    value={this.state.systemId}
                  >
                    {
                      this.state.systemList && this.state.systemList.map(unit =>
                        <option value={unit.id}> {unit.name}</option>

                      )
                    }
                  </select>
                </div>

              </div>
              <div className="input-group">
                <div className="left">
                  <span>SMU type:</span>
                </div>
                <div className="right">
                  <select
                    id="smu-type"
                    name="smuType"
                    onChange={this.selectChange.bind(this)}
                    value={this.state.smuType}
                  >
                    <option value="null">Both</option>
                    <option value="0">Connector</option>
                    <option value="1">Adapter</option>
                  </select>
                </div>

              </div>
              <div className="btn-row">
                <button
                  onClick={this.filterPort.bind(this)}
                  className="submit btn">Apply</button>
              </div>


            </div>

            <div className="toggle-tab">
              <div
                className={this.state.isSelectingSourcePort == true ? "tab tab-selecting" : "tab"}
                onClick={() => { this.toggleSourceTargetPort(true) }}
              >
                <span>Source Port:</span>
                {
                  this.state.sourcePort && <button onClick={() => { this.removeSelectedPort(true) }} className="btn selected-port">Port-{this.state.sourcePort.port_no}</button>
                }
              </div>
              <div
                className={this.state.isSelectingSourcePort == false ? "tab tab-selecting" : "tab"}
                onClick={() => { this.toggleSourceTargetPort(false) }}
              >
                <span >Target Port:</span>
                {
                  this.state.targetPort && <button onClick={() => { this.removeSelectedPort(false) }} className="btn selected-port">Port-{this.state.targetPort.port_no}</button>
                }
              </div>
            </div>

            <div className="search-group">
              <span className="margin-right">Search:</span>
              <input type="text" id="myInput" onKeyUp={() => { this.searchTableData() }} placeholder="Search for names.." title="Type in a name" />
            </div>
            <div className="port-section">
              {
                this.state.isSelectingSourcePort && <div className="search-table">

                  <table id="sourceTable">
                    <tr >
                      <th > <span>Port No.</span> </th>
                      <th > <span>SMU Type</span> </th>
                      <th > <span>Description</span> </th>
                    </tr>
                    {
                      this.state.tableData && this.state.tableData.map(port =>
                        <tr
                          onClick={() => { this.selectPort(port, true) }}
                          className={this.state.sourcePort.port_no == port.port_no ? "selected" : ""}
                        >
                          <td className="text-center" ><span>{port.port_no}</span> </td>
                          <td className="text-center" ><span>{port.smu_type_desc}</span> </td>
                          <td className="text-center" ><span>{port.description}</span> </td>
                        </tr>
                      )
                    }

                  </table>
                </div>
              }

              {
                !this.state.isSelectingSourcePort && <div className="search-table">

                  <table id="targetTable">
                    <tr >
                      <th > <span>Port No.</span> </th>
                      <th > <span>SMU Type</span> </th>
                      <th > <span>Description</span> </th>
                    </tr>
                    {
                      this.state.tableData && this.state.tableData.map(port =>
                        <tr
                          onClick={() => { this.selectPort(port, false) }}
                          className={this.state.targetPort.port_no == port.port_no ? "selected" : ""}
                        >
                          <td className="text-center" ><span>{port.port_no}</span> </td>
                          <td className="text-center" ><span>{port.smu_type_desc}</span> </td>
                          <td className="text-center" ><span>{port.description}</span> </td>
                        </tr>
                      )
                    }

                  </table>
                </div>
              }

            </div>
            <div className="btn-row search-route-btn">
              <button
                className="btn submit"
                onClick={this.searchRoute.bind(this)}
              >Search Route</button>
            </div>
          </div>
          <div className="connectivity-content">
            <div className="path-section">
              <h2>Shortest Route</h2>
              <div className="cyto-container">
                {/* {
                  this.state.routeNumber.map( route => 
                    
                    )
                } */}
                <div id="cy" > </div>
                
              </div>
            </div>
            <div className="remaining-task">

            </div>
          </div>
        </div>
      </div>


    )
  }
}

export default MultipleConnectivity
