<div className="system-section">

<h2 className="shift-down">Create Connection</h2>
<div className="duo-select-container">
  <div className="left-container">
    <div className="search-section">
      <div className="input-group">
        <div className="left">
          <span>System:</span>
        </div>
        <div className="right">
          <select id="system-id" name="sourceSystemId" onChange={(event) => { this.selectChange(event, true) }} value={this.state.sourceSystemId} >
            {
              this.state.systemList && this.state.systemList.map(unit =>
                <option value={unit.id}> {unit.name}, {unit.model}</option>

              )
            }
          </select>
        </div>

      </div>
      <div className="input-group">
        <div className="left">
          <span>SMU type:</span>
        </div>
        <div className="right">

          <input className="radio-btn" type="radio" id="connector-source" name="smuTypeSource" value="0" checked={this.state.smuTypeSource === "0"}
            onChange={(event) => { this.selectChange(event, true) }} />
          <label className="margin-right-20" htmlFor="connector-source"><span>Connector</span></label>
          <input className="radio-btn" type="radio" id="adapter-source" name="smuTypeSource" value="1" checked={this.state.smuTypeSource === "1"}
            onChange={(event) => { this.selectChange(event, true) }} />
          <label htmlFor="adapter-source"><span>Adapter</span></label>
        </div>
      </div>
    </div>
  </div>
  <div className="right-container">
    <div className="search-section">
      <div className="input-group">
        <div className="left">
          <span>System:</span>
        </div>
        <div className="right" style={{ position: "relative" }}>
          <select
            id="system-id"
            name="targetSystemId"
            onChange={(event) => { this.selectChange(event, false) }}
            value={this.state.targetSystemId}
          >
            {
              this.state.systemList && this.state.systemList.map(unit =>
                <option value={unit.id}> {unit.name}, {unit.model}</option>
              )
            }
          </select>
          <button className="front-panel-view fit-btn submit" style={{ padding: "5px 5px" }} onClick={() => { this.openPanelView() }}><i class="fas fa-network-wired"></i></button>
        </div>

      </div>
      <div className="input-group">
        <div className="left">
          <span>SMU type:</span>
        </div>
        <div className="right">

          <input className="radio-btn" type="radio" id="connector" name="smuTypeTarget" value="0" checked={this.state.smuTypeTarget === "0"}
            onChange={(event) => { this.selectChange(event, false) }} />
          <label className="margin-right-20" for="connector"><span>Connector</span></label>
          <input className="radio-btn" type="radio" id="adapter" name="smuTypeTarget" value="1" checked={this.state.smuTypeTarget === "1"}
            onChange={(event) => { this.selectChange(event, false) }} />
          <label for="connector"><span>Adapter</span></label>
        </div>

      </div>

   


    </div>
  </div>



</div>

<div className="search-group">
  <span className="margin-right">Search:</span>
  <input type="text" id="myInput" onKeyUp={() => { this.searchSourceTable(); this.searchTargetTable() }} placeholder="Search for names.." title="Type in a name" />
</div>
<div className="toggle-tab">
  <div className="tab" >
    <span>Source Port:</span>
    {
      this.state.sourcePort && <button onClick={() => { this.removeSelectedPort(true) }} className="btn selected-port">Port-{this.state.sourcePort.port_no}</button>
    }


  </div>
  <div className="tab" >
    <span >Target Port:</span>
    {
      this.state.targetPort && <button onClick={() => { this.removeSelectedPort(false) }} className="btn selected-port">Port-{this.state.targetPort.port_no}</button>
    }
  </div>
</div>


<div className="duo-select-container">
  <div className="left-container">
    <div className="port-section">
      {
        !this.state.isSelectingFiber && <div className="search-table">

          <table id="sourceTable">
            <tr className="tr-head">
              <th > <span>Port No.</span> </th>
              {/* <th > <span>SMU Type</span> </th> */}
              <th > <span>Description</span>

              </th>
            </tr>
            {
              this.state.tableSourceData && this.state.tableSourceData.map(port =>
                <tr
                  onClick={() => { this.selectPort(port, true) }}
                  className={this.state.sourcePort.port_no == port.port_no ? "selected" : (this.state.targetPort.port_no == port.port_no ? "selected" : "")}

                >
                  <td className="text-center" ><span>{port.port_no}</span> </td>
                  {/* <td className="text-center" ><span>{port.smu_type_desc}</span> </td> */}
                  <td className="text-center" ><span>{port.description}</span> </td>
                </tr>
              )
            }

          </table>
        </div>
      }
      {
        this.state.isSelectingFiber && <div className="fiber-selection">
          {
            this.state.fiberSourceList.map(fiber =>
              // <div className="fiber-select-box">

              <div id={`fiber-${fiber.fiber_no}`} className={fiber.status == 0 ? "recommended fiber-box" : "fiber-box"}  >
                <input className="fiber-checkbox" type="checkbox" />
                <h2>{fiber.fiber_no}</h2>
                <span className="fiber-box-header">{fiber.port_no}-{fiber.port_fiber_no}</span>
              </div>

              // </div>


            )
          }
        </div>
      }

    </div>


  </div>
  <div className="right-container">

    <div className="port-section">
      {
        !this.state.isSelectingFiber && <div className="search-table">

          <table id="targetTable">
            <tr className="tr-head">
              <th > <span>Port No.</span> </th>
              {/* <th > <span>SMU Type</span> </th> */}
              <th > <span>Description</span> </th>
            </tr>
            {
              this.state.tableTargetData && this.state.tableTargetData.map(port =>
                <tr
                  onClick={() => { this.selectPort(port, false) }}
                  className={this.state.sourcePort.port_no == port.port_no ? "selected" : (this.state.targetPort.port_no == port.port_no ? "selected" : "")}

                >
                  <td className="text-center" ><span>{port.port_no}</span> </td>
                  {/* <td className="text-center" ><span>{port.smu_type_desc}</span> </td> */}
                  <td className="text-center" ><span>{port.description}</span> </td>
                </tr>
              )
            }
          </table>
        </div>
      }
      {
        this.state.isSelectingFiber && <div className="fiber-selection">
          {
            this.state.fiberTargetList.map(fiber =>
              <div id={`fiber-${fiber.fiber_no}`}
                className={fiber.status != 0 ? "fiber-box" : "recommended fiber-box"}  >
                <h2>{fiber.fiber_no}</h2>
                <span className="fiber-box-header">{fiber.port_no}-{fiber.port_fiber_no}</span>
              </div>
            )
          }
        </div>
      }
    </div>
  </div>
</div>
<div className="btn-row search-route-btn">
  <button className="btn submit" onClick={this.searchRoute.bind(this)} >Search Route</button>
</div>

</div>



