
import React, { Component } from 'react'
import './Connectivity.scss';
import cytoscape from "cytoscape";
import Modal from 'react-modal';

import { Loading } from "./../../shared/Loading";


import * as SystemService from "./../../services/SystemService";
import * as InformUser from "../../shared/InformUser";
import { openConfirmation } from "./../../shared/Confirmation";
import * as TaskService from "../../services/TaskService";

export class Connectivity extends Component {

  token = JSON.parse(localStorage.getItem('userData')).token;
  cytoRoute;
  cy;
  graphStyle = cytoscape.stylesheet()
    .selector('edge')
    .css({
      'width': '2',
      "curve-style": "bezier",
      // "control-point-distances": [40, -40],
      // "control-point-weights": [0.250, 0.75],
      // "haystack-radius": 0,
      "control-point-step-size": 100,
      // "source-distance-from-node": "100px",
      // "line-cap": "round",
      // 'source-endpoint': '180deg',
      // 'target-endpoint': '0deg',
      // 'curve-style': 'segments',
      // 'control-point-step-size': '200',
      // "edge-distances": "100",
      'line-fill': 'linear-gradient',
      'line-gradient-stop-colors': 'data(colorCode)',
      'line-gradient-stop-positions': ['0%', '50%', '50%', '100%'],
      "loop-direction": 'data(loopDirection)',
      // "loop-sweep": "90deg",
      // 'line-color': 'data(colorCode)',
      'text-margin-x': 'data(shiftLabel)',
      'label': 'data(label)',
      "text-background-opacity": 1,
      // "color": "#000000",
      "text-background-color": "#b3b3b3",
      // "text-background-shape": "roundrectangle",
      // "text-border-color": "#000",
      "text-border-width": 0,
      // "text-border-opacity": 1,
      // "text-background-padding": "2px 5px",
      'color': '#EEEEEE',
      "text-background-color": "#282828",
      "text-background-opacity": "0.8",
      "text-background-shape": "roundrectangle",
      "text-background-padding": "5px 10px 5px 10px",



      // 'color': 'white',
      'font-size': '20px',
      // 'font-family': 'helvetica, arial, verdana, sans-serif',
      // 'font-weight': 300,
      // 'text-outline-width': 2,
      // 'text-outline-color': 'black',
      // 'control-point-distance': 100,
      'target-arrow-shape': 'data(arrow_target)',
      'source-arrow-shape': 'data(arrow_source)',
      'arrow-scale': '2',
      // 'target-arrow-shape': 'circle',
      // 'source-arrow-shape': 'circle',
      'source-arrow-color': 'data(sourceColor)',
      'target-arrow-color': 'data(targetColor)',
    })
    .selector(':selected')
    .css({
      'edge.opacity': 0.5,
      'width': '3',
      'color': 'white',
      'line-color': '#ffb3b3',
      // 'source-arrow-color': 'data(colorCode)',
      // 'target-arrow-color': 'data(colorCode)',
    })

    .selector('node')
    .css({
      'shape': 'rectangle',
      'background-clip': 'none',
      // 'background-width':'100%',
      // 'background-height':'auto',
      // 'background-fit':'node',
      'background-fit': 'contain',
      'background-opacity': '0',

      // 'width': '60',
      // 'height': '55',
      'content': 'data(name)',
      'text-valign': 'center',
      // 'text-outline-width': 1.5,
      // 'text-outline-color': '#EEEEEE',

      'color': '#EEEEEE',
      'font-size': '20px',
      "text-background-color": "#282828",
      "text-background-opacity": "0.8",
      "text-background-shape": "roundrectangle",
      "text-background-padding": "5px 10px 5px 10px",

      // 'background-image': 'url("assets/Robot_img_50.png")',
      // 'object-fit': 'cover',
      // 'background-fit':'contain',
      // 'background-repeat': 'no-repeat',
      // 'background-position': 'center center',
      // 'color': '#161616',
      // 'font-size': '1px',
      'font-weight': '600'


    })
    .selector(':selected')
    .style({
      // 'label': 'data(label)',
      // 'label': '',
      // 'color': 'black',
      // 'text-outline-width': 2,
      // 'text-outline-color': '#e9e9e9',
      // 'opacity': 1,
      // 'font-size': 18,
      // 'overlay-color': 'blue',
      // 'overlay-opacity': '0.5',
      // 'overlay-padding': '5',

      // 'shape': 'roundrectangle',
      // 'width': '60',
      // 'height': '55',
      'content': 'data(name)',
      'text-valign': 'center',
      'text-outline-width': 0.5,
      'text-outline-color': '#dce0d9',
      'background-color': '#1d2b36',
      // 'background-image': 'url("assets/Robot_img_50.png")',
      // 'object-fit': 'cover',
      // 'background-repeat': 'no-repeat',
      // 'background-position': 'center center',
      'color': '#31081f',
      'font-size': 14,
      'font-weight': '600',
      'border-width': '0',
      // 'border-color': 'white',
      // 'selection-box-color': 'white',
      // 'selection-box-border-color': 'white',
      // 'border-width': 1,
      // 'border-color': 'black',

    }).selector('.locked').style({
      'opacity': '0.5'
    }).selector(':parent').style({
      'border-width': '0',
      // 'overlay'

    })
  // graphStyle = cytoscape.stylesheet()
  //   .selector('edge')
  //   .css({
  //     'width': '1',
  //     // 'curve-style': 'bezier',
  //     // 'curve-style': 'segments',
  //     // 'curve-style': 'unbundled-bezier',
  //     // 'control-point-step-size': '100px',
  //     // 'line-fill': 'linear-gradient',
  //     // 'line-gradient-stop-colors': 'data(colorCode)',
  //     // 'line-gradient-stop-positions': ['0%', '50%', '50%', '100%'],
  //     // "loop-direction": "data(direction)",
  //     // "loop-sweep": "70deg",
  //     // 'line-color': 'data(colorCod)',
  //     // 'name': 'data(name)',
  //     'line-color': '#282828',
  //     // 'font-size': '12px',
  //     // 'font-family': 'helvetica, arial, verdana, sans-serif',
  //     // 'font-weight': 300,
  //     // 'text-outline-width': 2,
  //     // 'text-outline-color': 'black',
  //     // 'control-point-distance': 100,
  //     // 'target-arrow-shape': 'data(arrow_target)',
  //     // 'source-arrow-shape': 'data(arrow_source)',
  //     // 'target-arrow-shape': 'circle',
  //     // 'source-arrow-shape': 'circle',
  //     // 'source-arrow-color': 'data(sourceColor)',
  //     // 'target-arrow-color': 'data(targetColor)',
  //   })

  //   .selector('node') .css({
  //     'shape': 'circle',
  //     'width': '40',
  //     'height': '40',
  //     'background-color': '#282828',

  //     // 'background-clip': 'none',
  //     // 'background-fit': 'contain',
  //     // 'background-opacity': '0',



  //     'content': 'data(name)',
  //     'text-valign': 'center',
  //     'text-outline-width': 1,
  //     'text-outline-color': '#282828',
  //     'color': '#B3B3B3',
  //     'font-size': '12',
  //   })
  //   .selector(':parent') .style({
  //     'content': '',
  //     "text-valign": "top",
  //     "text-halign": "center",
  //     'border': '2px solid',
  //     'border-color': '#282828',
  //     'shape': 'roundrectangle',
  //     'text-outline-width': 1.5,
  //     'text-outline-color': '#282828',
  //     'color': '#B3B3B3',
  //     // 'background-color': 'red',
  //     'background-opacity': '0',

  //   })
  //   .selector('.hover').style({
  //     'text-outline-width': 2.5,
  //     'text-outline-color': '#121212',
  //     'color': '#B3B3B3',
  //     // 'background-color': '#1d2b36',
  //     // 'background-image': 'url("assets/Robot_img_50.png")',
  //     // 'object-fit': 'cover',
  //     // 'background-repeat': 'no-repeat',
  //     // 'background-position': 'center center',

  //     // 'font-size': 14,
  //     'font-weight': '600',
  //     // 'border-width': '0',
  //     'border-color': '#121212'
  //   });

  constructor(props) {
    super(props);
    this.state = {
      taskList: '',
      isSelectingSourcePort: true,
      sourceSystemId: 1,
      targetSystemId: 2,
      smuTypeSource: "0",
      smuTypeTarget: "1",

      tableSourceData: '',
      tableTargetData: '',
      sourceRow: '',
      targetRow: '',

      systemList: '',

      sourcePort: '',
      targetPort: '',

      remarkTextarea: '',

      routeData: '',
      horizontalLayout: '',
      loading: false,

      isSelectingFiber: false,
      fiberSourceList: '',
      fiberTargetList: '',

      displayingRoute: '',
      sourceFilterStatus: '',
      targetFilterStatus: '',
      panelView: false,
      showRouteOption: false,
    }



  }

  componentWillMount() {
    Modal.setAppElement('body');
    this.setState({ loading: true }, async () => {
      await this.searchRoute()

      this.setState({ loading: false })
    })

  }
  componentDidMount() {
    // this.setState({ loading: true }, async () => {
    //   // await this.getSystemList();
    //   // await this.getTask();
    //   // await this.sourceFilterPort();
    //   // await this.targetFilterPort();

    //   // document.getElementById('myInput').focus()
    //   this.setState({ loading: false, panelView: true })
    // });


  }

  async getTask() {
    await TaskService.get_tasks(this.token).then(result => {
      // console.log(result);
      if (result.status == 1) {
        this.setState({ taskList: result.data });
      } else {
        InformUser.unsuccess({ text: result.msg });
      }
    })
  }

  async getSystemList() {
    await SystemService.get_unit(this.token).then(result => {
      // console.log(result)
      if (result.status == 1) {
        this.setState({ systemList: result.data }, () => {
          // console.log(this.state.systemList)
        });
      } else {
        InformUser.unsuccess({ text: result.msg });
        // console.log("getSystemList", result.data)
      }
    })
  }

  searchSourceTable() {

    var input, filter, table, tr, td, cell, i, j;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    // if (this.state.isSelectingSourcePort) {
    table = document.getElementById("sourceTable");
    // } else {
    // table = document.getElementById("targetTable");
    // }

    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
      // Hide the row initially.
      tr[i].style.display = "none";

      td = tr[i].getElementsByTagName("td");
      for (var j = 0; j < td.length; j++) {
        cell = tr[i].getElementsByTagName("td")[j];
        if (cell) {
          if (cell.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
            break;
          }
        }
      }
    }
  }
  searchTargetTable() {

    var input, filter, table, tr, td, cell, i, j;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    // if (this.state.isSelectingSourcePort) {
    // table = document.getElementById("sourceTable");
    // } else {
    table = document.getElementById("targetTable");
    // }

    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
      // Hide the row initially.
      tr[i].style.display = "none";

      td = tr[i].getElementsByTagName("td");
      for (var j = 0; j < td.length; j++) {
        cell = tr[i].getElementsByTagName("td")[j];
        if (cell) {
          if (cell.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
            break;
          }
        }
      }
    }
  }
  selectChange = (event, isFromSource) => {
    this.setState({ [event.target.name]: event.target.value }, () => {
      if (isFromSource) {
        this.sourceFilterPort()
      } else {
        this.targetFilterPort()
      }

    });
  }


  async sourceFilterPort() {
    // console.log(this.state.sourceSystemId, this.state.smuType);
    await SystemService.get_port_filtering(this.state.sourceSystemId, this.state.smuTypeSource, this.token).then(result => {
      // console.log(result)
      if (result.status == 1) {
        this.setState({
          tableSourceData: result.data,
          sourceFilterStatus: this.filteringStatus(this.state.sourceSystemId, this.state.smuTypeSource)
        }, () => {
        })
      } else {
        InformUser.unsuccess({ text: result.msg })
      }
    });

  }

  filteringStatus() {
    if (this.state.sourceSystemId) {

    }
  }


  async targetFilterPort() {
    // console.log(this.state.targetSystemId, this.state.smuType);
    await SystemService.get_port_filtering(this.state.targetSystemId, this.state.smuTypeTarget, this.token).then(result => {
      // console.log(result)
      if (result.status == 1) {
        this.setState({ tableTargetData: result.data }, () => {
        })
      } else {
        InformUser.unsuccess({ text: result.msg })
      }
    });

  }

  selectPort(port, isFromSource) {
    // console.log(port)
    const source = this.state.sourcePort;
    const target = this.state.targetPort;

    if (isFromSource) {
      if (source.port_no !== port.port_no) {
        this.setState({ sourcePort: port });
      } else {
        this.removeSelectedPort(true)
      }

    } else {
      if (target.port_no !== port.port_no) {
        this.setState({ targetPort: port });
      } else {
        this.removeSelectedPort(false)
      }
    }


    // if (port.port_no == target.port_no) {
    //   this.removeSelectedPort(false)
    // } else {
    //   if (!source) {
    //     this.setState({ sourcePort: port });
    //   } else {
    //     if (port.port_no == source.port_no) {
    //       this.removeSelectedPort(true);
    //     } else {
    //       this.setState({ targetPort: port });
    //     }
    //     if (port.port_no == target.port_no) {
    //       this.removeSelectedPort(false);
    //     }

    //   }
    // }

















    // if (port.port_no != this.state.targetPort.port_no) {
    //   if (port.port_no != this.state.sourcePort.port_no) {
    //     this.setState({ sourcePort: port });
    //   } else {
    //     this.removeSelectedPort(true);
    //   }

    // } else {
    //   InformUser.unsuccess({
    //     text: "The port you are choosing is already selected to be target port. Please choose another port to create connectivity."
    //   })
    // }


    // if (port.port_no != this.state.sourcePort.port_no) {
    //   if (port.port_no != this.state.targetPort.port_no) {
    //     this.setState({ targetPort: port });
    //   } else {
    //     this.removeSelectedPort(false)
    //   }

    // } else {
    //   InformUser.unsuccess({
    //     text: "The port you are choosing is already selected to be target port. Please choose another port to create connectivity."
    //   })
    // }


  }
  removeSelectedPort(isSource) {
    if (isSource) {
      this.setState({ sourcePort: '' });
    } else {
      this.setState({ targetPort: '' });
    }
  }

  searchRoute_new = (data1, data2) => {
    // console.log(data1, data2);
    this.setState({
      sourcePort: data1,
      targetPort: data2,
    }, () => {
      this.searchRoute();
    })
  }

  async searchRoute() {

    // const sourceFiber = this.state.sourcePort.port_fiber;
    // const targetFiber = this.state.targetPort.port_fiber;

    const sourceFiber = this.props.source.port_fiber;
    const targetFiber = this.props.target.port_fiber;


    // console.log(sourceFiber, targetFiber)

    await TaskService.get_route_connectivity(1, sourceFiber, targetFiber, 2, this.token).then(result => {
      // console.log("Search Route:", result)
      if (result.status == 1) {
        if (result.data !== null) {
          var data = {
            nodes: result.data.nodes,
            edges: result.data.edges,
          }
          
          this.setState({
            routeData: data,
            horizontalLayout: result.data.horizontal,
            // displayingRoute: ` Port ${this.props.source.display_no} ( ${this.props.source.description} ) - Port ${this.props.target.display_no} ( ${this.props.source.description} )`,
            // isSelectingFiber: true,
          }, async () => {
            await this.renderCytoscapeElement()
          });
        } else {
          InformUser.unsuccess({ text: "No data received" })
        }

      } else {
        InformUser.unsuccess({ text: result.msg })
        this.props.close(false)
      }



      // this.setState({ routeData: data}, ()=> {
      //   this.renderCytoscapeElement()
      // })
    })
    // searchRoute() {

    //   const port_no1 = this.state.sourcePort.port_no;
    //   const port_no2 = this.state.targetPort.port_no;



    //   TaskService.get_route_connectivity(1, port_no1, port_no2, 2, this.token).then(result => {
    //     console.log("Search Route:", result)
    //     if (result.status == 1) {
    //       if (result.data !== null) {
    //         var data = {
    //           nodes: result.data.nodes,
    //           edges: result.data.edges,
    //         }
    //         this.setState({
    //           routeData: data,
    //           horizontalLayout: result.data.horizontal,
    //           displayingRoute: 'Route: [ Port-' + this.state.sourcePort.port_no + " to Port-" + this.state.targetPort.port_no + " ]",
    //           // isSelectingFiber: true,
    //         }, () => {
    //           this.renderCytoscapeElement()
    //         });
    //       } else {
    //         InformUser.unsuccess({ text: "No data received" })
    //       }

    //     } else {
    //       InformUser.unsuccess({ text: result.msg })
    //     }



    //     // this.setState({ routeData: data}, ()=> {
    //     //   this.renderCytoscapeElement()
    //     // })
    //   })




    // var data = {
    //   nodes: cytoRoute.nodes,
    //   edges: cytoRoute.edges,
    // }
    // this.setState({ routeData: data }, () => {
    //   console.log(this.state.routeData)
    //   this.renderCytoscapeElement()
    // })


  }

  matchFiber() {
    const port_no1 = this.state.sourcePort.port_no;
    const port_no2 = this.state.targetPort.port_no;


    TaskService.get_fiber_option(port_no1, port_no2, this.token).then(result => {
      // console.log("Fetch Fibers:", result)
      if (result.status == 1) {
        if (result.data !== null) {
          this.setState({
            // routeData: data,
            // horizontalLayout: result.data.horizontal,
            // displayingRoute: 'Route: [ Port-' +this.state.sourcePort.port_no +" to Port-" +this.state.targetPort.port_no +" ]",
            fiberSourceList: result.data.port_fiber1,
            fiberTargetList: result.data.port_fiber2,
            isSelectingFiber: true,
          }, () => {
            // this.renderCytoscapeElement()
            this.initialMatchFiber();
          });
        } else {
          InformUser.unsuccess({ text: "No data received" })
        }

      } else {
        InformUser.unsuccess({ text: result.msg })
      }



      // this.setState({ routeData: data}, ()=> {
      //   this.renderCytoscapeElement()
      // })
    })
  }
  initialMatchFiber() {
    // const myLine = new LeaderLine(
    //   document.getElementById('start'),
    //   document.getElementById('end')
    // );
    // console.log('yut')
    const fiber1 = this.state.fiberSourceList;
    const fiber2 = this.state.fiberTargetList;

    var fiber1Length = fiber1.filter(list =>
      list.status !== 0
    );
    var fiber2Length = fiber2.filter(list =>
      list.status !== 0
    );
    // console.log(fiber1Length, fiber2Length)
    var matchedArray = [];
    if (fiber1Length.length <= fiber2Length.length) {
      fiber1.map(fiber => {
        if (fiber.status === 1) {
          // console.log(fiber.fiber_no)

          fiber2.map(matchedFiber => {
            if (matchedFiber.port_fiber_no === fiber.port_fiber_no) {
              // console.log(fiber.fiber_no, matchedFiber.fiber_no)
              matchedArray.push({ fiber, matchedFiber });
            }
          });
        }
      });
      // console.log(matchedArray)
    } else {

    }

  }

  createHorizontalLayout() {
    const data = [
      ['1', '3', '5'],
      ['2', '4', '6']
    ];
    const horizontalLayout = this.state.horizontalLayout;
    var horizonFormat = [];

    // horizonData.push({ node: this.cy.$id(node) })
    horizontalLayout.forEach(route => {
      var h_route = [];
      route.forEach(node => {
        h_route.push({ node: this.cy.$id(node) })
      })
      horizonFormat.push(h_route);

    });


    // console.log("Creating H", horizonFormat)
    return horizonFormat;
  }

  renderCytoscapeElement() {

    this.cy = cytoscape({
      container: document.getElementById('cy'),
      style: this.graphStyle,
      zoom: 2,
      minZoom: 0.1,
      maxZoom: 2,
      wheelSensitivity: 0.1,
      elements: this.state.routeData,
      // zoomingEnabled: false,
      // userZoomingEnabled: false,
      // panningEnabled: false,
      // userPanningEnabled: false,
      // boxSelectionEnabled: false,
      // autoungrabify: true,
      // autounselectify: true,
      layout: {
        name: 'preset',
        // convergenceThreshold: 100, // end layout sooner, may be a bit lower quality
        // animate: false
      }
    });

    // this.initialBackgroundViewpoint();
    this.cy.pan({
      x: 0,
      y: 0
    });
    this.cy.center()
    // console.log(this.cy.width(), this.cy.height())
    // console.log( this.cy)
    //After Cytoscape instance was declared, Call nodes's ID to create layout
    // this.initialBackgroundViewpoint();
    // var horizontalData = this.createHorizontalLayout()
    // console.log("H Result",horizontalData)
    // this.cy.layout({
    //   name: 'cola',
    //   alignment: {
    //     // vertical: [
    //     //   [{  node: this.cy.$id('c-1') }, { node: this.cy.$id('c-2') },],
    //     //   [{  node: this.cy.$id('c-3') }, { node: this.cy.$id('c-4') },],
    //     //   [{  node: this.cy.$id('c-5') }, { node: this.cy.$id('c-6') },],
    //     // ],
    //     // horizontal: [
    //     //   [
    //     //     {
    //     //       node: this.cy.$id('1')
    //     //     },
    //     //     {
    //     //       node: this.cy.$id('3')
    //     //     },
    //     //     {
    //     //       node: this.cy.$id('2')
    //     //     },

    //     //   ]
    //     // ]
    //   },
    // }).run();


    // this.cy.on('tap', 'node', async (event) => {
    //   console.log(event.target.data())
    // })

    // this.cy.on('tap', ':parent', async (event) => {

    //   const connection_data = event.target.data().connection_data;
    //   console.log(connection_data);
    //   const dialog = await openConfirmation({
    //     title: "Create Connection",
    //     text: "Are you sure you want choose route ..."
    //   })
    //   if (dialog) {
    //     // console.log('yutyutyut')
    //     this.choosePath(connection_data);
    //   }
    // })
    // this.cy.on('mouseover', ':parent', (event) => {
    //   // console.log(event.target.data())
    //   const target = event.target;
    //   target.addClass('hover');
    // })
    // this.cy.on('mouseout', ':parent', (event) => {
    //   const target = event.target;
    //   target.removeClass('hover');
    // })
  }

  async choosePath(connection_data) {
    // console.log('yut before')
    const port_no1 = this.props.source.port_fiber;
    const port_no2 = this.props.target.port_fiber;

    // console.log(port_no1, port_no2, connection_data)
    await TaskService.create_connectivity(port_no1, port_no2, connection_data, this.token).then(async result => {
      // console.log("yut", result);
      if (result.status == 1) {
        await InformUser.success({ text: result.msg });
        // this.getTask();
        this.props.close(true)
      } else {
        InformUser.unsuccess({ text: result.msg })
      }
    })
  }

  initialBackgroundViewpoint() {
    //Initial Zooming for fitting background size when render
    // var zoomLevel = this.cy.height() / this.state.mapPicture.height;
    // console.log(zoomLevel)
    // this.cy.zoom(0.6)
    // this.cy.minZoom(zoomLevel)
    //Initial viewpoint when render
    // var cyMapWidth = this.state.mapPicture.width / 2;
    // var halfWidthRatio = this.cy.width() * cyMapWidth / (this.cy.width() / this.cy.zoom());

    // this.cy.pan({
    //     x: this.cy.width() / 2 - halfWidthRatio,
    //     y: 0
    // });
    // console.log('yut')
    // this.cy.fit()
    // this.cy.center()

    //Click background to adjust view
    // this.cy.on('tap', (event) => {
    //     this.cy.pan({
    //         x: this.cy.width() / 2 - halfWidthRatio,
    //         y: 0
    //     });
    //     this.cy.zoom(zoomLevel)
    // })
}

  openPanelView() {


    const source = this.state.tableSourceData

    const target = this.state.tableTargetData

    var sourceRow = this.chunkPort(source);
    var targetRow = this.chunkPort(source);
    // console.log(sourceRow);

    // var targetRow = chunk(target, targetSize);

    // const dividedSourceSize = sourceSize



    // console.log(fourDividedSource);
    this.setState({
      sourceRow: sourceRow,
      targetRow: targetRow,
      panelView: true,
    })
  }

  chunkPort(portArray) {
    const chunk = (arr, size) => arr.reduce((acc, e, i) => (i % size ? acc[acc.length - 1].push(e) : acc.push([e]), acc), []);
    var portSize = portArray.length / 3;
    var portRow = chunk(portArray, portSize);

    var fourDividedPort = []
    portRow.map(row => {
      var minorArray = chunk(row, 4)
      fourDividedPort.push(minorArray)
    })

    return fourDividedPort;

  }


  getPanelSelection() {
    this.setState({ panelView: false })
  }

  textareaHandle = (event) => {
    this.setState({ [event.target.name]: event.target.value })
  }

  toggleShowRouteOption() {
    if (this.state.showRouteOption) {
      this.setState({ showRouteOption: false })
    } else {
      this.setState({ showRouteOption: true })
    }
  }

  optionDisplayNone() {
    if (this.state.showRouteOption) {
      return { display: "none" }
    } else {
      // return { opacity: "0.5" }
    }
  }




  render() {
    // console.log(this.props)

    return (
      <div className="connectivity-visuals">
        <button onClick={()=>{ this.props.close()}} className="close fit-btn"><i class="fas fa-times"></i></button>

        <Loading open={this.state.loading} />
        <header className="h1">
          <h2 style={{marginRight:"0.2vw"}}>Connection Visualization</h2>
          {
            // !this.state.displayingRoute && <h2>Route Solution</h2>
          }
          {
            // this.state.displayingRoute && <h2> {this.state.displayingRoute} </h2>
          }
        </header>
        {/* <hr /> */}
        <div className="path-section">
          <div className="cyto-container">
            <div id="cy" > </div>
          </div>
        </div>

      </div>
    )
  }
}

export default Connectivity
