import React, { Component } from 'react'
import { environment } from '../../Environment';
import './ElementVisual.scss'
import * as TopologyService from "../../services/TopologyService";
import * as InformUser from "../../shared/InformUser";
import * as VisualService from "../../services/VisualService";
import { openConfirmation } from "../../shared/Confirmation";
import { onUserResponse } from "../../shared/UserResponse";
import { BrowserRouter as Router, Route, Switch, Redirect, useHistory } from "react-router-dom";
import Modal from 'react-modal';
import { Loading } from "../../shared/Loading";
import { InterconnectionList } from "../InterconnectionList/InterconnectionList";
import { UnitDashboard } from "../Overview/UnitDashboard/UnitDashboard";
import cytoscape from "cytoscape";
// import Modal from 'react-modal';


export class ElementVisual extends Component {


    token = JSON.parse(localStorage.getItem('userData')).token
    cy;
    sourceEdge;
    targetEdge;
    openingUnit;
    elementVisual = null;
    graphStyle = cytoscape.stylesheet()
    .selector('edge') .css({
        'width': '2',
        "curve-style": "bezier",
// "control-point-distances": [40, -40],
// "control-point-weights": [0.250, 0.75],
        // "haystack-radius": 0,
        "control-point-step-size": 100,
        // "source-distance-from-node": "20px",
        // "line-cap": "round",
        // 'source-endpoint': '180deg',
        // 'target-endpoint': '0deg',
        // 'curve-style': 'segments',
        // 'control-point-step-size': '200',
        // "edge-distances": "100",
        'line-fill': 'linear-gradient',
        'line-gradient-stop-colors': 'data(colorCode)',
        'line-gradient-stop-positions': ['0%', '50%', '50%', '100%'],
        "loop-direction": 'data(loopDirection)',
        // "loop-sweep": "90deg",
        // 'line-color': 'data(colorCode)',
        'text-margin-x': 'data(shiftLabel)',
        'label': 'data(label)',
        "text-background-opacity": 1,
        // "color": "#000000",
        // "text-background-color": "#b3b3b3",
        // "text-background-shape": "roundrectangle",
        // "text-border-color": "#000",
        "text-border-width": 0,
        // "text-margin-x": "50px",
        // "text-border-opacity": 1,
        // "text-background-padding": "2px 5px",
        'color': '#EEEEEE',
        "text-background-color": "#6b0f1a",
        "text-background-opacity": "0.9",
        "text-background-shape": "roundrectangle",
        "text-background-padding": "5px 10px 5px 10px",



        // 'color': 'white',
        'font-size': '20px',
        // 'font-family': 'helvetica, arial, verdana, sans-serif',
        // 'font-weight': 300,
        // 'text-outline-width': 2,
        // 'text-outline-color': 'black',
        // 'control-point-distance': 100,
        // 'target-arrow-shape': 'data(arrow_target)',
        // 'source-arrow-shape': 'data(arrow_source)',
        // 'target-arrow-shape': 'circle',
        // 'source-arrow-shape': 'circle',
        // 'source-arrow-color': 'data(sourceColor)',
        // 'target-arrow-color': 'data(targetColor)',
    })
    .selector(':selected') .css({
        'edge.opacity': 0.5,
        'width': '3',
        'color': 'white',
        'line-color': '#ffb3b3',
        // 'source-arrow-color': 'data(colorCode)',
        // 'target-arrow-color': 'data(colorCode)',
    })
    .selector('node') .css({
        'shape': 'rectangle',
        'background-clip': 'none',
        // 'background-width':'100%',
        // 'background-height':'auto',
        // 'background-fit':'node',
        'background-fit': 'contain',
        'background-opacity': '0',

        // 'width': '60',
        // 'height': '55',
        'content': 'data(name)',
        'text-valign': 'center',
        // 'text-outline-width': 1.5,
        // 'text-outline-color': '#EEEEEE',

        'color': '#EEEEEE',
        'font-size': '20px',
        "text-background-color": "#282828",
        "text-background-opacity": "0.8",
        "text-background-shape": "roundrectangle",
        "text-background-padding": "5px 10px 5px 10px",

        // 'background-image': 'url("assets/Robot_img_50.png")',
        // 'object-fit': 'cover',
        // 'background-fit':'contain',
        // 'background-repeat': 'no-repeat',
        // 'background-position': 'center center',
        // 'color': '#161616',
        // 'font-size': '1px',
        'font-weight': '600'


    })
    .selector(':selected').style({
        // 'label': 'data(label)',
        // 'label': '',
        // 'color': 'black',
        // 'text-outline-width': 2,
        // 'text-outline-color': '#e9e9e9',
        // 'opacity': 1,
        // 'font-size': 18,
        // 'overlay-color': 'blue',
        // 'overlay-opacity': '0.5',
        // 'overlay-padding': '5',

        // 'shape': 'roundrectangle',
        // 'width': '60',
        // 'height': '55',
        'content': 'data(name)',
        'text-valign': 'center',
        'text-outline-width': 0.5,
        'text-outline-color': '#dce0d9',
        'background-color': '#1d2b36',
        // 'background-image': 'url("assets/Robot_img_50.png")',
        // 'object-fit': 'cover',
        // 'background-repeat': 'no-repeat',
        // 'background-position': 'center center',
        'color': '#31081f',
        'font-size': 14,
        'font-weight': '600',
        'border-width': '0',
        // 'border-color': 'white',
        // 'selection-box-color': 'white',
        // 'selection-box-border-color': 'white',
        // 'border-width': 1,
        // 'border-color': 'black',

    })
    .selector('.locked').style({
        'opacity': '0.5'
    })
    .selector(':parent').style({
        'border-width': '0',
        // 'overlay'

    })
    // graphStyle = cytoscape.stylesheet()
    //     .selector('edge') .css({
    //         'width': '2',
    //         'curve-style': 'bezier',
    //         // 'curve-style': 'bezier',
    //         // 'curve-style': 'segments',
    //         // 'curve-style': 'unbundled-bezier',
    //         // 'control-point-step-size': '100px',
    //         'line-fill': 'linear-gradient',
    //         'line-gradient-stop-colors': 'data(colorCode)',
    //         'line-gradient-stop-positions': ['0%', '50%', '50%', '100%'],
    //         "loop-direction": "data(direction)",
    //         // "loop-sweep": "70deg",
    //         // 'line-color': 'data(colorCode)',
    //         'label': 'data(label)',
    //         "text-background-opacity": 1,
    //         "color": "#000000",
    //         "text-background-color": "#b3b3b3",
    //         "text-background-shape": "roundrectangle",
    //         "text-border-color": "#000",
    //         "text-border-width": 0,
    //         "text-border-opacity": 1,
    //         "text-background-padding": "2px 5px",
    //         // 'color': 'white',
    //         'font-size': '20px',
    //         // 'font-family': 'helvetica, arial, verdana, sans-serif',
    //         // 'font-weight': 300,
    //         // 'text-outline-width': 2,
    //         // 'text-outline-color': 'black',
    //         // 'control-point-distance': 100,
    //         // 'target-arrow-shape': 'data(arrow_target)',
    //         // 'source-arrow-shape': 'data(arrow_source)',
    //         // 'target-arrow-shape': 'circle',
    //         // 'source-arrow-shape': 'circle',
    //         // 'source-arrow-color': 'data(sourceColor)',
    //         // 'target-arrow-color': 'data(targetColor)',
    //     })
    //     .selector(':selected').css({
    //         'edge.opacity': 0.5,
    //         'width': '3',
    //         'color': 'white',
    //         'line-color': '#ffb3b3',
    //         // 'source-arrow-color': 'data(colorCode)',
    //         // 'target-arrow-color': 'data(colorCode)',
    //     })
    //     .selector('node') .css({
    //         'shape': 'rectangle',
    //         'background-clip': 'none',
    //         // 'background-width':'100%',
    //         // 'background-height':'auto',
    //         // 'background-fit':'node',
    //         'background-fit': 'contain',
    //         'background-opacity': '0',

    //         // 'width': '60',
    //         // 'height': '55',
    //         'content': 'data(name)',
    //         'text-valign': 'center',
    //         'text-outline-width': 1.5,
    //         'text-outline-color': '#EEEEEE',

    //         // 'background-image': 'url("assets/Robot_img_50.png")',
    //         // 'object-fit': 'cover',
    //         // 'background-fit':'contain',
    //         // 'background-repeat': 'no-repeat',
    //         // 'background-position': 'center center',
    //         'color': '#161616',
    //         'font-size': '20px',
    //         'font-weight': '600'


    //     })
    //     .selector(':selected').style({
    //         // 'label': 'data(label)',
    //         // 'label': '',
    //         // 'color': 'black',
    //         // 'text-outline-width': 2,
    //         // 'text-outline-color': '#e9e9e9',
    //         // 'opacity': 1,
    //         // 'font-size': 18,
    //         // 'overlay-color': 'blue',
    //         // 'overlay-opacity': '0.5',
    //         // 'overlay-padding': '5',

    //         // 'shape': 'roundrectangle',
    //         // 'width': '60',
    //         // 'height': '55',
    //         'content': 'data(name)',
    //         'text-valign': 'center',
    //         'text-outline-width': 0.5,
    //         'text-outline-color': '#dce0d9',
    //         'background-color': '#1d2b36',
    //         // 'background-image': 'url("assets/Robot_img_50.png")',
    //         // 'object-fit': 'cover',
    //         // 'background-repeat': 'no-repeat',
    //         // 'background-position': 'center center',
    //         'color': '#31081f',
    //         'font-size': 14,
    //         'font-weight': '600',
    //         'border-width': '0',
    //         // 'border-color': 'white',
    //         // 'selection-box-color': 'white',
    //         // 'selection-box-border-color': 'white',
    //         // 'border-width': 1,
    //         // 'border-color': 'black',

    //     }).selector('.locked').style({
    //         'opacity': '0.5'
    //     })

    constructor(props) {
        super(props);
        this.state = {
            navigateToMapManagement: false,
            graphData: '',
            containerHistory: [],

            showIntercon: false,

            loading: false,
            unitDashboardOpen: false,
        }
    }
    componentWillMount() {
        Modal.setAppElement('body');
    }
    componentDidMount() {

        if (this.props.element) {
            // console.log(this.props.location.state.elementVisual)
            this.elementVisual = this.props.element;
        } else {
            this.elementVisual = null;
        }


        this.setState({ loading: true }, async () => {
            // console.log(this.props)
            await this.getGlobalTopology(this.elementVisual)
            this.setState({ loading: false })
        })



        // console.log(this.state.graphData)
    }
    async getGlobalTopology(port_idx) {
        await VisualService.get_global_topology(null, port_idx, this.token).then(result => {
            // console.log(result.data)
            if (result.status == 1) {
                this.setState({
                    graphData: result.data,
                    containerHistory: [{
                        name: result.data.name,
                        ref_id: null,
                    }]
                }, () => {
                    this.initialMapSetup()
                })
            } else {
                InformUser.unsuccess({ text: result.msg })
            }
        })
    }
    initialMapSetup() {
        if (this.state.graphData) {
            var thereIs = this.state.graphData;
            if (thereIs && thereIs.name && thereIs.bg_path && thereIs.bg_height && thereIs.bg_width) {
                var bg = {
                    url: environment.api + thereIs.bg_path,
                    height: thereIs.bg_height,
                    width: thereIs.bg_width
                }
                // var iconUrl = environment.api + thereIs.img_path
                var cyData = {
                    nodes: thereIs.element,
                    edges: thereIs.edges,
                }
                this.setState({
                    graphData: cyData,
                    mapPicture: bg,
                    // registeredName: thereIs.name,
                    // registeredIcon: iconUrl,
                    // registeredDescription: "Still no description attribute"
                }, () => {
                    // console.log(this.state.graphData)
                    this.renderCytoscapeElement();
                })
            } else {
                InformUser.unsuccess({ text: "Data is incomplete" });
            }
        } else {
            InformUser.unsuccess({ text: "There is no data to generate editing map" });
        }
    }

    renderCytoscapeElement() {
        // console.log("mapPicture ", this.state.registeredIcon)
        // console.log(this.state.graphData)

        // Set up and render Cytoscape
        this.cy = window.cy = cytoscape({
            container: document.getElementById('cy'),
            style: this.graphStyle,
            zoom: 2,
            minZoom: 0.1,
            maxZoom: 2,
            wheelSensitivity: 0.2,
            elements: this.state.graphData,
            layout: {
                name: 'preset',
                // boundingBox: {
                //     x1: 0,
                //     y1: 0,
                //     x2: 100,
                //     y2: 100
                // },
            }
        });

        // Draw background with ext. Cytoscape-canvae
        let background = new Image();
        background.onload = () => {

            // console.log("height", background.height)
            // console.log("width", background.width)
            const bottomLayer = this.cy.cyCanvas({
                zIndex: -1,
            });
            const canvas = bottomLayer.getCanvas();
            const ctx = canvas.getContext("2d");
            this.cy.on("render cyCanvas.resize", evt => {
                bottomLayer.resetTransform(ctx);
                bottomLayer.clear(ctx);
                bottomLayer.setTransform(ctx);
                ctx.save();
                // Draw a background
                ctx.drawImage(background, 0, 0);

                // Draw text that follows the model
                // ctx.font = "24px Helvetica";
                // ctx.fillStyle = "black";
                // ctx.fillText("This text follows the model", 200, 300);

                // // Draw shadows under nodes
                // ctx.shadowColor = "black";
                // ctx.shadowBlur = 25 * cy.zoom();
                // ctx.fillStyle = "white";
                // cy.nodes().forEach(node => {
                //   const pos = node.position();
                //   ctx.beginPath();
                //   ctx.arc(pos.x, pos.y, 10, 0, 2 * Math.PI, false);
                //   ctx.fill();
                // });
                // ctx.restore();

                // // Draw text that is fixed in the canvas
                // bottomLayer.resetTransform(ctx);
                // ctx.save();
                // ctx.font = "24px Helvetica";
                // ctx.fillStyle = "red";
                // ctx.fillText("This text is fixed", 200, 200);
                // ctx.restore();

            });


        }
        background.src = this.state.mapPicture.url;

        this.initialBackgroundViewpoint();
        // cy.panBy({
        //     x: 0,
        //     y: 0
        // });

        // this.cy.on('tap', 'node', (event)=> {
        //     console.log(event.target.id())
        //     this.cy.nodes(':selected').map( node => {
        //         console.log(node.data())
        //     })
        //     // console.log(this.cy.nodes(':selected').length)
        // })


        this.cy.on('unselect', 'node', (event) => {
            this.setState({ resizeBox: false })
        })

        // Click background to add AddingElement to Map
        this.cy.on('tap', (event) => {
        })

        this.cy.on('tap', 'node', (event) => {
            // console.log(event.target.position())

            const target = event.target.data();
            if (!event.target.grabbable()) {
                event.target.unselect()
            } else {
                if (target.model_id == "-1") {
                    // console.log(node)
                    this.changeContainerPage(target, true);
                } else {
                    // cy.nodes().map(node => {
                    //     if (!node.selected()) {
                    //         node.toggleClass("isNotSelected", true);
                    //     } else {
                    //         node.toggleClass("isNotSelected", false);
                    //     }
                    // });
                    // cy.edges().map(edge => {
                    //     edge.toggleClass("isNotSelected", true);
                    // })
                    this.openDashboardUnit(event)
                }
            }
        })

        //Click Edges to open Interconnection
        this.cy.on('tap', 'edge', event => {
            var edgeData = event.target.data()
            // console.log(edgeData.source);
            this.openInterconModal(edgeData)
        })





        //Prevent node from being dragged outside range of background.
        this.cy.automove({
            nodesMatching: this.cy.nodes(),
            reposition: { x1: 0, x2: this.state.mapPicture.width, y1: 0, y2: this.state.mapPicture.height }
        });



    }
    openInterconModal(edge) {
        this.sourceEdge = this.cy.$id(edge.source).data();
        this.targetEdge = this.cy.$id(edge.target).data();

        this.setState({ showIntercon: true })

    }
    closeInterconModal = () => {
        this.setState({ showIntercon: false });
    }

    initialBackgroundViewpoint() {
        //Initial Zooming for fitting background size when render
        var zoomLevel = this.cy.height() / this.state.mapPicture.height;
        this.cy.zoom(zoomLevel)
        this.cy.minZoom(zoomLevel)
        //Initial viewpoint when render
        var cyMapWidth = this.state.mapPicture.width / 2;
        var halfWidthRatio = this.cy.width() * cyMapWidth / (this.cy.width() / this.cy.zoom());

        this.cy.pan({
            x: this.cy.width() / 2 - halfWidthRatio,
            y: 0
        });

        //Click background to adjust view
        this.cy.on('tap', (event) => {
            this.cy.pan({
                x: this.cy.width() / 2 - halfWidthRatio,
                y: 0
            });
            this.cy.zoom(zoomLevel)
        })
    }

    changeContainerPage(node, isForwardingPage) {

        if (isForwardingPage) {
            this.setState({ loading: false }, async () => {
                await VisualService.get_global_topology(node.ref_id,null, this.token).then(result => {
                    // console.log("choose result ", result)
                    var thereIs = result.data;
                    if (thereIs && thereIs.id && thereIs.name && thereIs.bg_path && thereIs.bg_height && thereIs.bg_width) {
                        this.pushPageHistory(node);
                        this.reformGraphData(result.data)
                    } else {
                        InformUser.unsuccess({ text: "Data is incomplete" });
                    }
                })
                this.setState({ loading: false })
            })
        } else {

            this.setState({ loading: false }, async () => {
                // console.log(node)
                if (!node.ref_id) {  //Case: backward to the Default global
                    await VisualService.get_global_topology(this.elementVisual, this.token).then(result => {
                        // console.log("choose result ", result)
                        var thereIs = result.data;
                        if (thereIs && thereIs.name && thereIs.bg_path && thereIs.bg_height && thereIs.bg_width) {
                            this.popPageHistory(node);
                            this.reformGraphData(result.data)
                        } else {
                            InformUser.unsuccess({ text: "Data is incomplete" });
                        }
                    })
                } else {  //Case: backward to nth Container
                    await VisualService.get_global_topology(node.ref_id, null, this.token).then(result => {
                        // console.log("choose result ", result)
                        var thereIs = result.data;
                        if (thereIs && thereIs.id && thereIs.name && thereIs.bg_path && thereIs.bg_height && thereIs.bg_width) {
                            this.popPageHistory(node);
                            this.reformGraphData(result.data)
                        } else {
                            InformUser.unsuccess({ text: "Data is incomplete" });
                        }
                    })
                }

                this.setState({ loading: false })
            })
        }



        //Call API to get new graphData

        //Reform
    }
    openDashboardUnit(event) {
        this.openingUnit = event.target.data();
        // console.log(event.target.data())
        this.openingUnit["model"] = this.openingUnit.model_name;
        this.openingUnit["img_path"] = event.target.style()["background-image"];
        // console.log(this.openingUnit)
        this.setState({ unitDashboardOpen: true })
    }
    pushPageHistory(node) {
        // console.log(this.state.containerHistory)
        const history = this.state.containerHistory;
        history.push(node);
        // console.log(this.state.containerHistory)
    }
    popPageHistory(node) {
        // console.log(this.state.containerHistory)
        const history = this.state.containerHistory;
        if (history.length <= 2) { //Try to backword to the Golbal Map
            history.pop()
            // console.log(this.state.containerHistory)
        } else {
            history.map((page, index) => {
                // console.log(page.ref_id, node.ref_id)
                if (page.ref_id == node.ref_id) {
                    history.splice(index + 1);
                }
                // console.log(history)

            })
        }

        this.setState({ containerHistory: history });

    }
    reformGraphData(thereIs) {

        var bg = {
            url: environment.api + thereIs.bg_path,
            height: thereIs.bg_height,
            width: thereIs.bg_width
        }
        var iconUrl = environment.api + thereIs.img_path
        var cyData = {
            nodes: thereIs.element,
            edges: thereIs.edges,
        }
        if (thereIs.cont_element && (thereIs.cont_element.length >= 1)) {
            cyData = {
                nodes: thereIs.cont_element.concat(thereIs.element),
                edges: thereIs.edges,
            }
        }
        this.setState({
            graphData: cyData,
            mapPicture: bg,
        }, () => {
            // console.log("Reform cyto", this.state.graphData)
            this.renderCytoscapeElement();
        })
    }


    editGlobal() {

        this.setState({ loading: true }, async () => {
            await TopologyService.edit_global(this.token).then(result => {
                // console.log(result)


                if (result.status == 1) {
                    this.setState({ graphData: result.data }, () => {
                        this.setState({ navigateToMapManagement: true });
                    })

                } else {
                    InformUser.unsuccess({ text: result.msg })
                }
                this.setState({ loading: false })

            })

        })

    }
    closeUnitDashboard = (detailOf, doRefresh) => {
        // console.log(detailOf)
        if (doRefresh) {
            this.setState({ loading: true }, async () => {
                await this.getGlobalTopology(detailOf.idx)
                this.setState({ loading: false })
            })
        }
        this.setState({ unitDashboardOpen: false })

    }

    render() {
        if (this.state.navigateToMapManagement) {
            return <Redirect push to={{
                pathname: "/map-management",
                state: { editingGraphData: this.state.graphData }
            }} />;
        }
        return (

            <div className="element-visualization-container">
                <button onClick={this.props.close} className="close fit-btn"><i class="fas fa-times"></i></button>
                <Loading open={this.state.loading} />
                <Modal
                    isOpen={this.state.showIntercon}
                    className="intercon-edge-body"
                    overlayClassName="no-overlay"
                    onRequestClose={this.closeInterconModal}
                    closeTimeoutMS={500}
                >
                    <InterconnectionList source={this.sourceEdge} target={this.targetEdge} close={this.closeInterconModal} />

                </Modal>
                <Modal
                    isOpen={this.state.unitDashboardOpen}
                    className="unit-dashboard-body"
                    overlayClassName="no-overlay"
                    onRequestClose={() => { this.closeUnitDashboard(null, false) }}
                // closeTimeoutMS={500}
                >
                    <UnitDashboard unit={this.openingUnit} close={this.closeUnitDashboard} />

                </Modal>

                <header>
                    <h1>Topology: Visualization</h1>
                    <div className="left">
                        <h2 className="header-name">
                            {
                                this.state.containerHistory.map(page =>
                                    <span onClick={() => { this.changeContainerPage(page, false) }} >{page.name}</span>
                                )
                            } Topology
                    </h2>
                        {
                            this.state.containerHistory.length > 1 && <button className="btn back-btn cancel " onClick={() => { this.changeContainerPage(this.state.containerHistory[this.state.containerHistory.length - 2], false) }}><i class="fas fa-backward back"></i></button>
                        }
                    </div>
                </header>

                <div id="cy"></div>
                {/* <button className="btn submit edit-global" onClick={this.editGlobal.bind(this)}><span>Edit Topology</span></button> */}
            </div>


        )
    }
}

export default ElementVisual
