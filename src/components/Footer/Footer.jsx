import React, { Component } from 'react'
import * as InfoService from "./../../services/InfoService";
import { openConfirmation } from "../../shared/Confirmation";
import { onUserResponse } from "../../shared/UserResponse";
import * as InformUser from "../../shared/InformUser";
import * as UserService from "./../../services/UserService";
import './Footer.scss'
import { Redirect } from 'react-router-dom'
import moment from 'moment';
import dayjs from 'dayjs'
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import * as TopologyService from "./../../services/TopologyService";



export class Footer extends Component {

    token = JSON.parse(localStorage.getItem('userData')).token
    time_offset = JSON.parse(localStorage.getItem('userData')).time_offset;
    interval;

    constructor(props) {
        super(props);
        this.state = {
            systemInfo: '',
            server_time: '',
            currentTime: '',
            kickOut: false,
            navigatesToSetting: false,
        }
    }

    componentDidMount() {
        this.getSystemInfo()
        this.continueTime()
    }
    componentWillUnmount() {
        if (this.interval) { clearInterval(this.interval) }
    }

    async getSystemInfo() {
        await InfoService.get_system_info(this.token).then(result => {
            if (result.status == 1) {
                if (result.data) {
                    this.setState({
                        systemInfo: result.data,
                        server_time: result.data.server_time
                    }, ()=>{
                        this.calculateTime();
                    })
                } else {
                    InformUser.unsuccess({ text: "No system information" })
                }
            } else {
                localStorage.removeItem('userData');
                this.setState({ kickOut: true }, () => {
                    window.location.reload(false);

                })
            }
        })
    }

    calculateTime() {
        var server_time = this.state.server_time;
        var offset = moment.duration(this.time_offset)
        var rawServerTime = moment(server_time).add(offset)
        var currentTime = rawServerTime.format("MMM D YYYY HH:mm:ss")
        // console.log('debug footer ',currentTime,server_time,this.time_offset,rawServerTime)
        this.setState({ currentTime: currentTime, server_time: rawServerTime})
    }

    continueTime(){
        this.interval = setInterval(() => {
            if( this.state.server_time ){
                let clientTime = this.state.server_time;
                clientTime = clientTime.add(1, 's')
                this.setState({ currentTime: clientTime.format("MMM D YYYY HH:mm:ss")})
            }
        }, 1000)
    }

    settingNavigation(){
        this.setState({ navigatesToSetting: true })
    }

    async navigatesTo(path) {
		const location = this.props.location.pathname;
		// console.log(this.props);
		if (location === "/map-management") {

			const dialog = await openConfirmation({
				title: "Topology Edit",
				text: "You are leaving the Edit Topology without applying the topology. Are you sure you want to cancel the topology?",
				actionLeft: "Leave anyway",
				actionRight: "Stay",
			});
			if (dialog && dialog === "confirm") {
				await this.cancelEdittingTopology();
				this.props.history.push(path);
			}
		}
		if (location === "/port-assignment") {
			const dialog = await openConfirmation({
				title: "Port Allocation",
				text: "You are leaving the Port Allocation without submitting changes or cancelling back to Customer Management. Are you sure you want to cancel the allocation?",
				actionLeft: "Leave anyway",
				actionRight: "Stay",
			});
			if (dialog && dialog === "confirm") {
				this.props.history.push(path);
			}
		}
		if ((location !== "/map-management") && (location !== "/port-assignment") ) {
			this.props.history.push(path);
		}
	}

    cancelEdittingTopology(path) {
		this.setState({ loading: true }, async () => {
			await TopologyService.cancel_topology(this.token).then(
				async (result) => {
					if (result.status == 1) {
						this.setState(
							{
								loading: false,
							},()=>{
								this.props.history.push(path);
							}
						);
					} else {
						this.setState(
							{
								loading: false
							}
						);
						InformUser.unsuccess({ text: result.msg });
					}
				}
			);
		});
	}


    render() {
        const { server_time, sw_version, timezone, uptime, } = this.state.systemInfo;
        if (this.state.kickOut) {
            return <Redirect push to={{
                pathname: "/"
            }} />;
        }
        return (
            <div className="footer">
                <span id="software-version" className="left-section">Software Version: {sw_version}</span>
                    <span id="user-time" className="right-section" onClick={()=>{this.navigatesTo("/general-setting")}}>{this.state.currentTime} ({this.time_offset})</span>
            </div>
        )
    }
}

export default withRouter(Footer)
