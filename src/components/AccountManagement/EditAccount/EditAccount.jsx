import React, { Component } from "react";
import "./EditAccount.scss";
import * as UserService from "./../../../services/UserService";
import * as InformUser from "./../../../shared/InformUser";
import { openConfirmation } from "../../../shared/Confirmation";

export class EditAccount extends Component {
    token = JSON.parse(localStorage.getItem("userData")).token;

    constructor(props) {
        super(props);
        this.state = {
            username: this.props.data.username,
            password: "",
            c_password: "",
            firstname: this.props.data.firstname,
            lastname: this.props.data.lastname,
            email: this.props.data.email,
            auth_level: this.props.data.auth_level,
            authLevel: this.props.authLevel,

            changePassword: false,
            invalided: false,
        };
    }

    componentDidMount() {
        // console.log(this.props.authLevel);
    }

    handleInput = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    };


    async updateAccountInfo(event) {
        event.preventDefault();

        const auth_level = parseInt(this.state.auth_level);
        var temp = "";

        this.state.authLevel.map((auth) => {
            if (auth_level === auth.level) {
                temp = auth.id;
            }
        });

        const firstname = this.state.firstname;
        const lastname = this.state.lastname;
        const email = this.state.email;
        const password = this.state.password;

        this.setState({ invalided: true })

            if( !firstname.trim() || !lastname.trim() || !email || !temp ){
                InformUser.unsuccess({ text: "The information is incomplete." })
            }
            // !password || !this.state.c_password
            

            if ( firstname.trim() && lastname.trim() && email && temp  ) {

                var dataIsNotOkay = await this.verifyData(
                    firstname,
                    lastname,
                    email
                );
                
                if (dataIsNotOkay.found) {
                    InformUser.unsuccess({ text: `${dataIsNotOkay.msg}` });
                    
                }
    
                if (!dataIsNotOkay.found) {
                    this.setState({ invalided: false })

                    const dialog = await openConfirmation({
                        title: "Update Account Information",
                        text: `Are you sure you want to change ${this.state.username} information?`,
                    });
                    if (dialog) {

                        var data = ""
                        if( this.state.changePassword ){
                            data = {
                                firstname: firstname,
                                lastname: lastname,
                                email: email,
                                auth_id: temp,
                                password: password,
                            };
                        }
                        if( !this.state.changePassword ){
                            data = {
                                firstname: firstname,
                                lastname: lastname,
                                email: email,
                                auth_id: temp,
                                password: "",
                            };
                        }
                        
                        await UserService.edit_user_account( this.props.data.id , data , this.token).then( async (result) => {
                            // console.log(result);
                            if (result.status === 1) {
                                    InformUser.success({ text: result.msg });
                                    this.props.close();
                             } else {
                                    InformUser.unsuccess({ text: result.msg });
                                }
                           
                            
                        });
                    }
                }
                
            }
        
        
        
       
        
    }

    async verifyData( firstname, lastname, email ) {
        var found = false;
        var msg = "";

        if(!email.match(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/)){
            found = true;
            msg = "Email is incorrect.";
        }
        
        if(!lastname.match(/^[A-Za-z-. ]{1,40}$/)){
            found = true;
            msg = "Lastname only allow . and - as special character with maximum of 40 characters.";
        }

        if(!firstname.match(/^[A-Za-z-. ]{1,40}$/)){
            found = true;
            msg = "Firstname only allow . and - as special character with maximum of 40 characters.";
        }

        if ( this.state.changePassword && ( this.state.password !== this.state.c_password ) ) {
            found = true;
            msg = "Password and Confirm password do not match. Please try again.";
        }

        if ( this.state.changePassword && ( !this.state.password || !this.state.c_password ) ) {
            found = true;
            msg = "Please fill the new password.";
        }

        if (this.validateNoQoute(this.state.password) || this.validateNoQoute(this.state.c_password)) {
			found = true;
			msg = "Single quote and Double quote are not allowed in password.";
		}

        // console.log(found, msg)
        return {
            found: found,
            msg: msg,
        };
    }

    validateInput(input){

        if(this.state.invalided){

            if( input === "FNAME_INPUT" ){
                if( !this.state.firstname.trim() || this.validateText(this.state.firstname) ) {
                    return " invalid-input "
                }
            }

            if( input === "LNAME_INPUT" ){
                if( !this.state.lastname.trim() || this.validateText(this.state.lastname) ) {
                    return " invalid-input "
                }
            }

            if( input === "EMAIL_INPUT" ){
                if( !this.state.email || this.validateEmail(this.state.email) ) {
                    return " invalid-input "
                }
            }

            if(  input === "PASSWORD_INPUT" ){
                if( !this.state.password || this.validateNoQoute(this.state.password) ||
                    !this.state.c_password || this.validateNoQoute(this.state.c_password) ||
                    (this.state.password !== this.state.c_password) ) {
                    return " invalid-input "
                }
            }

        }
    }

    validateNoQoute(input){
        
        if(!input.match(/^[^'"]*$/)){
            return true
        }
    }

    validateEmail(input){
        if(!input.match( /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/)){
            return true
        }
    }

    validateText(input){
        if(!input.match( /^[A-Za-z-. ]{1,40}$/)){
            return true
        }
    }

    handleInput = (event) => {
        // console.log(event.target.value, typeof event.target.value);
        this.setState({ [event.target.name]: event.target.value });
    };

    handleCheckbox = (event) => {
        // console.log(event.target.value, typeof event.target.value);
        const changePassword = this.state.changePassword;
        if(changePassword){
            this.setState({ 
                [event.target.name]: false,
                password: '',
                c_password: '',
            
            });  
        }if(!changePassword){
            this.setState({ [event.target.name]: true });
        }
    };
    render() {
        // console.log(data);

        return (
            <div className="edit-account-container">
                <header>
                    <h1>Account Edit</h1>
                </header>
                {/* <hr /> */}
                {/* <button onClick={() => { updateInput(data) }}>click</button> */}
                <div className="content">
                    <form onSubmit={this.updateAccountInfo.bind(this)} autocomplete="off">
                    <div className="regis-form">
                        <div className="input-group">
                            <div className="left">
                                <span>Username : </span>
                            </div>
                            <div className="right">
                                <input
                                    id="username-input1"
                                    className="disabled"
                                    name="username"
                                    type="text"
                                    value={this.state.username}
                                    placeholder="Username"
                                    // maxLength="40"
                                    onChange={this.handleInput}
                                    disabled
                                />
                            </div>
                        </div>
                        <div className="input-group">
                            <div className="left">
                                <span>Firstname : </span>
                            </div>
                            <div className="right">
                                <input
                                    id="firstname-input1"
                                    name="firstname"
                                    type="text"
                                    value={this.state.firstname}
                                    maxLength="40"
                                    className={this.validateInput("FNAME_INPUT")}
                                    placeholder="Firstname"
                                    onChange={this.handleInput}
                                />
                            </div>
                        </div>
                        <div className="input-group">
                            <div className="left">
                                <span>Lastname : </span>
                            </div>
                            <div className="right">
                                <input
                                    id="lastname-input1"
                                    name="lastname"
                                    type="text"
                                    value={this.state.lastname}
                                    maxLength="40"
                                    className={this.validateInput("LNAME_INPUT")}
                                    placeholder="Lastname"
                                    onChange={this.handleInput}
                                />
                            </div>
                        </div>
                        <div className="input-group">
                            <div className="left">
                                <span>Email : </span>
                            </div>
                            <div className="right">
                                <input
                                    id="email-input1"
                                    name="email"
                                    type="text"
                                    value={this.state.email}
                                    maxlength="40"
                                    className={this.validateInput("EMAIL_INPUT")}
                                    placeholder="Email"
                                    onChange={this.handleInput}
                                />
                            </div>
                        </div>
                        <div className="input-group">
                            <div className="left">
                                <span>Role : </span>
                            </div>
                            <div className="right">
                                <select
                                    name="auth_level"
                                    id="auth-id-input1"
                                    value={this.state.auth_level}
                                    onChange={this.handleInput}
                                >
                                    {this.state.authLevel &&
                                        this.state.authLevel.map((level) => (
                                            <option value={level.level}>
                                                {level.name}
                                            </option>
                                        ))}
                                </select>
                            </div>
                        </div>
                        <hr />
                        <div className="input-group">
                            <div className="left">
                                <span>Change Password : </span>
                            </div>
                            <div className="right checkbox">
                                <input
                                    id="change-password-checkbox1"
                                    name="changePassword"
                                    type="checkbox"
                                    // value={!this.state.changePassword}
                                    placeholder=""
                                    
                                    checked={this.state.changePassword}
                                    onChange={this.handleCheckbox}
                                />
                            </div>
                        </div>
                        {
                            this.state.changePassword && <div className="input-group">
                            <div className="left">
                                <span>New Password : </span>
                            </div>
                            <div className="right">
                                <input
                                    id="password-input1"
                                    name="password"
                                    type="password"
                                    value={this.state.password}
                                    maxLength="40"
                                    className={this.validateInput("PASSWORD_INPUT")}
                                    placeholder="Password"
                                    onChange={this.handleInput}
                                />
                            </div>
                        </div>
                        }
                        
                        {
                            this.state.changePassword && <div className="input-group">
                            <div className="left">
                                <span>Confirm Password : </span>
                            </div>
                            <div className="right">
                                <input
                                    id="c-password-input1"
                                    name="c_password"
                                    type="password"
                                    value={this.state.c_password}
                                    maxLength="40"
                                    className={this.validateInput("PASSWORD_INPUT")}
                                    placeholder="Confirm Pasword"
                                    onChange={this.handleInput}
                                />
                            </div>
                        </div>
                        }
                        
                    </div>
                
                    <div className="btn-row submit-regis">
                    <button
                        id="submit-btn1"
                        type="submit"
                        className="btn submit"
                        style={{marginRight:"0.5vw"}}
                    >
                        <span>Submit</span>
                    </button>
                    <button
                        id="cancel-btn1"
                        type="button"
                        className="btn cancel"
                        onClick={() => {
                            this.props.close();
                        }}
                    >
                        <span>Cancel</span>
                    </button>
                </div>
                    </form>
            
                </div>

               
            
            </div>
        );
    }
}

export default EditAccount;
