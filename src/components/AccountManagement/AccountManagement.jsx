import React, { Component } from "react";
import "./AccountManagement.scss";
import { Loading } from "./../../shared/Loading";
import * as InformUser from "./../../shared/InformUser";
import { openConfirmation } from "./../../shared/Confirmation";
import * as UserService from "./../../services/UserService";
import Modal from "react-modal";
import { EditAccount } from "./EditAccount/EditAccount";
import moment from "moment";

export class AccountManagement extends Component {
	token = JSON.parse(localStorage.getItem("userData")).token;
	time_offset = JSON.parse(localStorage.getItem("userData")).time_offset;
	edittingAccount;

	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			userAccountList: "",
			authLevel: "",

			username: "",
			password: "",
			c_password: "",
			firstname: "",
			lastname: "",
			email: "",
			auth_id: 1,

			openEditModal: false,
			invalided: false,
		};
	}

	componentDidMount() {
		this.setState({ loading: true }, async () => {
			await this.getUserAccount();
			await this.getAuthLevel();
			this.setState({ loading: false });
		});
	}

	async getUserAccount() {
		await UserService.get_user_account(this.token).then(async (result) => {
			// console.log("UserAccount", result);
			if (result.status === 1) {
				var accounts = result.data;
				var timeCaledAccounts = await this.processColumnOfTime(
					accounts
				);
				this.setState({ userAccountList: timeCaledAccounts });
			}
		});
	}
	async processColumnOfTime(logData) {
		var logs = JSON.parse(JSON.stringify(logData));
		// var  logs = logData;
		for (var i = 0; i < logs.length; i++) {
			// const newAddDate = await this.calculateTime(logs[i]["add_date"])
			const newRegisteredDate = await this.calculateTime(
				logs[i]["registered_date"]
			);
			// const newFinishDate = await this.calculateTime(logs[i]["finish_date"])
			// logs[i]["add_date"] = newAddDate;
			logs[i]["registered_date"] = newRegisteredDate;
			// logs[i]["finish_date"] = newFinishDate;
		}
		return logs;
	}
	async calculateTime(serverTime) {
		var rawServerTime = await moment(serverTime);
		// console.log(rawServerTime)
		// console.log(timeOffset)
		var offset = moment.duration(this.time_offset);
		// // var temp = moment(server_time).add(1, 'm')
		// console.log(offset)
		var currentTime = await rawServerTime
			.add(offset)
			.format("ddd YYYY-MM-DD HH:mm:ss");
		// console.log(currentTime)
		return currentTime;
		// console.log(currentTime)
	}

	async getAuthLevel() {
		await UserService.get_auth_level(this.token).then((result) => {
			if (result.status === 1) {
				this.setState({ authLevel: result.data });
			}
		});
	}

	handleInput = (event) => {
		this.setState({ [event.target.name]: event.target.value });
	};
	resetInput() {
		this.setState({
			username: "",
			password: "",
			c_password: "",
			firstname: "",
			lastname: "",
			email: "",
			auth_id: 1,
		});
	}

	async submitRegistration(event) {
		//Validate Input
		// console.log(this.state.systemName, this.state.macAddress, this.state.ipAddress, this.state.model_id)
		event.preventDefault();
		const username = this.state.username;
		const password = this.state.password;
		const firstname = this.state.firstname;
		const lastname = this.state.lastname;
		const email = this.state.email;
		const auth_id = this.state.auth_id;
		this.setState({ invalided: true });

		if (
			!username ||
			!password ||
			!firstname.trim() ||
			!lastname.trim() ||
			!email ||
			!auth_id
		) {
			InformUser.unsuccess({ text: "The information is incomplete." });
		}

		if (username && password && firstname.trim() && lastname.trim() && email && auth_id) {
			// console.log( username, password, firstname, lastname, email, auth_id )

			var dataIsNotOkay = await this.verifyData(
				username,
				firstname,
				lastname,
				email
			);

			if (dataIsNotOkay.found) {
				InformUser.unsuccess({ text: `${dataIsNotOkay.msg}` });
			}

			if (!dataIsNotOkay.found) {
				this.setState({ invalided: false });
				const dialog = await openConfirmation({
					title: "User Registration",
					text: `Are you sure you want to submit ${this.state.username} to the system?`,
				});
				if (dialog) {
					UserService.register_user_account(
						this.state.username,
						this.state.password,
						this.state.firstname,
						this.state.lastname,
						this.state.email,
						this.state.auth_id,
						this.token
					).then(async (result) => {
						// console.log(result);
						if (result.status === 1) {
							InformUser.success({ text: result.msg });
							this.resetInput();
							this.getUserAccount();
						} else {
							InformUser.unsuccess({ text: result.msg });
						}
					});
				}
			}
		}

		//Send Registration Info
	}

	async verifyData(username, firstname, lastname, email) {
		var found = false;
		var msg = "";

		if (!email.match(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/)) {
			found = true;
			msg = "Email is incorrect.";
		}

		if (!username.match(/^[\w-.]{3,40}$/)) {
			found = true;
			msg =
				"Username only allow _ . and - as special character in the range of 3 to 40 characters.";
		}

		
		if (!lastname.match(/^[A-Za-z-. ]{1,40}$/)) {
			found = true;
			msg =
				"Lastname only allow . and - as special character with maximum of 40 characters.";
		}

		if (!firstname.match(/^[A-Za-z-. ]{1,40}$/)) {
			found = true;
			msg = "Firstname only allow . and - as special character with maximum of 40 characters.";
		}
		if (this.state.password !== this.state.c_password) {
			found = true;
			msg = "Password and Confirm password do not match. Please try again.";
		}
		if (this.validateNoQoute(this.state.password) || this.validateNoQoute(this.state.c_password)) {
			found = true;
			msg = "Single quote and Double quote are not allowed in password.";
		}

		return {
			found: found,
			msg: msg,
		};
	}

	validateInput(input) {
		if (this.state.invalided) {
			if (input === "NAME_INPUT") {
				if (
					!this.state.username ||
					this.validateName(this.state.username)
				) {
					return " invalid-input ";
				}
			}

			if (input === "FNAME_INPUT") {
				if (
					!this.state.firstname.trim() ||
					this.validateText(this.state.firstname)
				) {
					return " invalid-input ";
				}
			}

			if (input === "LNAME_INPUT") {
				if (
					!this.state.lastname.trim() ||
					this.validateText(this.state.lastname)
				) {
					return " invalid-input ";
				}
			}

			if (input === "EMAIL_INPUT") {
				if (!this.state.email || this.validateEmail(this.state.email)) {
					return " invalid-input ";
				}
			}

			if (input === "PASSWORD_INPUT") {
				if (
					!this.state.password || this.validateNoQoute(this.state.password) || 
					!this.state.c_password || this.validateNoQoute(this.state.c_password) || 
					this.state.password !== this.state.c_password
				) {
					return " invalid-input ";
				}
			}
		}
	}
	validateNoQoute(input){
        
        if(!input.match(/^[^'"]*$/)){
            return true
        }
    }

	validateName(input) {
		if (!input.match(/^[\w-.]{0,40}$/)) {
			return true;
		}
	}

	validateText(input) {
		if (!input.match(/^[A-Za-z-. ]{1,40}$/)) {
			return true;
		}
	}

	validateEmail(input) {
		if (!input.match(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/)) {
			return true;
		}
	}

	async removeAccount(account) {
		const dialog = await openConfirmation({
			title: "Remove User Account",
			text: `Are you sure you want to delete ${account.username} account?`,
		});
		if (dialog) {
			UserService.remove_user_account(account.id, this.token).then(
				(result) => {
					if (result.status === 1) {
						InformUser.success({ text: result.msg });
						this.getUserAccount();
					} else {
						InformUser.unsuccess({ text: result.msg });
					}
				}
			);
		}
	}
	editAccount(account) {
		// console.log(account);

		this.edittingAccount = account;
		this.setState({ openEditModal: true });
	}

	closeEditModal = async () => {
		await this.getUserAccount();
		this.setState({ openEditModal: false });
	};

	render() {
		return (
			<div className="account-management">
				<Loading open={this.state.loading} />
				<Modal
					isOpen={this.state.openEditModal}
					className="edit-account-body"
					overlayClassName="default-overlay"
					onRequestClose={this.closeEditModal}
					// closeTimeoutMS={500}
				>
					<EditAccount
						data={this.edittingAccount}
						authLevel={this.state.authLevel}
						updateInput={this.updateInput}
						close={this.closeEditModal.bind(this)}
					/>
				</Modal>
				{/* <Modal
                    isOpen={this.state.openEditModal}
                    className="edit-unit-body"
                    overlayClassName="default-overlay"
                    onRequestClose={this.closeEditModal}
                // closeTimeoutMS={500}
                >
                    <EditSystem data={this.edittingSystem} updateInput={this.updateInput} closeEditModal={this.closeEditModal.bind(this)} system={this.edittingSystem} />
                </Modal> */}
				<header className="h1">
					<h1>MANAGEMENT: USER ACCOUNT</h1>
				</header>

				<div className="system-management-layout">
					<div className="registration">
						<header>
							<h2>Registration</h2>
						</header>
						<form
							onSubmit={this.submitRegistration.bind(this)}
							autocomplete="off"
						>
							<div className="regis-form">
								<div className="input-group">
									<div className="left">
										<span>Username : </span>
									</div>
									<div className="right">
										<input
											id="username-input"
											name="username"
											type="text"
											maxlength="40"
											value={this.state.username}
											className={this.validateInput(
												"NAME_INPUT"
											)}
											placeholder="Username"
											onChange={this.handleInput}
										/>
									</div>
								</div>
								<div className="input-group">
									<div className="left">
										<span>Firstname : </span>
									</div>
									<div className="right">
										<input
											id="firstname-input"
											name="firstname"
											type="text"
											maxlength="40"
											value={this.state.firstname}
											className={this.validateInput(
												"FNAME_INPUT"
											)}
											placeholder="Firstname"
											onChange={this.handleInput}
										/>
									</div>
								</div>
								<div className="input-group">
									<div className="left">
										<span>Lastname : </span>
									</div>
									<div className="right">
										<input
											id="lastname-input"
											name="lastname"
											type="text"
											maxlength="40"
											value={this.state.lastname}
											className={this.validateInput(
												"LNAME_INPUT"
											)}
											placeholder="Lastname"
											onChange={this.handleInput}
										/>
									</div>
								</div>
								<div className="input-group">
									<div className="left">
										<span>Email : </span>
									</div>
									<div className="right">
										<input
											id="email-input"
											name="email"
											type="text"
											maxlength="40"
											value={this.state.email}
											className={this.validateInput(
												"EMAIL_INPUT"
											)}
											placeholder="Email"
											onChange={this.handleInput}
										/>
									</div>
								</div>
								<div className="input-group">
									<div className="left">
										<span>Role : </span>
									</div>
									<div className="right">
										<select
											name="auth_id"
											id="role-select"
											value={this.state.auth_id}
											onChange={this.handleInput}
										>
											{this.state.authLevel &&
												this.state.authLevel.map(
													(level, index) => (
														<option
															key={`role-${
																index + 1
															}`}
															id={`role-${
																index + 1
															}`}
															value={level.id}
														>
															{level.name}
														</option>
													)
												)}
										</select>
									</div>
								</div>
								<hr />
								<div className="input-group">
									<div className="left">
										<span>Password : </span>
									</div>
									<div className="right">
										<input
											id="password-input"
											name="password"
											type="password"
											maxlength="40"
											value={this.state.password}
											className={this.validateInput(
												"PASSWORD_INPUT"
											)}
											placeholder="Password"
											onChange={this.handleInput}
										/>
									</div>
								</div>
								<div className="input-group">
									<div className="left">
										<span>Confirm Password : </span>
									</div>
									<div className="right">
										<input
											id="confirm-password"
											name="c_password"
											type="password"
											maxlength="40"
											value={this.state.c_password}
											className={this.validateInput(
												"PASSWORD_INPUT"
											)}
											placeholder="Password"
											onChange={this.handleInput}
										/>
									</div>
								</div>
							</div>
							<div className="btn-row submit-regis">
								<button
									id="submit-btn"
									type="submit"
									className="btn submit"
									style={{ marginRight: "0.5vw" }}
								>
									<span>Submit</span>
								</button>

								<button
									id="cancel-btn"
									type="button"
									className="btn cancel"
									onClick={this.resetInput.bind(this)}
								>
									<span>Clear</span>
								</button>
							</div>
						</form>
					</div>
					<div className="management-container">
						<div className="editting-container">
							<header>
								<h2>User Accounts</h2>
							</header>
							<div className="table-container">
								<table id="registered-unit-table">
									<thead>
										<tr className="tr-head">
											<th>
												<span>No</span>
											</th>
											<th>
												<span>Username</span>
											</th>
											<th>
												<span>Role</span>
											</th>
											<th>
												<span>Firstname</span>
											</th>
											<th>
												<span>Lastname</span>
											</th>
											<th>
												<span>Email</span>
											</th>
											<th>
												<span>Registered Date</span>
											</th>
											<th>
												<span>Actions</span>
											</th>
										</tr>
									</thead>
									<tbody>
										{this.state.userAccountList &&
											this.state.userAccountList.map(
												(account, index) => (
													<tr
														key={`unit-row-${
															index + 1
														}`}
														id={`unit-row-${
															index + 1
														}`}
													>
														{/* className={account.enabled === 0 ? "disabled" : ""} */}
														<td
															id={`id-${
																index + 1
															}`}
															className="text-center"
														>
															<span>
																{account.no}
															</span>
														</td>
														<td
															id={`username-${
																index + 1
															}`}
															className="text-center"
														>
															<span>
																{
																	account.username
																}
															</span>
														</td>
														<td
															id={`auth-name-${
																index + 1
															}`}
															className="text-center"
														>
															<span>
																{
																	account.auth_name
																}
															</span>
														</td>
														<td
															id={`firstname-${
																index + 1
															}`}
															className="text-center"
														>
															<span>
																{
																	account.firstname
																}
															</span>
														</td>
														<td
															id={`lastname-${
																index + 1
															}`}
															className="text-center"
														>
															<span>
																{
																	account.lastname
																}
															</span>
														</td>
														<td
															id={`email-${
																index + 1
															}`}
															className="text-center"
														>
															<span>
																{account.email}
															</span>
														</td>
														<td
															id={`registered-date-${
																index + 1
															}`}
															className="text-center"
														>
															<span>
																{
																	account.registered_date
																}
															</span>
														</td>
														<td
															id={`action-${
																index + 1
															}`}
															className="text-center action-row"
														>
															<button
																id={`btn-edit-${
																	index + 1
																}`}
																className="sm-btn submit  actions"
																onClick={() => {
																	this.editAccount(
																		account
																	);
																}}
															>
																{" "}
																<span>
																	Edit
																</span>{" "}
															</button>
															<button
																id={`btn-enable-${
																	index + 1
																}`}
																className="sm-btn submit"
																onClick={() => {
																	this.removeAccount(
																		account
																	);
																}}
															>
																{" "}
																<span>
																	Remove
																</span>
															</button>
														</td>
													</tr>
												)
											)}
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default AccountManagement;
