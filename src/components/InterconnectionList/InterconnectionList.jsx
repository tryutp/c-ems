import React, { Component } from 'react'
import * as TopologyService from "./../../services/TopologyService";
import * as InformUser from "../../shared/InformUser";
import { Loading } from "./../../shared/Loading";
import "./InterconnectionList.scss";



export class InterconnectionList extends Component {

    token = JSON.parse(localStorage.getItem('userData')).token

    constructor(props) {
        super(props);
        this.state = {
            interconList: '',
            loading: false,
        }
    }

    componentDidMount() {

        this.setState({ loading: true }, async ()=>{
            // console.log(this.props)
            if (this.props.source && this.props.target) {
                if(this.props.isOpenInsideEditing){
                    await this.getAddedInterconnection()
                }else{
                    await this.getInterconnactionList()
                }
                
            } 
            this.setState({ loading: false })
        })
        

    }
    async getInterconnactionList() {
        // console.log('yut')
        const source = this.props.source.node_id;
        const target = this.props.target.node_id;
        // console.log(this.props.source, target);
        await TopologyService.get_interconnection_visual(source, target, this.token).then(result => {
            // console.log(result)
            if (result.status === 1) {
                this.setState({ interconList: result.data });
            } else {
                
            }
        })

    }
    async getAddedInterconnection() {
        await TopologyService.get_addedInterconnection(this.props.source.id, this.props.target.id, this.token).then(result => {
            // console.log("added interconnect", result)
            if (result.status == 1) {
                if (result.data) {
                    this.setState({ interconList: result.data });
                    // this.addedInterconnection = result.data;
                }else{
                    InformUser.unsuccess({ text: result.msg });
                }
            }
        })
    }

    render() {



        return (
            <div className="interconn-list-container">
                <Loading open={this.state.loading} />
                <button onClick={this.props.close} className="close fit-btn"><i class="fas fa-times"></i></button>
                <header className="shift-down"><h2>Interconnection</h2></header>
                <div className="table-container">
                    <div className="table-size">
                        <table>
                            <tr className="tr-head">
                                <th ><span>Source Unit</span> </th>
                                <th><span>Port No.</span></th>
                                <th><span>SMU Type</span></th>
                                <th ><span>Target Unit</span> </th>
                                <th><span>Port No.</span></th>
                                <th><span>SMU Type</span></th>
                                <th ><span>Interconnection Name</span> </th>

                                {/* <th ><span>Task Name</span>  </th> */}
                                <th ><span>Status</span> </th>
                            </tr>
                            {
                                this.state.interconList && this.state.interconList.map(intercon =>
                                    <tr>
                                        <td className="text-center"><span>{intercon.unit_name1}</span></td>
                                        <td className="text-center"><span>{intercon.port_no1}</span></td>
                                        <td className="text-center"><span>{intercon.smu_type1}</span></td>
                                        <td className="text-center"><span>{intercon.unit_name2}</span></td>
                                        <td className="text-center"><span>{intercon.port_no2}</span></td>
                                        <td className="text-center"><span>{intercon.smu_type2}</span></td>
                                        <td className="text-center"><span>{intercon.name}</span></td>
                                        <td className="text-center"><span>{intercon.status_msg}</span></td>
                                    </tr>

                                )
                            }
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default InterconnectionList
