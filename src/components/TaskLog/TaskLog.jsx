import React, { Component } from "react";
import "./TaskLog.scss";
import ClipLoader from "react-spinners/ClipLoader";
import * as TaskService from "./../../services/TaskService";
import * as InformUser from "./../../shared/InformUser";
import { Loading } from "./../../shared/Loading";
import moment from "moment";
import ReactTooltip from "react-tooltip";

export class TaskLog extends Component {
	token = JSON.parse(localStorage.getItem("userData")).token;
	time_offset = JSON.parse(localStorage.getItem("userData")).time_offset;
	constructor(props) {
		super(props);
		this.state = {
			taskLog: "",
			totalLog: "",
			currentPageSource: 1,
			logPerPage: 22,
			// logPerPage: 5,

			filterSourceSearch: null,

			totalPage: "",
			pageIndex: "",

			loading: false,
			loadingSource: false,
		};
	}

	componentDidMount() {
		this.setState({ loading: true }, async () => {
			await this.getTaskLog();
			this.setState({ loading: false });
		});
	}

	async getTaskLog() {
		await this.getLogCount();
		var params = {
			page: this.state.currentPageSource,
			qty_per_page: this.state.logPerPage,
			search: this.state.filterSourceSearch,
			timeoffset: this.time_offset,
		};
		// console.log(params)

		await TaskService.get_task_log(params, this.token).then(
			async (result) => {
				console.log("taskLog", result);
				if (result.status === 1 && result.data) {
					var logs = result.data;
					console.log(logs)
					var timeCaledLogs = await this.processAddDateOfTime(logs);

					this.setState({ taskLog: timeCaledLogs });
					// await this.processStartDateOfTime(logs)
				}
				if (result.status !== 1) {
					InformUser.unsuccess({ text: result.msg });
				}
			}
		);
	}

	async processAddDateOfTime(logData) {
		// var  logs = JSON.parse(JSON.stringify(logData));
		var logs = logData;

		for (var i = 0; i < logs.length; i++) {
			const newAddDate = await this.calculateTime(logs[i]["add_date"]);
			const newStartDate = await this.calculateTime(
				logs[i]["act_start_date"]
			);
			const newFinishDate = await this.calculateTime(
				logs[i]["finish_date"]
			);
			logs[i]["add_date"] = newAddDate;
			logs[i]["act_start_date"] = newStartDate;
			logs[i]["finish_date"] = newFinishDate;
		}
		// await logs.map( async  (log,index) => {
		//     const newAddDate = await this.calculateTime(log.add_date)
		//     const newStartDate = await this.calculateTime(log.start_date)
		//     const newFinishDate = await this.calculateTime(log.finish_date)

		//     logs[index]["start_date"] = newStartDate;
		//     logs[index]["finish_date"] = newFinishDate;

		//     // console.log(newTime)
		//     // await temp.push(newTime)
		// } )
		// console.log(logs)
		// console.log(logs)
		return logs;

		// return temp;
	}

	async calculateTime(serverTime) {
		var rawServerTime = await moment(serverTime);
		// console.log(rawServerTime)
		// console.log(timeOffset)
		var offset = moment.duration(this.time_offset);
		// // var temp = moment(server_time).add(1, 'm')
		// console.log(offset)
		var currentTime = await rawServerTime
			.add(offset)
			.format("YYYY-MM-DD HH:mm:ss");
		// console.log(currentTime)
		return currentTime;
		// console.log(currentTime)
	}

	async getLogCount() {
		const search = this.state.filterSourceSearch;

		await TaskService.get_task_log_count(search, this.time_offset, this.token).then(
			(result) => {
				// console.log("count", result.data);
				if (result.status === 1 && result.data) {
					const log_qty = this.state.logPerPage;
					const totalPage =
						Math.ceil(result.data.count / log_qty) + 1;
					if (totalPage === 0) {
						totalPage = 1;
					}

					var pageIndex = [...Array(totalPage).keys()];
					pageIndex.shift();
					this.setState({
						totalPort: result.data.count,
						totalPage: totalPage,
						pageIndex: pageIndex,
					});
				}
			}
		);
	}

	handlePage = (event) => {
		let selected = parseInt(event.target.value);
		this.setState({ [event.target.name]: selected });

		this.setState(
			{ currentPageSource: selected, loadingSource: true },
			async () => {
				await this.getTaskLog();
				this.setState({ loadingSource: false }, () => {
					// console.log(
					// 	this.state.currentPageSource,
					// 	this.state.totalPage
					// );
				});
			}
		);
	};
	handleValueInput = (event) => {
		this.setState({ [event.target.name]: event.target.value });
	};
	forwardPage() {
		// console.log(this.state.totalPage);

		const page = parseInt(this.state.currentPageSource);
		if (page < this.state.totalPage - 1) {
			var pageNext = page + 1;
			// console.log(pageNext);
			this.setState(
				{ currentPageSource: pageNext, loadingSource: true },
				async () => {
					await this.getTaskLog();
					this.setState({ loadingSource: false });
				}
			);
		}
	}
	backwardPage() {
		// console.log('yut')

		const page = parseInt(this.state.currentPageSource);
		if (page > 1) {
			var pageNext = page - 1;
			this.setState(
				{ currentPageSource: pageNext, loadingSource: true },
				async () => {
					await this.getTaskLog();
					this.setState({ loadingSource: false });
				}
			);
		}
	}
	searchPortDescription = (event) => {
		event.preventDefault();
		this.setState(
			{ currentPageSource: 1, loadingSource: true },
			async () => {
				await this.getTaskLog();
				this.setState({ loadingSource: false });
			}
		);
	};

	render() {
		return (
			<div className="task-log-container">
				<Loading open={this.state.loading} />
				<header className="h1">
					<h1>LOG: CONNECTIVITY</h1>
				</header>
				<div className="content">
					<div className="table-container">
						<div className="lr-container">
							<div className="left">
								<div className="pagination-container">
									<span style={{ marginRight: "0.75vw" }}>
										Page:
									</span>
									<button
										id="previous-btn"
										onClick={() => {
											this.backwardPage(true);
										}}
										className={
											(this.state.currentPageSource === 1
												? " btn-disable "
												: " ") + "fit-btn submit next"
										}
										style={{ marginRight: "0.5vw" }}
									>
										<span>Previous</span>
									</button>

									<select
										name="currentPageSource"
										id="selection-page"
										value={this.state.currentPageSource}
										onChange={(event) => {
											this.handlePage(event, true);
										}}
									>
										{this.state.pageIndex &&
											this.state.pageIndex.map((page) => (
												<option
													className={
														page ===
														this.state
															.currentPageSource
															? "selected-page"
															: ""
													}
													value={page}
												>
													{page}
												</option>
											))}
									</select>
									<button
										id="next-btn"
										onClick={() => {
											this.forwardPage(true);
										}}
										className={
											(this.state.currentPageSource ===
											this.state.totalPage - 1
												? " btn-disable "
												: " ") + "fit-btn submit next"
										}
									>
										<span>Next</span>
									</button>
									<ClipLoader
										color="#B3B3B3"
										loading={this.state.loadingSource}
										size={20}
									/>
								</div>
							</div>
							<div className="right">
								<form
									onSubmit={(event) => {
										this.searchPortDescription(event, true);
									}}
								>
									<ReactTooltip
										id="searchSource"
										place="top" className="noteClass defaultTooltip" arrowColor="#404040"  effect="solid"  
									/>
									<input
										id="search-input"
										type="text"
										className="search-input"
										data-for="searchSource"
										data-tip="Port description, add date, start date, finish date, port description, task added user, and remark"
										placeholder="Search"
										maxLength={20} 
										name="filterSourceSearch"
										value={this.state.filterSourceSearch}
										onChange={this.handleValueInput}
									/>
									<button
										id="search-btn"
										className="fit-btn submit search-btn"
										type="submit"
									>
										<i class="fas fa-search"></i>
									</button>
								</form>
							</div>
						</div>
						<div className="table-size">
							<table id="connectivity-log-table">
								<thead>
									<tr className="tr-head">
										<th style={{ width: "5%" }}>
											<span>Task No.</span>
										</th>
										<th style={{ width: "10%" }}>
											<span>Operation</span>
										</th>
										<th style={{ width: "15%" }}>
											<span>Description</span>
										</th>
										<th style={{ width: "10%" }}>
											<span>Add Date</span>
										</th>
										<th style={{ width: "10%" }}>
											<span>Start Date</span>
										</th>
										<th style={{ width: "10%" }}>
											<span>Finish Date</span>
										</th>
										<th style={{ width: "10%" }}>
											<span>Add By</span>
										</th>
										<th style={{ width: "15%" }}>
											<span>Remark</span>
										</th>
										<th style={{ width: "5%" }}>
											<span>Result</span>
										</th>
									</tr>
								</thead>
								<tbody>
								{this.state.taskLog && (
									<ReactTooltip
										id="remarkTooltip"
										place="top"
										multiline="true"
										type="dark"
										effect="solid"
									/>
								)}
								{
                                    (this.state.taskLog.length > 0) && <ReactTooltip id={`remark-tooltip`} html={true} className="remarkClass defaultTooltip" place="top" effect="solid" />
                                }
								{this.state.taskLog &&
									this.state.taskLog.map((log, index) => (
										<tr
											key={`task-log-${index + 1}`}
											id={`task-log-${index + 1}`}
										>
											<td
												id={`task-no-${index + 1}`}
												className="text-center"
											>
												<span>{log.no}</span>
											</td>
											<td
												id={`task-operation-${
													index + 1
												}`}
												className="text-center"
											>
												<span>{log.operation}</span>
											</td>
											<td
												id={`task-description-${
													index + 1
												}`}
												className="text-center"
											>
												<span>{log.description}</span>
											</td>
											<td
												id={`task-add-date-${
													index + 1
												}`}
												className="text-center"
											>
												<span>{log.add_date}</span>
											</td>
											<td
												id={`task-act-start-date-${
													index + 1
												}`}
												className="text-center"
											>
												<span>
													{log.act_start_date}
												</span>
											</td>
											<td
												id={`task-finish-date-${
													index + 1
												}`}
												className="text-center"
											>
												<span>{log.finish_date}</span>
											</td>
											<td
												id={`task-add-by-${index + 1}`}
												className="text-center"
											>
												<span>{log.add_by}</span>
											</td>
											<td
												id={`task-remark-${index + 1}`}
												className="text-center"
											>
												<span
													data-for="remark-tooltip"
													data-tip={ log.remark.length > 20 ? `<span>${log.remark}</span>` : ""} 
													className="ellipsis-text"
													style={{ width: "11vw" }}
												>
													{log.remark}
												</span>
											</td>
											<td
												id={`task-result-desc-${
													index + 1
												}`}
												className="text-center"
											>
												<span>{log.result_desc}</span>
											</td>
										</tr>
									))}
								</tbody>
								
							</table>
							{this.state.taskLog.length === 0 && (
								<div
									id="no-data-table"
									className="no-remaining-task"
								>
									{
										this.state.filterSourceSearch && <span>
											There's no connectivity log that matched your keyword.
										</span>
									}
									{
										!this.state.filterSourceSearch && <span className="camera-offline">
											NO CONNECTIVITY LOG
										</span>
									}
									
								</div>
							)}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default TaskLog;
