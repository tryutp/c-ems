import React, { Component } from 'react'
import { environment } from '../../Environment';
import * as TopologyService from "../../services/TopologyService";
import "./Container.scss";

import cytoscape from "cytoscape";
import { Link, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'
import Modal from 'react-modal';
// import EdgeSelected from "./../../dialogs/EdgeSelected";

import TreeView from '@material-ui/lab/TreeView';
import TreeItem from '@material-ui/lab/TreeItem';
import { MinusSquare, PlusSquare, CloseSquare } from "../../shared/TreeViewIcon";

import { openConfirmation } from "../../shared/Confirmation";
import * as InformUser from "../../shared/InformUser";
import { Loading } from "../../shared/Loading";

export class Container extends Component {

    token = JSON.parse(localStorage.getItem('userData')).token

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loading: false,
            registeredName: '',
            registeredDescription: '',
            registeredIcon: '',

            editingTemplate: '',
            graphData: '',
            models: [],
            containerData: {
                id: "root",
                name: "root",
                children: []
            },
            mapPicture: {
                url: '',
                height: '',
                width: ''
            },
            edgeSource: '',
            edgeTarget: '',
            isScrollDown: true,
            loading: false,
            openRegistration: false,
            addingElement: '',
            resizeBox: false,
            changingSizeId: '',
            percentageNodeSize: '',
            originalSize: 0,
        }
    }
    componentWillMount() {
        Modal.setAppElement('body');
    }
    componentDidMount() {
        this.getData();
        // console.log(this.state.graphData)
    }

    iconFile = '';
    backgroundMapFile = '';

    generateIdCount = 0;
    originalSizeForAdjusting = [];

    cy;
    graphStyle = cytoscape.stylesheet()
        .selector('edge')
        .css({
            'width': '2',
            // 'curve-style': 'bezier',
            // 'curve-style': 'segments',
            // 'curve-style': 'unbundled-bezier',
            // 'control-point-step-size': '100px',
            // 'line-fill': 'linear-gradient',
            'line-gradient-stop-colors': 'data(colorCode)',
            // 'line-gradient-stop-positions': ['0%', '50%', '50%', '100%'],
            "loop-direction": "data(direction)",
            // "loop-sweep": "70deg",
            // 'line-color': 'data(colorCod)',
            'label': 'data(label)',
            // 'color': 'white',
            // 'font-size': '12px',
            // 'font-family': 'helvetica, arial, verdana, sans-serif',
            // 'font-weight': 300,
            // 'text-outline-width': 2,
            // 'text-outline-color': 'black',
            // 'control-point-distance': 100,
            // 'target-arrow-shape': 'data(arrow_target)',
            // 'source-arrow-shape': 'data(arrow_source)',
            // 'target-arrow-shape': 'circle',
            // 'source-arrow-shape': 'circle',
            // 'source-arrow-color': 'data(sourceColor)',
            // 'target-arrow-color': 'data(targetColor)',
        })
        .selector(':selected')
        .css({
            'edge.opacity': 0.5,
            'width': '3',
            'color': 'white',
            'line-color': '#ffb3b3',
            // 'source-arrow-color': 'data(colorCode)',
            // 'target-arrow-color': 'data(colorCode)',
        })

        .selector('node')
        .css({
            'shape': 'rectangle',
            'background-clip': 'none',
            // 'background-width':'100%',
            // 'background-height':'auto',
            // 'background-fit':'node',
            'background-fit': 'contain',
            'background-opacity': '0',

            // 'width': '60',
            // 'height': '55',
            'content': 'data(name)',
            'text-valign': 'center',
            'text-outline-width': 1.5,
            'text-outline-color': '#e9e9e9',

            // 'background-image': 'url("assets/Robot_img_50.png")',
            // 'object-fit': 'cover',
            // 'background-fit':'contain',
            // 'background-repeat': 'no-repeat',
            // 'background-position': 'center center',
            'color': '#31081f',
            'font-size': '12',


        })
        .selector(':selected')
        .style({
            // 'label': 'data(label)',
            // 'label': '',
            // 'color': 'black',
            // 'text-outline-width': 2,
            // 'text-outline-color': '#e9e9e9',
            // 'opacity': 1,
            // 'font-size': 18,
            // 'overlay-color': 'blue',
            // 'overlay-opacity': '0.5',
            // 'overlay-padding': '5',

            // 'shape': 'roundrectangle',
            'width': '60',
            'height': '55',
            'content': 'data(name)',
            'text-valign': 'center',
            'text-outline-width': 0.5,
            'text-outline-color': '#dce0d9',
            'background-color': '#1d2b36',
            // 'background-image': 'url("assets/Robot_img_50.png")',
            // 'object-fit': 'cover',
            // 'background-repeat': 'no-repeat',
            // 'background-position': 'center center',
            'color': '#31081f',
            'font-size': 14,
            'font-weight': '600',
            'border-width': '0',
            // 'border-color': 'white',
            // 'selection-box-color': 'white',
            // 'selection-box-border-color': 'white',
            // 'border-width': 1,
            // 'border-color': 'black',

        }).selector('.locked').style({
            'opacity': '0.5'
        })


    getData() {

        // TopologyService.get_containerList(this.token).then( result => {
        //     console.log("Template Containers, ", result.status)
        //     if(result.status == 1){

        //             this.setState( { templates: result.data })


        //     }else{
        //         console.log(result.msg)
        //     }
        // })
        this.setState({ loading: true }, async () => {
            await this.getContainerTree();
            await this.getElementList();
            this.setState({ loading: false })
        })



    }

    async getContainerTree() {
        // this.setState({ loading : true} , ()=>{
        await TopologyService.get_containerTree(this.token).then(result => {
            // console.log("tree",JSON.stringify(result))
            if (result.status == 1) {
                this.setState({ containerData: result.data })
            } else {
                InformUser.unsuccess({ text: result.msg });

            }
            // this.setState({ loading : false });
        })
        // })

    }
    async getElementList() {
        // this.setState({ loading: true }, ()=> {
        await TopologyService.get_elementList(this.token).then(result => {
            // console.log("Unit Models,", result.data)
            if (result.status == 1) {
                this.setState({ models: result.data })
            } else {
                InformUser.unsuccess({ text: result.msg });
            }
            // this.setState({ loading : false });
        })
        // })

    }
    setSize() {
        // console.log(this.state.percentageNodeSize)

    }


    renderCytoscapeElement() {
        // console.log("mapPicture ", this.state.registeredIcon)
        // console.log(this.state.graphData)

        // Set up and render Cytoscape
        this.cy = window.cy = cytoscape({
            container: document.getElementById('cy'),
            style: this.graphStyle,
            zoom: 2,
            minZoom: 0.1,
            maxZoom: 2,
            wheelSensitivity: 0.2,
            elements: this.state.graphData,
            layout: {
                name: 'preset',
                boundingBox: {
                    x1: 0,
                    y1: 0,
                    x2: 100,
                    y2: 100
                },
            }
        });

        // Draw background with ext. Cytoscape-canvae
        let background = new Image();
        background.onload = () => {

            // console.log("height", background.height)
            // console.log("width", background.width)
            const bottomLayer = this.cy.cyCanvas({
                zIndex: -1,
            });
            const canvas = bottomLayer.getCanvas();
            const ctx = canvas.getContext("2d");
            this.cy.on("render cyCanvas.resize", evt => {
                bottomLayer.resetTransform(ctx);
                bottomLayer.clear(ctx);
                bottomLayer.setTransform(ctx);
                ctx.save();
                // Draw a background
                ctx.drawImage(background, 0, 0);

                // Draw text that follows the model
                // ctx.font = "24px Helvetica";
                // ctx.fillStyle = "black";
                // ctx.fillText("This text follows the model", 200, 300);

                // // Draw shadows under nodes
                // ctx.shadowColor = "black";
                // ctx.shadowBlur = 25 * cy.zoom();
                // ctx.fillStyle = "white";
                // cy.nodes().forEach(node => {
                //   const pos = node.position();
                //   ctx.beginPath();
                //   ctx.arc(pos.x, pos.y, 10, 0, 2 * Math.PI, false);
                //   ctx.fill();
                // });
                // ctx.restore();

                // // Draw text that is fixed in the canvas
                // bottomLayer.resetTransform(ctx);
                // ctx.save();
                // ctx.font = "24px Helvetica";
                // ctx.fillStyle = "red";
                // ctx.fillText("This text is fixed", 200, 200);
                // ctx.restore();

            });


        }
        background.src = this.state.mapPicture.url;

        this.initialBackgroundViewpoint();
        // cy.panBy({
        //     x: 0,
        //     y: 0
        // });

        // this.cy.on('tap', 'node', (event)=> {
        //     console.log(event.target.id())
        //     this.cy.nodes(':selected').map( node => {
        //         console.log(node.data())
        //     })
        //     // console.log(this.cy.nodes(':selected').length)
        // })


        this.cy.on('unselect', 'node', (event) => {
            this.setState({ resizeBox: false })
        })

        // Click background to add AddingElement to Map
        this.cy.on('tap', (event) => {

            if (this.state.addingElement) {
                this.addNode(event.position);
            }
        })

        this.cy.on('tap', 'node', (event) => {

            // console.log(event.target.width())
            // console.log(event.target.outerWidth())
            // console.log(event.target.renderedWidth())
            // console.log(event.target.renderedOuterWidth())

            // event.target.style({
            //     'width': '250px'
            // })
        })

        //Click Edges to open Interconnection
        this.cy.on('tap', 'edge', event => {
            var edgeData = event.target.data()
            // console.log(edgeData.source);
            this.handleClickOpen(edgeData)
        })


        this.cy.on('cxttap', 'node', (event) => {
            // console.log(event.target.id())

            let target = event.target || event.cyTarget;
            target.select();
            contextMenu.showMenuItem('delete-node')
            // contextMenu.showMenuItem('adjust-size')
            contextMenu.showMenuItem('adjust-size')

            if (this.cy.nodes(':selected').length === 1) {    //Single
                if (target.grabbable()) {
                    contextMenu.showMenuItem('lock-node')
                    contextMenu.hideMenuItem('unlock-node')
                    contextMenu.hideMenuItem('toggle-lock-node')
                } else {

                    contextMenu.hideMenuItem('lock-node')
                    contextMenu.showMenuItem('unlock-node')
                    contextMenu.hideMenuItem('toggle-lock-node')
                }
            } else {
                contextMenu.hideMenuItem('lock-node')
                contextMenu.hideMenuItem('unlock-node')
                contextMenu.showMenuItem('toggle-lock-node')
            }


            // contextMenu.showMenuItem('unselect-all-nodes')
        })

        //Prevent node from being dragged outside range of background.
        this.cy.automove({
            nodesMatching: this.cy.nodes(),
            reposition: { x1: 0, x2: this.state.mapPicture.width, y1: 0, y2: this.state.mapPicture.height }
        });



        var contextMenu = this.cy.contextMenus({

            menuItems: [
                {
                    id: 'delete-node',
                    content: 'Delete this system',
                    tooltipText: 'Delete system from templete.',
                    image: { src: "add.svg", width: 12, height: 12, x: 6, y: 4 },
                    selector: 'node',
                    coreAsWell: false,
                    onClickFunction: async (event) => {


                        var nameTemp = []
                        this.cy.nodes(':selected').forEach(selected => {
                            nameTemp.push(selected.data().name);
                        })
                        const dialog = await openConfirmation({
                            title: "Delete Model box",
                            text: "Are you sure you want to delete '" + nameTemp + "' model box?"
                        })
                        if (dialog) {
                            this.cy.nodes(':selected').forEach(selected => {
                                var id = selected.id()
                                // this.syncDeleteState(id)
                                selected.remove()
                            })
                            this.cy.nodes(':selected').unselect();
                        }
                    }
                },
                // {
                //     id: 'adjustt-size',
                //     content: 'Adjust sytem size',
                //     tooltipText: 'Increase/Decrease the size of system.',
                //     selector: 'node',
                //     coreAsWell: false,
                //     show: false,
                //     onClickFunction: (event) => {
                //         console.log(event.target.width())
                //         this.setState({ 
                //             resizeBox: true,
                //             originalSize: event.target.width()
                //             // percentageNodeSize: event.target.width()
                //         })
                //         this.originalSize = event.target.width();
                //     },
                //     submenu: [
                //         {
                //             id: 'increase-size',
                //             content: 'Increase size',
                //             onClickFunction: (event) => {
                //                 this.setState({ 
                //                     resizeBox: true,
                //                     originalSize: event.target.width(),
                //                     changingSizeId: event.target.id(),
                //                     // percentageNodeSize: event.target.width()
                //                 })
                //                 this.originalSize = event.target.width();
                //                 // this.cy.nodes(':selected').forEach(selected => {
                //                 //     var id = selected.id()
                //                 //     this.adjustSize(id, true)
                //                 // })
                //                 // this.cy.nodes(':selected').unselect();
                //             },
                //         },
                //         {
                //             id: 'decrease-size',
                //             content: 'Decrease size',
                //             onClickFunction: (event) => {
                //                 this.cy.nodes(':selected').forEach(selected => {
                //                     var id = selected.id()
                //                     this.adjustSize(id, false)
                //                 })
                //                 this.cy.nodes(':selected').unselect();
                //             },
                //         },
                //     ],
                //     onClickFunction: (event) => {
                //     }
                // },
                {
                    id: 'adjust-size',
                    content: 'Adjust sytem size',
                    tooltipText: 'Increase/Decrease the size of system.',
                    selector: 'node',
                    coreAsWell: false,
                    onClickFunction: (event) => {
                        // console.log(event.target.width())
                        this.originalSizeForAdjusting = [];
                        this.cy.nodes(':selected').map(node => {
                            node.addClass('resizing')
                            this.originalSizeForAdjusting.push(node.width())
                        })
                        // console.log(this.originalSizeForAdjusting)
                        this.setState({
                            originalSize: event.target.width(),
                            percentageNodeSize: 100,
                            resizeBox: true,
                        });
                    },

                },

                {
                    id: 'lock-node',
                    content: 'Lock this system',
                    tooltipText: 'Prevent system from being dragged.',
                    selector: 'node',
                    coreAsWell: false,
                    show: true,
                    onClickFunction: (event) => {

                        this.cy.nodes(':selected').forEach(selected => {
                            if (selected.grabbable()) {
                                selected.ungrabify()
                                selected.addClass('locked')
                                var id = selected.id()
                                this.syncLockState(id, false)
                            }
                        })
                        this.cy.nodes(':selected').unselect();
                    }

                },
                {
                    id: 'unlock-node',
                    content: 'Unlock this system',
                    tooltipText: 'Prevent system from being dragged.',
                    selector: 'node',
                    coreAsWell: false,
                    show: false,
                    onClickFunction: async (event) => {
                        var nameTemp = []
                        this.cy.nodes(':selected').forEach(selected => {
                            nameTemp.push(selected.data().name);
                        })
                        const dialog = await openConfirmation({
                            title: "Unlock Model box",
                            text: "Are you sure you want to unlock '" + nameTemp + "' model box?"
                        })
                        if (dialog) {
                            this.cy.nodes(':selected').forEach(selected => {
                                var idx = selected.id()
                                if (!selected.grabbable()) {
                                    selected.grabify()
                                    selected.removeClass('locked')
                                    // console.log('here')
                                    this.syncLockState(idx, true)
                                }
                            })
                            this.cy.nodes(':selected').unselect();
                        }

                    }

                },
                {
                    id: 'toggle-lock-node',
                    content: 'Toggle-lock selected systems',
                    tooltipText: 'Toggle locking status of selected systems.',
                    selector: 'node',
                    coreAsWell: false,
                    show: false,
                    onClickFunction: async () => {
                        var nameTemp = []
                        this.cy.nodes(':selected').forEach(selected => {
                            nameTemp.push(selected.data().name);
                        })
                        const dialog = await openConfirmation({
                            title: "Unlock Model box",
                            text: "Are you sure you want to toggle '" + nameTemp + "' model box?"
                        })
                        if (dialog) {
                            this.cy.nodes(':selected').forEach(selected => {
                                if (selected.grabbable()) {
                                    selected.ungrabify()
                                    selected.addClass('locked')
                                    var id = selected.id()
                                    this.syncLockState(id, false)
                                } else {
                                    selected.grabify()
                                    selected.removeClass('locked')
                                    var id = selected.id()
                                    this.syncLockState(id, true)
                                }

                            })
                            this.cy.nodes(':selected').unselect();
                        }


                    }

                }
            ]
        })


    }
    cursorMove(visible) {
        if (visible) {
            document.getElementById('cy').classList.add("adding");
        } else {
            document.getElementById('cy').classList.remove("adding");
        }
    }
    initialBackgroundViewpoint() {
        //Initial Zooming for fitting background size when render
        var zoomLevel = this.cy.height() / this.state.mapPicture.height;
        this.cy.zoom(zoomLevel)
        this.cy.minZoom(zoomLevel)
        //Initial viewpoint when render
        var cyMapWidth = this.state.mapPicture.width / 2;
        var halfWidthRatio = this.cy.width() * cyMapWidth / (this.cy.width() / this.cy.zoom());

        this.cy.pan({
            x: this.cy.width() / 2 - halfWidthRatio,
            y: 0
        });

        //Click background to adjust view
        this.cy.on('tap', (event) => {
            this.cy.pan({
                x: this.cy.width() / 2 - halfWidthRatio,
                y: 0
            });
            this.cy.zoom(zoomLevel)
        })
    }

    adjustSize(id, isIncrease) {
        var newState = this.state.graphData
        newState.nodes.forEach((node, index) => {

            if (node.data.id == id) {
                // console.log( index)
                if (isIncrease) {
                    node.style.height = (parseInt(node.style.height) + 10).toString()
                    node.style.width = (parseInt(node.style.width) + 10).toString()
                } else {
                    node.style.height = (parseInt(node.style.height) - 10).toString()
                    node.style.width = (parseInt(node.style.width) - 10).toString()
                }

            }
        });
        this.setState({
            graphData: newState
        }, () => {
            this.renderCytoscapeElement();
        })
    }
    // syncDeleteState(id) {
    //     var newState = this.state.graphData
    //     newState.nodes.forEach((node, index) => {
    //         if (node.data.id == id) {
    //             newState.nodes.splice(index, 1)
    //         }
    //     });
    //     this.setState({
    //         graphData: newState
    //     })
    // }
    syncLockState(id, value) {
        var newState = this.state.graphData
        newState.nodes.forEach(node => {
            if (node.data.id == id) {
                node.grabbable = value
            }
        });
        this.setState({
            graphData: newState
        })
    }
    addModel(model) {

        // console.log(model)



        const addingElement = this.state.addingElement;
        if (!addingElement) {
            this.setState({ addingElement: model }, () => {
                this.cursorMove(true);
            });
        } else {
            if (addingElement.data.elm_id == model.data.elm_id) {  // click again to remove
                this.setState({ addingElement: '' }, () => {
                    this.cursorMove(false);
                });
            } else {  // click another to replace
                this.setState({ addingElement: model }, () => {
                    this.cursorMove(true);
                });
            }
        }



        // var elm_list = []  ['data']['id']
        // elm_list.push({'id': model['data']['id'], 'width_size': model['position']['x']})
        // console.log("Model ", model)





        // genModel['data']['id'] = 'yut'
        // console.log("Gen Model ", genModel.data.id)
        //Append plotId to ModelData
        // model['data']['id'] = this.generateIdCount.toString()
        // var newState = this.state.graphData
        // if (!newState) {
        //     newState = {
        //         nodes: []
        //     }
        //     newState.nodes.push(genModel);
        // } else {
        //     newState.nodes.push(genModel);
        // }


        // console.log("newState ",newState)
        // this.setState({
        //     graphData: newState
        // }, () => {
        //     this.generateIdCount += 1;
        //     this.renderCytoscapeElement();
        // })
    }


    addNode(position) {
        //Generate ID for displaying in Cytoscape
        // console.log(model)
        // console.log(this.state.graphData)

        const model = this.state.addingElement;

        TopologyService.check_to_add_into_container(this.state.editingTemplate.id, model.elm_id, this.token).then(result => {
            // console.log(result)
            if (result.status === 1) {
                let genModel = JSON.parse(JSON.stringify(model))

                genModel.data.id = "added-" + this.generateIdCount.toString()
                genModel.position.x = position.x;
                genModel.position.y = position.y;

                // console.log(genModel)
                if (this.cy) {
                    this.cy.add(genModel);

                }
            } else {
                InformUser.unsuccess({ text: result.msg })
            }
            this.setState({ addingElement: '' }, () => {
                this.generateIdCount += 1;
                this.cursorMove(false);
            })
        })





    }

    onIconChange = (event) => {
        if (event.target.files && event.target.files[0]) {
            let img = event.target.files[0];
            this.iconFile = img
            let imgSrc = URL.createObjectURL(img);
            const image = new Image();
            image.onload = () => {
                // this.iconWidth = image.width ;
                // this.iconHeight = image.height;
                // console.log(this.iconWidth, this.iconHeight)
                this.setState({
                    registeredIcon: imgSrc
                });
            }
            image.src = imgSrc
        }
    }
    onBackgroundUpload = (event) => {
        // console.log(event)
        if (event.target.files && event.target.files[0]) {
            let map = event.target.files[0];
            this.backgroundMapFile = map
            let imgSrc = URL.createObjectURL(map);
            var image = new Image();
            image.onload = () => {
                var bg = {
                    url: imgSrc,
                    height: image.height,
                    width: image.width
                }
                this.setState({
                    mapPicture: bg
                }, () => {
                    this.renderCytoscapeElement()

                });
            }
            image.src = imgSrc
        }
    }


    onTreeClicked(event, node) {
        event.preventDefault()
        // console.log(event, node)
        if (node.cont_id) {
            var container = { id: node.cont_id, name: node.name }
            this.chooseTemplate(container)
        } else {
            // console.log('there is no cont_id for fetching data in ChooseTemplate')
        }

    }
    chooseTemplate = (template) => {

        // console.log(template)
        // console.log(template)

        this.iconFile = ''
        this.backgroundMapFile = ''
        this.setState({ editingTemplate: template })
        this.setState({ loading: true }, async () => {
            await TopologyService.edit_container(template.id, this.token).then(result => {
                // console.log("result form ChooseTemplate ", result)
                if (result.status == 1) {
                    var thereIs = result.data;
                    if (thereIs && thereIs.id && thereIs.name && thereIs.bg_path && thereIs.bg_height && thereIs.bg_width) {
                        var bg = {
                            // 
                            url: environment.api + result.data.bg_path,
                            height: result.data.bg_height,
                            width: result.data.bg_width
                        }
                        // environment.api + 
                        var iconUrl = environment.api + result.data.img_path;
                        var cyData = {
                            nodes: result.data.element
                        }
                        this.setState({
                            graphData: cyData,
                            mapPicture: bg,
                            registeredName: result.data.name,
                            registeredIcon: iconUrl,
                            registeredDescription: "Still no description attribute",
                            openRegistration: true,
                            resizeBox: false
                        }, () => {
                            this.renderCytoscapeElement();
                        })
                    } else {
                        InformUser.unsuccess({ text: "Data is incomplete" });
                    }
                } else {
                    InformUser.unsuccess({ text: result.msg });
                }

            })
            this.setState({ loading: false })

        })




    }

    deleteTemplate = async (container, isFromMap) => {
        // console.log(container)

        if (isFromMap) {
            if (this.state.editingTemplate) {
                const dialog = await openConfirmation({
                    title: "Delete Container",
                    text: "Are you sure you want to delete '" + this.state.editingTemplate.name + "' container?"
                });
                if (dialog) {
                    TopologyService.delete_container(this.state.editingTemplate.id, this.token).then(result => {
                        if (result.status == 1) {
                            InformUser.success({ text: result.msg });
                            this.getData()
                            this.registerTemplate();
                        } else {
                            InformUser.unsuccess({ text: result.msg });
                        }
                    })
                }
            } else {
                // console.log('there is container editing currently');
            }
        } else {
            const dialog = await openConfirmation({
                title: "Delete Container",
                text: "Are you sure you want to delete '" + container.name + "' container?"
            })
            if (dialog) {
                TopologyService.delete_container(container.cont_id, this.token).then(result => {
                    if (result.status == 1) {
                        InformUser.success({ text: result.msg });
                        this.getData()
                        this.registerTemplate();
                    } else {
                        InformUser.unsuccess({ text: result.msg });
                    }
                })
            }

        }


    }
    submitRegistration() {



        var formData = new FormData();


        formData.append("name", this.state.registeredName)
        formData.append("width_size", this.state.mapPicture.width)
        formData.append("height_size", this.state.mapPicture.height)

        if (this.iconFile == '') {
            // formData.append("icon", undefined) 
        } else {
            formData.append("icon", this.iconFile)
        }
        if (this.backgroundMapFile == '') {
            // formData.append("bg", undefined )
        } else {
            formData.append("bg", this.backgroundMapFile)
        }

        var formatedElement = this.submitFormat()
        if (!this.state.graphData.nodes) {
            formData.append("element", undefined)
        } else {
            // console.log("yut", JSON.stringify(formatedElement))
            formData.append("element", JSON.stringify(formatedElement))
        }


        for (var pair of formData.entries()) {
            // console.log(pair[0] + ', ' + pair[1]);
        }

        // re-fetch template list
        if (!this.state.editingTemplate) {
            TopologyService.create_container(formData, this.token).then(result => {

                if (result.status == 1) {
                    InformUser.success({ text: result.msg });
                    this.getData()
                } else {
                    InformUser.unsuccess({ text: result.msg });
                }

            });
        } else {
            TopologyService.update_container(formData, this.state.editingTemplate.id, this.token).then(result => {

                if (result.status == 1) {
                    InformUser.success({ text: result.msg });
                    this.getData()
                } else {
                    InformUser.unsuccess({ text: result.msg });
                }
            })
        }

    }

    submitFormat() {
        const data = this.cy.nodes()
        if (data) {
            var rawData = data;
            var formatArr = [];

            data.map(node => {
                var oldWidth = node.style().width;
                var oldHeight = node.style().height;
                var noPxWidth = oldWidth.replace("px", '')
                var noPxHeight = oldHeight.replace("px", '')

                var temp = {
                    id: node.data().elm_id,
                    x_axist: node.position().x,
                    y_axist: node.position().y,
                    width_size: noPxWidth,
                    height_size: noPxHeight,
                }
                formatArr.push(temp);
            })
            // console.log("format ", formatArr);
            return formatArr
        }
    }

    handleTextInput = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    sliderInputHandler = (event) => {
        var size = event.target.value;
        this.setState({
            [event.target.name]: size
        })
        var originalSize = this.state.originalSize;
        this.cy.nodes(':selected').map((node, index) => {
            // console.log(originalSize)
            var finalSize = this.originalSizeForAdjusting[index] * size / 100 + "px"
            node.style({
                width: finalSize,
                height: finalSize,
            })
        })
    }
    registerTemplate() {


        this.setState({ openRegistration: true })
        // InformUser.success({ text: "Please enter the Container information and upload background map." })
        this.iconFile = ''
        this.setState({
            registeredName: '',
            registeredDescription: '',
            registeredIcon: '',
            editingTemplate: '',
            graphData: '',
            mapPicture: {
                url: '',
                height: '',
                width: ''
            },
        }, () => {
            this.renderCytoscapeElement();
        })
    }
    smoothScroll() {
        // console.log('hjfdg');
        var rightBox = document.getElementById('rightBox');
        rightBox.scrollIntoView({ behavior: "smooth" })
        // rightBox.animate({
        //     scrollTop : document.getElementById('cyHeader').offsetTop - document.getElementById('rightBox').offsetTop
        // }, {
        //     duration: 1000
        // })
        // rightBox.scrollTop = document.getElementById('cyHeader').offsetTop - document.getElementById('rightBox').offsetTop;

    }
    updateScroll() {
        this.setState({ isScrollDown: !this.state.isScrollDown })
    }
    onEditFromTree(event, container) {
        // console.log(container, this.state.editingTemplate)

        if (container.name === this.state.editingTemplate.name) {
            this.setState({ openRegistration: true })
        } else {
            this.onTreeClicked(event, container);
            this.setState({ openRegistration: true })
        }


    }



    render() {
        const renderTree = (nodes) => (

            <div className="tree-node">
                {/* <button
                    id={"delete-" + nodes.id}
                    onClick={(event) => { this.onEditFromTree(event, nodes) }}
                    className={nodes.id == "root" ? "edit-btn display-none" : "edit-btn close-btn"}
                ><i class="fas fa-edit"></i></button> */}
                <button
                    id={"delete-" + nodes.id}
                    onClick={() => { this.deleteTemplate(nodes, false) }}
                    className={nodes.id == "root" ? "delete-btn display-none" : "delete-btn close-btn"}
                ><i class="fas fa-trash-alt"></i></button>
                <TreeItem key={nodes.id} id={nodes.id} nodeId={nodes.id} label={nodes.name} onLabelClick={(event) => { this.onTreeClicked(event, nodes) }}>
                    {Array.isArray(nodes.children) ? nodes.children.map((node) => renderTree(node)) : null}
                </TreeItem>
            </div>

        );

        return (
            <div className="create-container">
                <Loading open={this.state.loading} />
                <header className="h1"><h1>Topology: Template Setting</h1></header>

                <div className="container-template-box">

                    <div className="left-box">
                        <div className="template-box">
                            <div className="lr-container shift-down">
                                <div className="left">
                                    <h2 className="padding10">Templates</h2>
                                </div>
                                <div className="right">
                                    <button id="create-new-container" className="sm-btn submit" onClick={this.registerTemplate.bind(this)}><span>New Template</span></button>
                                </div>
                            </div>


                            <div className="container-tree">
                                <TreeView
                                    // className={classes.root}
                                    // defaultCollapseIcon={<ExpandMoreIcon />}
                                    defaultCollapseIcon={<MinusSquare />}
                                    defaultExpandIcon={<PlusSquare />}
                                    defaultEndIcon={<CloseSquare />}
                                    defaultExpanded={['root']}
                                    onNodeToggle={this.nodeToggle}
                                // defaultExpandIcon={<ChevronRightIcon />}
                                >
                                    {renderTree(this.state.containerData)}
                                </TreeView>
                            </div>

                        </div>
                        <div className="unit-box">
                            <header><h2 className="padding10">Elements</h2></header>
                            <div className="model-box">
                                {

                                    this.state.models.map((model, index) =>
                                        // <p id={"add-model-"+index}  onClick={ ()=> this.addModel(model)} >{model.data.name}</p>

                                        <div id={"add-model-" + index} className={!this.state.addingElement ? "unit-form" : (this.state.addingElement.data.elm_id === model.data.elm_id ? "selected-to-add unit-form" : "unit-form")} onClick={() => this.addModel(model)}>
                                            <div className="image-box">
                                                <img src={model.style["background-image"]} alt="" />
                                            </div>
                                            <div className="name-box">
                                                <span>{model.data.name}</span>
                                            </div>
                                        </div>
                                    )


                                }
                            </div>

                        </div>


                    </div>
                    <Element name="topContainer" id="rightBox" className="right-box">
                        {/* <button id = "layoutButton" onClick = { this.handleClickOpen } > Layout </button>  */}
                        {
                            this.state.openRegistration && <div className="registration-box">
                                <div className="lr-container">
                                    <div className="left">
                                        <h2 className="padding10">Template Registration</h2>
                                    </div>
                                    <div className="right">
                                        <button onClick={() => { this.setState({ openRegistration: false }) }} className="close-btn diff-right"><i class="fas fa-times"></i></button>
                                    </div>
                                </div>


                                {/* <hr /> */}
                                <div className="registration-form">

                                    <div className="icon-form">

                                        <div className="icon-box">
                                            <img src={this.state.registeredIcon} />
                                        </div>
                                        <div className="icon-btn">
                                            <input id="icon-upload-btn" type="file" name="uploadIcon" id="uploadIcon" className="input-btn" onChange={this.onIconChange.bind(this)}></input>
                                            <label htmlFor="uploadIcon" className="sm-btn submit"><span>Upload Icon Pic</span></label>
                                        </div>

                                    </div>

                                    <div className="text-form">

                                        <div className="input-group">
                                            <div className="left">
                                                <label id="name-label" htmlFor="registeredName"><span>Name : </span></label>
                                            </div>
                                            <div className="right">
                                                <input id="name-id" type="text" id="registeredName" name="registeredName" placeholder="Room" value={this.state.registeredName} onChange={this.handleTextInput.bind(this)} />
                                            </div>

                                        </div>
                                        <div className="input-group">
                                            <div className="left">
                                                <label id="description-label" htmlFor="registeredDescription"><span>Description : </span></label>
                                            </div>
                                            <div className="right">
                                                <input id="description-id" id="registeredDescription" name="registeredDescription" placeholder="Server room" value={this.state.registeredDescription} onChange={this.handleTextInput.bind(this)} />
                                            </div>
                                        </div>
                                       
                                        <div className="input-group">
                                            <div className="left">
                                                <span>Background : </span>
                                            </div>
                                            <div className="right">
                                                <input style={{ display: "none" }} id="background-upload-btn" type="file" onChange={this.onBackgroundUpload.bind(this)} name="background-map" id="background-map" />
                                                <label htmlFor="background-map" className="sm-btn submit" ><span>Upload File</span></label>
                                            </div>

                                        </div>

                                    </div>


                                </div>
                                {/* <hr /> */}
                                <div className="submit-box">

                                    <button id="submit-btn" className="submit btn" onClick={this.submitRegistration.bind(this)}><span>Submit</span></button>
                                </div>




                            </div>
                        }

                        <div className={!this.state.openRegistration ? "map-box" : "half-map-box"}>
                            <div className="scroll-anchor">
                                {!this.state.isScrollDown && <Link style={{ position: 'absolute' }} onClick={this.updateScroll.bind(this)} to="topContainer" spy={true} smooth={true} duration={500} containerId="rightBox" >Move Up</Link>}
                            </div>

                            {this.state.mapPicture.url &&


                                <div className="lr-container">
                                    <div className="left">
                                        {/* <div className="map-header"> */}
                                        <Element name="cyContainer" className="element" > <h2 className="header-name">{this.state.editingTemplate.name} Template</h2></Element>


                                        {/* </div> */}
                                    </div>
                                    <div className="right">
                                        {
                                            this.state.resizeBox &&
                                            <div className="resize">
                                                <span className="edit-size">Edit size: </span>
                                                <input
                                                    type="range"
                                                    id="points"
                                                    name="percentageNodeSize"
                                                    value={this.state.percentageNodeSize}
                                                    min={0}
                                                    max={200}
                                                    onChange={this.sliderInputHandler.bind(this)}
                                                    step="1" />


                                                {/* <input style={{ width: "30px" }} type="number" name="percentageNodeSize" value={this.state.percentageNodeSize} min={this.state.originalSize - 100} max={this.state.originalSize + 100} onChange={this.handleTextInput.bind(this)} step="1" /> */}

                                                <span className="percentage">{this.state.percentageNodeSize}%</span>


                                            </div>
                                        }

                                    </div>
                                </div>


                            }
                            <div id="cy" > </div>
                        </div>


                    </Element>
                </div>
            </div>

        )
    }
}

export default Container